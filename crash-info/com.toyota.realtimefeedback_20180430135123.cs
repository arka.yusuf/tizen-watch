S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2378
Date: 2018-04-30 13:51:23+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: 1
      address not mapped to object
      si_addr = 0x14

Register Information
r0   = 0xb7c21d68, r1   = 0xb7c21d68
r2   = 0x00000000, r3   = 0x00000000
r4   = 0xb7b84ba0, r5   = 0xb7c133e0
r6   = 0xb7b84c40, r7   = 0x00000000
r8   = 0x00000000, r9   = 0x00000000
r10  = 0x0001869f, fp   = 0xb7b84c44
ip   = 0xb6c49cec, sp   = 0xbe881ca0
lr   = 0xb6a075d0, pc   = 0xb6bbb7a2
cpsr = 0xa0000030

Memory Information
MemTotal:   405512 KB
MemFree:     10556 KB
Buffers:      7292 KB
Cached:     110236 KB
VmPeak:      81508 KB
VmSize:      78180 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       23692 KB
VmRSS:       23692 KB
VmData:      36228 KB
VmStk:         136 KB
VmExe:          40 KB
VmLib:       31460 KB
VmPTE:          66 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2378 TID = 2378
2378 2383 2415 

Maps Information
b26af000 b2736000 rw-s anon_inode:dmabuf
b2736000 b2737000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b273f000 b2742000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b28fb000 b2982000 rw-s anon_inode:dmabuf
b2983000 b3182000 rw-p [stack:2385]
b3329000 b332a000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b3432000 b3c31000 rw-p [stack:2415]
b3c31000 b3c33000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b3c3b000 b3c52000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b3e6b000 b466a000 rw-p [stack:2383]
b4681000 b4fad000 r-xp /usr/lib/libsc-a3xx.so
b5211000 b5213000 r-xp /usr/lib/libdri2.so.0.0.0
b521b000 b5223000 r-xp /usr/lib/libdrm.so.2.4.0
b522b000 b522f000 r-xp /usr/lib/libxcb-xfixes.so.0.0.0
b5237000 b523a000 r-xp /usr/lib/libxcb-dri2.so.0.0.0
b5242000 b5243000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b524b000 b5256000 r-xp /usr/lib/libtbm.so.1.0.0
b525e000 b5261000 r-xp /usr/lib/libnative-buffer.so.0.1.0
b5269000 b526b000 r-xp /usr/lib/libgenlock.so
b5273000 b5278000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b5280000 b53bb000 r-xp /usr/lib/egl/libGLESv2.so
b53f7000 b53f9000 r-xp /usr/lib/libadreno_utils.so
b5403000 b542a000 r-xp /usr/lib/libgsl.so
b5439000 b5440000 r-xp /usr/lib/egl/eglsubX11.so
b544a000 b546c000 r-xp /usr/lib/egl/libEGL.so
b5475000 b54ea000 r-xp /usr/lib/evas/modules/engines/gl_x11/linux-gnueabi-armv7l-1.7.99/module.so
b56fa000 b5704000 r-xp /lib/libnss_files-2.13.so
b570d000 b5710000 r-xp /lib/libattr.so.1.1.0
b5718000 b571f000 r-xp /lib/libcrypt-2.13.so
b574f000 b5752000 r-xp /lib/libcap.so.2.21
b575a000 b575c000 r-xp /usr/lib/libiri.so
b5764000 b5781000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b578a000 b578e000 r-xp /usr/lib/libsmack.so.1.0.0
b5797000 b57c6000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b57ce000 b5862000 r-xp /usr/lib/libstdc++.so.6.0.16
b5875000 b5944000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b595a000 b597e000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5987000 b5a51000 r-xp /usr/lib/libCOREGL.so.4.0
b5a68000 b5a6a000 r-xp /usr/lib/libXau.so.6.0.0
b5a73000 b5a83000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5a8b000 b5a8e000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5a96000 b5aae000 r-xp /usr/lib/liblzma.so.5.0.3
b5ab7000 b5ab9000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b5ac1000 b5ac4000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b5acc000 b5ad0000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b5ad9000 b5ade000 r-xp /usr/lib/libecore_fb.so.1.7.99
b5ae8000 b5b0b000 r-xp /usr/lib/libjpeg.so.8.0.2
b5b23000 b5b39000 r-xp /lib/libexpat.so.1.6.0
b5b43000 b5b56000 r-xp /usr/lib/libxcb.so.1.1.0
b5b5f000 b5b65000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5b6d000 b5b6e000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5b78000 b5b90000 r-xp /usr/lib/libpng12.so.0.50.0
b5b98000 b5b9b000 r-xp /usr/lib/libEGL.so.1.4
b5ba3000 b5bb1000 r-xp /usr/lib/libGLESv2.so.2.0
b5bba000 b5bbb000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b5bc3000 b5bda000 r-xp /usr/lib/liblua-5.1.so
b5be4000 b5beb000 r-xp /usr/lib/libembryo.so.1.7.99
b5bf3000 b5bfd000 r-xp /usr/lib/libXext.so.6.4.0
b5c06000 b5c0a000 r-xp /usr/lib/libXtst.so.6.1.0
b5c12000 b5c18000 r-xp /usr/lib/libXrender.so.1.3.0
b5c20000 b5c26000 r-xp /usr/lib/libXrandr.so.2.2.0
b5c2e000 b5c2f000 r-xp /usr/lib/libXinerama.so.1.0.0
b5c39000 b5c3c000 r-xp /usr/lib/libXfixes.so.3.1.0
b5c44000 b5c46000 r-xp /usr/lib/libXgesture.so.7.0.0
b5c4e000 b5c50000 r-xp /usr/lib/libXcomposite.so.1.0.0
b5c58000 b5c5a000 r-xp /usr/lib/libXdamage.so.1.1.0
b5c62000 b5c69000 r-xp /usr/lib/libXcursor.so.1.0.2
b5c72000 b5c82000 r-xp /lib/libresolv-2.13.so
b5c86000 b5c88000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5c90000 b5c95000 r-xp /usr/lib/libffi.so.5.0.10
b5c9d000 b5c9e000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5ca6000 b5cef000 r-xp /usr/lib/libmdm.so.1.2.70
b5cf9000 b5cff000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5d07000 b5d0d000 r-xp /usr/lib/libecore_imf.so.1.7.99
b5d15000 b5d2f000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5d37000 b5d55000 r-xp /usr/lib/libsystemd.so.0.4.0
b5d60000 b5d61000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b5d69000 b5d6e000 r-xp /usr/lib/libxdgmime.so.1.1.0
b5d76000 b5d8d000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b5d95000 b5d9b000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b5da4000 b5dad000 r-xp /usr/lib/libcom-core.so.0.0.1
b5db7000 b5db9000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5dc2000 b5e18000 r-xp /usr/lib/libpixman-1.so.0.28.2
b5e25000 b5e7b000 r-xp /usr/lib/libfreetype.so.6.11.3
b5e87000 b5ecc000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b5ed5000 b5ee8000 r-xp /usr/lib/libfribidi.so.0.3.1
b5ef1000 b5f0b000 r-xp /usr/lib/libecore_con.so.1.7.99
b5f14000 b5f3e000 r-xp /usr/lib/libdbus-1.so.3.8.12
b5f47000 b5f50000 r-xp /usr/lib/libedbus.so.1.7.99
b5f58000 b5f69000 r-xp /usr/lib/libecore_input.so.1.7.99
b5f71000 b5f76000 r-xp /usr/lib/libecore_file.so.1.7.99
b5f7f000 b5fa1000 r-xp /usr/lib/libecore_evas.so.1.7.99
b5faa000 b5fc3000 r-xp /usr/lib/libeet.so.1.7.99
b5fd4000 b5ffc000 r-xp /usr/lib/libfontconfig.so.1.8.0
b5ffd000 b6006000 r-xp /usr/lib/libXi.so.6.1.0
b600e000 b60ef000 r-xp /usr/lib/libX11.so.6.3.0
b60fb000 b61b3000 r-xp /usr/lib/libcairo.so.2.11200.14
b61be000 b621c000 r-xp /usr/lib/libedje.so.1.7.99
b6226000 b6276000 r-xp /usr/lib/libecore_x.so.1.7.99
b6278000 b62e1000 r-xp /lib/libm-2.13.so
b62ea000 b62f0000 r-xp /lib/librt-2.13.so
b62f9000 b630f000 r-xp /lib/libz.so.1.2.5
b6318000 b64aa000 r-xp /usr/lib/libcrypto.so.1.0.0
b64cb000 b6512000 r-xp /usr/lib/libssl.so.1.0.0
b651e000 b654c000 r-xp /usr/lib/libidn.so.11.5.44
b6554000 b655d000 r-xp /usr/lib/libcares.so.2.1.0
b6566000 b6632000 r-xp /usr/lib/libxml2.so.2.7.8
b6640000 b6642000 r-xp /usr/lib/libiniparser.so.0
b664b000 b667f000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b6688000 b675b000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6766000 b677f000 r-xp /usr/lib/libnetwork.so.0.0.0
b6787000 b6790000 r-xp /usr/lib/libvconf.so.0.2.45
b6799000 b6869000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b686a000 b68ab000 r-xp /usr/lib/libeina.so.1.7.99
b68b4000 b68b9000 r-xp /usr/lib/libappcore-common.so.1.1
b68c1000 b68c7000 r-xp /usr/lib/libappcore-efl.so.1.1
b68cf000 b68d2000 r-xp /usr/lib/libbundle.so.0.1.22
b68da000 b68e0000 r-xp /usr/lib/libappsvc.so.0.1.0
b68e8000 b68fc000 r-xp /lib/libpthread-2.13.so
b6907000 b692a000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6932000 b693f000 r-xp /usr/lib/libaul.so.0.1.0
b6949000 b694b000 r-xp /lib/libdl-2.13.so
b6954000 b695f000 r-xp /lib/libunwind.so.8.0.1
b698c000 b6994000 r-xp /lib/libgcc_s-4.6.so.1
b6995000 b6ab9000 r-xp /lib/libc-2.13.so
b6ac7000 b6b3c000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6b46000 b6b52000 r-xp /usr/lib/libnotification.so.0.1.0
b6b5b000 b6b6a000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b6b73000 b6c41000 r-xp /usr/lib/libevas.so.1.7.99
b6c67000 b6da3000 r-xp /usr/lib/libelementary.so.1.7.99
b6dba000 b6ddb000 r-xp /usr/lib/libefl-extension.so.0.1.0
b6de3000 b6dfa000 r-xp /usr/lib/libecore.so.1.7.99
b6e11000 b6e13000 r-xp /usr/lib/libdlog.so.0.0.0
b6e1b000 b6e5f000 r-xp /usr/lib/libcurl.so.4.3.0
b6e68000 b6e6d000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b6e75000 b6e7a000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b6e82000 b6e92000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b6e9a000 b6ea2000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b6eaa000 b6eae000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b6eb6000 b6eba000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b6ec3000 b6ec5000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b6ed0000 b6edb000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b6ee5000 b6ee9000 r-xp /usr/lib/libsys-assert.so
b6ef2000 b6f0f000 r-xp /lib/ld-2.13.so
b6f18000 b6f22000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b7865000 b7cdf000 rw-p [heap]
be862000 be883000 rw-p [stack]
b7865000 b7cdf000 rw-p [heap]
be862000 be883000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2378)
Call Stack Count: 22
 0: (0xb6bbb7a2) [/usr/lib/libevas.so.1] + 0x487a2
 1: (0xb6bbba25) [/usr/lib/libevas.so.1] + 0x48a25
 2: evas_object_textblock_size_formatted_get + 0x5e (0xb6bc045b) [/usr/lib/libevas.so.1] + 0x4d45b
 3: (0xb61d3b89) [/usr/lib/libedje.so.1] + 0x15b89
 4: (0xb61d513b) [/usr/lib/libedje.so.1] + 0x1713b
 5: (0xb61d6d91) [/usr/lib/libedje.so.1] + 0x18d91
 6: (0xb61d8f8b) [/usr/lib/libedje.so.1] + 0x1af8b
 7: (0xb6206a31) [/usr/lib/libedje.so.1] + 0x48a31
 8: (0xb6207e77) [/usr/lib/libedje.so.1] + 0x49e77
 9: (0xb6204869) [/usr/lib/libedje.so.1] + 0x46869
10: (0xb6204c1b) [/usr/lib/libedje.so.1] + 0x46c1b
11: (0xb6204d55) [/usr/lib/libedje.so.1] + 0x46d55
12: (0xb6dee3f5) [/usr/lib/libecore.so.1] + 0xb3f5
13: (0xb6debe53) [/usr/lib/libecore.so.1] + 0x8e53
14: (0xb6def46b) [/usr/lib/libecore.so.1] + 0xc46b
15: ecore_main_loop_begin + 0x30 (0xb6def879) [/usr/lib/libecore.so.1] + 0xc879
16: appcore_efl_main + 0x332 (0xb68c4b47) [/usr/lib/libappcore-efl.so.1] + 0x3b47
17: ui_app_main + 0xb0 (0xb6eabed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
18: uib_app_run + 0xea (0xb6f1d3fb) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x53fb
19: main + 0x34 (0xb6f1d8e5) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x58e5
20: __libc_start_main + 0x114 (0xb69ac85c) [/lib/libc.so.6] + 0x1785c
21: (0xb6f1b10c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x310c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
dir(/opt/usr/data/pkgmgr/com.toyota.realtimefeedback/) failed. [2][No such file or directory]
04-30 13:51:09.199+0700 E/rpm-installer( 2344): coretpk-installer.c: _coretpk_installer_install_package(3579) > Deletion failed: [/opt/usr/data/pkgmgr/com.toyota.realtimefeedback/]
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-home:music-control-service-message-port]
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-30 13:51:09.219+0700 W/W_HOME  ( 1243): clock_shortcut.c: _music_service_messageport_cb(361) > mode:remote state:unknown 
04-30 13:51:09.219+0700 E/W_HOME  ( 1243): clock_shortcut.c: _mp_state_get(104) > (s_info.music_service.state != 1) -> _mp_state_get() return
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-home:music-control-service-message-port]
04-30 13:51:09.219+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-30 13:51:09.239+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 13:51:09.239+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 13:51:09.239+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-30 13:51:09.239+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-30 13:51:09.239+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-30 13:51:09.239+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-30 13:51:09.239+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-home:music-control-service-message-port]
04-30 13:51:09.239+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-30 13:51:09.239+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-30 13:51:09.239+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-30 13:51:09.369+0700 E/PKGMGR_OBSERVER( 2343): pkg_observer.c: __pkg_observer_cb(586) > pkg observer info : req_id[23430002] pkg_type[tpk] pkgid[com.toyota.realtimefeedback] key[start] val[install]
04-30 13:51:09.379+0700 I/watchface-app( 1307): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
04-30 13:51:09.389+0700 W/AUL_AMD (  923): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(984) > pkgid(com.toyota.realtimefeedback), key(start), value(install)
04-30 13:51:09.389+0700 E/PKGMGR_OBSERVER( 2343): pkg_observer.c: __pkg_observer_cb(586) > pkg observer info : req_id[23430002] pkg_type[tpk] pkgid[com.toyota.realtimefeedback] key[install_percent] val[30]
04-30 13:51:09.399+0700 I/watchface-app( 1307): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
04-30 13:51:09.419+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [INSTALL, STARTED]
04-30 13:51:09.449+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [INSTALL, PROCESSING]
04-30 13:51:09.889+0700 E/PKGMGR_SERVER( 2311): pkgmgr-server.c: exit_server(1620) > exit_server Start [backend_status=0, queue_status=1] 
04-30 13:51:09.999+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:09.999+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:10.069+0700 W/CERT_SVC( 2344): XmlsecAdapter.cpp: validateFile(286) > [38;5;202;1m## [validate]: uriList does not exist[0m
04-30 13:51:10.119+0700 E/rpm-installer( 2344): coretpk-parser.c: __coretpk_parser_verify_metadata(48) > (ret == 1) metadata is empty.
04-30 13:51:10.119+0700 E/rpm-installer( 2344): coretpk-parser.c: __coretpk_parser_append_path(417) > (ret == 1) NodeSet is empty. (//@portrait-effectimage)
04-30 13:51:10.119+0700 E/rpm-installer( 2344): coretpk-parser.c: __coretpk_parser_append_path(417) > (ret == 1) NodeSet is empty. (//*[name()='widget-application']//*[name()='support-size']/@preview)
04-30 13:51:10.119+0700 E/rpm-installer( 2344): coretpk-parser.c: __coretpk_parser_widget_replace_widget_tag(541) > (ret == 1) NodeSet is empty. (//*[name() = 'widget-application'])
04-30 13:51:10.219+0700 E/PKGMGR_PARSER( 2344): pkgmgr_parser.c: pkgmgr_parser_check_manifest_validation(2167) > Manifest is Valid
04-30 13:51:10.219+0700 E/PKGMGR_PARSER( 2344): pkgmgr_parser.c: __parse_manifest_for_installation(1910) > parsing manifest for installation: /opt/share/packages/com.toyota.realtimefeedback.xml
04-30 13:51:10.219+0700 E/PKGMGR_PARSER( 2344): pkgmgr_parser_signature.c: __ps_check_mdm_policy(979) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
04-30 13:51:10.219+0700 E/PKGMGR_PARSER( 2344): pkgmgr_parser.c: __ps_process_tag(866) > tag is NULL
04-30 13:51:10.289+0700 E/PKGMGR_PARSER( 2344): pkgmgr_parser_db.c: __exec_query_no_msg(586) > sqlite3_exec failed, error: [UNIQUE constraint failed: package_localized_info.package, package_localized_info.package_locale]
04-30 13:51:10.289+0700 E/PKGMGR_PARSER( 2344): pkgmgr_parser_db.c: __verify_label_cb(470) > package_label is PKGMGR_PARSER_EMPTY_STR
04-30 13:51:10.359+0700 I/PRIVACY-MANAGER-CLIENT( 2344): SocketClient.cpp: SocketClient(37) > Client created
04-30 13:51:10.609+0700 I/efl-extension( 2344): efl_extension.c: eext_mod_init(40) > Init
04-30 13:51:10.609+0700 I/efl-extension( 2344): efl_extension.c: eext_mod_shutdown(46) > Shutdown
04-30 13:51:10.949+0700 E/PKGMGR_PARSER( 2344): pkgmgr_parser.c: __parse_manifest_for_installation(1952) > parsing manifest for installation Done
04-30 13:51:10.999+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:10.999+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:11.029+0700 E/PKGMGR_CERT( 2344): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(426) > Transaction Begin
04-30 13:51:11.039+0700 E/PKGMGR_CERT( 2344): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(495) > Id:Count = 1 124
04-30 13:51:11.039+0700 E/PKGMGR_CERT( 2344): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(495) > Id:Count = 2 124
04-30 13:51:11.039+0700 E/PKGMGR_CERT( 2344): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(495) > Id:Count = 19 3
04-30 13:51:11.039+0700 E/PKGMGR_CERT( 2344): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(495) > Id:Count = 20 3
04-30 13:51:11.039+0700 E/PKGMGR_CERT( 2344): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(495) > Id:Count = 21 3
04-30 13:51:11.039+0700 E/PKGMGR_CERT( 2344): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(495) > Id:Count = 22 3
04-30 13:51:11.059+0700 E/PKGMGR_CERT( 2344): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(575) > Transaction Commit and End
04-30 13:51:11.059+0700 E/PKGMGR_OBSERVER( 2343): pkg_observer.c: __pkg_observer_cb(586) > pkg observer info : req_id[23430002] pkg_type[tpk] pkgid[com.toyota.realtimefeedback] key[install_percent] val[60]
04-30 13:51:11.069+0700 I/watchface-app( 1307): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
04-30 13:51:11.079+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [INSTALL, PROCESSING]
04-30 13:51:11.159+0700 E/rpm-installer( 2344): installer-util.c: _installer_util_get_configuration_value(601) > [signature]=[on]
04-30 13:51:11.169+0700 E/rpm-installer( 2344): coretpk-installer.c: _coretpk_installer_apply_smack(3199) > groupid = [YcOEplX7zg6C3qqKLGmSFPicM8ebmf2xSz6gzbiwXDI=] for shared/trusted.
04-30 13:51:11.249+0700 E/rpm-installer( 2344): coretpk-installer.c: __post_install_for_mmc(742) > (handle == NULL) handle is NULL.
04-30 13:51:11.249+0700 E/PKGMGR_OBSERVER( 2343): pkg_observer.c: __pkg_observer_cb(586) > pkg observer info : req_id[23430002] pkg_type[tpk] pkgid[com.toyota.realtimefeedback] key[install_percent] val[100]
04-30 13:51:11.259+0700 I/watchface-app( 1307): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
04-30 13:51:11.269+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [INSTALL, PROCESSING]
04-30 13:51:11.349+0700 E/rpm-installer( 2344): coretpk-installer.c: __set_softreset_script(127) > (ret != PMINFO_R_OK) dont have SUPPORT_SOFTRESET_SCRIPT_METADATA_KEY
04-30 13:51:11.519+0700 W/AUL_PAD ( 1885): sigchild.h: __launchpad_process_sigchld(188) > dead_pid = 2329 pgid = 1885
04-30 13:51:11.519+0700 W/AUL_PAD ( 1885): sigchild.h: __launchpad_process_sigchld(189) > ssi_code = 2 ssi_status = 9
04-30 13:51:11.609+0700 W/AUL_PAD ( 1885): sigchild.h: __launchpad_process_sigchld(197) > after __sigchild_action
04-30 13:51:11.619+0700 I/AUL_AMD (  923): amd_main.c: __app_dead_handler(262) > __app_dead_handler, pid: 2329
04-30 13:51:11.619+0700 W/AUL     (  923): app_signal.c: aul_send_app_terminated_signal(799) > aul_send_app_terminated_signal pid(2329)
04-30 13:51:11.709+0700 W/AUL_PAD ( 1885): sigchild.h: __launchpad_process_sigchld(188) > dead_pid = 2330 pgid = 1885
04-30 13:51:11.709+0700 W/AUL_PAD ( 1885): sigchild.h: __launchpad_process_sigchld(189) > ssi_code = 2 ssi_status = 9
04-30 13:51:11.809+0700 W/AUL_PAD ( 1885): sigchild.h: __launchpad_process_sigchld(197) > after __sigchild_action
04-30 13:51:11.829+0700 I/AUL_AMD (  923): amd_main.c: __app_dead_handler(262) > __app_dead_handler, pid: 2330
04-30 13:51:11.829+0700 W/AUL     (  923): app_signal.c: aul_send_app_terminated_signal(799) > aul_send_app_terminated_signal pid(2330)
04-30 13:51:11.889+0700 E/PKGMGR_SERVER( 2311): pkgmgr-server.c: exit_server(1620) > exit_server Start [backend_status=0, queue_status=1] 
04-30 13:51:11.999+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:11.999+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:12.999+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:12.999+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:13.269+0700 I/GATE    (  925): <GATE-M>BATTERY_LEVEL_88</GATE-M>
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_update(98) > [windicator_battery_update:98] 
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(312) > [_battery_icon_update:312] battery level(88), length(2)
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(336) > [_battery_icon_update:336] 88%
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(351) > [_battery_icon_update:351] battery_level: 88, isCharging: 0
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(385) > [_battery_icon_update:385] battery file : change_level_90
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(464) > [_battery_icon_update:464] [ATT] Battery level : 90
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(531) > [_battery_icon_update:531] Normal charging status !!
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_capacity_cb(566) > [_battery_capacity_cb:566] percentage 88
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_update(98) > [windicator_battery_update:98] 
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(312) > [_battery_icon_update:312] battery level(88), length(2)
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(336) > [_battery_icon_update:336] 88%
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(351) > [_battery_icon_update:351] battery_level: 88, isCharging: 0
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(385) > [_battery_icon_update:385] battery file : change_level_90
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(464) > [_battery_icon_update:464] [ATT] Battery level : 90
04-30 13:51:13.269+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(531) > [_battery_icon_update:531] Normal charging status !!
04-30 13:51:13.269+0700 I/watchface-viewer( 1307): viewer-part-resource-evas.cpp: CreateTextImage(1028) > style[DEFAULT='font=Default font_size=40 color=#FFFFFF ellipsis=-1 align=center ']
04-30 13:51:13.269+0700 I/watchface-viewer( 1307): viewer-part-resource-evas.cpp: CreateTextImage(1042) > formatted size 53x32
04-30 13:51:13.269+0700 I/watchface-viewer( 1307): viewer-image-file-loader.cpp: OnImageLoadingDoneIdlerCb(792) > ImagesLoadingDoneSignal().Emit()
04-30 13:51:13.889+0700 E/PKGMGR_SERVER( 2311): pkgmgr-server.c: exit_server(1620) > exit_server Start [backend_status=0, queue_status=1] 
04-30 13:51:13.989+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:13.989+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:14.999+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:14.999+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:15.889+0700 E/PKGMGR_SERVER( 2311): pkgmgr-server.c: exit_server(1620) > exit_server Start [backend_status=0, queue_status=1] 
04-30 13:51:15.999+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:15.999+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:16.339+0700 E/PKGMGR_OBSERVER( 2343): pkg_observer.c: __pkg_observer_cb(586) > pkg observer info : req_id[23430002] pkg_type[tpk] pkgid[com.toyota.realtimefeedback] key[end] val[ok]
04-30 13:51:16.359+0700 I/watchface-app( 1307): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
04-30 13:51:16.369+0700 W/AUL_AMD (  923): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(984) > pkgid(com.toyota.realtimefeedback), key(end), value(ok)
04-30 13:51:16.379+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [INSTALL, COMPLETED]
04-30 13:51:16.379+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_cb_package_manager_event(7086) > package install complete
04-30 13:51:16.379+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_package_install_event(5040) > 
04-30 13:51:16.379+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_get_index_from_install_req_list(2046) > Found in install_req_list?[0], index[-1]
04-30 13:51:16.379+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_package_install_event(5066) > triggered from Gear itself.
04-30 13:51:16.439+0700 E/WMS     ( 1015): wms_db.c: wms_db_package_event_insert_record(190) > 
04-30 13:51:16.459+0700 E/WMS     ( 1015): wms_event_handler.c: _wms_event_handler_send_mgr_wapps_install_res(2892) > send_install_response completed : END
04-30 13:51:16.479+0700 E/PKGMGR_INFO( 2343): pkgmgrinfo_appinfo.c: pkgmgrinfo_appinfo_get_list(1026) > callback is stopped.
04-30 13:51:16.489+0700 E/PKGMGR_OBSERVER( 2343): pkg_observer.c: main(620) > OBSERVER end
04-30 13:51:16.509+0700 E/PKGMGR_INFO(  926): pkgmgrinfo_appinfo.c: pkgmgrinfo_appinfo_get_list(922) > (component == PMINFO_SVC_APP) PMINFO_SVC_APP is done
04-30 13:51:16.519+0700 E/PKGMGR_SERVER( 2311): pkgmgr-server.c: sighandler(486) > child NORMAL exit [2343]
04-30 13:51:16.539+0700 W/APPS    ( 1243): AppsViewNecklace.cpp: onAddItem(356) >  [Real Feed Back]
04-30 13:51:16.639+0700 W/APPS    ( 1243): AppsItem.cpp: setLastIndex(1266) >  This is not last index app [25:Galaxy Apps]
04-30 13:51:16.639+0700 E/Vi::Animations( 1243): result Vi::Animations::_AnimationManager::addAnimation(Vi::Animations::Visual&, const string*, Vi::Animations::Animation&)(288) > [E_OBJ_ALREADY_EXIST] Animation with keyName already exists. key name = hide
04-30 13:51:16.639+0700 I/Vi::Animations( 1243): result Vi::Animations::_VisualImpl::addAnimation(const string*, Vi::Animations::Animation&)(6999) > [E_OBJ_ALREADY_EXIST] Propagating.
04-30 13:51:16.639+0700 W/APPS    ( 1243): AppsItem.cpp: setLastIndex(1273) >  This is last index app [26:Real Feed Back], focusIdx[25]
04-30 13:51:16.639+0700 W/APPS    ( 1243): AppsViewNecklace.cpp: __onSignalHideNextPage(7063) >  Hide next page [0->0]
04-30 13:51:16.639+0700 E/EFL     ( 1243): elementary<1243> elm_layout.c:1021 _elm_layout_smart_content_set() could not swallow 0xb824bed0 into part 'elm.swallow.event.0'
04-30 13:51:16.649+0700 W/APPS    ( 1243): AppsItem.cpp: aniFocusIndex(2367) >  [Galaxy Apps:25] Focused[1], focusIdx[25]
04-30 13:51:16.649+0700 W/APPS    ( 1243): AppsItem.cpp: setLastIndex(1273) >  This is last index app [26:Real Feed Back], focusIdx[25]
04-30 13:51:16.649+0700 W/APPS    ( 1243): AppsViewNecklace.cpp: __onSignalHideNextPage(7063) >  Hide next page [0->0]
04-30 13:51:16.649+0700 W/APPS    ( 1243): AppsViewNecklace.cpp: setBubbleColor(2479) >  [249, 249, 249, 255]
04-30 13:51:16.649+0700 W/APPS    ( 1243): db.c: apps_db_read_list(621) >  
04-30 13:51:16.999+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:16.999+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:17.889+0700 E/PKGMGR_SERVER( 2311): pkgmgr-server.c: exit_server(1620) > exit_server Start [backend_status=0, queue_status=1] 
04-30 13:51:17.999+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:17.999+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:18.799+0700 W/AUL     ( 2377): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.toyota.realtimefeedback)
04-30 13:51:18.809+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 0
04-30 13:51:18.819+0700 I/AUL_AMD (  923): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/launch_app, ret : 0
04-30 13:51:18.829+0700 I/AUL_AMD (  923): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /bin/bash, ret : 0
04-30 13:51:18.829+0700 E/AUL_AMD (  923): amd_launch.c: _start_app(1772) > no caller appid info, ret: -1
04-30 13:51:18.829+0700 W/AUL_AMD (  923): amd_launch.c: _start_app(1782) > caller pid : 2377
04-30 13:51:18.849+0700 E/RESOURCED( 1136): block.c: block_prelaunch_state(138) > insert data com.toyota.realtimefeedback, table num : 2
04-30 13:51:18.849+0700 W/AUL_AMD (  923): amd_launch.c: _start_app(2218) > pad pid(-5)
04-30 13:51:18.859+0700 W/AUL_PAD ( 1885): launchpad.c: __launchpad_main_loop(630) > Candidate is not prepared, enter legacy logic
04-30 13:51:18.859+0700 W/AUL_PAD ( 1885): launchpad.c: __send_result_to_caller(272) > Check app launching
04-30 13:51:18.859+0700 E/AUL_PAD ( 1885): launchpad.c: __send_result_to_caller(278) > launching failed
04-30 13:51:18.859+0700 W/AUL_AMD (  923): amd_launch.c: _start_app(2229) > Launch with legacy way : no launchpad
04-30 13:51:18.859+0700 W/AUL_AMD (  923): amd_launch.c: start_process(606) > child process: 2378
04-30 13:51:18.909+0700 W/AUL_AMD (  923): amd_launch.c: __send_app_launch_signal(397) > send launch signal done: 2378
04-30 13:51:18.909+0700 W/AUL     (  923): app_signal.c: aul_send_app_launch_request_signal(521) > aul_send_app_launch_request_signal app(com.toyota.realtimefeedback) pid(2378) type(uiapp) bg(0)
04-30 13:51:18.909+0700 W/AUL     ( 2377): launch.c: app_request_to_launchpad(298) > request cmd(0) result(2378)
04-30 13:51:18.909+0700 W/STARTER ( 1141): pkg-monitor.c: _app_mgr_status_cb(400) > [_app_mgr_status_cb:400] Launch request [2378]
04-30 13:51:18.969+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 13:51:18.979+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2378
04-30 13:51:18.989+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 12
04-30 13:51:18.999+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:18.999+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:19.129+0700 I/efl-extension( 2378): efl_extension.c: eext_mod_init(40) > Init
04-30 13:51:19.239+0700 I/CAPI_APPFW_APPLICATION( 2378): app_main.c: ui_app_main(704) > app_efl_main
04-30 13:51:19.329+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 14
04-30 13:51:19.339+0700 W/AUL_AMD (  923): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2378
04-30 13:51:19.429+0700 W/AUL_AMD (  923): amd_request.c: __request_handler(669) > __request_handler: 12
04-30 13:51:19.819+0700 W/AUL_AMD (  923): amd_status.c: __socket_monitor_cb(1277) > (2378) was created
04-30 13:51:19.819+0700 I/CAPI_APPFW_APPLICATION( 2378): app_main.c: _ui_app_appcore_create(563) > app_appcore_create
04-30 13:51:19.889+0700 E/PKGMGR_SERVER( 2311): pkgmgr-server.c: exit_server(1620) > exit_server Start [backend_status=0, queue_status=1] 
04-30 13:51:19.909+0700 W/AUL_AMD (  923): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-30 13:51:19.909+0700 W/AUL_AMD (  923): amd_launch.c: __grab_timeout_handler(1453) > back key ungrab error
04-30 13:51:19.999+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:19.999+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:20.039+0700 I/Adreno-EGL( 2378): <qeglDrvAPI_eglInitialize:410>: EGL 1.4 QUALCOMM build:  ()
04-30 13:51:20.039+0700 I/Adreno-EGL( 2378): OpenGL ES Shader Compiler Version: E031.24.00.16
04-30 13:51:20.039+0700 I/Adreno-EGL( 2378): Build Date: 09/02/15 Wed
04-30 13:51:20.039+0700 I/Adreno-EGL( 2378): Local Branch: 
04-30 13:51:20.039+0700 I/Adreno-EGL( 2378): Remote Branch: 
04-30 13:51:20.039+0700 I/Adreno-EGL( 2378): Local Patches: 
04-30 13:51:20.039+0700 I/Adreno-EGL( 2378): Reconstruct Branch: 
04-30 13:51:20.409+0700 W/W_HOME  ( 1243): event_manager.c: _ecore_x_message_cb(421) > state: 0 -> 1
04-30 13:51:20.409+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:20.409+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:20.409+0700 W/W_INDICATOR( 1142): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-30 13:51:20.409+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-30 13:51:20.409+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:20.409+0700 W/W_HOME  ( 1243): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 0
04-30 13:51:20.449+0700 E/EFL     ( 2378): ecore_evas<2378> ecore_evas_extn.c:2204 ecore_evas_extn_plug_connect() Extn plug failed to connect:ipctype=0, svcname=elm_indicator_portrait, svcnum=0, svcsys=0
04-30 13:51:20.469+0700 I/efl-extension( 2378): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-30 13:51:20.469+0700 E/EFL     ( 2378): elementary<2378> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7b2e650 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-30 13:51:20.469+0700 E/EFL     ( 2378): elementary<2378> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7b2e650 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-30 13:51:20.469+0700 E/EFL     ( 2378): elementary<2378> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7b2e650 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-30 13:51:20.469+0700 E/EFL     ( 2378): elementary<2378> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7b2e650 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-30 13:51:20.469+0700 E/EFL     ( 2378): elementary<2378> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7b2e650 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
04-30 13:51:20.469+0700 I/efl-extension( 2378): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7b2eab8, elm_image, _activated_obj : 0x0, activated : 1
04-30 13:51:20.579+0700 I/efl-extension( 2378): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-30 13:51:20.579+0700 I/efl-extension( 2378): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-30 13:51:20.579+0700 I/efl-extension( 2378): efl_extension_rotary.c: eext_rotary_event_handler_add(77) > init_count: 0
04-30 13:51:20.579+0700 I/efl-extension( 2378): efl_extension_rotary.c: _init_Xi2_system(314) > In
04-30 13:51:20.739+0700 I/efl-extension( 2378): efl_extension_rotary.c: _init_Xi2_system(375) > Done
04-30 13:51:20.739+0700 I/efl-extension( 2378): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7b43f20, elm_image, _activated_obj : 0xb7b2eab8, activated : 1
04-30 13:51:20.739+0700 I/efl-extension( 2378): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-30 13:51:20.879+0700 E/EFL     ( 2378): elementary<2378> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7b2eab8 in function: elm_progressbar_pulse_set, of type: 'elm_image' when expecting type: 'elm_progressbar'
04-30 13:51:20.879+0700 E/EFL     ( 2378): elementary<2378> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7b2eab8 in function: elm_progressbar_pulse, of type: 'elm_image' when expecting type: 'elm_progressbar'
04-30 13:51:20.919+0700 E/EFL     ( 2378): elementary<2378> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-30 13:51:20.949+0700 E/EFL     ( 2378): elementary<2378> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb7b87620) will be pushed
04-30 13:51:20.989+0700 I/APP_CORE( 2378): appcore-efl.c: __do_app(453) > [APP 2378] Event: RESET State: CREATED
04-30 13:51:20.989+0700 I/CAPI_APPFW_APPLICATION( 2378): app_main.c: _ui_app_appcore_reset(645) > app_appcore_reset
04-30 13:51:20.999+0700 I/CAPI_WATCH_APPLICATION( 1307): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-30 13:51:20.999+0700 E/watchface-app( 1307): watchface.cpp: OnAppTimeTick(1157) > 
04-30 13:51:21.069+0700 I/APP_CORE( 2378): appcore-efl.c: __do_app(522) > Legacy lifecycle: 0
04-30 13:51:21.069+0700 I/APP_CORE( 2378): appcore-efl.c: __do_app(524) > [APP 2378] Initial Launching, call the resume_cb
04-30 13:51:21.069+0700 I/CAPI_APPFW_APPLICATION( 2378): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
04-30 13:51:21.079+0700 W/APP_CORE( 2378): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3200003
04-30 13:51:21.079+0700 E/EFL     ( 2378): elementary<2378> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7b2f098 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-30 13:51:21.079+0700 E/EFL     ( 2378): elementary<2378> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7b2f098 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-30 13:51:21.089+0700 E/EFL     ( 2378): elementary<2378> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7b2f098 : elm_genlist] mx(99999639), my(0), minx(0), miny(0), px(0), py(0)
04-30 13:51:21.089+0700 E/EFL     ( 2378): elementary<2378> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7b2f098 : elm_genlist] cw(99999999), ch(0), pw(360), ph(360)
04-30 13:51:21.209+0700 I/APP_CORE( 2378): appcore-efl.c: __do_app(453) > [APP 2378] Event: RESUME State: RUNNING
04-30 13:51:21.209+0700 I/GATE    ( 2378): <GATE-M>APP_FULLY_LOADED_realtimefeedback</GATE-M>
04-30 13:51:21.209+0700 W/AUL     (  923): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1243) status(bg) type(uiapp)
04-30 13:51:21.219+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1243] goes to (4)
04-30 13:51:21.219+0700 E/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1243)'s state(4)
04-30 13:51:21.219+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2378] goes to (3)
04-30 13:51:21.219+0700 W/AUL_AMD (  923): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-30 13:51:21.219+0700 W/AUL_AMD (  923): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-30 13:51:21.219+0700 W/AUL     (  923): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.toyota.realtimefeedback) pid(2378) status(fg) type(uiapp)
04-30 13:51:21.229+0700 W/W_HOME  ( 1243): event_manager.c: _window_visibility_cb(460) > Window [0x2400003] is now visible(1)
04-30 13:51:21.229+0700 W/W_HOME  ( 1243): event_manager.c: _window_visibility_cb(470) > state: 1 -> 0
04-30 13:51:21.229+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:1(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:21.229+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:6, app_state:1 win_state:1(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:21.229+0700 W/W_HOME  ( 1243): main.c: _window_visibility_cb(996) > Window [0x2400003] is now visible(1)
04-30 13:51:21.239+0700 I/APP_CORE( 1243): appcore-efl.c: __do_app(453) > [APP 1243] Event: PAUSE State: RUNNING
04-30 13:51:21.239+0700 I/CAPI_APPFW_APPLICATION( 1243): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-30 13:51:21.239+0700 W/W_HOME  ( 1243): main.c: _appcore_pause_cb(489) > appcore pause
04-30 13:51:21.239+0700 W/W_HOME  ( 1243): event_manager.c: _app_pause_cb(390) > state: 1 -> 2
04-30 13:51:21.239+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:1(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:21.239+0700 W/W_INDICATOR( 1142): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-30 13:51:21.239+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-30 13:51:21.239+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:0, app_state:2 win_state:1(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:21.239+0700 W/W_HOME  ( 1243): main.c: home_pause(550) > clock/widget paused
04-30 13:51:21.239+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:1(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-30 13:51:21.249+0700 I/MESSAGE_PORT(  919): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-30 13:51:21.249+0700 I/wnotib  ( 1243): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 1
04-30 13:51:21.249+0700 E/wnotib  ( 1243): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
04-30 13:51:21.249+0700 W/wnotib  ( 1243): w-notification-board-noti-manager.c: wnb_nm_postpone_updating_job(997) > Set is_notiboard_update_postponed to true with is_for_VI 0, notiboard panel creation count [2], notiboard card appending count [2].
04-30 13:51:21.299+0700 W/WATCH_CORE( 1307): appcore-watch.c: __widget_pause(1113) > widget_pause
04-30 13:51:21.299+0700 W/AUL     ( 1307): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.techgraphy.DigitalTick) pid(1307) status(bg) type(watchapp)
04-30 13:51:21.299+0700 E/watchface-app( 1307): watchface.cpp: OnAppPause(1122) > 
04-30 13:51:21.309+0700 W/MUSIC_CONTROL_SERVICE( 1728): music-control-service.c: _music_control_service_pasre_request(565) > [33m[TID:1728]   [com.samsung.w-home]register msg port [false][0m
04-30 13:51:21.589+0700 I/efl-extension( 2361): efl_extension.c: eext_mod_init(40) > Init
04-30 13:51:21.609+0700 E/AUL     (  923): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-30 13:51:21.749+0700 I/APP_CORE( 1243): appcore-efl.c: __do_app(453) > [APP 1243] Event: MEM_FLUSH State: PAUSED
04-30 13:51:21.889+0700 E/PKGMGR_SERVER( 2311): pkgmgr-server.c: exit_server(1620) > exit_server Start [backend_status=0, queue_status=1] 
04-30 13:51:21.959+0700 E/rpm-installer( 2344): rpm-appcore-intf.c: main(273) > ------------------------------------------------
04-30 13:51:21.959+0700 E/rpm-installer( 2344): rpm-appcore-intf.c: main(274) >  [END] installer: result=[0]
04-30 13:51:21.959+0700 E/rpm-installer( 2344): rpm-appcore-intf.c: main(275) > ------------------------------------------------
04-30 13:51:21.969+0700 E/PKGMGR_SERVER( 2311): pkgmgr-server.c: sighandler(486) > child NORMAL exit [2344]
04-30 13:51:22.019+0700 E/RESOURCED( 1136): procfs.c: proc_get_oom_score_adj(178) > fopen /proc/2344/oom_score_adj failed
04-30 13:51:22.019+0700 E/RESOURCED( 1136): proc-main.c: resourced_proc_status_change(1501) > Empty pid or process not exists. 2344
04-30 13:51:22.029+0700 I/AUL_PAD ( 2360): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
04-30 13:51:22.369+0700 I/UXT     ( 2361): Uxt_ObjectManager.cpp: OnInitialized(753) > Initialized.
04-30 13:51:22.509+0700 I/GATE    ( 2317): <GATE-M>FORCE_CLOSED_realtimefeedback1</GATE-M>
04-30 13:51:22.549+0700 I/AUL_PAD ( 2361): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
04-30 13:51:22.569+0700 I/Adreno-EGL( 2361): <qeglDrvAPI_eglInitialize:410>: EGL 1.4 QUALCOMM build:  ()
04-30 13:51:22.569+0700 I/Adreno-EGL( 2361): OpenGL ES Shader Compiler Version: E031.24.00.16
04-30 13:51:22.569+0700 I/Adreno-EGL( 2361): Build Date: 09/02/15 Wed
04-30 13:51:22.569+0700 I/Adreno-EGL( 2361): Local Branch: 
04-30 13:51:22.569+0700 I/Adreno-EGL( 2361): Remote Branch: 
04-30 13:51:22.569+0700 I/Adreno-EGL( 2361): Local Patches: 
04-30 13:51:22.569+0700 I/Adreno-EGL( 2361): Reconstruct Branch: 
04-30 13:51:23.029+0700 W/WATCH_CORE( 1307): appcore-watch.c: __signal_lcd_status_handler(1231) > signal_lcd_status_signal: LCDOff
04-30 13:51:23.029+0700 W/W_HOME  ( 1243): dbus.c: _dbus_message_recv_cb(204) > LCD off
04-30 13:51:23.029+0700 W/W_HOME  ( 1243): gesture.c: _manual_render_cancel_schedule(226) > cancel schedule, manual render
04-30 13:51:23.029+0700 W/W_HOME  ( 1243): gesture.c: _manual_render_disable_timer_del(157) > timer del
04-30 13:51:23.029+0700 W/W_HOME  ( 1243): gesture.c: _manual_render_enable(138) > 1
04-30 13:51:23.029+0700 W/W_HOME  ( 1243): event_manager.c: _lcd_off_cb(723) > lcd state: 0
04-30 13:51:23.029+0700 W/W_HOME  ( 1243): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:1(0) pm_state:0 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-30 13:51:23.029+0700 W/STARTER ( 1141): clock-mgr.c: _on_lcd_signal_receive_cb(1284) > [_on_lcd_signal_receive_cb:1284] _on_lcd_signal_receive_cb, 1284, Pre LCD off by [timeout]
04-30 13:51:23.029+0700 W/STARTER ( 1141): clock-mgr.c: _pre_lcd_off(1089) > [_pre_lcd_off:1089] Will LCD OFF as wake_up_setting[1]
04-30 13:51:23.029+0700 E/STARTER ( 1141): scontext_util.c: sconstext_util_check_lock_type(47) > [sconstext_util_check_lock_type:47] current lock state :[0],testmode[0]
04-30 13:51:23.029+0700 E/STARTER ( 1141): scontext_util.c: scontext_util_handle_lock_state(72) > [scontext_util_handle_lock_state:72] wear state[0],bPossible [0],usage [0]
04-30 13:51:23.029+0700 W/STARTER ( 1141): clock-mgr.c: _check_reserved_popup_status(211) > [_check_reserved_popup_status:211] Current reserved apps status : 0
04-30 13:51:23.029+0700 W/STARTER ( 1141): clock-mgr.c: _check_reserved_apps_status(247) > [_check_reserved_apps_status:247] Current reserved apps status : 0
04-30 13:51:23.069+0700 W/WAKEUP-SERVICE( 1633): WakeupService.cpp: OnReceiveDisplayChanged(979) > [0;32mINFO: LCDOff receive data : -1226155252[0;m
04-30 13:51:23.069+0700 W/WAKEUP-SERVICE( 1633): WakeupService.cpp: OnReceiveDisplayChanged(980) > [0;32mINFO: WakeupServiceStop[0;m
04-30 13:51:23.069+0700 W/WAKEUP-SERVICE( 1633): WakeupService.cpp: WakeupServiceStop(399) > [0;32mINFO: SEAMLESS WAKEUP STOP REQUEST[0;m
04-30 13:51:23.069+0700 E/WAKEUP-SERVICE( 1633): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
04-30 13:51:23.069+0700 E/WAKEUP-SERVICE( 1633): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
04-30 13:51:23.069+0700 I/TIZEN_N_SOUND_MANAGER( 1633): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Disable start
04-30 13:51:23.129+0700 I/CAPI_NETWORK_CONNECTION( 2378): connection.c: connection_create(453) > New handle created[0xb7bf1cd8]
04-30 13:51:23.139+0700 I/TIZEN_N_SOUND_MANAGER( 1633): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Disable end. (ret: 0x0)
04-30 13:51:23.139+0700 W/TIZEN_N_SOUND_MANAGER( 1633): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
04-30 13:51:23.139+0700 W/WAKEUP-SERVICE( 1633): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 0[0;m
04-30 13:51:23.139+0700 I/HIGEAR  ( 1633): WakeupService.cpp: WakeupServiceStop(403) > [svoice:Application:WakeupServiceStop:IN]
04-30 13:51:23.199+0700 W/SHealthCommon( 1475): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:3[0;m
04-30 13:51:23.199+0700 W/W_INDICATOR( 1142): windicator_util.c: _pm_state_changed_cb(917) > [_pm_state_changed_cb:917] LCD off
04-30 13:51:23.199+0700 W/W_INDICATOR( 1142): windicator_connection.c: windicator_connection_pause(2268) > [windicator_connection_pause:2268] 
04-30 13:51:23.199+0700 W/W_INDICATOR( 1142): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
04-30 13:51:23.209+0700 W/STARTER ( 1141): clock-mgr.c: _on_lcd_signal_receive_cb(1297) > [_on_lcd_signal_receive_cb:1297] _on_lcd_signal_receive_cb, 1297, Post LCD off by [timeout]
04-30 13:51:23.209+0700 W/STARTER ( 1141): clock-mgr.c: _post_lcd_off(1190) > [_post_lcd_off:1190] LCD OFF as reserved app[(null)] alpm mode[0]
04-30 13:51:23.209+0700 W/STARTER ( 1141): clock-mgr.c: _post_lcd_off(1197) > [_post_lcd_off:1197] Current reserved apps status : 0
04-30 13:51:23.209+0700 W/STARTER ( 1141): clock-mgr.c: _post_lcd_off(1215) > [_post_lcd_off:1215] raise homescreen after 20 sec. home_visible[0]
04-30 13:51:23.209+0700 E/ALARM_MANAGER( 1141): alarm-lib.c: alarmmgr_add_alarm_withcb(1178) > trigger_at_time(20), start(30-4-2018, 13:51:43), repeat(1), interval(20), type(-1073741822)
04-30 13:51:23.209+0700 W/W_INDICATOR( 1142): windicator_dbus.c: _windicator_dbus_lcd_off_completed_cb(620) > [_windicator_dbus_lcd_off_completed_cb:620] LCD Off completed signal is received
04-30 13:51:23.209+0700 W/W_INDICATOR( 1142): windicator_dbus.c: _windicator_dbus_lcd_off_completed_cb(625) > [_windicator_dbus_lcd_off_completed_cb:625] Moment bar status -> idle. (Hide Moment bar)
04-30 13:51:23.209+0700 W/W_INDICATOR( 1142): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
04-30 13:51:23.209+0700 I/APP_CORE( 2378): appcore-efl.c: __do_app(453) > [APP 2378] Event: PAUSE State: RUNNING
04-30 13:51:23.209+0700 I/CAPI_APPFW_APPLICATION( 2378): app_main.c: _ui_app_appcore_pause(611) > app_appcore_pause
04-30 13:51:23.219+0700 W/APP_CORE( 2378): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3200003] -> redirected win[600b83] for com.toyota.realtimefeedback[2378]
04-30 13:51:23.229+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __is_cached_cookie(230) > Find cached cookie for [1141].
04-30 13:51:23.279+0700 W/SHealthCommon( 1725): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:3[0;m
04-30 13:51:23.279+0700 W/SHealthServiceCommon( 1725): SHealthServiceController.cpp: OnSystemUtilLcdStateChanged(645) > [1;35m ###[0;m
04-30 13:51:23.309+0700 E/ALARM_MANAGER(  927): alarm-manager-schedule.c: _alarm_next_duetime(509) > alarm_id: 1762527879, next duetime: 1525071103
04-30 13:51:23.309+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __alarm_add_to_list(496) > [alarm-server]: After add alarm_id(1762527879)
04-30 13:51:23.309+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __alarm_create(1061) > [alarm-server]:alarm_context.c_due_time(1525071303), due_time(1525071103)
04-30 13:51:23.319+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
04-30 13:51:23.319+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
04-30 13:51:23.319+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 30-4-2018, 06:51:43 (UTC).
04-30 13:51:23.319+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
04-30 13:51:23.319+0700 E/ALARM_MANAGER(  927): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
04-30 13:51:23.329+0700 E/EFL     ( 2378): elementary<2378> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7b2f098 : elm_genlist] mx(0), my(14), minx(0), miny(0), px(0), py(0)
04-30 13:51:23.329+0700 E/EFL     ( 2378): elementary<2378> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7b2f098 : elm_genlist] cw(360), ch(374), pw(360), ph(360)
04-30 13:51:23.359+0700 W/Adreno-EGL( 2378): <qeglDrvAPI_eglMakeCurrent:3102>: EGL_BAD_ACCESS
04-30 13:51:23.359+0700 E/EFL     ( 2378): evas-gl_x11<2378> evas_x_main.c:646 eng_window_use() eglMakeCurrent() failed!
04-30 13:51:23.379+0700 I/CAPI_NETWORK_CONNECTION( 2378): connection.c: connection_destroy(471) > Destroy handle: 0xb7bf1cd8
04-30 13:51:23.549+0700 W/CRASH_MANAGER( 2317): worker.c: worker_job(1205) > 1102378726561152507108
