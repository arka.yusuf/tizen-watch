S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2841
Date: 2018-04-23 14:22:18+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2841, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0x00000000
r2   = 0x00000001, r3   = 0xb6f60be9
r4   = 0x00000000, r5   = 0x00000000
r6   = 0xb6f60be9, r7   = 0xbe82ffd8
r8   = 0xb6f60be9, r9   = 0xb6f60be9
r10  = 0xb6f60be9, fp   = 0xb6f60be9
ip   = 0xb6f72318, sp   = 0xbe82ff58
lr   = 0xb6a4be8c, pc   = 0xb6a4c0b4
cpsr = 0x60000010

Memory Information
MemTotal:   405512 KB
MemFree:      6856 KB
Buffers:      8724 KB
Cached:     109696 KB
VmPeak:     116472 KB
VmSize:     109724 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       27548 KB
VmRSS:       27548 KB
VmData:      37356 KB
VmStk:         136 KB
VmExe:          36 KB
VmLib:       35904 KB
VmPTE:          78 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2841 TID = 2841
2841 2846 2854 2861 

Maps Information
b04b5000 b04b9000 r-xp /usr/lib/libogg.so.0.7.1
b04c1000 b04e3000 r-xp /usr/lib/libvorbis.so.0.4.3
b04eb000 b0532000 r-xp /usr/lib/libsndfile.so.1.0.26
b053e000 b0587000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b0590000 b0595000 r-xp /usr/lib/libjson.so.0.0.1
b1e36000 b1f3c000 r-xp /usr/lib/libicuuc.so.57.1
b1f52000 b20da000 r-xp /usr/lib/libicui18n.so.57.1
b20ea000 b20f7000 r-xp /usr/lib/libail.so.0.1.0
b2100000 b2103000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b210b000 b2143000 r-xp /usr/lib/libpulse.so.0.16.2
b2144000 b2147000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b214f000 b21b0000 r-xp /usr/lib/libasound.so.2.0.0
b21ba000 b21d3000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b21dc000 b21e0000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b21e8000 b21f3000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b2200000 b2204000 r-xp /usr/lib/libmmfsession.so.0.0.0
b220d000 b2225000 r-xp /usr/lib/libmmfsound.so.0.1.0
b2236000 b223d000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b2245000 b2250000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b2258000 b225a000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b2262000 b2263000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b226b000 b2273000 r-xp /usr/lib/libfeedback.so.0.1.4
b228c000 b228d000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b22df000 b22e0000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b2308000 b238f000 rw-s anon_inode:dmabuf
b23b9000 b2440000 rw-s anon_inode:dmabuf
b24f0000 b2577000 rw-s anon_inode:dmabuf
b2700000 b2703000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b2944000 b29cb000 rw-s anon_inode:dmabuf
b29cc000 b31cb000 rw-p [stack:2861]
b3372000 b3373000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b347b000 b3c7a000 rw-p [stack:2854]
b3c7a000 b3c7c000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b3c84000 b3c9b000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b3eac000 b46ab000 rw-p [stack:2846]
b46c2000 b4fee000 r-xp /usr/lib/libsc-a3xx.so
b5252000 b5254000 r-xp /usr/lib/libdri2.so.0.0.0
b525c000 b5264000 r-xp /usr/lib/libdrm.so.2.4.0
b526c000 b5270000 r-xp /usr/lib/libxcb-xfixes.so.0.0.0
b5278000 b527b000 r-xp /usr/lib/libxcb-dri2.so.0.0.0
b5283000 b5284000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b528c000 b5297000 r-xp /usr/lib/libtbm.so.1.0.0
b529f000 b52a2000 r-xp /usr/lib/libnative-buffer.so.0.1.0
b52aa000 b52ac000 r-xp /usr/lib/libgenlock.so
b52b4000 b52b9000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b52c1000 b53fc000 r-xp /usr/lib/egl/libGLESv2.so
b5438000 b543a000 r-xp /usr/lib/libadreno_utils.so
b5444000 b546b000 r-xp /usr/lib/libgsl.so
b547a000 b5481000 r-xp /usr/lib/egl/eglsubX11.so
b548b000 b54ad000 r-xp /usr/lib/egl/libEGL.so
b54b6000 b552b000 r-xp /usr/lib/evas/modules/engines/gl_x11/linux-gnueabi-armv7l-1.7.99/module.so
b573b000 b5745000 r-xp /lib/libnss_files-2.13.so
b574e000 b5751000 r-xp /lib/libattr.so.1.1.0
b5759000 b5760000 r-xp /lib/libcrypt-2.13.so
b5790000 b5793000 r-xp /lib/libcap.so.2.21
b579b000 b579d000 r-xp /usr/lib/libiri.so
b57a5000 b57c2000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b57cb000 b57cf000 r-xp /usr/lib/libsmack.so.1.0.0
b57d8000 b5807000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b580f000 b58a3000 r-xp /usr/lib/libstdc++.so.6.0.16
b58b6000 b5985000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b599b000 b59bf000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b59c8000 b5a92000 r-xp /usr/lib/libCOREGL.so.4.0
b5aa9000 b5aab000 r-xp /usr/lib/libXau.so.6.0.0
b5ab4000 b5ac4000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5acc000 b5acf000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5ad7000 b5aef000 r-xp /usr/lib/liblzma.so.5.0.3
b5af8000 b5afa000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b5b02000 b5b05000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b5b0d000 b5b11000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b5b1a000 b5b1f000 r-xp /usr/lib/libecore_fb.so.1.7.99
b5b29000 b5b4c000 r-xp /usr/lib/libjpeg.so.8.0.2
b5b64000 b5b7a000 r-xp /lib/libexpat.so.1.6.0
b5b84000 b5b97000 r-xp /usr/lib/libxcb.so.1.1.0
b5ba0000 b5ba6000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5bae000 b5baf000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5bb9000 b5bd1000 r-xp /usr/lib/libpng12.so.0.50.0
b5bd9000 b5bdc000 r-xp /usr/lib/libEGL.so.1.4
b5be4000 b5bf2000 r-xp /usr/lib/libGLESv2.so.2.0
b5bfb000 b5bfc000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b5c04000 b5c1b000 r-xp /usr/lib/liblua-5.1.so
b5c25000 b5c2c000 r-xp /usr/lib/libembryo.so.1.7.99
b5c34000 b5c3e000 r-xp /usr/lib/libXext.so.6.4.0
b5c47000 b5c4b000 r-xp /usr/lib/libXtst.so.6.1.0
b5c53000 b5c59000 r-xp /usr/lib/libXrender.so.1.3.0
b5c61000 b5c67000 r-xp /usr/lib/libXrandr.so.2.2.0
b5c6f000 b5c70000 r-xp /usr/lib/libXinerama.so.1.0.0
b5c7a000 b5c7d000 r-xp /usr/lib/libXfixes.so.3.1.0
b5c85000 b5c87000 r-xp /usr/lib/libXgesture.so.7.0.0
b5c8f000 b5c91000 r-xp /usr/lib/libXcomposite.so.1.0.0
b5c99000 b5c9b000 r-xp /usr/lib/libXdamage.so.1.1.0
b5ca3000 b5caa000 r-xp /usr/lib/libXcursor.so.1.0.2
b5cb3000 b5cc3000 r-xp /lib/libresolv-2.13.so
b5cc7000 b5cc9000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5cd1000 b5cd6000 r-xp /usr/lib/libffi.so.5.0.10
b5cde000 b5cdf000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5ce7000 b5d30000 r-xp /usr/lib/libmdm.so.1.2.70
b5d3a000 b5d40000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5d48000 b5d4e000 r-xp /usr/lib/libecore_imf.so.1.7.99
b5d56000 b5d70000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5d78000 b5d96000 r-xp /usr/lib/libsystemd.so.0.4.0
b5da1000 b5da2000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b5daa000 b5daf000 r-xp /usr/lib/libxdgmime.so.1.1.0
b5db7000 b5dce000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b5dd6000 b5ddc000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b5de5000 b5dee000 r-xp /usr/lib/libcom-core.so.0.0.1
b5df8000 b5dfa000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e03000 b5e59000 r-xp /usr/lib/libpixman-1.so.0.28.2
b5e66000 b5ebc000 r-xp /usr/lib/libfreetype.so.6.11.3
b5ec8000 b5f0d000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b5f16000 b5f29000 r-xp /usr/lib/libfribidi.so.0.3.1
b5f32000 b5f4c000 r-xp /usr/lib/libecore_con.so.1.7.99
b5f55000 b5f7f000 r-xp /usr/lib/libdbus-1.so.3.8.12
b5f88000 b5f91000 r-xp /usr/lib/libedbus.so.1.7.99
b5f99000 b5faa000 r-xp /usr/lib/libecore_input.so.1.7.99
b5fb2000 b5fb7000 r-xp /usr/lib/libecore_file.so.1.7.99
b5fc0000 b5fe2000 r-xp /usr/lib/libecore_evas.so.1.7.99
b5feb000 b6004000 r-xp /usr/lib/libeet.so.1.7.99
b6015000 b603d000 r-xp /usr/lib/libfontconfig.so.1.8.0
b603e000 b6047000 r-xp /usr/lib/libXi.so.6.1.0
b604f000 b6130000 r-xp /usr/lib/libX11.so.6.3.0
b613c000 b61f4000 r-xp /usr/lib/libcairo.so.2.11200.14
b61ff000 b625d000 r-xp /usr/lib/libedje.so.1.7.99
b6267000 b62b7000 r-xp /usr/lib/libecore_x.so.1.7.99
b62b9000 b6322000 r-xp /lib/libm-2.13.so
b632b000 b6331000 r-xp /lib/librt-2.13.so
b633a000 b6350000 r-xp /lib/libz.so.1.2.5
b6359000 b64eb000 r-xp /usr/lib/libcrypto.so.1.0.0
b650c000 b6553000 r-xp /usr/lib/libssl.so.1.0.0
b655f000 b658d000 r-xp /usr/lib/libidn.so.11.5.44
b6595000 b659e000 r-xp /usr/lib/libcares.so.2.1.0
b65a7000 b6673000 r-xp /usr/lib/libxml2.so.2.7.8
b6681000 b6683000 r-xp /usr/lib/libiniparser.so.0
b668c000 b66c0000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b66c9000 b679c000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b67a7000 b67c0000 r-xp /usr/lib/libnetwork.so.0.0.0
b67c8000 b67d1000 r-xp /usr/lib/libvconf.so.0.2.45
b67da000 b68aa000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b68ab000 b68ec000 r-xp /usr/lib/libeina.so.1.7.99
b68f5000 b68fa000 r-xp /usr/lib/libappcore-common.so.1.1
b6902000 b6908000 r-xp /usr/lib/libappcore-efl.so.1.1
b6910000 b6913000 r-xp /usr/lib/libbundle.so.0.1.22
b691b000 b6921000 r-xp /usr/lib/libappsvc.so.0.1.0
b6929000 b693d000 r-xp /lib/libpthread-2.13.so
b6948000 b696b000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6973000 b6980000 r-xp /usr/lib/libaul.so.0.1.0
b698a000 b698c000 r-xp /lib/libdl-2.13.so
b6995000 b69a0000 r-xp /lib/libunwind.so.8.0.1
b69cd000 b69d5000 r-xp /lib/libgcc_s-4.6.so.1
b69d6000 b6afa000 r-xp /lib/libc-2.13.so
b6b08000 b6b7d000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6b87000 b6b93000 r-xp /usr/lib/libnotification.so.0.1.0
b6b9c000 b6bab000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b6bb4000 b6c82000 r-xp /usr/lib/libevas.so.1.7.99
b6ca8000 b6de4000 r-xp /usr/lib/libelementary.so.1.7.99
b6dfb000 b6e1c000 r-xp /usr/lib/libefl-extension.so.0.1.0
b6e24000 b6e3b000 r-xp /usr/lib/libecore.so.1.7.99
b6e52000 b6e54000 r-xp /usr/lib/libdlog.so.0.0.0
b6e5c000 b6ea0000 r-xp /usr/lib/libcurl.so.4.3.0
b6ea9000 b6eae000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b6eb6000 b6ebb000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b6ec3000 b6ed3000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b6edb000 b6ee3000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b6eeb000 b6eef000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b6ef7000 b6efb000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b6f04000 b6f06000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b6f11000 b6f1c000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b6f26000 b6f2a000 r-xp /usr/lib/libsys-assert.so
b6f33000 b6f50000 r-xp /lib/ld-2.13.so
b6f59000 b6f62000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b834c000 b8893000 rw-p [heap]
be810000 be831000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2841)
Call Stack Count: 1
 0: strlen + 0x4 (0xb6a4c0b4) [/lib/libc.so.6] + 0x760b4
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
eceived
04-23 14:22:15.959+0700 I/MESSAGE_PORT(  962): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-23 14:22:15.959+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-23 14:22:15.959+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-23 14:22:15.959+0700 I/MESSAGE_PORT(  962): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.windicator:org.tizen.idled.ReservedApp]
04-23 14:22:15.959+0700 I/MESSAGE_PORT(  962): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-23 14:22:15.959+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-23 14:22:15.959+0700 I/MESSAGE_PORT(  962): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-23 14:22:15.959+0700 W/STARTER ( 1179): clock-mgr.c: _on_lcd_signal_receive_cb(1271) > [_on_lcd_signal_receive_cb:1271] _on_lcd_signal_receive_cb, 1271, Post LCD on by [powerkey]
04-23 14:22:15.959+0700 W/STARTER ( 1179): clock-mgr.c: _post_lcd_on(1059) > [_post_lcd_on:1059] LCD ON as reserved app[(null)] alpm mode[0]
04-23 14:22:15.959+0700 W/W_INDICATOR( 1180): windicator_connection.c: windicator_connection_resume(2158) > [windicator_connection_resume:2158] 
04-23 14:22:15.959+0700 W/W_INDICATOR( 1180): windicator_connection.c: windicator_connection_resume(2223) > [windicator_connection_resume:2223] primary rssi level : (0), s_info.rssi_level : (8) / primary svc type : (1)
04-23 14:22:15.959+0700 W/W_INDICATOR( 1180): windicator_connection.c: _rssi_changed_cb(1924) > [_rssi_changed_cb:1924] roaming status : 0
04-23 14:22:15.959+0700 W/W_INDICATOR( 1180): windicator_connection.c: _rssi_changed_cb(1933) > [_rssi_changed_cb:1933] svc type : 1
04-23 14:22:15.959+0700 W/W_INDICATOR( 1180): windicator_connection.c: _rssi_changed_cb(1968) > [_rssi_changed_cb:1968] Rssi level has already been 0!, Don't need to show rssi down animation. Just Show No Signal icon
04-23 14:22:15.959+0700 W/W_INDICATOR( 1180): windicator_connection.c: _rssi_icon_set(1124) > [_rssi_icon_set:1124] RSSI level : 8/5, (0xb8fb1b50)
04-23 14:22:15.959+0700 E/W_INDICATOR( 1180): windicator_connection.c: _rssi_icon_set(1147) > [_rssi_icon_set:1147] Set RSSI SHOW sw.icon_0 (rssi_level : 8) (rssi_hide : 0)(b8fb1b50)
04-23 14:22:15.959+0700 W/W_INDICATOR( 1180): windicator_connection.c: _rssi_icon_set(1215) > [_rssi_icon_set:1215] NETWORK_ATT or NETWORK_TMB : there is no roaming icon
04-23 14:22:15.959+0700 W/W_INDICATOR( 1180): windicator_connection.c: _rssi_icon_set(1217) > [_rssi_icon_set:1217] rssi name : set_rssi_verizon_No_signal (0xb8fb1b50)
04-23 14:22:15.969+0700 W/W_INDICATOR( 1180): windicator_connection.c: _tapi_changed_cb(2115) > [_tapi_changed_cb:2115] modem_power : 0
04-23 14:22:15.969+0700 W/W_INDICATOR( 1180): windicator_connection.c: _connection_type_changed_cb(1324) > [_connection_type_changed_cb:1324] wifi state : 2
04-23 14:22:15.969+0700 W/W_INDICATOR( 1180): windicator_connection.c: _connection_type_changed_cb(1327) > [_connection_type_changed_cb:1327] Show wifi icon!
04-23 14:22:15.969+0700 W/W_INDICATOR( 1180): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
04-23 14:22:15.969+0700 W/W_INDICATOR( 1180): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
04-23 14:22:15.969+0700 W/W_INDICATOR( 1180): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 2
04-23 14:22:15.969+0700 W/W_INDICATOR( 1180): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 14 / signal : type_wifi_connected_02
04-23 14:22:15.969+0700 E/W_INDICATOR( 1180): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 14) / (hide : 0)
04-23 14:22:15.969+0700 W/W_INDICATOR( 1180): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-23 14:22:15.969+0700 E/W_INDICATOR( 1180): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-23 14:22:15.969+0700 W/W_INDICATOR( 1180): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
04-23 14:22:15.969+0700 W/W_INDICATOR( 1180): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-23 14:22:15.969+0700 E/W_INDICATOR( 1180): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-23 14:22:15.979+0700 W/W_HOME  ( 1254): gesture.c: _manual_render_enable(138) > 0
04-23 14:22:15.989+0700 E/WAKEUP-SERVICE( 1685): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
04-23 14:22:15.989+0700 I/TIZEN_N_SOUND_MANAGER( 1685): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Enable start
04-23 14:22:15.999+0700 I/TIZEN_N_SOUND_MANAGER( 1685): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Enable end. (ret: 0x0)
04-23 14:22:15.999+0700 W/TIZEN_N_SOUND_MANAGER( 1685): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
04-23 14:22:15.999+0700 W/WAKEUP-SERVICE( 1685): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 1[0;m
04-23 14:22:15.999+0700 I/HIGEAR  ( 1685): WakeupService.cpp: WakeupServiceStart(393) > [svoice:Application:WakeupServiceStart:IN]
04-23 14:22:16.059+0700 W/W_INDICATOR( 1180): windicator_dbus.c: _msg_reserved_app_cb(341) > [_msg_reserved_app_cb:341] Moment view is already shown or call is enabled. moment view [0]
04-23 14:22:16.289+0700 I/APP_CORE( 1254): appcore-efl.c: __do_app(453) > [APP 1254] Event: MEM_FLUSH State: PAUSED
04-23 14:22:16.569+0700 E/EFL     ( 2841): ecore_x<2841> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=718787 button=1
04-23 14:22:16.579+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.579+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.629+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.629+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.639+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.639+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.649+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.649+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.659+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.659+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.669+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.669+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.679+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.679+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.689+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.689+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.699+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.699+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.719+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.719+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.719+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4292 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] add hold animator
04-23 14:22:16.719+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.719+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.719+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(26)
04-23 14:22:16.719+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.749+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.749+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.749+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.749+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.749+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.749+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.749+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.749+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.749+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(35)
04-23 14:22:16.749+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.769+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.769+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.769+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.769+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.769+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(39)
04-23 14:22:16.769+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.779+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.779+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.789+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.789+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.789+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.789+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.789+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(44)
04-23 14:22:16.789+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.799+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.799+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.799+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.799+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.809+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.809+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.809+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(52)
04-23 14:22:16.809+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.819+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.819+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.819+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.819+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.819+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(56)
04-23 14:22:16.819+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.839+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.839+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.839+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.839+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.839+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(61)
04-23 14:22:16.839+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.859+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.859+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.859+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.859+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.859+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.859+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.859+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.859+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.859+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(74)
04-23 14:22:16.859+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.879+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.879+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.879+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.879+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.879+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(79)
04-23 14:22:16.879+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.899+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.899+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.899+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.899+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.899+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.899+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.899+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(85)
04-23 14:22:16.899+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.909+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.909+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.909+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.909+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.909+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(86)
04-23 14:22:16.909+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.919+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.919+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.919+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.919+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.929+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.929+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.929+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(88)
04-23 14:22:16.929+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.939+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.939+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.939+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.939+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.939+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(91)
04-23 14:22:16.949+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.949+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.949+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.959+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.959+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.959+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(92)
04-23 14:22:16.959+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:16.969+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.969+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.969+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.969+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.969+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(92)
04-23 14:22:16.989+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:16.989+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:16.999+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:16.999+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:16.999+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(93)
04-23 14:22:16.999+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.019+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:17.019+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:17.039+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:17.039+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:17.039+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(94)
04-23 14:22:17.039+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.049+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:17.049+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:17.049+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:17.049+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:17.049+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(95)
04-23 14:22:17.049+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.069+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:17.069+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:17.069+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:17.069+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:17.069+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(96)
04-23 14:22:17.069+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.079+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:17.079+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:17.089+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:17.089+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:17.089+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:17.089+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:17.089+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(96)
04-23 14:22:17.099+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:17.099+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:17.099+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:17.099+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:17.099+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(98)
04-23 14:22:17.099+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.119+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:17.119+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:17.119+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] direction_x(0), direction_y(1)
04-23 14:22:17.119+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] drag_child_locked_y(0)
04-23 14:22:17.119+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb8615b00 : elm_genlist] move content x(0), y(98)
04-23 14:22:17.119+0700 E/EFL     ( 2841): ecore_x<2841> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=719335 button=1
04-23 14:22:17.119+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2629 _elm_scroll_scroll_to_y() [0xb8615b00 : elm_genlist] t_in(0.320000), pos_y(129)
04-23 14:22:17.139+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.090473)
04-23 14:22:17.139+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(100)
04-23 14:22:17.139+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.149+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.183276)
04-23 14:22:17.149+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(103)
04-23 14:22:17.149+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.169+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.274410)
04-23 14:22:17.169+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(106)
04-23 14:22:17.169+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.189+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.360417)
04-23 14:22:17.189+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(109)
04-23 14:22:17.189+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.199+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.441076)
04-23 14:22:17.199+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(111)
04-23 14:22:17.199+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.219+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.516177)
04-23 14:22:17.219+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(114)
04-23 14:22:17.219+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.239+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.586343)
04-23 14:22:17.239+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(116)
04-23 14:22:17.239+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.249+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.650224)
04-23 14:22:17.249+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(118)
04-23 14:22:17.249+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.269+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.709128)
04-23 14:22:17.269+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(119)
04-23 14:22:17.269+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.289+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.762550)
04-23 14:22:17.289+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(121)
04-23 14:22:17.289+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.299+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.810631)
04-23 14:22:17.299+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(123)
04-23 14:22:17.299+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.319+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.853238)
04-23 14:22:17.319+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(124)
04-23 14:22:17.319+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.339+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.890408)
04-23 14:22:17.339+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(125)
04-23 14:22:17.339+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.349+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.922197)
04-23 14:22:17.349+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(126)
04-23 14:22:17.349+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.369+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.948549)
04-23 14:22:17.369+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(127)
04-23 14:22:17.369+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.389+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.969461)
04-23 14:22:17.389+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(128)
04-23 14:22:17.389+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.399+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.984950)
04-23 14:22:17.399+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(128)
04-23 14:22:17.419+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.995025)
04-23 14:22:17.419+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(128)
04-23 14:22:17.439+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.999671)
04-23 14:22:17.439+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(128)
04-23 14:22:17.449+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] time(0.998871)
04-23 14:22:17.449+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2604 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] animation stop!!
04-23 14:22:17.449+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:2607 _elm_scroll_scroll_to_y_animator() [0xb8615b00 : elm_genlist] ECORE_CALLBACK_CANCEL : px(0), py(129)
04-23 14:22:17.449+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.669+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.689+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.709+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.719+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.739+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.749+0700 W/SHealthCommon( 1750): CpuLock.cpp: CheckAndReset(168) > [1;40;33mREQUEST POWER LOCK CPU [5000][0;m
04-23 14:22:17.759+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.769+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.789+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.789+0700 W/SHealthServiceCommon( 1750): StepLevelSensorProxy.cpp: StepLevelMonitorUpdated(380) > [1;40;33mlast step level is [2][0;m
04-23 14:22:17.799+0700 W/SHealthServiceCommon( 1750): StepLevelSensorProxy.cpp: StepLevelMonitorUpdated(393) > [1;40;33mOnNotWearingStart[0;m
04-23 14:22:17.799+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.809+0700 W/SHealthCommon( 1750): CpuLock.cpp: CheckAndReset(168) > [1;40;33mREQUEST POWER LOCK CPU [4939][0;m
04-23 14:22:17.819+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.829+0700 E/EFL     ( 2841): ecore_x<2841> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=720038 button=1
04-23 14:22:17.829+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:17.839+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:17.839+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:17.839+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.849+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:17.849+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:17.849+0700 E/EFL     ( 2841): <2841> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-23 14:22:17.889+0700 W/SHealthCommon( 1750): CpuLock.cpp: CheckAndReset(168) > [1;40;33mREQUEST POWER LOCK CPU [4859][0;m
04-23 14:22:17.939+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] mouse move
04-23 14:22:17.939+0700 E/EFL     ( 2841): elementary<2841> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8615b00 : elm_genlist] hold(0), freeze(0)
04-23 14:22:17.939+0700 W/SHealthCommon( 1750): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
04-23 14:22:17.949+0700 E/EFL     ( 2841): ecore_x<2841> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=720158 button=1
04-23 14:22:18.019+0700 W/SHealthCommon( 1750): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
04-23 14:22:18.149+0700 W/SHealthCommon( 1750): TimelineSessionStorage.cpp: OnTriggered(1290) > [1;40;33mlocalStartTime: 1524441600000.000000[0;m
04-23 14:22:18.249+0700 E/GL SELECTED( 2841): ID GL : TRM2
04-23 14:22:18.269+0700 W/SHealthCommon( 1750): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: timeline_session_updated, pendingClientInfoList.size(): 0[0;m
04-23 14:22:18.279+0700 W/SHealthCommon( 1750): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: timeline_summary_updated, pendingClientInfoList.size(): 0[0;m
04-23 14:22:18.279+0700 W/SHealthServiceCommon( 1750): EnergyExpenditureFeatureController.cpp: OnTotalEnergyExpenditureChanged(119) > [1;40;33mstart 1524416400000.000000, end 1524468138288.000000, calories 967.719766[0;m
04-23 14:22:18.279+0700 W/SHealthCommon( 1750): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: energy_expenditure_updated, pendingClientInfoList.size(): 1[0;m
04-23 14:22:18.539+0700 E/EFL     ( 2841): ecore_x<2841> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=720754 button=1
04-23 14:22:18.689+0700 E/EFL     ( 2841): ecore_x<2841> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=720907 button=1
04-23 14:22:18.699+0700 I/efl-extension( 2841): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-23 14:22:18.699+0700 I/efl-extension( 2841): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-23 14:22:18.699+0700 I/efl-extension( 2841): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb87188e8, elm_image, _activated_obj : 0xb862aa50, activated : 1
04-23 14:22:18.699+0700 I/efl-extension( 2841): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-23 14:22:19.179+0700 W/CRASH_MANAGER( 2862): worker.c: worker_job(1205) > 1102841726561152446813
