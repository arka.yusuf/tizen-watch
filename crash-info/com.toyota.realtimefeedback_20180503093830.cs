S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2035
Date: 2018-05-03 09:38:30+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2035, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb58f5688
r2   = 0xb58f5c98, r3   = 0x00000000
r4   = 0xb7138da8, r5   = 0xb1e00f40
r6   = 0x00000013, r7   = 0xbe818208
r8   = 0x00002eb5, r9   = 0xb7138da8
r10  = 0x00000000, fp   = 0x00000000
ip   = 0xb58f58dc, sp   = 0xbe8181e8
lr   = 0xb58e350f, pc   = 0xb58e3544
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:      4640 KB
Buffers:      9080 KB
Cached:     103580 KB
VmPeak:     470948 KB
VmSize:     468784 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       29028 KB
VmRSS:       29028 KB
VmData:     408552 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:         424 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2035 TID = 2035
2035 2063 2065 

Maps Information
b0675000 b06fc000 rw-s anon_inode:dmabuf
b06fc000 b0783000 rw-s anon_inode:dmabuf
b1f76000 b1f7a000 r-xp /usr/lib/libogg.so.0.7.1
b1f82000 b1fa4000 r-xp /usr/lib/libvorbis.so.0.4.3
b1fac000 b1ff3000 r-xp /usr/lib/libsndfile.so.1.0.26
b1fff000 b2048000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b2051000 b2056000 r-xp /usr/lib/libjson.so.0.0.1
b38f7000 b39fd000 r-xp /usr/lib/libicuuc.so.57.1
b3a13000 b3b9b000 r-xp /usr/lib/libicui18n.so.57.1
b3bab000 b3bb8000 r-xp /usr/lib/libail.so.0.1.0
b3bc1000 b3bc4000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3bcc000 b3c04000 r-xp /usr/lib/libpulse.so.0.16.2
b3c05000 b3c08000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3c10000 b3c71000 r-xp /usr/lib/libasound.so.2.0.0
b3c7b000 b3c94000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3c9d000 b3ca1000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3ca9000 b3cb4000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3cc1000 b3cc5000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3cce000 b3ce6000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3cf7000 b3cfe000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3d06000 b3d11000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3d19000 b3d1b000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3d23000 b3d24000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3d2c000 b3d34000 r-xp /usr/lib/libfeedback.so.0.1.4
b3d39000 b3d3a000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3d42000 b3d45000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3d4d000 b3d4e000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3d83000 b3e0a000 rw-s anon_inode:dmabuf
b4009000 b4090000 rw-s anon_inode:dmabuf
b4091000 b4890000 rw-p [stack:2065]
b4a34000 b4a35000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b3c000 b533b000 rw-p [stack:2063]
b533b000 b533d000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b5345000 b535c000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b5369000 b536b000 r-xp /usr/lib/libdri2.so.0.0.0
b5373000 b537e000 r-xp /usr/lib/libtbm.so.1.0.0
b5386000 b538e000 r-xp /usr/lib/libdrm.so.2.4.0
b5396000 b5398000 r-xp /usr/lib/libgenlock.so
b53a0000 b53a5000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b53ad000 b53b8000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b55c1000 b568b000 r-xp /usr/lib/libCOREGL.so.4.0
b569c000 b56ac000 r-xp /usr/lib/libmdm-common.so.1.1.25
b56b4000 b56ba000 r-xp /usr/lib/libxcb-render.so.0.0.0
b56c2000 b56c3000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b56cc000 b56cf000 r-xp /usr/lib/libEGL.so.1.4
b56d7000 b56e5000 r-xp /usr/lib/libGLESv2.so.2.0
b56ee000 b5737000 r-xp /usr/lib/libmdm.so.1.2.70
b5740000 b5746000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b574e000 b5757000 r-xp /usr/lib/libcom-core.so.0.0.1
b5760000 b5818000 r-xp /usr/lib/libcairo.so.2.11200.14
b5823000 b583c000 r-xp /usr/lib/libnetwork.so.0.0.0
b5844000 b5850000 r-xp /usr/lib/libnotification.so.0.1.0
b5859000 b5868000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b5871000 b5892000 r-xp /usr/lib/libefl-extension.so.0.1.0
b589a000 b589f000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b58a7000 b58ac000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b58b4000 b58c4000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b58cc000 b58d4000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b58dc000 b58e6000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a8b000 b5a95000 r-xp /lib/libnss_files-2.13.so
b5a9e000 b5b6d000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5b83000 b5ba7000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5bb0000 b5bb6000 r-xp /usr/lib/libappsvc.so.0.1.0
b5bbe000 b5bc2000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5bcf000 b5bda000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5be2000 b5be4000 r-xp /usr/lib/libiniparser.so.0
b5bed000 b5bf2000 r-xp /usr/lib/libappcore-common.so.1.1
b5bfa000 b5bfc000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5c05000 b5c09000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5c16000 b5c18000 r-xp /usr/lib/libXau.so.6.0.0
b5c20000 b5c27000 r-xp /lib/libcrypt-2.13.so
b5c57000 b5c59000 r-xp /usr/lib/libiri.so
b5c62000 b5df4000 r-xp /usr/lib/libcrypto.so.1.0.0
b5e15000 b5e5c000 r-xp /usr/lib/libssl.so.1.0.0
b5e68000 b5e96000 r-xp /usr/lib/libidn.so.11.5.44
b5e9e000 b5ea7000 r-xp /usr/lib/libcares.so.2.1.0
b5eb1000 b5ec4000 r-xp /usr/lib/libxcb.so.1.1.0
b5ecd000 b5ed0000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5ed8000 b5eda000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5ee3000 b5faf000 r-xp /usr/lib/libxml2.so.2.7.8
b5fbc000 b5fbe000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5fc7000 b5fcc000 r-xp /usr/lib/libffi.so.5.0.10
b5fd4000 b5fd5000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5fdd000 b5fe0000 r-xp /lib/libattr.so.1.1.0
b5fe8000 b607c000 r-xp /usr/lib/libstdc++.so.6.0.16
b608f000 b60ac000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b60b6000 b60ce000 r-xp /usr/lib/libpng12.so.0.50.0
b60d6000 b60ec000 r-xp /lib/libexpat.so.1.6.0
b60f6000 b613a000 r-xp /usr/lib/libcurl.so.4.3.0
b6143000 b614d000 r-xp /usr/lib/libXext.so.6.4.0
b6157000 b615b000 r-xp /usr/lib/libXtst.so.6.1.0
b6163000 b6169000 r-xp /usr/lib/libXrender.so.1.3.0
b6171000 b6177000 r-xp /usr/lib/libXrandr.so.2.2.0
b617f000 b6180000 r-xp /usr/lib/libXinerama.so.1.0.0
b6189000 b6192000 r-xp /usr/lib/libXi.so.6.1.0
b619a000 b619d000 r-xp /usr/lib/libXfixes.so.3.1.0
b61a6000 b61a8000 r-xp /usr/lib/libXgesture.so.7.0.0
b61b0000 b61b2000 r-xp /usr/lib/libXcomposite.so.1.0.0
b61ba000 b61bc000 r-xp /usr/lib/libXdamage.so.1.1.0
b61c4000 b61cb000 r-xp /usr/lib/libXcursor.so.1.0.2
b61d3000 b61d6000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b61df000 b61e3000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b61ec000 b61f1000 r-xp /usr/lib/libecore_fb.so.1.7.99
b61fa000 b62db000 r-xp /usr/lib/libX11.so.6.3.0
b62e6000 b6309000 r-xp /usr/lib/libjpeg.so.8.0.2
b6321000 b6337000 r-xp /lib/libz.so.1.2.5
b6340000 b6342000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b634a000 b63bf000 r-xp /usr/lib/libsqlite3.so.0.8.6
b63c9000 b63e3000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b63eb000 b641f000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b6428000 b64fb000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6507000 b6517000 r-xp /lib/libresolv-2.13.so
b651b000 b6533000 r-xp /usr/lib/liblzma.so.5.0.3
b653b000 b653e000 r-xp /lib/libcap.so.2.21
b6546000 b6575000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b657d000 b657e000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b6587000 b658d000 r-xp /usr/lib/libecore_imf.so.1.7.99
b6595000 b65ac000 r-xp /usr/lib/liblua-5.1.so
b65b5000 b65bc000 r-xp /usr/lib/libembryo.so.1.7.99
b65c4000 b65ca000 r-xp /lib/librt-2.13.so
b65d3000 b6629000 r-xp /usr/lib/libpixman-1.so.0.28.2
b6637000 b668d000 r-xp /usr/lib/libfreetype.so.6.11.3
b6699000 b66c1000 r-xp /usr/lib/libfontconfig.so.1.8.0
b66c2000 b6707000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b6710000 b6723000 r-xp /usr/lib/libfribidi.so.0.3.1
b672b000 b6745000 r-xp /usr/lib/libecore_con.so.1.7.99
b674f000 b6758000 r-xp /usr/lib/libedbus.so.1.7.99
b6760000 b67b0000 r-xp /usr/lib/libecore_x.so.1.7.99
b67b2000 b67bb000 r-xp /usr/lib/libvconf.so.0.2.45
b67c3000 b67d4000 r-xp /usr/lib/libecore_input.so.1.7.99
b67dc000 b67e1000 r-xp /usr/lib/libecore_file.so.1.7.99
b67e9000 b680b000 r-xp /usr/lib/libecore_evas.so.1.7.99
b6814000 b6855000 r-xp /usr/lib/libeina.so.1.7.99
b685e000 b6877000 r-xp /usr/lib/libeet.so.1.7.99
b6888000 b68f1000 r-xp /lib/libm-2.13.so
b68fa000 b6900000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6909000 b690a000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b6912000 b6935000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b693d000 b6942000 r-xp /usr/lib/libxdgmime.so.1.1.0
b694a000 b6974000 r-xp /usr/lib/libdbus-1.so.3.8.12
b697d000 b6994000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b699c000 b69a7000 r-xp /lib/libunwind.so.8.0.1
b69d4000 b69f2000 r-xp /usr/lib/libsystemd.so.0.4.0
b69fc000 b6b20000 r-xp /lib/libc-2.13.so
b6b2e000 b6b36000 r-xp /lib/libgcc_s-4.6.so.1
b6b37000 b6b3b000 r-xp /usr/lib/libsmack.so.1.0.0
b6b44000 b6b4a000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b52000 b6c22000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6c23000 b6c81000 r-xp /usr/lib/libedje.so.1.7.99
b6c8b000 b6ca2000 r-xp /usr/lib/libecore.so.1.7.99
b6cb9000 b6d87000 r-xp /usr/lib/libevas.so.1.7.99
b6dad000 b6ee9000 r-xp /usr/lib/libelementary.so.1.7.99
b6f00000 b6f14000 r-xp /lib/libpthread-2.13.so
b6f1f000 b6f21000 r-xp /usr/lib/libdlog.so.0.0.0
b6f29000 b6f2c000 r-xp /usr/lib/libbundle.so.0.1.22
b6f34000 b6f36000 r-xp /lib/libdl-2.13.so
b6f3f000 b6f4c000 r-xp /usr/lib/libaul.so.0.1.0
b6f5e000 b6f64000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f6d000 b6f71000 r-xp /usr/lib/libsys-assert.so
b6f7a000 b6f97000 r-xp /lib/ld-2.13.so
b6fa0000 b6fa5000 r-xp /usr/bin/launchpad-loader
b701c000 b843c000 rw-p [heap]
be7f8000 be819000 rw-p [stack]
b701c000 b843c000 rw-p [heap]
be7f8000 be819000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2035)
Call Stack Count: 21
 0: set_targeted_view + 0x87 (0xb58e3544) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7544
 1: pop_from_stack_uib_vc + 0x38 (0xb58e3a49) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a49
 2: uib_views_destroy_callback + 0x38 (0xb58e3a05) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a05
 3: (0xb6cd0af9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_object_del + 0x94 (0xb6ce7c39) [/usr/lib/libevas.so.1] + 0x2ec39
 5: (0xb6cda37b) [/usr/lib/libevas.so.1] + 0x2137b
 6: evas_free + 0x200 (0xb6cdaab5) [/usr/lib/libevas.so.1] + 0x21ab5
 7: (0xb67f8cf1) [/usr/lib/libecore_evas.so.1] + 0xfcf1
 8: (0xb6eab953) [/usr/lib/libelementary.so.1] + 0xfe953
 9: (0xb6c963f5) [/usr/lib/libecore.so.1] + 0xb3f5
10: (0xb6c93e53) [/usr/lib/libecore.so.1] + 0x8e53
11: (0xb6c9746b) [/usr/lib/libecore.so.1] + 0xc46b
12: ecore_main_loop_iterate + 0x22 (0xb6c977db) [/usr/lib/libecore.so.1] + 0xc7db
13: elm_shutdown + 0x2c (0xb6e6844d) [/usr/lib/libelementary.so.1] + 0xbb44d
14: appcore_efl_main + 0x454 (0xb6f61c69) [/usr/lib/libappcore-efl.so.1] + 0x3c69
15: ui_app_main + 0xb0 (0xb5c06ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16:  + 0x0 (0xb58e140f) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x540f
17: main + 0x34 (0xb58e1b11) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5b11
18: (0xb6fa1a53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
19: __libc_start_main + 0x114 (0xb6a1385c) [/lib/libc.so.6] + 0x1785c
20: (0xb6fa1e0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
watch.c: __widget_pause(1113) > widget_pause
05-03 09:36:17.189+0700 W/AUL     ( 1319): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.techgraphy.DigitalTick) pid(1319) status(bg) type(watchapp)
05-03 09:36:17.189+0700 E/watchface-app( 1319): watchface.cpp: OnAppPause(1122) > 
05-03 09:36:17.199+0700 W/SHealthCommon( 1810): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:3[0;m
05-03 09:36:17.199+0700 W/SHealthServiceCommon( 1810): SHealthServiceController.cpp: OnSystemUtilLcdStateChanged(645) > [1;35m ###[0;m
05-03 09:36:17.199+0700 W/MUSIC_CONTROL_SERVICE( 1760): music-control-service.c: _music_control_service_pasre_request(565) > [33m[TID:1760]   [com.samsung.w-home]register msg port [false][0m
05-03 09:36:17.209+0700 E/ALARM_MANAGER(  949): alarm-manager-schedule.c: _alarm_next_duetime(509) > alarm_id: 2096445829, next duetime: 1525314997
05-03 09:36:17.209+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __alarm_add_to_list(496) > [alarm-server]: After add alarm_id(2096445829)
05-03 09:36:17.209+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __alarm_create(1061) > [alarm-server]:alarm_context.c_due_time(1525315177), due_time(1525314997)
05-03 09:36:17.219+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
05-03 09:36:17.219+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
05-03 09:36:17.219+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 3-5-2018, 02:36:37 (UTC).
05-03 09:36:17.219+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
05-03 09:36:17.229+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
05-03 09:36:17.569+0700 W/AUL_AMD (  946): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
05-03 09:36:17.569+0700 W/AUL_AMD (  946): amd_launch.c: __grab_timeout_handler(1453) > back key ungrab error
05-03 09:36:17.679+0700 I/APP_CORE( 1225): appcore-efl.c: __do_app(453) > [APP 1225] Event: MEM_FLUSH State: PAUSED
05-03 09:36:19.129+0700 I/GATE    (  947): <GATE-M>BATTERY_LEVEL_42</GATE-M>
05-03 09:36:19.139+0700 W/W_INDICATOR( 1150): windicator_battery.c: windicator_battery_update(98) > [windicator_battery_update:98] 
05-03 09:36:19.139+0700 I/watchface-viewer( 1319): viewer-data-provider.cpp: AddPendingChanges(2527) > added [60] to pending list
05-03 09:36:19.139+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(312) > [_battery_icon_update:312] battery level(42), length(2)
05-03 09:36:19.139+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(336) > [_battery_icon_update:336] 42%
05-03 09:36:19.139+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(351) > [_battery_icon_update:351] battery_level: 42, isCharging: 0
05-03 09:36:19.139+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(385) > [_battery_icon_update:385] battery file : change_level_45
05-03 09:36:19.139+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(464) > [_battery_icon_update:464] [ATT] Battery level : 45
05-03 09:36:19.139+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(531) > [_battery_icon_update:531] Normal charging status !!
05-03 09:36:20.879+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:36:20.879+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:36:20.879+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:36:20.909+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e54060]
05-03 09:36:20.979+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:36:20.979+0700 E/JSON PARSING( 2035): ų\µ;
05-03 09:36:20.979+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e54060
05-03 09:36:22.169+0700 I/APP_CORE( 1150): appcore-efl.c: __do_app(453) > [APP 1150] Event: MEM_FLUSH State: PAUSED
05-03 09:36:22.179+0700 I/APP_CORE( 1225): appcore-efl.c: __do_app(453) > [APP 1225] Event: MEM_FLUSH State: PAUSED
05-03 09:36:26.009+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:36:26.009+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:36:26.009+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:36:26.039+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e4d1c0]
05-03 09:36:26.059+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:36:26.059+0700 E/JSON PARSING( 2035): Ąöų\µ;
05-03 09:36:26.059+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e4d1c0
05-03 09:36:31.099+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:36:31.099+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:36:31.099+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:36:31.129+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e4e0d0]
05-03 09:36:31.149+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:36:31.149+0700 E/JSON PARSING( 2035): pēų\µ;
05-03 09:36:31.149+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e4e0d0
05-03 09:36:36.169+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:36:36.169+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:36:36.169+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:36:36.199+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e4cd80]
05-03 09:36:36.849+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:36:36.849+0700 E/JSON PARSING( 2035):  Ųų\µ;
05-03 09:36:36.849+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e4cd80
05-03 09:36:36.989+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __alarm_handler_idle(1486) > Lock the display not to enter LCD OFF
05-03 09:36:36.999+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
05-03 09:36:37.019+0700 W/AUL     (  949): app_signal.c: aul_update_freezer_status(456) > aul_update_freezer_status pid(1149) type(wakeup)
05-03 09:36:37.019+0700 E/RESOURCED( 1131): freezer-process.c: freezer_process_pid_set(150) > Cant find process info for 1149
05-03 09:36:37.019+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __alarm_expired(1447) > alarm_id[2096445829] is expired.
05-03 09:36:37.019+0700 E/ALARM_MANAGER(  949): alarm-manager-schedule.c: _alarm_next_duetime(509) > alarm_id: 2096445829, next duetime: 1525315017
05-03 09:36:37.019+0700 E/ALARM_MANAGER(  949): alarm-manager-schedule.c: __find_next_alarm_to_be_scheduled(547) > The duetime of alarm(436816151) is OVER.
05-03 09:36:37.019+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
05-03 09:36:37.019+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 3-5-2018, 02:36:57 (UTC).
05-03 09:36:37.019+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
05-03 09:36:37.019+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __alarm_handler_idle(1512) > Unlock the display from LCD OFF
05-03 09:36:37.019+0700 E/ALARM_MANAGER( 1149): alarm-lib.c: __handle_expiry_method_call(152) > [alarm-lib] : Alarm expired for [ALARM.astarter] : Alarm id [2096445829]
05-03 09:36:37.019+0700 W/STARTER ( 1149): clock-mgr.c: __starter_clock_mgr_homescreen_alarm_cb(968) > [__starter_clock_mgr_homescreen_alarm_cb:968] homescreen alarm timer expired [2096445829]
05-03 09:36:37.029+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
05-03 09:36:37.039+0700 W/AUL     ( 1149): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.samsung.w-home)
05-03 09:36:37.039+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 0
05-03 09:36:37.039+0700 W/AUL_AMD (  946): amd_launch.c: _start_app(1782) > caller pid : 1149
05-03 09:36:37.049+0700 W/AUL     (  946): app_signal.c: aul_send_app_resume_request_signal(567) > aul_send_app_resume_request_signal app(com.samsung.w-home) pid(1225) type(uiapp) bg(0)
05-03 09:36:37.049+0700 W/AUL_AMD (  946): amd_launch.c: __nofork_processing(1229) > __nofork_processing, cmd: 0, pid: 1225
05-03 09:36:37.049+0700 I/APP_CORE( 1225): appcore-efl.c: __do_app(453) > [APP 1225] Event: RESET State: PAUSED
05-03 09:36:37.049+0700 I/CAPI_APPFW_APPLICATION( 1225): app_main.c: app_appcore_reset(245) > app_appcore_reset
05-03 09:36:37.049+0700 W/CAPI_APPFW_APP_CONTROL( 1225): app_control.c: app_control_error(136) > [app_control_get_extra_data] KEY_NOT_FOUND(0xffffff82)
05-03 09:36:37.059+0700 W/AUL_AMD (  946): amd_launch.c: __reply_handler(999) > listen fd(23) , send fd(22), pid(1225), cmd(0)
05-03 09:36:37.059+0700 W/AUL     ( 1149): launch.c: app_request_to_launchpad(298) > request cmd(0) result(1225)
05-03 09:36:37.059+0700 E/W_HOME  ( 1225): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
05-03 09:36:37.059+0700 W/W_HOME  ( 1225): main.c: _app_control_progress(1606) > Service value : show_clock
05-03 09:36:37.059+0700 W/W_HOME  ( 1225): main.c: _app_control_progress(1623) > Show clock operation
05-03 09:36:37.059+0700 E/W_HOME  ( 1225): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
05-03 09:36:37.059+0700 W/W_HOME  ( 1225): gesture.c: _clock_show(242) > clock show
05-03 09:36:37.059+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __is_cached_cookie(230) > Find cached cookie for [1149].
05-03 09:36:37.059+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __alarm_remove_from_list(575) > [alarm-server]:Remove alarm id(2096445829)
05-03 09:36:37.059+0700 E/ALARM_MANAGER(  949): alarm-manager-schedule.c: __find_next_alarm_to_be_scheduled(547) > The duetime of alarm(436816151) is OVER.
05-03 09:36:37.069+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
05-03 09:36:37.069+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
05-03 09:36:37.069+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 3-5-2018, 02:39:37 (UTC).
05-03 09:36:37.069+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
05-03 09:36:37.069+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
05-03 09:36:37.069+0700 E/ALARM_MANAGER(  949): alarm-manager.c: alarm_manager_alarm_delete(2462) > alarm_id[2096445829] is removed.
05-03 09:36:37.079+0700 W/STARTER ( 1149): pkg-monitor.c: _app_mgr_status_cb(421) > [_app_mgr_status_cb:421] Resume request [1225]
05-03 09:36:37.089+0700 W/W_HOME  ( 1225): gesture.c: _clock_show(257) > home raise
05-03 09:36:37.089+0700 E/W_HOME  ( 1225): gesture.c: gesture_win_aux_set(402) > wm.policy.win.do.not.use.deiconify.approve:-1
05-03 09:36:37.089+0700 E/wnotibp ( 2057): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
05-03 09:36:37.089+0700 W/wnotibp ( 2057): wnotiboard-popup-control.c: _ctrl_home_raise_cb(164) > [0, 2, 1]
05-03 09:36:37.089+0700 W/W_HOME  ( 1225): dbus_util.c: home_dbus_home_raise_signal_send(298) > Sending HOME RAISE signal, result:0
05-03 09:36:37.089+0700 W/W_HOME  ( 1225): gesture.c: _clock_show(260) > home raise done
05-03 09:36:37.089+0700 W/W_HOME  ( 1225): gesture.c: _clock_show(267) > show clock
05-03 09:36:37.089+0700 W/W_HOME  ( 1225): index.c: index_hide(338) > hide VI:0 visibility:0 vi:(nil)
05-03 09:36:37.089+0700 W/W_HOME  ( 1225): gesture.c: _manual_render(182) > 
05-03 09:36:37.089+0700 W/W_INDICATOR( 1150): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
05-03 09:36:37.139+0700 I/APP_CORE( 1225): appcore-efl.c: __do_app(529) > Legacy lifecycle: 1
05-03 09:36:38.049+0700 W/AUL_AMD (  946): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
05-03 09:36:38.049+0700 W/AUL_AMD (  946): amd_launch.c: __grab_timeout_handler(1453) > back key ungrab error
05-03 09:36:41.859+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:36:41.859+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:36:41.859+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:36:41.889+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e49330]
05-03 09:36:41.939+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:36:41.939+0700 E/JSON PARSING( 2035): ŠČų\µ;
05-03 09:36:41.939+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e49330
05-03 09:36:46.979+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:36:46.979+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:36:46.979+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:36:47.009+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e3fbf0]
05-03 09:36:47.069+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:36:47.069+0700 E/JSON PARSING( 2035): ¹ų\µ;
05-03 09:36:47.069+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e3fbf0
05-03 09:36:50.719+0700 E/CAPI_MEDIA_CONTROLLER( 1382): media_controller_main.c: __mc_main_check_connection(34) > [0m[No-error] Timer is Called but there is working Process, connection_cnt = 3
05-03 09:36:52.089+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:36:52.089+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:36:52.089+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:36:52.119+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e45bd0]
05-03 09:36:52.139+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:36:52.139+0700 E/JSON PARSING( 2035): 0Ŗų\µ;
05-03 09:36:52.139+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e45bd0
05-03 09:36:57.179+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:36:57.179+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:36:57.179+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:36:57.209+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e852b0]
05-03 09:36:57.349+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:36:57.349+0700 E/JSON PARSING( 2035): ąų\µ;
05-03 09:36:57.349+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e852b0
05-03 09:37:02.379+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:02.379+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:02.379+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:02.419+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e7ca20]
05-03 09:37:02.449+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:02.449+0700 E/JSON PARSING( 2035): ų\µ;
05-03 09:37:02.449+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e7ca20
05-03 09:37:07.489+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:07.489+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:07.489+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:07.509+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e81e38]
05-03 09:37:07.579+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:07.579+0700 E/JSON PARSING( 2035): @|ų\µ;
05-03 09:37:07.579+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e81e38
05-03 09:37:12.609+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:12.609+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:12.609+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:12.639+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e45bd0]
05-03 09:37:12.659+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:12.659+0700 E/JSON PARSING( 2035): šlų\µ;
05-03 09:37:12.659+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e45bd0
05-03 09:37:17.689+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:17.699+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:17.699+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:17.719+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e82770]
05-03 09:37:17.839+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:17.839+0700 E/JSON PARSING( 2035):  ]ų\µ;
05-03 09:37:17.839+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e82770
05-03 09:37:22.849+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:22.859+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:22.859+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:22.879+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e4d1c0]
05-03 09:37:22.919+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:22.919+0700 E/JSON PARSING( 2035): PNų\µ;
05-03 09:37:22.919+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e4d1c0
05-03 09:37:27.939+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:27.939+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:27.939+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:27.979+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e45bd0]
05-03 09:37:28.019+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:28.019+0700 E/JSON PARSING( 2035): 
05-03 09:37:28.019+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e45bd0
05-03 09:37:33.059+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:33.059+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:33.059+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:33.089+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e7edd0]
05-03 09:37:33.149+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:33.149+0700 E/JSON PARSING( 2035): °/ų\µ;
05-03 09:37:33.149+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e7edd0
05-03 09:37:38.169+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:38.169+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:38.169+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:38.199+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e5ca10]
05-03 09:37:38.249+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:38.249+0700 E/JSON PARSING( 2035): ` ų\µ;
05-03 09:37:38.249+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e5ca10
05-03 09:37:43.289+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:43.289+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:43.289+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:43.329+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1ecc468]
05-03 09:37:43.379+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:43.379+0700 E/JSON PARSING( 2035): ų\µ;
05-03 09:37:43.379+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1ecc468
05-03 09:37:48.409+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:48.409+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:48.409+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:48.439+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e45bd0]
05-03 09:37:48.479+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:48.479+0700 E/JSON PARSING( 2035): Ąų\µ;
05-03 09:37:48.479+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e45bd0
05-03 09:37:49.419+0700 I/GATE    (  947): <GATE-M>BATTERY_LEVEL_41</GATE-M>
05-03 09:37:49.429+0700 W/W_INDICATOR( 1150): windicator_battery.c: windicator_battery_update(98) > [windicator_battery_update:98] 
05-03 09:37:49.429+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(312) > [_battery_icon_update:312] battery level(41), length(2)
05-03 09:37:49.429+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(336) > [_battery_icon_update:336] 41%
05-03 09:37:49.429+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(351) > [_battery_icon_update:351] battery_level: 41, isCharging: 0
05-03 09:37:49.429+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(385) > [_battery_icon_update:385] battery file : change_level_45
05-03 09:37:49.429+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(464) > [_battery_icon_update:464] [ATT] Battery level : 45
05-03 09:37:49.429+0700 W/W_INDICATOR( 1150): windicator_battery.c: _battery_icon_update(531) > [_battery_icon_update:531] Normal charging status !!
05-03 09:37:50.739+0700 E/CAPI_MEDIA_CONTROLLER( 1382): media_controller_main.c: __mc_main_check_connection(34) > [0m[No-error] Timer is Called but there is working Process, connection_cnt = 3
05-03 09:37:53.509+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:53.509+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:53.509+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:53.539+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e8f138]
05-03 09:37:53.589+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:53.589+0700 E/JSON PARSING( 2035): pņų\µ;
05-03 09:37:53.589+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e8f138
05-03 09:37:58.599+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:37:58.599+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:37:58.599+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:37:58.629+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e3d040]
05-03 09:37:58.769+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:37:58.769+0700 E/JSON PARSING( 2035):  ćų\µ;
05-03 09:37:58.769+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e3d040
05-03 09:38:03.799+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:38:03.799+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:38:03.799+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:38:03.829+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e88528]
05-03 09:38:03.859+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:38:03.859+0700 E/JSON PARSING( 2035): ŠÓų\µ;
05-03 09:38:03.859+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e88528
05-03 09:38:08.889+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:38:08.889+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:38:08.889+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:38:08.929+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e3fbf0]
05-03 09:38:08.979+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:38:08.979+0700 E/JSON PARSING( 2035): Äų\µ;
05-03 09:38:08.979+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e3fbf0
05-03 09:38:13.999+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:38:14.009+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:38:14.009+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:38:14.029+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1ec89d0]
05-03 09:38:14.069+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:38:14.069+0700 E/JSON PARSING( 2035): 0µų\µ;
05-03 09:38:14.069+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1ec89d0
05-03 09:38:19.099+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:38:19.099+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:38:19.099+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:38:19.119+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1ecc468]
05-03 09:38:19.319+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:38:19.319+0700 E/JSON PARSING( 2035): ą„ų\µ;
05-03 09:38:19.319+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1ecc468
05-03 09:38:24.339+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:38:24.339+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:38:24.339+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:38:24.369+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e58580]
05-03 09:38:24.509+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:38:24.519+0700 E/JSON PARSING( 2035): ų\µ;
05-03 09:38:24.519+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e58580
05-03 09:38:29.359+0700 E/PKGMGR  ( 2472): pkgmgr.c: __pkgmgr_client_uninstall(2061) > uninstall pkg start.
05-03 09:38:29.569+0700 E/PKGMGR_SERVER( 2474): pkgmgr-server.c: main(2242) > package manager server start
05-03 09:38:29.669+0700 E/PKGMGR_SERVER( 2474): pm-mdm.c: _pm_check_mdm_policy(1078) > [0;31m[_pm_check_mdm_policy(): 1078](ret != MDM_RESULT_SUCCESS) can not connect mdm server[0;m
05-03 09:38:29.679+0700 E/PKGMGR  ( 2472): pkgmgr.c: __pkgmgr_client_uninstall(2186) > uninstall pkg finish, ret=[24720002]
05-03 09:38:29.689+0700 E/PKGMGR_SERVER( 2475): pkgmgr-server.c: queue_job(1962) > UNINSTALL start, pkgid=[com.toyota.realtimefeedback]
05-03 09:38:29.689+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:38:29.689+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:38:29.689+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2035): preference.c: preference_get_string(1258) > preference_get_string(2035) : URL error
05-03 09:38:29.809+0700 E/rpm-installer( 2475): rpm-appcore-intf.c: main(120) > ------------------------------------------------
05-03 09:38:29.809+0700 E/rpm-installer( 2475): rpm-appcore-intf.c: main(121) >  [START] rpm-installer: version=[201610629.1]
05-03 09:38:29.809+0700 E/rpm-installer( 2475): rpm-appcore-intf.c: main(122) > ------------------------------------------------
05-03 09:38:29.819+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_create(453) > New handle created[0xb1e7c7c8]
05-03 09:38:29.929+0700 E/rpm-installer( 2475): rpm-cmdline.c: __ri_is_core_tpk_app(358) > This is a core tpk app.
05-03 09:38:29.949+0700 E/JSON PARSING( 2035): No data could be display
05-03 09:38:29.949+0700 E/JSON PARSING( 2035): @ų\µ;
05-03 09:38:29.949+0700 I/CAPI_NETWORK_CONNECTION( 2035): connection.c: connection_destroy(471) > Destroy handle: 0xb1e7c7c8
05-03 09:38:30.039+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:38:30.039+0700 I/watchface-app( 1319): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
05-03 09:38:30.049+0700 W/W_HOME  ( 1225): clock_event.c: _pkgmgr_event_cb(194) > Pkg:com.toyota.realtimefeedback is being uninstalled
05-03 09:38:30.049+0700 W/W_HOME  ( 1225): dbox.c: uninstall_cb(1434) > uninstalled:com.toyota.realtimefeedback
05-03 09:38:30.049+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2035
05-03 09:38:30.049+0700 E/rpm-installer( 2475): installer-util.c: __check_running_app(1774) > app[com.toyota.realtimefeedback] is running, need to terminate it.
05-03 09:38:30.049+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 12
05-03 09:38:30.059+0700 W/AUL_AMD (  946): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(984) > pkgid(com.toyota.realtimefeedback), key(start), value(uninstall)
05-03 09:38:30.059+0700 W/AUL_AMD (  946): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(997) > __amd_pkgmgrinfo_start_handler
05-03 09:38:30.059+0700 W/AUL     ( 2475): launch.c: app_request_to_launchpad(284) > request cmd(5) to(2035)
05-03 09:38:30.059+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 5
05-03 09:38:30.059+0700 W/AUL     (  946): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(com.toyota.realtimefeedback) pid(2035) type(uiapp)
05-03 09:38:30.059+0700 W/AUL_AMD (  946): amd_launch.c: __reply_handler(999) > listen fd(23) , send fd(22), pid(2035), cmd(4)
05-03 09:38:30.059+0700 I/APP_CORE( 2035): appcore-efl.c: __do_app(453) > [APP 2035] Event: TERMINATE State: PAUSED
05-03 09:38:30.069+0700 W/AUL     ( 2475): launch.c: app_request_to_launchpad(298) > request cmd(5) result(0)
05-03 09:38:30.069+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 22
05-03 09:38:30.069+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(999) > app status : 4
05-03 09:38:30.069+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:38:30.079+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2035
05-03 09:38:30.079+0700 W/APP_CORE( 2035): appcore-efl.c: appcore_efl_main(1788) > power off : 0
05-03 09:38:30.079+0700 W/APP_CORE( 2035): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3400002] -> redirected win[60021b] for com.toyota.realtimefeedback[2035]
05-03 09:38:30.099+0700 E/WMS     ( 1057): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [UNINSTALL, STARTED]
05-03 09:38:30.129+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
05-03 09:38:30.129+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
05-03 09:38:30.129+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 3-5-2018, 02:39:37 (UTC).
05-03 09:38:30.129+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
05-03 09:38:30.139+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
05-03 09:38:30.159+0700 I/APP_CORE( 2035): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
05-03 09:38:30.159+0700 I/CAPI_APPFW_APPLICATION( 2035): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
05-03 09:38:30.179+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:38:30.179+0700 I/efl-extension( 2035): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 09:38:30.179+0700 I/efl-extension( 2035): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb7150258 is freed
05-03 09:38:30.179+0700 E/EFL     ( 2035): elementary<2035> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb71499f8 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
05-03 09:38:30.179+0700 I/efl-extension( 2035): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb714a440, obj: 0xb714a440
05-03 09:38:30.179+0700 I/efl-extension( 2035): efl_extension_rotary.c: _object_deleted_cb(618) > done
05-03 09:38:30.179+0700 E/EFL     ( 2035): elementary<2035> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 09:38:30.189+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2035
05-03 09:38:30.189+0700 E/EFL     ( 2035): elementary<2035> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 09:38:30.189+0700 E/EFL     ( 2035): elementary<2035> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb714a440 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 09:38:30.189+0700 E/EFL     ( 2035): elementary<2035> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb714a440 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 09:38:30.189+0700 E/EFL     ( 2035): elementary<2035> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb714a440 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 09:38:30.189+0700 E/EFL     ( 2035): elementary<2035> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb714a440 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 09:38:30.189+0700 I/efl-extension( 2035): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 09:38:30.189+0700 I/efl-extension( 2035): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb7178390 is freed
05-03 09:38:30.189+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 09:38:30.189+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb714a440, elm_genlist, func : 0xb587eea1
05-03 09:38:30.189+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 09:38:30.199+0700 I/APP_CORE( 2035): appcore-efl.c: __after_loop(1243) > [APP 2035] After terminate() callback
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb1e11b00 is freed
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7157508, elm_genlist, func : 0xb587eea1
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb1e1b348
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb1e2b338 is freed
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb1e0c078, elm_scroller, func : 0xb5882379
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb1e1b348, elm_image, func : 0xb5882379
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 09:38:30.199+0700 I/efl-extension( 2035): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb1e0c078 : elm_scroller] rotary callabck is deleted
05-03 09:38:30.289+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:38:30.299+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2035
05-03 09:38:30.399+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:38:30.409+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2035
05-03 09:38:30.509+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:38:30.519+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
05-03 09:38:30.549+0700 E/PKGMGR_PARSER( 2475): pkgmgr_parser_signature.c: __ps_check_mdm_policy_by_pkgid(1056) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
05-03 09:38:30.579+0700 I/PRIVACY-MANAGER-CLIENT( 2475): SocketClient.cpp: SocketClient(37) > Client created
05-03 09:38:30.579+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: mainloop(227) > Got incoming connection
05-03 09:38:30.579+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: connectionThread(253) > Starting connection thread
05-03 09:38:30.579+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketStream.h: SocketStream(31) > Created
05-03 09:38:30.579+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketConnection.h: SocketConnection(44) > Created
05-03 09:38:30.579+0700 I/PRIVACY-MANAGER-CLIENT( 2475): SocketStream.h: SocketStream(31) > Created
05-03 09:38:30.579+0700 I/PRIVACY-MANAGER-CLIENT( 2475): SocketConnection.h: SocketConnection(44) > Created
05-03 09:38:30.579+0700 I/PRIVACY-MANAGER-CLIENT( 2475): SocketClient.cpp: connect(62) > Client connected
05-03 09:38:30.579+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: connectionService(304) > Calling service
05-03 09:38:30.589+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: connectionService(307) > Removing client
05-03 09:38:30.589+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: connectionService(311) > Call served
05-03 09:38:30.589+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: connectionThread(262) > Client serviced
05-03 09:38:30.589+0700 I/PRIVACY-MANAGER-CLIENT( 2475): SocketClient.cpp: disconnect(72) > Client disconnected
05-03 09:38:30.729+0700 W/AUL_PAD ( 1896): sigchild.h: __launchpad_process_sigchld(188) > dead_pid = 2035 pgid = 2035
05-03 09:38:30.729+0700 W/AUL_PAD ( 1896): sigchild.h: __launchpad_process_sigchld(189) > ssi_code = 2 ssi_status = 11
05-03 09:38:30.789+0700 W/AUL_PAD ( 1896): sigchild.h: __launchpad_process_sigchld(197) > after __sigchild_action
05-03 09:38:30.789+0700 I/AUL_AMD (  946): amd_main.c: __app_dead_handler(262) > __app_dead_handler, pid: 2035
05-03 09:38:30.789+0700 W/AUL     (  946): app_signal.c: aul_send_app_terminated_signal(799) > aul_send_app_terminated_signal pid(2035)
05-03 09:38:30.849+0700 W/CRASH_MANAGER( 2479): worker.c: worker_job(1205) > 1102035726561152531511
