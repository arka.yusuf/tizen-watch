S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2123
Date: 2018-04-27 11:40:52+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2123, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb593e238
r2   = 0xb593e238, r3   = 0xb592a28d
r4   = 0xbedbe444, r5   = 0xb5c50800
r6   = 0x00000274, r7   = 0xbedbe098
r8   = 0x00000000, r9   = 0xffffffff
r10  = 0xb6cf38e4, fp   = 0xb6cf38e4
ip   = 0xb5c5b084, sp   = 0xbedbe030
lr   = 0xb592a2af, pc   = 0xb592a3a0
cpsr = 0xa0000030

Memory Information
MemTotal:   405512 KB
MemFree:      9164 KB
Buffers:      8244 KB
Cached:     114116 KB
VmPeak:      91560 KB
VmSize:      89396 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       23936 KB
VmRSS:       23936 KB
VmData:      29164 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:          58 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2123 TID = 2123
2123 2151 2163 

Maps Information
b1d0b000 b1d0e000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b1e8a000 b1e8e000 r-xp /usr/lib/libogg.so.0.7.1
b1e96000 b1eb8000 r-xp /usr/lib/libvorbis.so.0.4.3
b1ec0000 b1f07000 r-xp /usr/lib/libsndfile.so.1.0.26
b1f13000 b1f5c000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1f65000 b1f6a000 r-xp /usr/lib/libjson.so.0.0.1
b380b000 b3911000 r-xp /usr/lib/libicuuc.so.57.1
b3927000 b3aaf000 r-xp /usr/lib/libicui18n.so.57.1
b3abf000 b3acc000 r-xp /usr/lib/libail.so.0.1.0
b3ad5000 b3ad8000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3ae0000 b3b18000 r-xp /usr/lib/libpulse.so.0.16.2
b3b19000 b3b1c000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3b24000 b3b85000 r-xp /usr/lib/libasound.so.2.0.0
b3b8f000 b3ba8000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3bb1000 b3bb5000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3bbd000 b3bc8000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3bd5000 b3bd9000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3be2000 b3bfa000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3c0b000 b3c12000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3c1a000 b3c25000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3c2d000 b3c2f000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3c37000 b3c38000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3c40000 b3c48000 r-xp /usr/lib/libfeedback.so.0.1.4
b3c58000 b3c59000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3cbe000 b3d45000 rw-s anon_inode:dmabuf
b3d45000 b3dcc000 rw-s anon_inode:dmabuf
b3e79000 b3f00000 rw-s anon_inode:dmabuf
b4006000 b4007000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b4052000 b40d9000 rw-s anon_inode:dmabuf
b40da000 b48d9000 rw-p [stack:2163]
b4a7d000 b4a7e000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b85000 b5384000 rw-p [stack:2151]
b5384000 b5386000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b538e000 b53a5000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b53b2000 b53b4000 r-xp /usr/lib/libdri2.so.0.0.0
b53bc000 b53c7000 r-xp /usr/lib/libtbm.so.1.0.0
b53cf000 b53d7000 r-xp /usr/lib/libdrm.so.2.4.0
b53df000 b53e1000 r-xp /usr/lib/libgenlock.so
b53e9000 b53ee000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b53f6000 b5401000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b560a000 b56d4000 r-xp /usr/lib/libCOREGL.so.4.0
b56e5000 b56f5000 r-xp /usr/lib/libmdm-common.so.1.1.25
b56fd000 b5703000 r-xp /usr/lib/libxcb-render.so.0.0.0
b570b000 b570c000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5715000 b5718000 r-xp /usr/lib/libEGL.so.1.4
b5720000 b572e000 r-xp /usr/lib/libGLESv2.so.2.0
b5737000 b5780000 r-xp /usr/lib/libmdm.so.1.2.70
b5789000 b578f000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5797000 b57a0000 r-xp /usr/lib/libcom-core.so.0.0.1
b57a9000 b5861000 r-xp /usr/lib/libcairo.so.2.11200.14
b586c000 b5885000 r-xp /usr/lib/libnetwork.so.0.0.0
b588d000 b5899000 r-xp /usr/lib/libnotification.so.0.1.0
b58a2000 b58b1000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b58ba000 b58db000 r-xp /usr/lib/libefl-extension.so.0.1.0
b58e3000 b58e8000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b58f0000 b58f5000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b58fd000 b590d000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5915000 b591d000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5925000 b592f000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5ad4000 b5ade000 r-xp /lib/libnss_files-2.13.so
b5ae7000 b5bb6000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5bcc000 b5bf0000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5bf9000 b5bff000 r-xp /usr/lib/libappsvc.so.0.1.0
b5c07000 b5c0b000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5c18000 b5c23000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5c2b000 b5c2d000 r-xp /usr/lib/libiniparser.so.0
b5c36000 b5c3b000 r-xp /usr/lib/libappcore-common.so.1.1
b5c43000 b5c45000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5c4e000 b5c52000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5c5f000 b5c61000 r-xp /usr/lib/libXau.so.6.0.0
b5c69000 b5c70000 r-xp /lib/libcrypt-2.13.so
b5ca0000 b5ca2000 r-xp /usr/lib/libiri.so
b5cab000 b5e3d000 r-xp /usr/lib/libcrypto.so.1.0.0
b5e5e000 b5ea5000 r-xp /usr/lib/libssl.so.1.0.0
b5eb1000 b5edf000 r-xp /usr/lib/libidn.so.11.5.44
b5ee7000 b5ef0000 r-xp /usr/lib/libcares.so.2.1.0
b5efa000 b5f0d000 r-xp /usr/lib/libxcb.so.1.1.0
b5f16000 b5f19000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5f21000 b5f23000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5f2c000 b5ff8000 r-xp /usr/lib/libxml2.so.2.7.8
b6005000 b6007000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b6010000 b6015000 r-xp /usr/lib/libffi.so.5.0.10
b601d000 b601e000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b6026000 b6029000 r-xp /lib/libattr.so.1.1.0
b6031000 b60c5000 r-xp /usr/lib/libstdc++.so.6.0.16
b60d8000 b60f5000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b60ff000 b6117000 r-xp /usr/lib/libpng12.so.0.50.0
b611f000 b6135000 r-xp /lib/libexpat.so.1.6.0
b613f000 b6183000 r-xp /usr/lib/libcurl.so.4.3.0
b618c000 b6196000 r-xp /usr/lib/libXext.so.6.4.0
b61a0000 b61a4000 r-xp /usr/lib/libXtst.so.6.1.0
b61ac000 b61b2000 r-xp /usr/lib/libXrender.so.1.3.0
b61ba000 b61c0000 r-xp /usr/lib/libXrandr.so.2.2.0
b61c8000 b61c9000 r-xp /usr/lib/libXinerama.so.1.0.0
b61d2000 b61db000 r-xp /usr/lib/libXi.so.6.1.0
b61e3000 b61e6000 r-xp /usr/lib/libXfixes.so.3.1.0
b61ef000 b61f1000 r-xp /usr/lib/libXgesture.so.7.0.0
b61f9000 b61fb000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6203000 b6205000 r-xp /usr/lib/libXdamage.so.1.1.0
b620d000 b6214000 r-xp /usr/lib/libXcursor.so.1.0.2
b621c000 b621f000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b6228000 b622c000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6235000 b623a000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6243000 b6324000 r-xp /usr/lib/libX11.so.6.3.0
b632f000 b6352000 r-xp /usr/lib/libjpeg.so.8.0.2
b636a000 b6380000 r-xp /lib/libz.so.1.2.5
b6389000 b638b000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b6393000 b6408000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6412000 b642c000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6434000 b6468000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b6471000 b6544000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6550000 b6560000 r-xp /lib/libresolv-2.13.so
b6564000 b657c000 r-xp /usr/lib/liblzma.so.5.0.3
b6584000 b6587000 r-xp /lib/libcap.so.2.21
b658f000 b65be000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b65c6000 b65c7000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b65d0000 b65d6000 r-xp /usr/lib/libecore_imf.so.1.7.99
b65de000 b65f5000 r-xp /usr/lib/liblua-5.1.so
b65fe000 b6605000 r-xp /usr/lib/libembryo.so.1.7.99
b660d000 b6613000 r-xp /lib/librt-2.13.so
b661c000 b6672000 r-xp /usr/lib/libpixman-1.so.0.28.2
b6680000 b66d6000 r-xp /usr/lib/libfreetype.so.6.11.3
b66e2000 b670a000 r-xp /usr/lib/libfontconfig.so.1.8.0
b670b000 b6750000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b6759000 b676c000 r-xp /usr/lib/libfribidi.so.0.3.1
b6774000 b678e000 r-xp /usr/lib/libecore_con.so.1.7.99
b6798000 b67a1000 r-xp /usr/lib/libedbus.so.1.7.99
b67a9000 b67f9000 r-xp /usr/lib/libecore_x.so.1.7.99
b67fb000 b6804000 r-xp /usr/lib/libvconf.so.0.2.45
b680c000 b681d000 r-xp /usr/lib/libecore_input.so.1.7.99
b6825000 b682a000 r-xp /usr/lib/libecore_file.so.1.7.99
b6832000 b6854000 r-xp /usr/lib/libecore_evas.so.1.7.99
b685d000 b689e000 r-xp /usr/lib/libeina.so.1.7.99
b68a7000 b68c0000 r-xp /usr/lib/libeet.so.1.7.99
b68d1000 b693a000 r-xp /lib/libm-2.13.so
b6943000 b6949000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6952000 b6953000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b695b000 b697e000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6986000 b698b000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6993000 b69bd000 r-xp /usr/lib/libdbus-1.so.3.8.12
b69c6000 b69dd000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b69e5000 b69f0000 r-xp /lib/libunwind.so.8.0.1
b6a1d000 b6a3b000 r-xp /usr/lib/libsystemd.so.0.4.0
b6a45000 b6b69000 r-xp /lib/libc-2.13.so
b6b77000 b6b7f000 r-xp /lib/libgcc_s-4.6.so.1
b6b80000 b6b84000 r-xp /usr/lib/libsmack.so.1.0.0
b6b8d000 b6b93000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b9b000 b6c6b000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6c6c000 b6cca000 r-xp /usr/lib/libedje.so.1.7.99
b6cd4000 b6ceb000 r-xp /usr/lib/libecore.so.1.7.99
b6d02000 b6dd0000 r-xp /usr/lib/libevas.so.1.7.99
b6df6000 b6f32000 r-xp /usr/lib/libelementary.so.1.7.99
b6f49000 b6f5d000 r-xp /lib/libpthread-2.13.so
b6f68000 b6f6a000 r-xp /usr/lib/libdlog.so.0.0.0
b6f72000 b6f75000 r-xp /usr/lib/libbundle.so.0.1.22
b6f7d000 b6f7f000 r-xp /lib/libdl-2.13.so
b6f88000 b6f95000 r-xp /usr/lib/libaul.so.0.1.0
b6fa7000 b6fad000 r-xp /usr/lib/libappcore-efl.so.1.1
b6fb6000 b6fba000 r-xp /usr/lib/libsys-assert.so
b6fc3000 b6fe0000 r-xp /lib/ld-2.13.so
b6fe9000 b6fee000 r-xp /usr/bin/launchpad-loader
b8436000 b8901000 rw-p [heap]
bed9e000 bedbf000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2123)
Call Stack Count: 15
 0: loadAllNotification + 0xf (0xb592a3a0) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x53a0
 1: _on_resume_cb + 0x22 (0xb592a2af) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x52af
 2: (0xb5c4f6ad) [/usr/lib/libcapi-appfw-application.so.0] + 0x16ad
 3: (0xb6faa151) [/usr/lib/libappcore-efl.so.1] + 0x3151
 4: (0xb6faa527) [/usr/lib/libappcore-efl.so.1] + 0x3527
 5: (0xb6cdce53) [/usr/lib/libecore.so.1] + 0x8e53
 6: (0xb6ce046b) [/usr/lib/libecore.so.1] + 0xc46b
 7: ecore_main_loop_begin + 0x30 (0xb6ce0879) [/usr/lib/libecore.so.1] + 0xc879
 8: appcore_efl_main + 0x332 (0xb6faab47) [/usr/lib/libappcore-efl.so.1] + 0x3b47
 9: ui_app_main + 0xb0 (0xb5c4fed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
10: uib_app_run + 0xea (0xb592a227) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5227
11: main + 0x34 (0xb592a641) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5641
12:  + 0x0 (0xb6feaa53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
13: __libc_start_main + 0x114 (0xb6a5c85c) [/lib/libc.so.6] + 0x1785c
14: (0xb6feae0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
89+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] time(0.286079)
04-27 11:40:34.889+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(117)
04-27 11:40:34.889+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:34.939+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] time(0.526696)
04-27 11:40:34.939+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(121)
04-27 11:40:34.939+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:34.989+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] time(0.719693)
04-27 11:40:34.989+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(124)
04-27 11:40:34.989+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.039+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] time(0.849447)
04-27 11:40:35.039+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(126)
04-27 11:40:35.039+0700 E/weather-agent( 2152): AgentMain.cpp: TerminateService(97) > [0;40;31mTerminateService[0;m
04-27 11:40:35.039+0700 I/CAPI_APPFW_APPLICATION( 2152): service_app_main.c: service_app_exit(441) > service_app_exit
04-27 11:40:35.039+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.069+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 22
04-27 11:40:35.069+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(999) > app status : 4
04-27 11:40:35.069+0700 E/weather-agent( 2152): AgentMain.cpp: AppTerminate(283) > [0;40;31mAppTerminate[0;m
04-27 11:40:35.089+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] time(0.953157)
04-27 11:40:35.089+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(128)
04-27 11:40:35.089+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.139+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] time(0.993648)
04-27 11:40:35.139+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2612 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] ECORE_CALLBACK_RENEW : px(0), py(128)
04-27 11:40:35.179+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2589 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] time(0.996837)
04-27 11:40:35.179+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2604 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] animation stop!!
04-27 11:40:35.179+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:2607 _elm_scroll_scroll_to_y_animator() [0xb8564868 : elm_genlist] ECORE_CALLBACK_CANCEL : px(0), py(129)
04-27 11:40:35.179+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.269+0700 E/weather-agent( 2152): AgentMain.cpp: AppTerminate(287) > [0;40;31mdevice_power_release_lock success[0;m
04-27 11:40:35.299+0700 W/AUL_AMD (  927): amd_status.c: __app_terminate_timer_cb(168) > send SIGKILL: No such process
04-27 11:40:35.419+0700 E/CAPI_APPFW_APP_CONTROL( 2152): app_control.c: app_control_error(138) > [app_control_destroy] INVALID_PARAMETER(0xffffffea)
04-27 11:40:35.419+0700 E/weather-agent( 2152): AgentMain.cpp: ~AgentMain(77) > [0;40;31mapp_control_destroy failed[0;m
04-27 11:40:35.439+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.489+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.509+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.529+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.549+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.569+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.589+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 11:40:35.599+0700 E/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: HandleReceivedMessage(588) > Connection closed
04-27 11:40:35.599+0700 E/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: HandleReceivedMessage(610) > All connections of client(2152) are closed. delete client info
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: OnIpcClientDisconnected(178) > MessagePort Ipc disconnected
04-27 11:40:35.599+0700 E/MESSAGE_PORT(  921): MessagePortStub.cpp: OnIpcClientDisconnected(181) > Unregister - client =  2152
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: UnregisterMessagePortByDiscon(273) > _MessagePortService::UnregisterMessagePortByDiscon
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.599+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
04-27 11:40:35.609+0700 E/RESOURCED( 1117): procfs.c: proc_get_oom_score_adj(178) > fopen /proc/2152/oom_score_adj failed
04-27 11:40:35.609+0700 E/RESOURCED( 1117): proc-main.c: resourced_proc_status_change(1501) > Empty pid or process not exists. 2152
04-27 11:40:35.619+0700 E/EFL     ( 2123): ecore_x<2123> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=167483 button=1
04-27 11:40:35.629+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] mouse move
04-27 11:40:35.629+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] mouse move
04-27 11:40:35.629+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] hold(0), freeze(0)
04-27 11:40:35.629+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] mouse move
04-27 11:40:35.629+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] hold(0), freeze(0)
04-27 11:40:35.629+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] mouse move
04-27 11:40:35.629+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] hold(0), freeze(0)
04-27 11:40:35.629+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:35.649+0700 W/AUL     ( 2162): daemon-manager-release-agent.c: main(12) > release agent : [2:/com.samsung.weather-agent]
04-27 11:40:35.649+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(669) > __request_handler: 23
04-27 11:40:35.649+0700 W/AUL_AMD (  927): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 0
04-27 11:40:35.649+0700 W/AUL_AMD (  927): amd_request.c: __request_handler(1032) > pkg_status: installed, dead pid: 2152
04-27 11:40:35.649+0700 W/AUL_AMD (  927): amd_request.c: __send_app_termination_signal(528) > send dead signal done
04-27 11:40:35.659+0700 I/AUL_AMD (  927): amd_main.c: __app_dead_handler(262) > __app_dead_handler, pid: 2152
04-27 11:40:35.659+0700 W/AUL     (  927): app_signal.c: aul_send_app_terminated_signal(799) > aul_send_app_terminated_signal pid(2152)
04-27 11:40:35.689+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] mouse move
04-27 11:40:35.689+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] hold(0), freeze(0)
04-27 11:40:35.699+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] mouse move
04-27 11:40:35.699+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] hold(0), freeze(0)
04-27 11:40:35.709+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] mouse move
04-27 11:40:35.709+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] hold(0), freeze(0)
04-27 11:40:35.719+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] mouse move
04-27 11:40:35.719+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8564868 : elm_genlist] hold(0), freeze(0)
04-27 11:40:35.749+0700 E/EFL     ( 2123): ecore_x<2123> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=167637 button=1
04-27 11:40:36.009+0700 E/GL SELECTED( 2123): ID GL : TRM2
04-27 11:40:36.459+0700 E/EFL     ( 2123): ecore_x<2123> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=168352 button=1
04-27 11:40:36.579+0700 E/EFL     ( 2123): ecore_x<2123> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=168473 button=1
04-27 11:40:36.589+0700 I/efl-extension( 2123): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:40:36.589+0700 I/efl-extension( 2123): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:40:36.589+0700 I/efl-extension( 2123): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb86d18b8, elm_image, _activated_obj : 0xb8573478, activated : 1
04-27 11:40:36.589+0700 I/efl-extension( 2123): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:40:36.599+0700 E/EFL     ( 2123): elementary<2123> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-27 11:40:36.599+0700 E/EFL     ( 2123): elementary<2123> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb86d1d20) will be pushed
04-27 11:40:36.609+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:36.609+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:36.609+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb861e7e8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:40:36.609+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb861e7e8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:40:36.629+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb861e7e8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:40:36.629+0700 E/EFL     ( 2123): elementary<2123> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb861e7e8 : elm_genlist] cw(360), ch(245), pw(360), ph(360)
04-27 11:40:36.669+0700 E/EFL     ( 2123): elementary<2123> elc_naviframe.c:2796 _push_transition_cb() item(0xb86d1d20) will transition
04-27 11:40:37.069+0700 W/AUL_AMD (  927): amd_status.c: __app_terminate_timer_cb(168) > send SIGKILL: No such process
04-27 11:40:37.089+0700 E/EFL     ( 2123): elementary<2123> elc_naviframe.c:1193 _on_item_push_finished() item(0xb85b79c8) transition finished
04-27 11:40:37.089+0700 E/EFL     ( 2123): elementary<2123> elc_naviframe.c:1218 _on_item_show_finished() item(0xb86d1d20) transition finished
04-27 11:40:37.099+0700 E/EFL     ( 2123): <2123> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:40:37.579+0700 E/EFL     (  893): ecore_x<893> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3600002 time=168536
04-27 11:40:37.579+0700 E/EFL     ( 2123): ecore_x<2123> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=168536
04-27 11:40:37.579+0700 E/EFL     (  893): ecore_x<893> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=168536
04-27 11:40:38.709+0700 I/CAPI_NETWORK_CONNECTION( 2123): connection.c: connection_create(453) > New handle created[0xb3f01580]
04-27 11:40:38.749+0700 I/CAPI_NETWORK_CONNECTION( 2123): connection.c: connection_destroy(471) > Destroy handle: 0xb3f01580
04-27 11:40:40.579+0700 E/WMS     ( 1018): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
04-27 11:40:41.699+0700 W/WATCH_CORE( 1308): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
04-27 11:40:41.699+0700 I/WATCH_CORE( 1308): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
04-27 11:40:41.699+0700 I/CAPI_WATCH_APPLICATION( 1308): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-27 11:40:41.699+0700 E/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1157) > 
04-27 11:40:41.699+0700 I/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-27 11:40:41.709+0700 E/wnoti-service( 1371): wnoti-db-utility.c: context_wearonoff_status_cb(1781) > status changed from 1 to 2 
04-27 11:40:41.709+0700 E/WMS     ( 1018): wms_event_handler.c: _wms_event_handler_cb_wearonoff_monitor(23513) > wear_monitor_status update[0] = 1 -> 2
04-27 11:40:41.709+0700 E/WMS     ( 1018): wms_event_handler.c: _wms_event_handler_send_wear_monitor_status(10697) > show noti while wearing gear disabled
04-27 11:40:41.719+0700 W/SHealthServiceCommon( 1751): ContextRestingHeartrateProxy.cpp: OnRestingHrUpdatedCB(347) > [1;40;33mhrValue: 1008[0;m
04-27 11:40:43.799+0700 I/CAPI_NETWORK_CONNECTION( 2123): connection.c: connection_create(453) > New handle created[0xb3f019d8]
04-27 11:40:43.819+0700 E/JSON PARSING( 2123): No data could be display
04-27 11:40:43.819+0700 E/JSON PARSING( 2123): `ѱ�蓵;
04-27 11:40:43.819+0700 I/CAPI_NETWORK_CONNECTION( 2123): connection.c: connection_destroy(471) > Destroy handle: 0xb3f019d8
04-27 11:40:44.129+0700 W/W_INDICATOR( 1125): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
04-27 11:40:44.129+0700 W/W_INDICATOR( 1125): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
04-27 11:40:44.129+0700 W/W_INDICATOR( 1125): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 2
04-27 11:40:44.129+0700 W/W_INDICATOR( 1125): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 14 / signal : type_wifi_connected_02
04-27 11:40:44.129+0700 E/W_INDICATOR( 1125): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 14) / (hide : 0)
04-27 11:40:44.129+0700 W/W_INDICATOR( 1125): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-27 11:40:44.129+0700 E/W_INDICATOR( 1125): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-27 11:40:44.129+0700 W/W_INDICATOR( 1125): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
04-27 11:40:48.869+0700 I/CAPI_NETWORK_CONNECTION( 2123): connection.c: connection_create(453) > New handle created[0xb3f01a28]
04-27 11:40:49.189+0700 I/CAPI_NETWORK_CONNECTION( 2123): connection.c: connection_destroy(471) > Destroy handle: 0xb3f01a28
04-27 11:40:50.979+0700 W/WATCH_CORE( 1308): appcore-watch.c: __signal_lcd_status_handler(1231) > signal_lcd_status_signal: LCDOff
04-27 11:40:50.979+0700 W/W_HOME  ( 1216): dbus.c: _dbus_message_recv_cb(204) > LCD off
04-27 11:40:50.979+0700 W/W_HOME  ( 1216): gesture.c: _manual_render_cancel_schedule(226) > cancel schedule, manual render
04-27 11:40:50.979+0700 W/W_HOME  ( 1216): gesture.c: _manual_render_disable_timer_del(157) > timer del
04-27 11:40:50.979+0700 W/W_HOME  ( 1216): gesture.c: _manual_render_enable(138) > 1
04-27 11:40:50.979+0700 W/W_HOME  ( 1216): event_manager.c: _lcd_off_cb(723) > lcd state: 0
04-27 11:40:50.979+0700 W/W_HOME  ( 1216): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:1(0) pm_state:0 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:40:50.989+0700 W/WAKEUP-SERVICE( 1679): WakeupService.cpp: OnReceiveDisplayChanged(979) > [0;32mINFO: LCDOff receive data : -1226253556[0;m
04-27 11:40:50.989+0700 W/STARTER ( 1124): clock-mgr.c: _on_lcd_signal_receive_cb(1284) > [_on_lcd_signal_receive_cb:1284] _on_lcd_signal_receive_cb, 1284, Pre LCD off by [timeout]
04-27 11:40:50.989+0700 W/STARTER ( 1124): clock-mgr.c: _pre_lcd_off(1089) > [_pre_lcd_off:1089] Will LCD OFF as wake_up_setting[1]
04-27 11:40:50.989+0700 E/STARTER ( 1124): scontext_util.c: sconstext_util_check_lock_type(47) > [sconstext_util_check_lock_type:47] current lock state :[0],testmode[0]
04-27 11:40:50.989+0700 E/STARTER ( 1124): scontext_util.c: scontext_util_handle_lock_state(72) > [scontext_util_handle_lock_state:72] wear state[0],bPossible [0],usage [0]
04-27 11:40:50.989+0700 W/STARTER ( 1124): clock-mgr.c: _check_reserved_popup_status(211) > [_check_reserved_popup_status:211] Current reserved apps status : 0
04-27 11:40:50.989+0700 W/STARTER ( 1124): clock-mgr.c: _check_reserved_apps_status(247) > [_check_reserved_apps_status:247] Current reserved apps status : 0
04-27 11:40:50.989+0700 W/WAKEUP-SERVICE( 1679): WakeupService.cpp: OnReceiveDisplayChanged(980) > [0;32mINFO: WakeupServiceStop[0;m
04-27 11:40:50.989+0700 W/WAKEUP-SERVICE( 1679): WakeupService.cpp: WakeupServiceStop(399) > [0;32mINFO: SEAMLESS WAKEUP STOP REQUEST[0;m
04-27 11:40:50.999+0700 E/WAKEUP-SERVICE( 1679): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
04-27 11:40:51.009+0700 E/WAKEUP-SERVICE( 1679): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
04-27 11:40:51.009+0700 I/TIZEN_N_SOUND_MANAGER( 1679): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Disable start
04-27 11:40:51.019+0700 I/TIZEN_N_SOUND_MANAGER( 1679): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Disable end. (ret: 0x0)
04-27 11:40:51.019+0700 W/TIZEN_N_SOUND_MANAGER( 1679): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
04-27 11:40:51.019+0700 W/WAKEUP-SERVICE( 1679): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 0[0;m
04-27 11:40:51.019+0700 I/HIGEAR  ( 1679): WakeupService.cpp: WakeupServiceStop(403) > [svoice:Application:WakeupServiceStop:IN]
04-27 11:40:51.189+0700 W/SHealthCommon( 1751): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:3[0;m
04-27 11:40:51.189+0700 W/SHealthServiceCommon( 1751): SHealthServiceController.cpp: OnSystemUtilLcdStateChanged(645) > [1;35m ###[0;m
04-27 11:40:51.199+0700 W/W_INDICATOR( 1125): windicator_util.c: _pm_state_changed_cb(917) > [_pm_state_changed_cb:917] LCD off
04-27 11:40:51.199+0700 W/W_INDICATOR( 1125): windicator_connection.c: windicator_connection_pause(2268) > [windicator_connection_pause:2268] 
04-27 11:40:51.199+0700 W/W_INDICATOR( 1125): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
04-27 11:40:51.209+0700 W/STARTER ( 1124): clock-mgr.c: _on_lcd_signal_receive_cb(1297) > [_on_lcd_signal_receive_cb:1297] _on_lcd_signal_receive_cb, 1297, Post LCD off by [timeout]
04-27 11:40:51.209+0700 W/STARTER ( 1124): clock-mgr.c: _post_lcd_off(1190) > [_post_lcd_off:1190] LCD OFF as reserved app[(null)] alpm mode[0]
04-27 11:40:51.209+0700 W/W_INDICATOR( 1125): windicator_dbus.c: _windicator_dbus_lcd_off_completed_cb(620) > [_windicator_dbus_lcd_off_completed_cb:620] LCD Off completed signal is received
04-27 11:40:51.209+0700 W/W_INDICATOR( 1125): windicator_dbus.c: _windicator_dbus_lcd_off_completed_cb(625) > [_windicator_dbus_lcd_off_completed_cb:625] Moment bar status -> idle. (Hide Moment bar)
04-27 11:40:51.209+0700 W/W_INDICATOR( 1125): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
04-27 11:40:51.209+0700 W/STARTER ( 1124): clock-mgr.c: _post_lcd_off(1197) > [_post_lcd_off:1197] Current reserved apps status : 0
04-27 11:40:51.209+0700 W/STARTER ( 1124): clock-mgr.c: _post_lcd_off(1215) > [_post_lcd_off:1215] raise homescreen after 20 sec. home_visible[0]
04-27 11:40:51.209+0700 E/ALARM_MANAGER( 1124): alarm-lib.c: alarmmgr_add_alarm_withcb(1178) > trigger_at_time(20), start(27-4-2018, 11:41:11), repeat(1), interval(20), type(-1073741822)
04-27 11:40:51.219+0700 I/APP_CORE( 2123): appcore-efl.c: __do_app(453) > [APP 2123] Event: PAUSE State: RUNNING
04-27 11:40:51.219+0700 I/CAPI_APPFW_APPLICATION( 2123): app_main.c: _ui_app_appcore_pause(611) > app_appcore_pause
04-27 11:40:51.219+0700 W/APP_CORE( 2123): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3600002] -> redirected win[60049d] for com.toyota.realtimefeedback[2123]
04-27 11:40:51.229+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __is_cached_cookie(230) > Find cached cookie for [1124].
04-27 11:40:51.279+0700 W/SHealthCommon( 1501): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:3[0;m
04-27 11:40:51.289+0700 E/ALARM_MANAGER(  931): alarm-manager-schedule.c: _alarm_next_duetime(509) > alarm_id: 1002624532, next duetime: 1524804071
04-27 11:40:51.289+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __alarm_add_to_list(496) > [alarm-server]: After add alarm_id(1002624532)
04-27 11:40:51.289+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __alarm_create(1061) > [alarm-server]:alarm_context.c_due_time(1524804378), due_time(1524804071)
04-27 11:40:51.299+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
04-27 11:40:51.299+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
04-27 11:40:51.299+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 27-4-2018, 04:41:11 (UTC).
04-27 11:40:51.299+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
04-27 11:40:51.299+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __save_module_log(1780) > The file is not ready.
04-27 11:40:51.299+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
04-27 11:40:51.309+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __save_module_log(1780) > The file is not ready.
04-27 11:40:52.769+0700 W/wnotibp ( 1954): wnotiboard-popup-control.c: _ctrl_lcd_on_cb(93) > ::APP:: view state=0, 2, 0
04-27 11:40:52.769+0700 I/wnotibp ( 1954): wnotiboard-popup-control.c: _ctrl_lcd_on_cb(141) > There is no additional work.
04-27 11:40:52.779+0700 W/WATCH_CORE( 1308): appcore-watch.c: __signal_lcd_status_handler(1231) > signal_lcd_status_signal: LCDOn
04-27 11:40:52.779+0700 I/WATCH_CORE( 1308): appcore-watch.c: __signal_lcd_status_handler(1250) > Call the time_tick_cb
04-27 11:40:52.779+0700 I/CAPI_WATCH_APPLICATION( 1308): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
04-27 11:40:52.779+0700 E/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1157) > 
04-27 11:40:52.779+0700 I/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1168) > set force update!!
04-27 11:40:52.779+0700 I/WATCH_CORE( 1308): appcore-watch.c: __signal_lcd_status_handler(1257) > Call widget_provider_update_event
04-27 11:40:52.789+0700 W/W_HOME  ( 1216): dbus.c: _dbus_message_recv_cb(186) > LCD on
04-27 11:40:52.789+0700 W/W_HOME  ( 1216): gesture.c: _manual_render_disable_timer_set(167) > timer set
04-27 11:40:52.789+0700 W/W_HOME  ( 1216): gesture.c: _manual_render_disable_timer_del(157) > timer del
04-27 11:40:52.789+0700 W/W_HOME  ( 1216): gesture.c: _apps_status_get(128) > apps status:0
04-27 11:40:52.789+0700 W/W_HOME  ( 1216): gesture.c: _lcd_on_cb(303) > move_to_clock:0 clock_visible:0 info->offtime:1794
04-27 11:40:52.789+0700 W/W_HOME  ( 1216): gesture.c: _manual_render_schedule(209) > schedule, manual render
04-27 11:40:52.789+0700 W/W_HOME  ( 1216): event_manager.c: _lcd_on_cb(715) > lcd state: 1
04-27 11:40:52.789+0700 W/W_HOME  ( 1216): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:1(0) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:40:52.789+0700 W/STARTER ( 1124): clock-mgr.c: _on_lcd_signal_receive_cb(1258) > [_on_lcd_signal_receive_cb:1258] _on_lcd_signal_receive_cb, 1258, Pre LCD on by [powerkey] after screen off time [1794]ms
04-27 11:40:52.789+0700 W/STARTER ( 1124): clock-mgr.c: _pre_lcd_on(1027) > [_pre_lcd_on:1027] Will LCD ON as reserved app[(null)] alpm mode[0]
04-27 11:40:52.789+0700 W/W_INDICATOR( 1125): windicator_dbus.c: _windicator_dbus_lcd_changed_cb(538) > [_windicator_dbus_lcd_changed_cb:538] LCD ON signal is received
04-27 11:40:52.789+0700 W/W_INDICATOR( 1125): windicator_dbus.c: _windicator_dbus_lcd_changed_cb(559) > [_windicator_dbus_lcd_changed_cb:559] 559, str=[powerkey],charge : 0, connected : 0
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [starter:org.tizen.idled.ReservedApp]
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [starter:org.tizen.idled.ReservedApp]
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-27 11:40:52.789+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 11:40:52.799+0700 I/APP_CORE( 2123): appcore-efl.c: __do_app(453) > [APP 2123] Event: RESUME State: PAUSED
04-27 11:40:52.799+0700 I/CAPI_APPFW_APPLICATION( 2123): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
04-27 11:40:52.809+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __is_cached_cookie(230) > Find cached cookie for [1124].
04-27 11:40:52.809+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __alarm_remove_from_list(575) > [alarm-server]:Remove alarm id(1002624532)
04-27 11:40:52.809+0700 E/ALARM_MANAGER(  931): alarm-manager-schedule.c: __find_next_alarm_to_be_scheduled(547) > The duetime of alarm(436816151) is OVER.
04-27 11:40:52.819+0700 W/WAKEUP-SERVICE( 1679): WakeupService.cpp: OnReceiveDisplayChanged(970) > [0;32mINFO: LCDOn receive data : -1226253556[0;m
04-27 11:40:52.819+0700 W/WAKEUP-SERVICE( 1679): WakeupService.cpp: OnReceiveDisplayChanged(971) > [0;32mINFO: WakeupServiceStart[0;m
04-27 11:40:52.819+0700 W/WAKEUP-SERVICE( 1679): WakeupService.cpp: WakeupServiceStart(367) > [0;32mINFO: SEAMLESS WAKEUP START REQUEST[0;m
04-27 11:40:52.819+0700 W/WAKEUP-SERVICE( 1679): WakeupService.cpp: WakeupServiceStart(387) > [0;32mINFO: 500[0;m
04-27 11:40:52.819+0700 I/TIZEN_N_SOUND_MANAGER( 1679): sound_manager_product.c: sound_manager_svoice_set_param(1287) > [SVOICE] set param [keyword length] : 500
04-27 11:40:52.829+0700 W/W_HOME  ( 1216): gesture.c: _widget_updated_cb(188) > widget updated
04-27 11:40:52.829+0700 W/W_HOME  ( 1216): gesture.c: _manual_render_disable_timer_del(157) > timer del
04-27 11:40:52.829+0700 W/W_HOME  ( 1216): gesture.c: _manual_render(182) > 
04-27 11:40:52.829+0700 W/W_HOME  ( 1216): gesture.c: _manual_render(182) > 
04-27 11:40:52.869+0700 W/W_INDICATOR( 1125): windicator_util.c: _pm_state_changed_cb(912) > [_pm_state_changed_cb:912] LCD on
04-27 11:40:52.869+0700 W/W_INDICATOR( 1125): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(57) > [windicator_ongoing_info_shealth_update:57] windicator_shealth_update
04-27 11:40:52.869+0700 W/SHealthCommon( 1751): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:1[0;m
04-27 11:40:52.869+0700 W/SHealthServiceCommon( 1751): SHealthServiceController.cpp: OnSystemUtilLcdStateChanged(645) > [1;35m ###[0;m
04-27 11:40:52.869+0700 W/SHealthServiceCommon( 1751): EnergyExpenditureFeatureController.cpp: OnTotalEnergyExpenditureChanged(119) > [1;40;33mstart 1524762000000.000000, end 1524804052880.000000, calories 785.861230[0;m
04-27 11:40:52.869+0700 W/SHealthCommon( 1751): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: energy_expenditure_updated, pendingClientInfoList.size(): 1[0;m
04-27 11:40:52.889+0700 W/SHealthCommon( 1501): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:1[0;m
04-27 11:40:52.919+0700 W/W_HOME  ( 1216): gesture.c: _manual_render_enable(138) > 0
04-27 11:40:52.949+0700 I/HealthDataService( 1286): RequestHandler.cpp: OnHealthIpcMessageSync(123) > [1;35mServer Received: SHARE_GET[0;m
04-27 11:40:52.949+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
04-27 11:40:52.949+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
04-27 11:40:52.949+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 27-4-2018, 04:46:18 (UTC).
04-27 11:40:52.949+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
04-27 11:40:52.949+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __save_module_log(1780) > The file is not ready.
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(78) > [windicator_ongoing_info_shealth_update:78] Result : 0
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(99) > [windicator_ongoing_info_shealth_update:99] status : none
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(103) > [windicator_ongoing_info_shealth_update:103] application_id: 
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(112) > [windicator_ongoing_info_shealth_update:112] launch_operation : 
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(118) > [windicator_ongoing_info_shealth_update:118] extra_data_key : 
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(124) > [windicator_ongoing_info_shealth_update:124] extra_data_value : 
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(132) > [windicator_ongoing_info_shealth_update:132] image_path : 
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(135) > [windicator_ongoing_info_shealth_update:135] image_path_sub : 
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(138) > [windicator_ongoing_info_shealth_update:138] message_string :  
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(144) > [windicator_ongoing_info_shealth_update:144] [Update] SHealth status is none, so hide icon and text!
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info.c: windicator_ongoing_info_remove(191) > [windicator_ongoing_info_remove:191] Ongoing info type[1]
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 11:40:52.959+0700 W/W_INDICATOR( 1125): windicator_ongoing_info.c: windicator_ongoing_info_remove(191) > [windicator_ongoing_info_remove:191] Ongoing info type[2]
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
04-27 11:40:52.959+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 11:40:52.959+0700 E/EFL     ( 1125): <1125> elm_main.c:1622 elm_object_signal_emit() safety check failed: obj == NULL
04-27 11:40:52.969+0700 I/TIZEN_N_SOUND_MANAGER( 1125): sound_manager.c: sound_manager_get_volume(84) > returns : type=3, volume=11, ret=0x0
04-27 11:40:52.969+0700 W/TIZEN_N_SOUND_MANAGER( 1125): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_get_volume] ERROR_NONE (0x00000000)
04-27 11:40:52.969+0700 W/W_INDICATOR( 1125): windicator_quick_setting_brightness.c: windicator_quick_setting_brightness_update(94) > [windicator_quick_setting_brightness_update:94] hyun 20
04-27 11:40:52.969+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
04-27 11:40:52.969+0700 E/ALARM_MANAGER(  931): alarm-manager.c: alarm_manager_alarm_delete(2462) > alarm_id[1002624532] is removed.
04-27 11:40:52.979+0700 E/ALARM_MANAGER(  931): alarm-manager.c: __save_module_log(1780) > The file is not ready.
04-27 11:40:52.979+0700 W/STARTER ( 1124): clock-mgr.c: __reserved_apps_message_received_cb(586) > [__reserved_apps_message_received_cb:586] appid[com.samsung.windicator]
04-27 11:40:52.979+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 11:40:52.979+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 11:40:52.979+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 11:40:52.979+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-27 11:40:52.979+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-27 11:40:52.979+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 11:40:52.979+0700 I/MESSAGE_PORT(  921): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.windicator:org.tizen.idled.ReservedApp]
04-27 11:40:52.979+0700 I/MESSAGE_PORT(  921): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-27 11:40:52.979+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-27 11:40:52.979+0700 I/MESSAGE_PORT(  921): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 11:40:52.979+0700 W/STARTER ( 1124): clock-mgr.c: _on_lcd_signal_receive_cb(1271) > [_on_lcd_signal_receive_cb:1271] _on_lcd_signal_receive_cb, 1271, Post LCD on by [powerkey]
04-27 11:40:52.979+0700 W/STARTER ( 1124): clock-mgr.c: _post_lcd_on(1059) > [_post_lcd_on:1059] LCD ON as reserved app[(null)] alpm mode[0]
04-27 11:40:52.989+0700 W/W_INDICATOR( 1125): windicator_connection.c: windicator_connection_resume(2158) > [windicator_connection_resume:2158] 
04-27 11:40:52.989+0700 W/W_INDICATOR( 1125): windicator_connection.c: windicator_connection_resume(2223) > [windicator_connection_resume:2223] primary rssi level : (0), s_info.rssi_level : (8) / primary svc type : (1)
04-27 11:40:52.989+0700 W/W_INDICATOR( 1125): windicator_connection.c: _rssi_changed_cb(1924) > [_rssi_changed_cb:1924] roaming status : 0
04-27 11:40:52.989+0700 W/W_INDICATOR( 1125): windicator_connection.c: _rssi_changed_cb(1933) > [_rssi_changed_cb:1933] svc type : 1
04-27 11:40:52.989+0700 W/W_INDICATOR( 1125): windicator_connection.c: _rssi_changed_cb(1968) > [_rssi_changed_cb:1968] Rssi level has already been 0!, Don't need to show rssi down animation. Just Show No Signal icon
04-27 11:40:52.989+0700 W/W_INDICATOR( 1125): windicator_connection.c: _rssi_icon_set(1124) > [_rssi_icon_set:1124] RSSI level : 8/5, (0xb8f65958)
04-27 11:40:52.989+0700 E/W_INDICATOR( 1125): windicator_connection.c: _rssi_icon_set(1147) > [_rssi_icon_set:1147] Set RSSI SHOW sw.icon_0 (rssi_level : 8) (rssi_hide : 0)(b8f65958)
04-27 11:40:52.989+0700 W/W_INDICATOR( 1125): windicator_connection.c: _rssi_icon_set(1215) > [_rssi_icon_set:1215] NETWORK_ATT or NETWORK_TMB : there is no roaming icon
04-27 11:40:52.989+0700 W/W_INDICATOR( 1125): windicator_connection.c: _rssi_icon_set(1217) > [_rssi_icon_set:1217] rssi name : set_rssi_verizon_No_signal (0xb8f65958)
04-27 11:40:52.999+0700 W/W_INDICATOR( 1125): windicator_connection.c: _tapi_changed_cb(2115) > [_tapi_changed_cb:2115] modem_power : 0
04-27 11:40:52.999+0700 W/W_INDICATOR( 1125): windicator_connection.c: _connection_type_changed_cb(1324) > [_connection_type_changed_cb:1324] wifi state : 2
04-27 11:40:52.999+0700 W/W_INDICATOR( 1125): windicator_connection.c: _connection_type_changed_cb(1327) > [_connection_type_changed_cb:1327] Show wifi icon!
04-27 11:40:52.999+0700 W/W_INDICATOR( 1125): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
04-27 11:40:52.999+0700 W/W_INDICATOR( 1125): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
04-27 11:40:52.999+0700 W/W_INDICATOR( 1125): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 2
04-27 11:40:52.999+0700 W/W_INDICATOR( 1125): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 14 / signal : type_wifi_connected_02
04-27 11:40:52.999+0700 E/W_INDICATOR( 1125): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 14) / (hide : 0)
04-27 11:40:52.999+0700 W/W_INDICATOR( 1125): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-27 11:40:52.999+0700 E/W_INDICATOR( 1125): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-27 11:40:52.999+0700 W/W_INDICATOR( 1125): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
04-27 11:40:52.999+0700 W/W_INDICATOR( 1125): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-27 11:40:52.999+0700 E/W_INDICATOR( 1125): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-27 11:40:53.129+0700 W/TIZEN_N_SOUND_MANAGER( 1679): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_set_param] ERROR_NONE (0x00000000)
04-27 11:40:53.139+0700 W/W_INDICATOR( 1125): windicator_dbus.c: _msg_reserved_app_cb(341) > [_msg_reserved_app_cb:341] Moment view is already shown or call is enabled. moment view [0]
04-27 11:40:53.139+0700 E/WAKEUP-SERVICE( 1679): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
04-27 11:40:53.149+0700 E/WAKEUP-SERVICE( 1679): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
04-27 11:40:53.149+0700 I/TIZEN_N_SOUND_MANAGER( 1679): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Enable start
04-27 11:40:53.169+0700 I/TIZEN_N_SOUND_MANAGER( 1679): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Enable end. (ret: 0x0)
04-27 11:40:53.169+0700 W/TIZEN_N_SOUND_MANAGER( 1679): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
04-27 11:40:53.169+0700 W/WAKEUP-SERVICE( 1679): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 1[0;m
04-27 11:40:53.169+0700 I/HIGEAR  ( 1679): WakeupService.cpp: WakeupServiceStart(393) > [svoice:Application:WakeupServiceStart:IN]
04-27 11:40:53.389+0700 W/CRASH_MANAGER( 2171): worker.c: worker_job(1205) > 1102123726561152480405
