S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2046
Date: 2018-04-27 11:36:25+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2046, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb58af238
r2   = 0xb58af238, r3   = 0xb589b28d
r4   = 0xbe872444, r5   = 0xb5bc1800
r6   = 0x00000274, r7   = 0xbe871be8
r8   = 0xb9115db8, r9   = 0xb6f1c898
r10  = 0xb6f1c778, fp   = 0xb6f1c828
ip   = 0xb5bcc084, sp   = 0xbe871b80
lr   = 0xb589b2af, pc   = 0xb589b3a0
cpsr = 0xa0000030

Memory Information
MemTotal:   405512 KB
MemFree:      3292 KB
Buffers:      6752 KB
Cached:     113260 KB
VmPeak:     120792 KB
VmSize:     118628 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       27768 KB
VmRSS:       26308 KB
VmData:      58396 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:          84 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2046 TID = 2046
2046 2077 2080 2090 

Maps Information
b0c0b000 b0c0e000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b0ef6000 b1e27000 rw-p [stack:2090]
b1e27000 b1e2b000 r-xp /usr/lib/libogg.so.0.7.1
b1e33000 b1e55000 r-xp /usr/lib/libvorbis.so.0.4.3
b1e5d000 b1ea4000 r-xp /usr/lib/libsndfile.so.1.0.26
b1eb0000 b1ef9000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1f02000 b1f07000 r-xp /usr/lib/libjson.so.0.0.1
b37a8000 b38ae000 r-xp /usr/lib/libicuuc.so.57.1
b38c4000 b3a4c000 r-xp /usr/lib/libicui18n.so.57.1
b3a5c000 b3a69000 r-xp /usr/lib/libail.so.0.1.0
b3a72000 b3a75000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3a7d000 b3ab5000 r-xp /usr/lib/libpulse.so.0.16.2
b3ab6000 b3ab9000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3ac1000 b3b22000 r-xp /usr/lib/libasound.so.2.0.0
b3b2c000 b3b45000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3b4e000 b3b52000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3b5a000 b3b65000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3b72000 b3b8a000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3bf8000 b3c7f000 rw-s anon_inode:dmabuf
b3c7f000 b3d06000 rw-s anon_inode:dmabuf
b3d39000 b3d3d000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3d46000 b3d51000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3d59000 b3d5a000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3de1000 b3e68000 rw-s anon_inode:dmabuf
b3e68000 b3eef000 rw-s anon_inode:dmabuf
b3ef0000 b46ef000 rw-p [stack:2080]
b4a03000 b4a0a000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b4a12000 b4a14000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b4a1c000 b4a1d000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b4a25000 b4a2d000 r-xp /usr/lib/libfeedback.so.0.1.4
b4a3d000 b4a3e000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b4a6d000 b4a6e000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4af6000 b52f5000 rw-p [stack:2077]
b52f5000 b52f7000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b52ff000 b5316000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b5323000 b5325000 r-xp /usr/lib/libdri2.so.0.0.0
b532d000 b5338000 r-xp /usr/lib/libtbm.so.1.0.0
b5340000 b5348000 r-xp /usr/lib/libdrm.so.2.4.0
b5350000 b5352000 r-xp /usr/lib/libgenlock.so
b535a000 b535f000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b5367000 b5372000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b557b000 b5645000 r-xp /usr/lib/libCOREGL.so.4.0
b5656000 b5666000 r-xp /usr/lib/libmdm-common.so.1.1.25
b566e000 b5674000 r-xp /usr/lib/libxcb-render.so.0.0.0
b567c000 b567d000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5686000 b5689000 r-xp /usr/lib/libEGL.so.1.4
b5691000 b569f000 r-xp /usr/lib/libGLESv2.so.2.0
b56a8000 b56f1000 r-xp /usr/lib/libmdm.so.1.2.70
b56fa000 b5700000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5708000 b5711000 r-xp /usr/lib/libcom-core.so.0.0.1
b571a000 b57d2000 r-xp /usr/lib/libcairo.so.2.11200.14
b57dd000 b57f6000 r-xp /usr/lib/libnetwork.so.0.0.0
b57fe000 b580a000 r-xp /usr/lib/libnotification.so.0.1.0
b5813000 b5822000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b582b000 b584c000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5854000 b5859000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b5861000 b5866000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b586e000 b587e000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5886000 b588e000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5896000 b58a0000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a45000 b5a4f000 r-xp /lib/libnss_files-2.13.so
b5a58000 b5b27000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5b3d000 b5b61000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b6a000 b5b70000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b78000 b5b7c000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b89000 b5b94000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5b9c000 b5b9e000 r-xp /usr/lib/libiniparser.so.0
b5ba7000 b5bac000 r-xp /usr/lib/libappcore-common.so.1.1
b5bb4000 b5bb6000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5bbf000 b5bc3000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5bd0000 b5bd2000 r-xp /usr/lib/libXau.so.6.0.0
b5bda000 b5be1000 r-xp /lib/libcrypt-2.13.so
b5c11000 b5c13000 r-xp /usr/lib/libiri.so
b5c1c000 b5dae000 r-xp /usr/lib/libcrypto.so.1.0.0
b5dcf000 b5e16000 r-xp /usr/lib/libssl.so.1.0.0
b5e22000 b5e50000 r-xp /usr/lib/libidn.so.11.5.44
b5e58000 b5e61000 r-xp /usr/lib/libcares.so.2.1.0
b5e6b000 b5e7e000 r-xp /usr/lib/libxcb.so.1.1.0
b5e87000 b5e8a000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e92000 b5e94000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e9d000 b5f69000 r-xp /usr/lib/libxml2.so.2.7.8
b5f76000 b5f78000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f81000 b5f86000 r-xp /usr/lib/libffi.so.5.0.10
b5f8e000 b5f8f000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5f97000 b5f9a000 r-xp /lib/libattr.so.1.1.0
b5fa2000 b6036000 r-xp /usr/lib/libstdc++.so.6.0.16
b6049000 b6066000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b6070000 b6088000 r-xp /usr/lib/libpng12.so.0.50.0
b6090000 b60a6000 r-xp /lib/libexpat.so.1.6.0
b60b0000 b60f4000 r-xp /usr/lib/libcurl.so.4.3.0
b60fd000 b6107000 r-xp /usr/lib/libXext.so.6.4.0
b6111000 b6115000 r-xp /usr/lib/libXtst.so.6.1.0
b611d000 b6123000 r-xp /usr/lib/libXrender.so.1.3.0
b612b000 b6131000 r-xp /usr/lib/libXrandr.so.2.2.0
b6139000 b613a000 r-xp /usr/lib/libXinerama.so.1.0.0
b6143000 b614c000 r-xp /usr/lib/libXi.so.6.1.0
b6154000 b6157000 r-xp /usr/lib/libXfixes.so.3.1.0
b6160000 b6162000 r-xp /usr/lib/libXgesture.so.7.0.0
b616a000 b616c000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6174000 b6176000 r-xp /usr/lib/libXdamage.so.1.1.0
b617e000 b6185000 r-xp /usr/lib/libXcursor.so.1.0.2
b618d000 b6190000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b6199000 b619d000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b61a6000 b61ab000 r-xp /usr/lib/libecore_fb.so.1.7.99
b61b4000 b6295000 r-xp /usr/lib/libX11.so.6.3.0
b62a0000 b62c3000 r-xp /usr/lib/libjpeg.so.8.0.2
b62db000 b62f1000 r-xp /lib/libz.so.1.2.5
b62fa000 b62fc000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b6304000 b6379000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6383000 b639d000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b63a5000 b63d9000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b63e2000 b64b5000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b64c1000 b64d1000 r-xp /lib/libresolv-2.13.so
b64d5000 b64ed000 r-xp /usr/lib/liblzma.so.5.0.3
b64f5000 b64f8000 r-xp /lib/libcap.so.2.21
b6500000 b652f000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b6537000 b6538000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b6541000 b6547000 r-xp /usr/lib/libecore_imf.so.1.7.99
b654f000 b6566000 r-xp /usr/lib/liblua-5.1.so
b656f000 b6576000 r-xp /usr/lib/libembryo.so.1.7.99
b657e000 b6584000 r-xp /lib/librt-2.13.so
b658d000 b65e3000 r-xp /usr/lib/libpixman-1.so.0.28.2
b65f1000 b6647000 r-xp /usr/lib/libfreetype.so.6.11.3
b6653000 b667b000 r-xp /usr/lib/libfontconfig.so.1.8.0
b667c000 b66c1000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b66ca000 b66dd000 r-xp /usr/lib/libfribidi.so.0.3.1
b66e5000 b66ff000 r-xp /usr/lib/libecore_con.so.1.7.99
b6709000 b6712000 r-xp /usr/lib/libedbus.so.1.7.99
b671a000 b676a000 r-xp /usr/lib/libecore_x.so.1.7.99
b676c000 b6775000 r-xp /usr/lib/libvconf.so.0.2.45
b677d000 b678e000 r-xp /usr/lib/libecore_input.so.1.7.99
b6796000 b679b000 r-xp /usr/lib/libecore_file.so.1.7.99
b67a3000 b67c5000 r-xp /usr/lib/libecore_evas.so.1.7.99
b67ce000 b680f000 r-xp /usr/lib/libeina.so.1.7.99
b6818000 b6831000 r-xp /usr/lib/libeet.so.1.7.99
b6842000 b68ab000 r-xp /lib/libm-2.13.so
b68b4000 b68ba000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b68c3000 b68c4000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b68cc000 b68ef000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b68f7000 b68fc000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6904000 b692e000 r-xp /usr/lib/libdbus-1.so.3.8.12
b6937000 b694e000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6956000 b6961000 r-xp /lib/libunwind.so.8.0.1
b698e000 b69ac000 r-xp /usr/lib/libsystemd.so.0.4.0
b69b6000 b6ada000 r-xp /lib/libc-2.13.so
b6ae8000 b6af0000 r-xp /lib/libgcc_s-4.6.so.1
b6af1000 b6af5000 r-xp /usr/lib/libsmack.so.1.0.0
b6afe000 b6b04000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b0c000 b6bdc000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6bdd000 b6c3b000 r-xp /usr/lib/libedje.so.1.7.99
b6c45000 b6c5c000 r-xp /usr/lib/libecore.so.1.7.99
b6c73000 b6d41000 r-xp /usr/lib/libevas.so.1.7.99
b6d67000 b6ea3000 r-xp /usr/lib/libelementary.so.1.7.99
b6eba000 b6ece000 r-xp /lib/libpthread-2.13.so
b6ed9000 b6edb000 r-xp /usr/lib/libdlog.so.0.0.0
b6ee3000 b6ee6000 r-xp /usr/lib/libbundle.so.0.1.22
b6eee000 b6ef0000 r-xp /lib/libdl-2.13.so
b6ef9000 b6f06000 r-xp /usr/lib/libaul.so.0.1.0
b6f18000 b6f1e000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f27000 b6f2b000 r-xp /usr/lib/libsys-assert.so
b6f34000 b6f51000 r-xp /lib/ld-2.13.so
b6f5a000 b6f5f000 r-xp /usr/bin/launchpad-loader
b80bc000 b913d000 rw-p [heap]
be852000 be873000 rw-p [stack]
b80bc000 b913d000 rw-p [heap]
be852000 be873000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2046)
Call Stack Count: 21
 0: loadAllNotification + 0xf (0xb589b3a0) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x53a0
 1: _on_resume_cb + 0x22 (0xb589b2af) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x52af
 2: (0xb5bc06ad) [/usr/lib/libcapi-appfw-application.so.0] + 0x16ad
 3: (0xb6f1b251) [/usr/lib/libappcore-efl.so.1] + 0x3251
 4: (0xb5ba8d1f) [/usr/lib/libappcore-common.so.1] + 0x1d1f
 5: (0xb6efd495) [/usr/lib/libaul.so.0] + 0x4495
 6: (0xb6efd927) [/usr/lib/libaul.so.0] + 0x4927
 7: (0xb6efdfc1) [/usr/lib/libaul.so.0] + 0x4fc1
 8: (0xb6efdf87) [/usr/lib/libaul.so.0] + 0x4f87
 9: g_main_context_dispatch + 0xbc (0xb6b417a9) [/usr/lib/libglib-2.0.so.0] + 0x357a9
10: (0xb6c55ca7) [/usr/lib/libecore.so.1] + 0x10ca7
11: (0xb6c50b4f) [/usr/lib/libecore.so.1] + 0xbb4f
12: (0xb6c515a7) [/usr/lib/libecore.so.1] + 0xc5a7
13: ecore_main_loop_begin + 0x30 (0xb6c51879) [/usr/lib/libecore.so.1] + 0xc879
14: appcore_efl_main + 0x332 (0xb6f1bb47) [/usr/lib/libappcore-efl.so.1] + 0x3b47
15: ui_app_main + 0xb0 (0xb5bc0ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16: uib_app_run + 0xea (0xb589b227) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5227
17: main + 0x34 (0xb589b641) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5641
18:  + 0x0 (0xb6f5ba53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
19: __libc_start_main + 0x114 (0xb69cd85c) [/lib/libc.so.6] + 0x1785c
20: (0xb6f5be0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
) > MessagePort message received
04-27 11:36:23.359+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-27 11:36:23.359+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-27 11:36:23.359+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 11:36:23.359+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-home:music-control-service-message-port]
04-27 11:36:23.359+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-27 11:36:23.359+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-27 11:36:23.359+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 11:36:23.359+0700 W/W_HOME  ( 1203): clock_shortcut.c: _music_service_messageport_cb(361) > mode:remote state:unknown 
04-27 11:36:23.359+0700 E/W_HOME  ( 1203): clock_shortcut.c: _mp_state_get(104) > (s_info.music_service.state != 1) -> _mp_state_get() return
04-27 11:36:23.359+0700 W/W_HOME  ( 1203): clock_shortcut.c: _music_service_messageport_cb(361) > mode:remote state:unknown 
04-27 11:36:23.359+0700 E/W_HOME  ( 1203): clock_shortcut.c: _mp_state_get(104) > (s_info.music_service.state != 1) -> _mp_state_get() return
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-home:music-control-service-message-port]
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-home:music-control-service-message-port]
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
04-27 11:36:23.369+0700 I/MESSAGE_PORT(  965): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
04-27 11:36:23.619+0700 E/EFL     ( 1203): ecore_x<1203> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=126878 button=1
04-27 11:36:23.619+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:23.619+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 11:36:23.619+0700 W/W_HOME  ( 1203): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:36:23.639+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:23.639+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:23.649+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:23.649+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:23.659+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:23.659+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:23.669+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:23.669+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:23.679+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:23.679+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:23.679+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4292 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] add hold animator
04-27 11:36:23.679+0700 W/W_HOME  ( 1203): home_navigation.c: _drag_start_cb(1262) > drag start
04-27 11:36:23.689+0700 W/W_HOME  ( 1203): home_navigation.c: _is_rightedge(187) > (720 360) not right edge: 0 0 0xb7a64708 -> 360 0 0xb7bde2c8
04-27 11:36:23.689+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 11:36:23.699+0700 W/W_HOME  ( 1203): event_manager.c: _home_scroll_cb(579) > scroll,will,start
04-27 11:36:23.699+0700 W/W_HOME  ( 1203): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 2, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:36:23.699+0700 W/W_HOME  ( 1203): event_manager.c: _home_scroll_cb(579) > scroll,start
04-27 11:36:23.699+0700 W/W_HOME  ( 1203): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 3, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:36:23.699+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:23.699+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:23.699+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb79d2bf8 : elm_scroller] direction_x(1), direction_y(0)
04-27 11:36:23.699+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb79d2bf8 : elm_scroller] drag_child_locked_x(0)
04-27 11:36:23.699+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb79d2bf8 : elm_scroller] move content x(639), y(0)
04-27 11:36:23.699+0700 W/W_HOME  ( 1203): event_manager.c: _clock_view_obscured_cb(645) > state: 1 -> 0
04-27 11:36:23.699+0700 W/W_HOME  ( 1203): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 3, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:36:23.699+0700 W/W_INDICATOR( 1126): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 11:36:23.699+0700 W/W_INDICATOR( 1126): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 11:36:23.709+0700 W/W_HOME  ( 1203): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 3, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:36:23.709+0700 I/ELM_RPANEL( 1203): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
04-27 11:36:23.709+0700 E/W_HOME  ( 1203): page_indicator.c: page_indicator_item_at(292) > (index < 0 || index >= PAGE_INDICATOR_MAX) -> page_indicator_item_at() return
04-27 11:36:23.709+0700 E/W_HOME  ( 1203): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
04-27 11:36:23.759+0700 E/EFL     ( 1203): ecore_x<1203> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=126997 button=1
04-27 11:36:23.759+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:23.759+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:23.759+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:23.759+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:23.759+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:23.759+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:23.769+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2676 _elm_scroll_scroll_to_x() [0xb79d2bf8 : elm_scroller] t_in(0.120000), pos_x(360)
04-27 11:36:23.769+0700 W/W_HOME  ( 1203): home_navigation.c: _anim_start_cb(1293) > anim start
04-27 11:36:23.769+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 11:36:23.779+0700 W/W_HOME  ( 1203): home_navigation.c: _up_cb(1250) > up
04-27 11:36:23.779+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 11:36:23.779+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_add(850) > add timer:1
04-27 11:36:23.779+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_add(854) > add timer:2
04-27 11:36:23.779+0700 W/W_HOME  ( 1203): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 3, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:36:23.789+0700 W/W_HOME  ( 1203): index.c: index_show(300) > is_paused:0 show VI:1 visibility:0 vi:(nil)
04-27 11:36:23.789+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb79d2bf8 : elm_scroller] time(0.153600)
04-27 11:36:23.789+0700 I/ELM_RPANEL( 1203): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
04-27 11:36:23.789+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb79d2bf8 : elm_scroller] ECORE_CALLBACK_RENEW : px(596), py(0)
04-27 11:36:23.809+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb79d2bf8 : elm_scroller] time(0.681507)
04-27 11:36:23.809+0700 I/wnotib  ( 1203): w-notification-board-common.c: wnb_common_set_panel_displayed_state(4327) > Set is_notiboard_displayed to 1.
04-27 11:36:23.819+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_activate(3236) > page_index: 0.
04-27 11:36:23.819+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_select(586) > 0
04-27 11:36:23.819+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_select(596) > select index:1
04-27 11:36:23.819+0700 E/W_HOME  ( 1203): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
04-27 11:36:23.819+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_show(539) > 
04-27 11:36:23.819+0700 W/W_HOME  ( 1203): index.c: index_show(300) > is_paused:0 show VI:1 visibility:1 vi:0xb040b4c0
04-27 11:36:23.819+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: wnb_nm_control_home_indicator(59) > Hide home indicator.
04-27 11:36:23.819+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_noti_indicator_hide(496) > 
04-27 11:36:23.819+0700 E/wnoti-service( 1362): wnoti-server-mgr-stub.c: __wnoti_change_new_flag_stub(3143) > change_type : 0, value : 0
04-27 11:36:23.829+0700 I/ELM_RPANEL( 1203): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
04-27 11:36:23.829+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb79d2bf8 : elm_scroller] ECORE_CALLBACK_RENEW : px(448), py(0)
04-27 11:36:23.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb79d2bf8 : elm_scroller] time(0.938056)
04-27 11:36:23.849+0700 I/ELM_RPANEL( 1203): elm-rpanel.c: _panel_swap_effect(3376) > tobj_item_02 is null
04-27 11:36:23.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb79d2bf8 : elm_scroller] ECORE_CALLBACK_RENEW : px(377), py(0)
04-27 11:36:23.869+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb79d2bf8 : elm_scroller] time(0.996850)
04-27 11:36:23.879+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2564 _elm_scroll_scroll_to_x_animator() [0xb79d2bf8 : elm_scroller] ECORE_CALLBACK_RENEW : px(360), py(0)
04-27 11:36:23.889+0700 W/WATCH_CORE( 1299): appcore-watch.c: __widget_pause(1113) > widget_pause
04-27 11:36:23.889+0700 W/AUL     ( 1299): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.techgraphy.DigitalTick) pid(1299) status(bg) type(watchapp)
04-27 11:36:23.889+0700 E/watchface-app( 1299): watchface.cpp: OnAppPause(1122) > 
04-27 11:36:23.899+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2541 _elm_scroll_scroll_to_x_animator() [0xb79d2bf8 : elm_scroller] time(0.979963)
04-27 11:36:23.899+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2556 _elm_scroll_scroll_to_x_animator() [0xb79d2bf8 : elm_scroller] animation stop!!
04-27 11:36:23.899+0700 W/wnotib  ( 1203): w-notification-board-panel-manager.c: _wnb_pm_anim_stop_cb(96) > notiboard scroller anim stop [360][0][360][360]
04-27 11:36:23.909+0700 W/wnotib  ( 1203): w-notification-board-noti-manager.c: wnb_nm_do_postponed_job(981) > No postponed update with is_for_VI: 1.
04-27 11:36:23.909+0700 W/W_HOME  ( 1203): home_navigation.c: _anim_stop_cb(1319) > anim stop
04-27 11:36:23.909+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 11:36:23.909+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_add(850) > add timer:1
04-27 11:36:23.909+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_add(854) > add timer:2
04-27 11:36:23.909+0700 I/efl-extension( 1203): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(489) > [0xb79d2bf8 : elm_scroller] detent_count(0)
04-27 11:36:23.909+0700 I/efl-extension( 1203): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(490) > [0xb79d2bf8 : elm_scroller] pagenumber_v(0), pagenumber_h(1)
04-27 11:36:23.909+0700 I/efl-extension( 1203): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_scroll_animatioin_stop_cb(512) > [0xb79d2bf8 : elm_scroller] CurrentPage(1)
04-27 11:36:23.909+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:2559 _elm_scroll_scroll_to_x_animator() [0xb79d2bf8 : elm_scroller] ECORE_CALLBACK_CANCEL : px(360), py(0)
04-27 11:36:24.019+0700 E/W_HOME  ( 1203): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
04-27 11:36:24.029+0700 W/W_HOME  ( 1203): event_manager.c: _home_scroll_cb(579) > scroll,will,done
04-27 11:36:24.029+0700 W/W_HOME  ( 1203): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 1, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:36:24.029+0700 W/W_HOME  ( 1203): event_manager.c: _home_scroll_cb(579) > scroll,done
04-27 11:36:24.029+0700 W/W_HOME  ( 1203): event_manager.c: _state_control(176) > control:5, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:36:24.469+0700 E/EFL     ( 1203): ecore_x<1203> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=127732 button=1
04-27 11:36:24.469+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:24.469+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 11:36:24.479+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:24.479+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:24.489+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:24.489+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:24.549+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:24.549+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:24.569+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] mouse move
04-27 11:36:24.569+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb79d2bf8 : elm_scroller] hold(0), freeze(0)
04-27 11:36:24.589+0700 E/EFL     ( 1203): ecore_x<1203> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=127841 button=1
04-27 11:36:24.589+0700 W/W_HOME  ( 1203): home_navigation.c: _up_cb(1250) > up
04-27 11:36:24.589+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_del(822) > delete timer
04-27 11:36:24.589+0700 W/W_HOME  ( 1203): home_navigation.c: _nav_finish_timer_add(854) > add timer:2
04-27 11:36:24.589+0700 W/W_HOME  ( 1203): noti_broker.c: _handler_indicator_hide(550) > 
04-27 11:36:24.589+0700 W/W_HOME  ( 1203): index.c: index_hide(338) > hide VI:1 visibility:1 vi:(nil)
04-27 11:36:24.639+0700 W/wnotib  ( 1203): w-notification-board-basic-panel.c: wnb_bp_create_second_depth_view(2700) > is_real_canvas: 0, launch_thread_list_view: 0, noti_detail_type: 0
04-27 11:36:24.649+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:36:24.659+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c9e508 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:36:24.659+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c9e508 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:36:24.679+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:36:24.679+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:36:24.679+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:36:24.679+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:36:24.719+0700 E/EFL     ( 1203): evas_main<1203> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:36:24.719+0700 E/EFL     ( 1203): evas_main<1203> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:36:24.719+0700 E/EFL     ( 1203): evas_main<1203> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:36:24.719+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: app.icon
04-27 11:36:24.719+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: vi.app.icon
04-27 11:36:24.719+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: badge.image
04-27 11:36:24.719+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:36:24.739+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:36:24.739+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 11:36:24.739+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-27 11:36:24.739+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:36:24.739+0700 I/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1232, 123, -1016.
04-27 11:36:24.749+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c9e508 : elm_genlist] mx(0), my(46), minx(0), miny(0), px(0), py(0)
04-27 11:36:24.749+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c9e508 : elm_genlist] cw(360), ch(406), pw(360), ph(360)
04-27 11:36:24.759+0700 W/ELM_RPANEL( 1203): elm-rpanel.c: _realized_cb(6399) > Manually render for dump image.
04-27 11:36:24.899+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_set_current_detail_noti(1441) > Set current_detail_noti_db_id to 0
04-27 11:36:24.899+0700 I/efl-extension( 1203): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb7c9e508, obj: 0xb7c9e508
04-27 11:36:24.909+0700 I/efl-extension( 1203): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-27 11:36:24.919+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c9e508 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:36:24.919+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c9e508 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:36:24.919+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c9e508 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:36:24.919+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c9e508 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:36:24.919+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 11:36:24.919+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7c9e508, elm_genlist, func : 0xb6835ea1
04-27 11:36:24.919+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 11:36:25.049+0700 I/GATE    (  971): <GATE-M>BATTERY_LEVEL_45</GATE-M>
04-27 11:36:25.049+0700 I/watchface-viewer( 1299): viewer-data-provider.cpp: AddPendingChanges(2527) > added [60] to pending list
04-27 11:36:25.049+0700 W/W_INDICATOR( 1126): windicator_battery.c: windicator_battery_update(98) > [windicator_battery_update:98] 
04-27 11:36:25.059+0700 W/W_INDICATOR( 1126): windicator_battery.c: _battery_icon_update(312) > [_battery_icon_update:312] battery level(45), length(2)
04-27 11:36:25.059+0700 W/W_INDICATOR( 1126): windicator_battery.c: _battery_icon_update(336) > [_battery_icon_update:336] 45%
04-27 11:36:25.059+0700 W/W_INDICATOR( 1126): windicator_battery.c: _battery_icon_update(351) > [_battery_icon_update:351] battery_level: 45, isCharging: 0
04-27 11:36:25.059+0700 W/W_INDICATOR( 1126): windicator_battery.c: _battery_icon_update(385) > [_battery_icon_update:385] battery file : change_level_45
04-27 11:36:25.059+0700 W/W_INDICATOR( 1126): windicator_battery.c: _battery_icon_update(464) > [_battery_icon_update:464] [ATT] Battery level : 45
04-27 11:36:25.059+0700 W/W_INDICATOR( 1126): windicator_battery.c: _battery_icon_update(531) > [_battery_icon_update:531] Normal charging status !!
04-27 11:36:25.449+0700 I/wnotib  ( 1203): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 8, ret: -3, request_id: 0
04-27 11:36:25.449+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_rpanel_item_panel_clicked_cb(4440) > noti_detail_type: 0
04-27 11:36:25.459+0700 W/wnotib  ( 1203): w-notification-board-basic-panel.c: wnb_bp_create_second_depth_view(2700) > is_real_canvas: 1, launch_thread_list_view: 0, noti_detail_type: 0
04-27 11:36:25.459+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_set_scroll_config(521) > Before thumbscroll_sensitivity_friction: 1.600000
04-27 11:36:25.459+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_set_scroll_config(523) > After thumbscroll_sensitivity_friction: 0.300000
04-27 11:36:25.459+0700 I/wnotib  ( 1203): w-notification-board-common.c: wnb_common_get_first_action_info(6221) > No need to add default actions for companion noti.
04-27 11:36:25.469+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:36:25.469+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_set_current_detail_noti(1441) > Set current_detail_noti_db_id to 1232
04-27 11:36:25.469+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7c9dac0, elm_genlist, _activated_obj : 0xb79f7020, activated : 1
04-27 11:36:25.469+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:36:25.479+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c9dac0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:36:25.479+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c9dac0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:36:25.489+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:36:25.489+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:36:25.489+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:36:25.489+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:36:25.529+0700 E/EFL     ( 1203): evas_main<1203> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:36:25.529+0700 E/EFL     ( 1203): evas_main<1203> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:36:25.529+0700 E/EFL     ( 1203): evas_main<1203> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:36:25.529+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: app.icon
04-27 11:36:25.529+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: vi.app.icon
04-27 11:36:25.529+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: badge.image
04-27 11:36:25.529+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:36:25.539+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:36:25.539+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 11:36:25.539+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-27 11:36:25.539+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:36:25.539+0700 I/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1232, 123, -1016.
04-27 11:36:25.549+0700 I/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_init_action_drawer_for_second_depth(950) > Display the action drawer for 1232, 123, -1016
04-27 11:36:25.549+0700 W/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_init_action_drawer_for_second_depth(966) > Initialize the action drawer.
04-27 11:36:25.549+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_initialize(5603) > Init drawer.
04-27 11:36:25.549+0700 I/efl-extension( 1203): efl_extension_more_option.c: eext_more_option_add(325) > called!!
04-27 11:36:25.549+0700 I/efl-extension( 1203): efl_extension_more_option.c: _drawer_layout_create(185) > called!!
04-27 11:36:25.549+0700 I/efl-extension( 1203): efl_extension_more_option.c: _more_option_data_init(248) > mold is initialized!!
04-27 11:36:25.549+0700 I/efl-extension( 1203): efl_extension_more_option.c: _panel_create(204) > called!!
04-27 11:36:25.569+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_add(2165) > called!!
04-27 11:36:25.579+0700 E/EFL     (  936): ecore_x<936> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x2400003 time=127841
04-27 11:36:25.579+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: _eext_rotary_selector_data_init(591) > rsd is initialized!!
04-27 11:36:25.589+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:36:25.589+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: _event_area_callback_add(505) > called!!
04-27 11:36:25.599+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: _rotary_selector_show_cb(810) > called!!
04-27 11:36:25.599+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: _items_invalidate(924) > item_count is zero!!
04-27 11:36:25.599+0700 I/wnotib  ( 1203): w-notification-board-common.c: wnb_common_create_sending_popup(6515) > progress_start: 0, use_requesting: 0.
04-27 11:36:25.649+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xae1abf50 : elm_scroller] mx(360), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:36:25.649+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xae1abf50 : elm_scroller] cw(360), ch(0), pw(0), ph(0)
04-27 11:36:25.649+0700 W/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_init_action_drawer_for_second_depth(1011) > Set the drawer item now.
04-27 11:36:25.649+0700 W/wnotib  ( 1203): w-notification-board-action.c: wnb_action_set_item_info(5240) > db_id: 1232, category_id: 123, application_id: -1016, clear_all_items: 1
04-27 11:36:25.649+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_terminate_input_selector(5550) > No need to close w-input-selector.
04-27 11:36:25.649+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:36:25.669+0700 W/AUL_AMD (  969): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-27 11:36:25.669+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_terminate_noti_composer(5585) > ret : 0, is_running : 0
04-27 11:36:25.669+0700 I/efl-extension( 1203): efl_extension_more_option.c: eext_more_option_items_clear(572) > called!!
04-27 11:36:25.669+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_items_clear(2473) > called!!
04-27 11:36:25.669+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_create_pages(4129) > Create drawer pages for 123, -1016
04-27 11:36:25.669+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_create_pages(4929) > No need to add default actions for companion noti.
04-27 11:36:25.669+0700 I/efl-extension( 1203): efl_extension_more_option.c: eext_more_option_item_append(458) > called!!
04-27 11:36:25.669+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_item_append(2255) > called!!
04-27 11:36:25.689+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_item_domain_translatable_part_text_set(2579) > called!!
04-27 11:36:25.689+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_item_part_content_set(2638) > called!!
04-27 11:36:25.689+0700 I/efl-extension( 1203): efl_extension_more_option.c: eext_more_option_item_append(458) > called!!
04-27 11:36:25.689+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_item_append(2255) > called!!
04-27 11:36:25.699+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_item_domain_translatable_part_text_set(2579) > called!!
04-27 11:36:25.699+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_item_part_content_set(2638) > called!!
04-27 11:36:25.699+0700 I/efl-extension( 1203): efl_extension_more_option.c: eext_more_option_item_append(458) > called!!
04-27 11:36:25.699+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_item_append(2255) > called!!
04-27 11:36:25.709+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_item_domain_translatable_part_text_set(2579) > called!!
04-27 11:36:25.709+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_item_part_content_set(2638) > called!!
04-27 11:36:25.709+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_create_pages(5030) > Number of pages: 3 for -1016
04-27 11:36:25.729+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:36:25.729+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:36:25.729+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:36:25.729+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1203): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:36:25.769+0700 E/EFL     ( 1203): evas_main<1203> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:36:25.769+0700 E/EFL     ( 1203): evas_main<1203> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:36:25.769+0700 E/EFL     ( 1203): evas_main<1203> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:36:25.779+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: app.icon
04-27 11:36:25.779+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: vi.app.icon
04-27 11:36:25.779+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: badge.image
04-27 11:36:25.779+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:36:25.779+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:36:25.779+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 11:36:25.779+0700 W/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-27 11:36:25.779+0700 I/wnotib  ( 1203): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:36:25.779+0700 I/wnotib  ( 1203): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1232, 123, -1016.
04-27 11:36:25.799+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7c9dac0 : elm_genlist] mx(0), my(37), minx(0), miny(0), px(0), py(0)
04-27 11:36:25.799+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7c9dac0 : elm_genlist] cw(360), ch(397), pw(360), ph(360)
04-27 11:36:25.809+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xae1abf50 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:36:25.809+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xae1abf50 : elm_scroller] cw(360), ch(0), pw(360), ph(0)
04-27 11:36:25.819+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xae1abf50 : elm_scroller] mx(0), my(360), minx(0), miny(0), px(0), py(0)
04-27 11:36:25.819+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xae1abf50 : elm_scroller] cw(360), ch(360), pw(360), ph(0)
04-27 11:36:25.819+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xae1abf50 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:36:25.819+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xae1abf50 : elm_scroller] cw(360), ch(360), pw(360), ph(360)
04-27 11:36:25.839+0700 E/EFL     ( 1203): ecore_x<1203> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=127841
04-27 11:36:25.839+0700 E/EFL     ( 1203): ecore_x<1203> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=128963 button=1
04-27 11:36:25.839+0700 E/EFL     ( 1203): ecore_x<1203> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=129061 button=1
04-27 11:36:25.849+0700 E/EFL     (  936): ecore_x<936> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=127841
04-27 11:36:25.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c9dac0 : elm_genlist] mouse move
04-27 11:36:25.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c9dac0 : elm_genlist] hold(0), freeze(0)
04-27 11:36:25.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c9dac0 : elm_genlist] mouse move
04-27 11:36:25.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c9dac0 : elm_genlist] hold(0), freeze(0)
04-27 11:36:25.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c9dac0 : elm_genlist] mouse move
04-27 11:36:25.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c9dac0 : elm_genlist] hold(0), freeze(0)
04-27 11:36:25.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c9dac0 : elm_genlist] mouse move
04-27 11:36:25.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c9dac0 : elm_genlist] hold(0), freeze(0)
04-27 11:36:25.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb7c9dac0 : elm_genlist] mouse move
04-27 11:36:25.849+0700 E/EFL     ( 1203): elementary<1203> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb7c9dac0 : elm_genlist] hold(0), freeze(0)
04-27 11:36:25.849+0700 W/wnotib  ( 1203): w-notification-board-basic-panel.c: _wnb_bp_inline_action_btn_click_cb(4770) > ACTION BUTTON IS CLICKED~~~~~~~~~~
04-27 11:36:25.849+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_open_app_cb(3281) > Action clicked for 123, -1016
04-27 11:36:25.849+0700 W/AUL     ( 1203): launch.c: app_request_to_launchpad(284) > request cmd(0) to(com.toyota.realtimefeedback)
04-27 11:36:25.849+0700 W/AUL_AMD (  969): amd_request.c: __request_handler(669) > __request_handler: 0
04-27 11:36:25.859+0700 W/AUL_AMD (  969): amd_launch.c: _start_app(1782) > caller pid : 1203
04-27 11:36:25.859+0700 I/AUL_AMD (  969): amd_launch.c: __check_app_control_privilege(1693) > Skip the privilege check in case of preloaded apps
04-27 11:36:25.869+0700 W/AUL     (  969): app_signal.c: aul_send_app_resume_request_signal(567) > aul_send_app_resume_request_signal app(com.toyota.realtimefeedback) pid(2046) type(uiapp) bg(0)
04-27 11:36:25.879+0700 W/AUL_AMD (  969): amd_launch.c: __nofork_processing(1229) > __nofork_processing, cmd: 0, pid: 2046
04-27 11:36:25.879+0700 W/STARTER ( 1125): pkg-monitor.c: _app_mgr_status_cb(421) > [_app_mgr_status_cb:421] Resume request [2046]
04-27 11:36:25.879+0700 W/AUL_AMD (  969): amd_launch.c: __reply_handler(999) > listen fd(22) , send fd(18), pid(2046), cmd(0)
04-27 11:36:25.879+0700 W/AUL     ( 1203): launch.c: app_request_to_launchpad(298) > request cmd(0) result(2046)
04-27 11:36:25.879+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_trigger_action_timer(5096) > Remove previous action timer for 1232, -1016.
04-27 11:36:25.879+0700 W/wnotib  ( 1203): w-notification-board-common.c: wnb_common_perform_auto_deletion(3824) > auto_remove is set
04-27 11:36:25.879+0700 I/wnotib  ( 1203): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 21, ret: -3, request_id: 0
04-27 11:36:25.879+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_open_app_cb(3326) > Finish action successfully.
04-27 11:36:25.879+0700 I/wnotib  ( 1203): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 24, ret: -3, request_id: 0
04-27 11:36:25.879+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: _item_update_animator_cb(1546) > called
04-27 11:36:25.879+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: _item_update_animator_cb(1568) > item_list(0x-1337172688), count(3)
04-27 11:36:25.889+0700 I/APP_CORE( 2046): appcore-efl.c: __do_app(453) > [APP 2046] Event: RESET State: PAUSED
04-27 11:36:25.889+0700 I/CAPI_APPFW_APPLICATION( 2046): app_main.c: _ui_app_appcore_reset(645) > app_appcore_reset
04-27 11:36:25.889+0700 I/APP_CORE( 2046): appcore-efl.c: __do_app(529) > Legacy lifecycle: 0
04-27 11:36:25.889+0700 I/APP_CORE( 2046): appcore-efl.c: __do_app(531) > [APP 2046] App already running, raise the window
04-27 11:36:25.899+0700 W/AUL     (  969): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.toyota.realtimefeedback) pid(2046) status(fg) type(uiapp)
04-27 11:36:25.899+0700 I/APP_CORE( 2046): appcore-efl.c: __do_app(535) > [APP 2046] Call the resume_cb
04-27 11:36:25.899+0700 I/CAPI_APPFW_APPLICATION( 2046): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
04-27 11:36:25.909+0700 I/efl-extension( 1203): efl_extension_more_option.c: _panel_inactive_cb(99) > more_option is closed!!
04-27 11:36:25.909+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_closed(5424) > More option closed!
04-27 11:36:25.919+0700 I/wnotib  ( 1203): w-notification-board-action.c: _wnb_action_closed(5427) > Set the rotary focus to the parent of drawer: 0xb7c9dac0
04-27 11:36:25.919+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7c9dac0, elm_genlist, _activated_obj : 0xb7c9dac0, activated : 1
04-27 11:36:25.919+0700 I/efl-extension( 1203): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xae10d220, elm_layout, _activated_obj : 0xb7c9dac0, activated : 0
04-27 11:36:25.919+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_selected_item_get(3146) > called!!
04-27 11:36:25.919+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_selected_item_set(3107) > called!!
04-27 11:36:25.919+0700 I/efl-extension( 1203): efl_extension_rotary_selector.c: eext_rotary_selector_selected_item_set(3140) > selected item index(0)
04-27 11:36:25.919+0700 W/efl-extension( 1203): efl_extension_events.c: eext_object_event_callback_del(325) > This object(0xae10baa0) hasn't been registered before
04-27 11:36:25.929+0700 W/STARTER ( 1125): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2046] goes to (3)
04-27 11:36:26.459+0700 W/CRASH_MANAGER( 2094): worker.c: worker_job(1205) > 1102046726561152480378
