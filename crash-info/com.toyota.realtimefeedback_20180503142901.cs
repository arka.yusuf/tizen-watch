S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 3314
Date: 2018-05-03 14:29:01+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: 1
      address not mapped to object
      si_addr = 0x28

Register Information
r0   = 0xb590d1e0, r1   = 0x00000000
r2   = 0x00000000, r3   = 0x00000000
r4   = 0xb7d21420, r5   = 0x00000000
r6   = 0xb7d32220, r7   = 0xbeead210
r8   = 0x00000000, r9   = 0xb7d32220
r10  = 0xbeead2a8, fp   = 0x00000000
ip   = 0xb7ceb738, sp   = 0xbeead190
lr   = 0xb58f83c1, pc   = 0xb58f8848
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:      8740 KB
Buffers:      4892 KB
Cached:     104004 KB
VmPeak:      56540 KB
VmSize:      52440 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       18796 KB
VmRSS:       16952 KB
VmData:      22628 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       20076 KB
VmPTE:          46 KB
VmSwap:          0 KB

Threads Information
Threads: 2
PID = 3314 TID = 3314
3314 3338 

Maps Information
b3c4e000 b3c51000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3c59000 b3ce0000 rw-s anon_inode:dmabuf
b3ce0000 b3d67000 rw-s anon_inode:dmabuf
b3e42000 b3ec9000 rw-s anon_inode:dmabuf
b3ec9000 b3f50000 rw-s anon_inode:dmabuf
b4a0c000 b4a0d000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b4aca000 b4acb000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b53000 b5352000 rw-p [stack:3338]
b5352000 b5354000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b535c000 b5373000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b5380000 b5382000 r-xp /usr/lib/libdri2.so.0.0.0
b538a000 b5395000 r-xp /usr/lib/libtbm.so.1.0.0
b539d000 b53a5000 r-xp /usr/lib/libdrm.so.2.4.0
b53ad000 b53af000 r-xp /usr/lib/libgenlock.so
b53b7000 b53bc000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b53c4000 b53cf000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b55d8000 b56a2000 r-xp /usr/lib/libCOREGL.so.4.0
b56b3000 b56c3000 r-xp /usr/lib/libmdm-common.so.1.1.25
b56cb000 b56d1000 r-xp /usr/lib/libxcb-render.so.0.0.0
b56d9000 b56da000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b56e3000 b56e6000 r-xp /usr/lib/libEGL.so.1.4
b56ee000 b56fc000 r-xp /usr/lib/libGLESv2.so.2.0
b5705000 b574e000 r-xp /usr/lib/libmdm.so.1.2.70
b5757000 b575d000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5765000 b576e000 r-xp /usr/lib/libcom-core.so.0.0.1
b5777000 b582f000 r-xp /usr/lib/libcairo.so.2.11200.14
b583a000 b5853000 r-xp /usr/lib/libnetwork.so.0.0.0
b585b000 b5867000 r-xp /usr/lib/libnotification.so.0.1.0
b5870000 b587f000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b5888000 b58a9000 r-xp /usr/lib/libefl-extension.so.0.1.0
b58b1000 b58b6000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b58be000 b58c3000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b58cb000 b58db000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b58e3000 b58eb000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b58f3000 b58fd000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5aa2000 b5aac000 r-xp /lib/libnss_files-2.13.so
b5ab5000 b5b84000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5b9a000 b5bbe000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5bc7000 b5bcd000 r-xp /usr/lib/libappsvc.so.0.1.0
b5bd5000 b5bd9000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5be6000 b5bf1000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5bf9000 b5bfb000 r-xp /usr/lib/libiniparser.so.0
b5c04000 b5c09000 r-xp /usr/lib/libappcore-common.so.1.1
b5c11000 b5c13000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5c1c000 b5c20000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5c2d000 b5c2f000 r-xp /usr/lib/libXau.so.6.0.0
b5c37000 b5c3e000 r-xp /lib/libcrypt-2.13.so
b5c6e000 b5c70000 r-xp /usr/lib/libiri.so
b5c79000 b5e0b000 r-xp /usr/lib/libcrypto.so.1.0.0
b5e2c000 b5e73000 r-xp /usr/lib/libssl.so.1.0.0
b5e7f000 b5ead000 r-xp /usr/lib/libidn.so.11.5.44
b5eb5000 b5ebe000 r-xp /usr/lib/libcares.so.2.1.0
b5ec8000 b5edb000 r-xp /usr/lib/libxcb.so.1.1.0
b5ee4000 b5ee7000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5eef000 b5ef1000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5efa000 b5fc6000 r-xp /usr/lib/libxml2.so.2.7.8
b5fd3000 b5fd5000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5fde000 b5fe3000 r-xp /usr/lib/libffi.so.5.0.10
b5feb000 b5fec000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5ff4000 b5ff7000 r-xp /lib/libattr.so.1.1.0
b5fff000 b6093000 r-xp /usr/lib/libstdc++.so.6.0.16
b60a6000 b60c3000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b60cd000 b60e5000 r-xp /usr/lib/libpng12.so.0.50.0
b60ed000 b6103000 r-xp /lib/libexpat.so.1.6.0
b610d000 b6151000 r-xp /usr/lib/libcurl.so.4.3.0
b615a000 b6164000 r-xp /usr/lib/libXext.so.6.4.0
b616e000 b6172000 r-xp /usr/lib/libXtst.so.6.1.0
b617a000 b6180000 r-xp /usr/lib/libXrender.so.1.3.0
b6188000 b618e000 r-xp /usr/lib/libXrandr.so.2.2.0
b6196000 b6197000 r-xp /usr/lib/libXinerama.so.1.0.0
b61a0000 b61a9000 r-xp /usr/lib/libXi.so.6.1.0
b61b1000 b61b4000 r-xp /usr/lib/libXfixes.so.3.1.0
b61bd000 b61bf000 r-xp /usr/lib/libXgesture.so.7.0.0
b61c7000 b61c9000 r-xp /usr/lib/libXcomposite.so.1.0.0
b61d1000 b61d3000 r-xp /usr/lib/libXdamage.so.1.1.0
b61db000 b61e2000 r-xp /usr/lib/libXcursor.so.1.0.2
b61ea000 b61ed000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b61f6000 b61fa000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6203000 b6208000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6211000 b62f2000 r-xp /usr/lib/libX11.so.6.3.0
b62fd000 b6320000 r-xp /usr/lib/libjpeg.so.8.0.2
b6338000 b634e000 r-xp /lib/libz.so.1.2.5
b6357000 b6359000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b6361000 b63d6000 r-xp /usr/lib/libsqlite3.so.0.8.6
b63e0000 b63fa000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6402000 b6436000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b643f000 b6512000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b651e000 b652e000 r-xp /lib/libresolv-2.13.so
b6532000 b654a000 r-xp /usr/lib/liblzma.so.5.0.3
b6552000 b6555000 r-xp /lib/libcap.so.2.21
b655d000 b658c000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b6594000 b6595000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b659e000 b65a4000 r-xp /usr/lib/libecore_imf.so.1.7.99
b65ac000 b65c3000 r-xp /usr/lib/liblua-5.1.so
b65cc000 b65d3000 r-xp /usr/lib/libembryo.so.1.7.99
b65db000 b65e1000 r-xp /lib/librt-2.13.so
b65ea000 b6640000 r-xp /usr/lib/libpixman-1.so.0.28.2
b664e000 b66a4000 r-xp /usr/lib/libfreetype.so.6.11.3
b66b0000 b66d8000 r-xp /usr/lib/libfontconfig.so.1.8.0
b66d9000 b671e000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b6727000 b673a000 r-xp /usr/lib/libfribidi.so.0.3.1
b6742000 b675c000 r-xp /usr/lib/libecore_con.so.1.7.99
b6766000 b676f000 r-xp /usr/lib/libedbus.so.1.7.99
b6777000 b67c7000 r-xp /usr/lib/libecore_x.so.1.7.99
b67c9000 b67d2000 r-xp /usr/lib/libvconf.so.0.2.45
b67da000 b67eb000 r-xp /usr/lib/libecore_input.so.1.7.99
b67f3000 b67f8000 r-xp /usr/lib/libecore_file.so.1.7.99
b6800000 b6822000 r-xp /usr/lib/libecore_evas.so.1.7.99
b682b000 b686c000 r-xp /usr/lib/libeina.so.1.7.99
b6875000 b688e000 r-xp /usr/lib/libeet.so.1.7.99
b689f000 b6908000 r-xp /lib/libm-2.13.so
b6911000 b6917000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6920000 b6921000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b6929000 b694c000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6954000 b6959000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6961000 b698b000 r-xp /usr/lib/libdbus-1.so.3.8.12
b6994000 b69ab000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b69b3000 b69be000 r-xp /lib/libunwind.so.8.0.1
b69eb000 b6a09000 r-xp /usr/lib/libsystemd.so.0.4.0
b6a13000 b6b37000 r-xp /lib/libc-2.13.so
b6b45000 b6b4d000 r-xp /lib/libgcc_s-4.6.so.1
b6b4e000 b6b52000 r-xp /usr/lib/libsmack.so.1.0.0
b6b5b000 b6b61000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b69000 b6c39000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6c3a000 b6c98000 r-xp /usr/lib/libedje.so.1.7.99
b6ca2000 b6cb9000 r-xp /usr/lib/libecore.so.1.7.99
b6cd0000 b6d9e000 r-xp /usr/lib/libevas.so.1.7.99
b6dc4000 b6f00000 r-xp /usr/lib/libelementary.so.1.7.99
b6f17000 b6f2b000 r-xp /lib/libpthread-2.13.so
b6f36000 b6f38000 r-xp /usr/lib/libdlog.so.0.0.0
b6f40000 b6f43000 r-xp /usr/lib/libbundle.so.0.1.22
b6f4b000 b6f4d000 r-xp /lib/libdl-2.13.so
b6f56000 b6f63000 r-xp /usr/lib/libaul.so.0.1.0
b6f75000 b6f7b000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f84000 b6f88000 r-xp /usr/lib/libsys-assert.so
b6f91000 b6fae000 r-xp /lib/ld-2.13.so
b6fb7000 b6fbc000 r-xp /usr/bin/launchpad-loader
b7bc8000 b7db4000 rw-p [heap]
bee8d000 beeae000 rw-p [stack]
bee8d000 beeae000 rw-p [stack]
End of Maps Information

Callstack Information (PID:3314)
Call Stack Count: 16
 0: loadAllBodyDefect + 0xf (0xb58f8848) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5848
 1:  + 0x0 (0xb58f83c1) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x53c1
 2: (0xb588ff4d) [/usr/lib/libefl-extension.so.0] + 0x7f4d
 3: (0xb6ce7af9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_event_feed_key_up + 0x4cc (0xb6cefd81) [/usr/lib/libevas.so.1] + 0x1fd81
 5: (0xb61ebc9d) [/usr/lib/libecore_input_evas.so.1] + 0x1c9d
 6: (0xb6caae53) [/usr/lib/libecore.so.1] + 0x8e53
 7: (0xb6cae46b) [/usr/lib/libecore.so.1] + 0xc46b
 8: ecore_main_loop_begin + 0x30 (0xb6cae879) [/usr/lib/libecore.so.1] + 0xc879
 9: appcore_efl_main + 0x332 (0xb6f78b47) [/usr/lib/libappcore-efl.so.1] + 0x3b47
10: ui_app_main + 0xb0 (0xb5c1ded5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
11: uib_app_run + 0xea (0xb58f851f) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x551f
12: main + 0x34 (0xb58f8bed) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5bed
13: (0xb6fb8a53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
14: __libc_start_main + 0x114 (0xb6a2a85c) [/lib/libc.so.6] + 0x1785c
15: (0xb6fb8e0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
3369): WeatherInformationSharer.cpp: PrintSharedCityDailyWeatherInfoForBundle(211) > [0;40;31mdailyForecastNumber : [0;m
05-03 14:28:56.428+0700 E/weather-common( 3369): WeatherInformationSharer.cpp: BundleGetString(87) > [0;40;31mFail to bundle_get_str : -126[0;m
05-03 14:28:56.428+0700 E/weather-common( 3369): StringHelper.cpp: StringToInt(133) > [0;40;31m[StringToInt(): 133] (str.length() <= 0) [return][0;m
05-03 14:28:56.458+0700 E/weather-widget( 1468): WidgetMain.cpp: RequestUpdateForEachInstances(138) > [0;40;31mrequest widget_service_trigger_update to /opt/usr/share/live_magazine/com.samsung.weather-widget_1221_14.908159.png[0;m
05-03 14:28:56.458+0700 E/weather-widget( 1468): WidgetMain.cpp: UpdateWidgetInstance(701) > [0;40;31mUpdateWidgetInstance[0;m
05-03 14:28:56.478+0700 E/weather-common( 1468): CPType.cpp: Renew(90) > [0;40;31mCPType is renewed : 5[0;m
05-03 14:28:56.478+0700 E/weather-common( 1468): DataManager.cpp: LoadData(326) > [0;40;31mnewCpTypeStr : TWC[0;m
05-03 14:28:56.478+0700 E/weather-common( 1468): DataManager.cpp: LoadData(329) > [0;40;31mweather data loaded[0;m
05-03 14:28:56.488+0700 E/weather-widget( 1468): WidgetViewData.cpp: SetWidgetLifeCycleState(382) > [0;40;31mSetWidgetLifeCycleState, mWidgetLifeCycleState:1[0;m
05-03 14:28:56.488+0700 E/weather-widget( 1468): WidgetMain.cpp: UpdateWidgetInstance(727) > [0;40;31mcontent is NULL[0;m
05-03 14:28:56.488+0700 E/weather-widget( 1468): WidgetMainView.cpp: Resume(1236) > [0;40;31mWidgetMainView::Resume[0;m
05-03 14:28:56.518+0700 E/weather-widget( 1468): WidgetMainView.cpp: AddTouchEventCallback(143) > [0;40;31mAddTouchEventCallback[0;m
05-03 14:28:56.518+0700 E/weather-widget( 1468): WidgetMainView.cpp: UpdateViewPage(267) > [0;40;31mUpdateViewPage[0;m
05-03 14:28:56.518+0700 E/weather-widget( 1468): WidgetMainView.cpp: HideFindingLocationView(595) > [0;40;31m[HideFindingLocationView(): 595] (mFindingLocationView == NULL) [return][0;m
05-03 14:28:56.518+0700 E/weather-widget( 1468): WidgetMainView.cpp: UpdateViewPage(276) > [0;40;31mUpdateViewPage locationId : e0ae79d8e25f1902b7edbb7c162fdd27b2def8c7cf049d7bd71824fa00491abe[0;m
05-03 14:28:56.518+0700 E/weather-widget( 1468): WidgetMainView.cpp: UpdateViewExceptLifeIndexProgress(377) > [0;40;31mUpdateViewExceptLifeIndexProgress[0;m
05-03 14:28:56.518+0700 E/weather-common( 1468): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 14:28:56.518+0700 E/weather-agent( 3369): NotiFromOutsideForWatch.cpp: CheckPreCondition(318) > [0;40;31mCheckPreCondition for NotiFromOutsideForWatch[0;m
05-03 14:28:56.518+0700 E/weather-agent( 3369): NotiFromOutsideForWatch.cpp: CheckPreCondition(320) > [0;40;31m[CheckPreCondition(): 320] (!cityPtr) [return][0;m
05-03 14:28:56.518+0700 E/weather-agent( 3369): NotiProduct.h: Request(41) > [0;40;31m[Request(): 41] (CheckPreCondition(cityPtr, appControl) == false) [return][0;m
05-03 14:28:56.518+0700 E/weather-agent( 3369): NotiCombinationForWatch.cpp: CheckPreCondition(316) > [0;40;31mCheckPreCondition for NotiCombinationForWatch[0;m
05-03 14:28:56.518+0700 E/weather-agent( 3369): NotiCombinationForWatch.cpp: CheckPreCondition(318) > [0;40;31m[CheckPreCondition(): 318] (!cityPtr) [return][0;m
05-03 14:28:56.518+0700 E/weather-agent( 3369): NotiProduct.h: Request(41) > [0;40;31m[Request(): 41] (CheckPreCondition(cityPtr, appControl) == false) [return][0;m
05-03 14:28:56.518+0700 E/weather-agent( 3369): AgentController.cpp: RequestNotification(336) > [0;40;31mNotification has been made for watch[0;m
05-03 14:28:56.518+0700 E/weather-agent( 3369): AgentMain.cpp: StartServiceTerminateTimer(134) > [0;40;31mthere is same appControl. we should erase it from mAgentControllerPtrVector[0;m
05-03 14:28:56.518+0700 E/weather-common( 3369): Location.cpp: RemoveEventListener(470) > [0;40;31meventListener:0xb7258830 not exist[0;m
05-03 14:28:56.518+0700 E/weather-agent( 3369): AgentMain.cpp: StartServiceTerminateTimer(152) > [0;40;31mStart ServiceTerminateTimer. Wait ...[0;m
05-03 14:28:56.548+0700 E/weather-common( 3369): CurlNetworkConnection.cpp: SGIOPipeWatchCB(852) > [0;40;31mthere is no message in the pipe, ret:0[0;m
05-03 14:28:56.558+0700 E/weather-widget( 1468): WidgetViewData.cpp: GetWidgetLifeCycleState(389) > [0;40;31mGetWidgetLifeCycleState, mWidgetLifeCycleState:1[0;m
05-03 14:28:56.558+0700 E/weather-widget( 1468): WidgetMainViewElement.cpp: CreateHighLowTemperature(249) > [0;40;31mLTR[0;m
05-03 14:28:56.558+0700 E/weather-widget( 1468): WidgetMainViewElement.cpp: CreateIconAndTemperature(355) > [0;40;31mLTR[0;m
05-03 14:28:56.558+0700 E/weather-common( 1468): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 14:28:56.568+0700 E/weather-common( 1468): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 14:28:56.568+0700 E/weather-common( 1468): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 14:28:56.568+0700 E/EFL     ( 1468): evas_main<1468> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 14:28:56.568+0700 E/EFL     ( 1468): evas_main<1468> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 14:28:56.568+0700 E/EFL     ( 1468): evas_main<1468> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 14:28:56.568+0700 E/weather-common( 1468): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 14:28:56.568+0700 E/weather-widget( 1468): WidgetMainViewElement.cpp: CreateRightLifeIndexWeatherInfo(524) > [0;40;31mcolorCode : AO094[0;m
05-03 14:28:56.568+0700 E/EFL     ( 1468): evas_main<1468> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 14:28:56.568+0700 E/EFL     ( 1468): evas_main<1468> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 14:28:56.568+0700 E/EFL     ( 1468): evas_main<1468> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
05-03 14:28:56.568+0700 E/weather-common( 1468): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 14:28:56.568+0700 E/weather-common( 1468): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 14:28:56.578+0700 E/weather-common( 1468): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 14:28:56.578+0700 E/weather-common( 1468): VconfUtil.cpp: GetVconfIntValue(164) > [0;40;31mFailed vconf_get_int [ret : -1, key : db/retailmode/enabled, value : -1][0;m
05-03 14:28:56.578+0700 E/weather-widget( 1468): WidgetMainViewData.cpp: GetProgressAngleOnUVIndexWithCityIndex(142) > [0;40;31muv_index : 1, deltaAngle : 32.000000[0;m
05-03 14:28:56.578+0700 E/weather-widget( 1468): WidgetMainViewElement.cpp: DrawLifeIndexText(603) > [0;40;31mleftText : 100%, leftFontCode : AT094[0;m
05-03 14:28:56.578+0700 E/weather-widget( 1468): WidgetMainViewElement.cpp: DrawLifeIndexText(604) > [0;40;31mrightText : Moderate, rightFontCode : AT0941[0;m
05-03 14:28:56.578+0700 E/weather-widget( 1468): WidgetMainViewElement.cpp: SetFontCodeForCairo(682) > [0;40;31mfont : Tizen:style=normal:weight=regular[0;m
05-03 14:28:56.588+0700 E/weather-widget( 1468): WidgetMainViewElement.cpp: DrawLifeIndexText(629) > [0;40;31m[Left] textAngle : 14.946725, endAngle : 223.999997, startAngle : 209.053272[0;m
05-03 14:28:56.588+0700 E/weather-widget( 1468): WidgetMainViewElement.cpp: SetFontCodeForCairo(682) > [0;40;31mfont : Tizen:style=normal:weight=regular[0;m
05-03 14:28:56.598+0700 E/weather-widget( 1468): WidgetMainViewElement.cpp: DrawLifeIndexText(660) > [0;40;31m[Right] textAngle : 25.267083, endAngle : 27.267083, startAngle : 2.000000[0;m
05-03 14:28:56.608+0700 E/weather-common( 1468): SamsungLogManager.cpp: OnWeatherWidgetResumed(248) > [0;40;31mOnWeatherWidgetResumed[0;m
05-03 14:28:56.608+0700 I/CAPI_WIDGET_APPLICATION( 1468): widget_app.c: __provider_update_cb(281) > received updating signal
05-03 14:28:56.918+0700 W/WATCH_CORE( 1308): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
05-03 14:28:56.918+0700 I/WATCH_CORE( 1308): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
05-03 14:28:56.918+0700 I/CAPI_WATCH_APPLICATION( 1308): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 14:28:56.918+0700 E/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1157) > 
05-03 14:28:56.918+0700 I/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1168) > set force update!!
05-03 14:28:56.928+0700 E/wnoti-service( 1378): wnoti-db-utility.c: context_wearonoff_status_cb(1781) > status changed from 1 to 2 
05-03 14:28:56.928+0700 E/WMS     ( 1010): wms_event_handler.c: _wms_event_handler_cb_wearonoff_monitor(23513) > wear_monitor_status update[0] = 1 -> 2
05-03 14:28:58.168+0700 E/weather-agent( 3369): AgentMain.cpp: TerminateService(97) > [0;40;31mTerminateService[0;m
05-03 14:28:58.168+0700 I/CAPI_APPFW_APPLICATION( 3369): service_app_main.c: service_app_exit(441) > service_app_exit
05-03 14:28:58.168+0700 W/AUL_AMD (  967): amd_request.c: __request_handler(669) > __request_handler: 22
05-03 14:28:58.168+0700 W/AUL_AMD (  967): amd_request.c: __request_handler(999) > app status : 4
05-03 14:28:58.168+0700 E/weather-agent( 3369): AgentMain.cpp: AppTerminate(283) > [0;40;31mAppTerminate[0;m
05-03 14:28:58.178+0700 E/weather-agent( 3369): AgentMain.cpp: AppTerminate(287) > [0;40;31mdevice_power_release_lock success[0;m
05-03 14:28:58.278+0700 E/weather-common( 3369): CurlNetworkConnection.cpp: DestroyAllHttpRequest(577) > [0;40;31m[DestroyAllHttpRequest(): 577] Failed get curl multi session [return][0;m
05-03 14:28:58.288+0700 E/CAPI_APPFW_APP_CONTROL( 3369): app_control.c: app_control_error(138) > [app_control_destroy] INVALID_PARAMETER(0xffffffea)
05-03 14:28:58.288+0700 E/weather-agent( 3369): AgentMain.cpp: ~AgentMain(77) > [0;40;31mapp_control_destroy failed[0;m
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 14:28:58.578+0700 E/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: HandleReceivedMessage(588) > Connection closed
05-03 14:28:58.578+0700 E/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: HandleReceivedMessage(610) > All connections of client(3369) are closed. delete client info
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: OnIpcClientDisconnected(178) > MessagePort Ipc disconnected
05-03 14:28:58.578+0700 E/MESSAGE_PORT(  963): MessagePortStub.cpp: OnIpcClientDisconnected(181) > Unregister - client =  3369
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: UnregisterMessagePortByDiscon(273) > _MessagePortService::UnregisterMessagePortByDiscon
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.578+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: unregistermessageport(257) > unregistermessageport
05-03 14:28:58.608+0700 W/AUL     ( 3384): daemon-manager-release-agent.c: main(12) > release agent : [2:/com.samsung.weather-agent]
05-03 14:28:58.608+0700 W/AUL_AMD (  967): amd_request.c: __request_handler(669) > __request_handler: 23
05-03 14:28:58.608+0700 W/AUL_AMD (  967): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 0
05-03 14:28:58.608+0700 W/AUL_AMD (  967): amd_request.c: __request_handler(1032) > pkg_status: installed, dead pid: 3369
05-03 14:28:58.608+0700 W/AUL_AMD (  967): amd_request.c: __send_app_termination_signal(528) > send dead signal done
05-03 14:28:58.608+0700 I/AUL_AMD (  967): amd_main.c: __app_dead_handler(262) > __app_dead_handler, pid: 3369
05-03 14:28:58.608+0700 W/AUL     (  967): app_signal.c: aul_send_app_terminated_signal(799) > aul_send_app_terminated_signal pid(3369)
05-03 14:28:58.948+0700 I/APP_CORE( 3314): appcore-efl.c: __do_app(453) > [APP 3314] Event: MEM_FLUSH State: PAUSED
05-03 14:29:00.168+0700 W/AUL_AMD (  967): amd_status.c: __app_terminate_timer_cb(168) > send SIGKILL: No such process
05-03 14:29:00.458+0700 W/APPS    ( 1221): apps_main.c: _time_changed_cb(409) >  date : 3->3
05-03 14:29:00.648+0700 W/WATCH_CORE( 1308): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
05-03 14:29:00.648+0700 I/WATCH_CORE( 1308): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
05-03 14:29:00.648+0700 I/CAPI_WATCH_APPLICATION( 1308): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 14:29:00.648+0700 E/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1157) > 
05-03 14:29:00.648+0700 I/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1168) > set force update!!
05-03 14:29:00.648+0700 I/watchface-viewer( 1308): viewer-part-resource-evas.cpp: CreateTextImage(1028) > style[DEFAULT='font=Default font_size=40 color=#FFFFFF ellipsis=-1 align=center ']
05-03 14:29:00.648+0700 I/watchface-viewer( 1308): viewer-part-resource-evas.cpp: CreateTextImage(1042) > formatted size 42x40
05-03 14:29:00.648+0700 W/W_HOME  ( 1221): dbus.c: _dbus_message_recv_cb(169) > gesture:wristup
05-03 14:29:00.648+0700 W/W_HOME  ( 1221): gesture.c: _manual_render_schedule(209) > schedule, manual render
05-03 14:29:00.648+0700 I/watchface-viewer( 1308): viewer-part-resource-evas.cpp: CreateTextImage(1028) > style[DEFAULT='font=Default font_size=40 color=#FFFFFF ellipsis=-1 align=center ']
05-03 14:29:00.648+0700 I/watchface-viewer( 1308): viewer-part-resource-evas.cpp: CreateTextImage(1042) > formatted size 42x40
05-03 14:29:00.648+0700 I/watchface-viewer( 1308): viewer-image-file-loader.cpp: OnImageLoadingDoneIdlerCb(792) > ImagesLoadingDoneSignal().Emit()
05-03 14:29:00.658+0700 W/WAKEUP-SERVICE( 1646): WakeupService.cpp: OnReceiveGestureChanged(995) > [0;32mINFO: wakeup receive data : -1204845776[0;m
05-03 14:29:00.658+0700 W/WAKEUP-SERVICE( 1646): WakeupService.cpp: OnReceiveGestureChanged(996) > [0;32mINFO: WakeupServiceStart[0;m
05-03 14:29:00.658+0700 W/WAKEUP-SERVICE( 1646): WakeupService.cpp: WakeupServiceStart(367) > [0;32mINFO: SEAMLESS WAKEUP START REQUEST[0;m
05-03 14:29:00.658+0700 W/WAKEUP-SERVICE( 1646): WakeupService.cpp: WakeupServiceStart(387) > [0;32mINFO: 500[0;m
05-03 14:29:00.658+0700 I/TIZEN_N_SOUND_MANAGER( 1646): sound_manager_product.c: sound_manager_svoice_set_param(1287) > [SVOICE] set param [keyword length] : 500
05-03 14:29:00.668+0700 W/wnotibp ( 2116): wnotiboard-popup-control.c: _ctrl_lcd_on_cb(93) > ::APP:: view state=0, 2, 0
05-03 14:29:00.668+0700 I/wnotibp ( 2116): wnotiboard-popup-control.c: _ctrl_lcd_on_cb(141) > There is no additional work.
05-03 14:29:00.668+0700 W/TIZEN_N_SOUND_MANAGER( 1646): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_set_param] ERROR_NONE (0x00000000)
05-03 14:29:00.668+0700 E/WAKEUP-SERVICE( 1646): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
05-03 14:29:00.668+0700 E/WAKEUP-SERVICE( 1646): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
05-03 14:29:00.668+0700 I/TIZEN_N_SOUND_MANAGER( 1646): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Enable start
05-03 14:29:00.668+0700 W/WATCH_CORE( 1308): appcore-watch.c: __signal_lcd_status_handler(1231) > signal_lcd_status_signal: LCDOn
05-03 14:29:00.668+0700 I/WATCH_CORE( 1308): appcore-watch.c: __signal_lcd_status_handler(1250) > Call the time_tick_cb
05-03 14:29:00.668+0700 I/CAPI_WATCH_APPLICATION( 1308): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 14:29:00.668+0700 E/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1157) > 
05-03 14:29:00.668+0700 I/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1168) > set force update!!
05-03 14:29:00.678+0700 I/WATCH_CORE( 1308): appcore-watch.c: __signal_lcd_status_handler(1257) > Call widget_provider_update_event
05-03 14:29:00.678+0700 W/W_HOME  ( 1221): dbus.c: _dbus_message_recv_cb(186) > LCD on
05-03 14:29:00.678+0700 W/W_HOME  ( 1221): gesture.c: _manual_render_disable_timer_set(167) > timer set
05-03 14:29:00.678+0700 W/W_HOME  ( 1221): gesture.c: _manual_render_disable_timer_del(157) > timer del
05-03 14:29:00.678+0700 W/W_HOME  ( 1221): gesture.c: _apps_status_get(128) > apps status:0
05-03 14:29:00.678+0700 W/W_HOME  ( 1221): gesture.c: _lcd_on_cb(303) > move_to_clock:0 clock_visible:0 info->offtime:6984
05-03 14:29:00.678+0700 W/W_HOME  ( 1221): gesture.c: _manual_render_schedule(209) > schedule, manual render
05-03 14:29:00.678+0700 W/W_HOME  ( 1221): event_manager.c: _lcd_on_cb(715) > lcd state: 1
05-03 14:29:00.678+0700 W/W_HOME  ( 1221): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:1(0) pm_state:1 home_visible:0 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 1, apptray edit visibility : 0
05-03 14:29:00.678+0700 W/W_HOME  ( 1221): gesture.c: _widget_updated_cb(188) > widget updated
05-03 14:29:00.678+0700 W/W_HOME  ( 1221): gesture.c: _manual_render_disable_timer_del(157) > timer del
05-03 14:29:00.678+0700 W/W_HOME  ( 1221): gesture.c: _manual_render(182) > 
05-03 14:29:00.678+0700 W/STARTER ( 1141): clock-mgr.c: _on_lcd_signal_receive_cb(1258) > [_on_lcd_signal_receive_cb:1258] _on_lcd_signal_receive_cb, 1258, Pre LCD on by [gesture] after screen off time [6984]ms
05-03 14:29:00.678+0700 W/STARTER ( 1141): clock-mgr.c: _pre_lcd_on(1027) > [_pre_lcd_on:1027] Will LCD ON as reserved app[(null)] alpm mode[0]
05-03 14:29:00.678+0700 W/W_INDICATOR( 1142): windicator_dbus.c: _windicator_dbus_lcd_changed_cb(538) > [_windicator_dbus_lcd_changed_cb:538] LCD ON signal is received
05-03 14:29:00.678+0700 W/W_INDICATOR( 1142): windicator_dbus.c: _windicator_dbus_lcd_changed_cb(559) > [_windicator_dbus_lcd_changed_cb:559] 559, str=[gesture],charge : 0, connected : 0
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [starter:org.tizen.idled.ReservedApp]
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [starter:org.tizen.idled.ReservedApp]
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
05-03 14:29:00.678+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 14:29:00.688+0700 I/APP_CORE( 3314): appcore-efl.c: __do_app(453) > [APP 3314] Event: RESUME State: PAUSED
05-03 14:29:00.688+0700 I/CAPI_APPFW_APPLICATION( 3314): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
05-03 14:29:00.688+0700 I/GATE    ( 3314): <GATE-M>APP_FULLY_LOADED_realtimefeedback</GATE-M>
05-03 14:29:00.708+0700 W/W_HOME  ( 1221): gesture.c: _manual_render(182) > 
05-03 14:29:00.718+0700 E/ALARM_MANAGER(  970): alarm-manager.c: __is_cached_cookie(230) > Find cached cookie for [1141].
05-03 14:29:00.718+0700 E/ALARM_MANAGER(  970): alarm-manager.c: __alarm_remove_from_list(575) > [alarm-server]:Remove alarm id(414477093)
05-03 14:29:00.718+0700 E/ALARM_MANAGER(  970): alarm-manager-schedule.c: __find_next_alarm_to_be_scheduled(547) > The duetime of alarm(436816151) is OVER.
05-03 14:29:00.728+0700 W/SHealthCommon( 1734): CpuLock.cpp: CheckAndReset(168) > [1;40;33mREQUEST POWER LOCK CPU [5000][0;m
05-03 14:29:00.738+0700 I/TIZEN_N_SOUND_MANAGER( 1646): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Enable end. (ret: 0x0)
05-03 14:29:00.738+0700 W/TIZEN_N_SOUND_MANAGER( 1646): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
05-03 14:29:00.738+0700 W/WAKEUP-SERVICE( 1646): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 1[0;m
05-03 14:29:00.738+0700 I/HIGEAR  ( 1646): WakeupService.cpp: WakeupServiceStart(393) > [svoice:Application:WakeupServiceStart:IN]
05-03 14:29:00.738+0700 W/WAKEUP-SERVICE( 1646): WakeupService.cpp: OnReceiveDisplayChanged(970) > [0;32mINFO: LCDOn receive data : -1226134772[0;m
05-03 14:29:00.738+0700 W/WAKEUP-SERVICE( 1646): WakeupService.cpp: OnReceiveDisplayChanged(971) > [0;32mINFO: WakeupServiceStart[0;m
05-03 14:29:00.738+0700 W/WAKEUP-SERVICE( 1646): WakeupService.cpp: WakeupServiceStart(367) > [0;32mINFO: SEAMLESS WAKEUP START REQUEST[0;m
05-03 14:29:00.738+0700 W/WAKEUP-SERVICE( 1646): WakeupService.cpp: WakeupServiceStart(387) > [0;32mINFO: 500[0;m
05-03 14:29:00.738+0700 I/TIZEN_N_SOUND_MANAGER( 1646): sound_manager_product.c: sound_manager_svoice_set_param(1287) > [SVOICE] set param [keyword length] : 500
05-03 14:29:00.738+0700 W/TIZEN_N_SOUND_MANAGER( 1646): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_set_param] ERROR_NONE (0x00000000)
05-03 14:29:00.738+0700 E/WAKEUP-SERVICE( 1646): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
05-03 14:29:00.738+0700 E/WAKEUP-SERVICE( 1646): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
05-03 14:29:00.738+0700 I/TIZEN_N_SOUND_MANAGER( 1646): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Enable start
05-03 14:29:00.748+0700 W/W_INDICATOR( 1142): windicator_util.c: _pm_state_changed_cb(912) > [_pm_state_changed_cb:912] LCD on
05-03 14:29:00.748+0700 W/W_INDICATOR( 1142): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(57) > [windicator_ongoing_info_shealth_update:57] windicator_shealth_update
05-03 14:29:00.758+0700 W/SHealthCommon( 1471): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:1[0;m
05-03 14:29:00.768+0700 W/W_HOME  ( 1221): gesture.c: _manual_render_enable(138) > 0
05-03 14:29:00.818+0700 I/HealthDataService( 1276): RequestHandler.cpp: OnHealthIpcMessageSync(123) > [1;35mServer Received: SHARE_GET[0;m
05-03 14:29:00.838+0700 E/ALARM_MANAGER(  970): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
05-03 14:29:00.838+0700 W/SHealthServiceCommon( 1734): PedometerSensorProxy.cpp: SOnContextPedometerUpdated(171) > [1;35mpedometerMode: 1[0;m
05-03 14:29:00.838+0700 W/SHealthServiceCommon( 1734): PedometerSensorProxy.cpp: ProcessBatchSteps(323) > [1;40;33m1[0;m
05-03 14:29:00.838+0700 W/W_INDICATOR( 1142): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(78) > [windicator_ongoing_info_shealth_update:78] Result : 0
05-03 14:29:00.838+0700 W/W_INDICATOR( 1142): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(99) > [windicator_ongoing_info_shealth_update:99] status : none
05-03 14:29:00.838+0700 W/W_INDICATOR( 1142): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(103) > [windicator_ongoing_info_shealth_update:103] application_id: 
05-03 14:29:00.838+0700 W/W_INDICATOR( 1142): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(112) > [windicator_ongoing_info_shealth_update:112] launch_operation : 
05-03 14:29:00.838+0700 W/W_INDICATOR( 1142): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(118) > [windicator_ongoing_info_shealth_update:118] extra_data_key : 
05-03 14:29:00.838+0700 W/W_INDICATOR( 1142): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(124) > [windicator_ongoing_info_shealth_update:124] extra_data_value : 
05-03 14:29:00.838+0700 W/W_INDICATOR( 1142): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(132) > [windicator_ongoing_info_shealth_update:132] image_path : 
05-03 14:29:00.838+0700 W/W_INDICATOR( 1142): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(135) > [windicator_ongoing_info_shealth_update:135] image_path_sub : 
05-03 14:29:00.838+0700 W/W_INDICATOR( 1142): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(138) > [windicator_ongoing_info_shealth_update:138] message_string :  
05-03 14:29:00.838+0700 W/W_INDICATOR( 1142): windicator_ongoing_info_shealth.c: windicator_ongoing_info_shealth_update(144) > [windicator_ongoing_info_shealth_update:144] [Update] SHealth status is none, so hide icon and text!
05-03 14:29:00.838+0700 W/W_INDICATOR( 1142): windicator_ongoing_info.c: windicator_ongoing_info_remove(191) > [windicator_ongoing_info_remove:191] Ongoing info type[1]
05-03 14:29:00.838+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 14:29:00.838+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 14:29:00.838+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 14:29:00.838+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
05-03 14:29:00.838+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 14:29:00.838+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 14:29:00.838+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
05-03 14:29:00.838+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 14:29:00.848+0700 E/ALARM_MANAGER(  970): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
05-03 14:29:00.848+0700 E/ALARM_MANAGER(  970): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 3-5-2018, 07:36:44 (UTC).
05-03 14:29:00.848+0700 E/ALARM_MANAGER(  970): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
05-03 14:29:00.848+0700 W/W_INDICATOR( 1142): windicator_ongoing_info.c: windicator_ongoing_info_remove(191) > [windicator_ongoing_info_remove:191] Ongoing info type[2]
05-03 14:29:00.848+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 14:29:00.848+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 14:29:00.848+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 14:29:00.848+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
05-03 14:29:00.848+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 14:29:00.848+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 14:29:00.848+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
05-03 14:29:00.848+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 14:29:00.848+0700 E/EFL     ( 1142): <1142> elm_main.c:1622 elm_object_signal_emit() safety check failed: obj == NULL
05-03 14:29:00.858+0700 I/TIZEN_N_SOUND_MANAGER( 1142): sound_manager.c: sound_manager_get_volume(84) > returns : type=3, volume=11, ret=0x0
05-03 14:29:00.858+0700 W/TIZEN_N_SOUND_MANAGER( 1142): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_get_volume] ERROR_NONE (0x00000000)
05-03 14:29:00.858+0700 W/W_INDICATOR( 1142): windicator_quick_setting_brightness.c: windicator_quick_setting_brightness_update(94) > [windicator_quick_setting_brightness_update:94] hyun 20
05-03 14:29:00.868+0700 E/ALARM_MANAGER(  970): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
05-03 14:29:00.868+0700 I/TIZEN_N_SOUND_MANAGER( 1646): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Enable end. (ret: 0x0)
05-03 14:29:00.868+0700 W/TIZEN_N_SOUND_MANAGER( 1646): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
05-03 14:29:00.868+0700 W/WAKEUP-SERVICE( 1646): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 1[0;m
05-03 14:29:00.868+0700 I/HIGEAR  ( 1646): WakeupService.cpp: WakeupServiceStart(393) > [svoice:Application:WakeupServiceStart:IN]
05-03 14:29:00.868+0700 E/ALARM_MANAGER(  970): alarm-manager.c: alarm_manager_alarm_delete(2462) > alarm_id[414477093] is removed.
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: windicator_connection_resume(2158) > [windicator_connection_resume:2158] 
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: windicator_connection_resume(2223) > [windicator_connection_resume:2223] primary rssi level : (0), s_info.rssi_level : (8) / primary svc type : (1)
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_changed_cb(1924) > [_rssi_changed_cb:1924] roaming status : 0
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_changed_cb(1933) > [_rssi_changed_cb:1933] svc type : 1
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_changed_cb(1968) > [_rssi_changed_cb:1968] Rssi level has already been 0!, Don't need to show rssi down animation. Just Show No Signal icon
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_icon_set(1124) > [_rssi_icon_set:1124] RSSI level : 8/5, (0xb82bccb0)
05-03 14:29:00.878+0700 E/W_INDICATOR( 1142): windicator_connection.c: _rssi_icon_set(1147) > [_rssi_icon_set:1147] Set RSSI SHOW sw.icon_0 (rssi_level : 8) (rssi_hide : 0)(b82bccb0)
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_icon_set(1215) > [_rssi_icon_set:1215] NETWORK_ATT or NETWORK_TMB : there is no roaming icon
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: _rssi_icon_set(1217) > [_rssi_icon_set:1217] rssi name : set_rssi_verizon_No_signal (0xb82bccb0)
05-03 14:29:00.878+0700 W/STARTER ( 1141): clock-mgr.c: __reserved_apps_message_received_cb(586) > [__reserved_apps_message_received_cb:586] appid[com.samsung.windicator]
05-03 14:29:00.878+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 14:29:00.878+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 14:29:00.878+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 14:29:00.878+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
05-03 14:29:00.878+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
05-03 14:29:00.878+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 14:29:00.878+0700 I/MESSAGE_PORT(  963): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.windicator:org.tizen.idled.ReservedApp]
05-03 14:29:00.878+0700 I/MESSAGE_PORT(  963): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
05-03 14:29:00.878+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
05-03 14:29:00.878+0700 I/MESSAGE_PORT(  963): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 14:29:00.878+0700 W/STARTER ( 1141): clock-mgr.c: _on_lcd_signal_receive_cb(1271) > [_on_lcd_signal_receive_cb:1271] _on_lcd_signal_receive_cb, 1271, Post LCD on by [gesture]
05-03 14:29:00.878+0700 W/STARTER ( 1141): clock-mgr.c: _post_lcd_on(1059) > [_post_lcd_on:1059] LCD ON as reserved app[(null)] alpm mode[0]
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: _tapi_changed_cb(2115) > [_tapi_changed_cb:2115] modem_power : 0
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: _connection_type_changed_cb(1324) > [_connection_type_changed_cb:1324] wifi state : 2
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: _connection_type_changed_cb(1327) > [_connection_type_changed_cb:1327] Show wifi icon!
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
05-03 14:29:00.878+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
05-03 14:29:00.888+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 3
05-03 14:29:00.888+0700 W/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 15 / signal : type_wifi_connected_03
05-03 14:29:00.888+0700 E/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 15) / (hide : 0)
05-03 14:29:00.888+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
05-03 14:29:00.888+0700 E/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
05-03 14:29:00.888+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
05-03 14:29:00.888+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
05-03 14:29:00.888+0700 E/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
05-03 14:29:00.888+0700 W/SHealthCommon( 1734): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: pedo_update, pendingClientInfoList.size(): 1[0;m
05-03 14:29:00.888+0700 W/SHealthCommon( 1734): CpuLock.cpp: CheckAndReset(168) > [1;40;33mREQUEST POWER LOCK CPU [4839][0;m
05-03 14:29:00.958+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_check_retry_err(507) > key(pedometer_goal_achieve_percents), check retry err: -21/(2/No such file or directory).
05-03 14:29:00.958+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_get_key(1101) > _preference_get_key(pedometer_goal_achieve_percents) step(-17825744) failed(2 / No such file or directory)
05-03 14:29:00.958+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: preference_get_int(1132) > preference_get_int(1734) : key(pedometer_goal_achieve_percents) error
05-03 14:29:00.958+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_check_retry_err(507) > key(best_step_goal), check retry err: -21/(2/No such file or directory).
05-03 14:29:00.958+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_get_key(1101) > _preference_get_key(best_step_goal) step(-17825744) failed(2 / No such file or directory)
05-03 14:29:00.958+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: preference_get_int(1132) > preference_get_int(1734) : key(best_step_goal) error
05-03 14:29:00.958+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: preference_is_existing(1514) > Error : key(best_pedometer_history_count) is not exist
05-03 14:29:00.958+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_check_retry_err(507) > key(host_pedo_default_goal), check retry err: -21/(2/No such file or directory).
05-03 14:29:00.958+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_get_key(1101) > _preference_get_key(host_pedo_default_goal) step(-17825744) failed(2 / No such file or directory)
05-03 14:29:00.958+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: preference_get_int(1132) > preference_get_int(1734) : key(host_pedo_default_goal) error
05-03 14:29:00.958+0700 W/SHealthCommon( 1734): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
05-03 14:29:00.968+0700 W/SHealthCommon( 1734): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
05-03 14:29:00.978+0700 W/SHealthCommon( 1734): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:1[0;m
05-03 14:29:00.978+0700 W/SHealthServiceCommon( 1734): SHealthServiceController.cpp: OnSystemUtilLcdStateChanged(645) > [1;35m ###[0;m
05-03 14:29:00.978+0700 W/SHealthServiceCommon( 1734): EnergyExpenditureFeatureController.cpp: OnTotalEnergyExpenditureChanged(119) > [1;40;33mstart 1525280400000.000000, end 1525332540984.000000, calories 975.736415[0;m
05-03 14:29:00.978+0700 W/SHealthCommon( 1734): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: energy_expenditure_updated, pendingClientInfoList.size(): 1[0;m
05-03 14:29:00.978+0700 W/SHealthCommon( 1734): TimelineSessionStorage.cpp: OnTriggered(1290) > [1;40;33mlocalStartTime: 1525305600000.000000[0;m
05-03 14:29:00.998+0700 W/W_INDICATOR( 1142): windicator_dbus.c: _msg_reserved_app_cb(341) > [_msg_reserved_app_cb:341] Moment view is already shown or call is enabled. moment view [0]
05-03 14:29:01.008+0700 W/WATCH_CORE( 1308): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 3
05-03 14:29:01.008+0700 I/WATCH_CORE( 1308): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
05-03 14:29:01.008+0700 I/CAPI_WATCH_APPLICATION( 1308): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 14:29:01.008+0700 E/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1157) > 
05-03 14:29:01.008+0700 I/watchface-app( 1308): watchface.cpp: OnAppTimeTick(1168) > set force update!!
05-03 14:29:01.008+0700 W/SHealthCommon( 1734): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: timeline_session_updated, pendingClientInfoList.size(): 0[0;m
05-03 14:29:01.008+0700 W/SHealthCommon( 1734): CpuLock.cpp: CheckAndReset(168) > [1;40;33mREQUEST POWER LOCK CPU [5000][0;m
05-03 14:29:01.008+0700 E/WMS     ( 1010): wms_event_handler.c: _wms_event_handler_cb_wearonoff_monitor(23513) > wear_monitor_status update[0] = 2 -> 1
05-03 14:29:01.008+0700 E/wnoti-service( 1378): wnoti-db-utility.c: context_wearonoff_status_cb(1781) > status changed from 2 to 1 
05-03 14:29:01.008+0700 E/wnoti-service( 1378): wnoti-native-client.c: handle_cache_notification(790) > >>
05-03 14:29:01.018+0700 W/SHealthServiceCommon( 1734): PedometerSensorProxy.cpp: SOnContextPedometerUpdated(171) > [1;35mpedometerMode: 0[0;m
05-03 14:29:01.028+0700 W/SHealthCommon( 1734): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: pedo_update, pendingClientInfoList.size(): 1[0;m
05-03 14:29:01.028+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_check_retry_err(507) > key(pedometer_goal_achieve_percents), check retry err: -21/(2/No such file or directory).
05-03 14:29:01.028+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_get_key(1101) > _preference_get_key(pedometer_goal_achieve_percents) step(-17825744) failed(2 / No such file or directory)
05-03 14:29:01.028+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: preference_get_int(1132) > preference_get_int(1734) : key(pedometer_goal_achieve_percents) error
05-03 14:29:01.028+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_check_retry_err(507) > key(best_step_goal), check retry err: -21/(2/No such file or directory).
05-03 14:29:01.028+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_get_key(1101) > _preference_get_key(best_step_goal) step(-17825744) failed(2 / No such file or directory)
05-03 14:29:01.028+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: preference_get_int(1132) > preference_get_int(1734) : key(best_step_goal) error
05-03 14:29:01.028+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: preference_is_existing(1514) > Error : key(best_pedometer_history_count) is not exist
05-03 14:29:01.028+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_check_retry_err(507) > key(host_pedo_default_goal), check retry err: -21/(2/No such file or directory).
05-03 14:29:01.028+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: _preference_get_key(1101) > _preference_get_key(host_pedo_default_goal) step(-17825744) failed(2 / No such file or directory)
05-03 14:29:01.028+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 1734): preference.c: preference_get_int(1132) > preference_get_int(1734) : key(host_pedo_default_goal) error
05-03 14:29:01.028+0700 W/SHealthCommon( 1734): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
05-03 14:29:01.028+0700 W/SHealthCommon( 1734): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
05-03 14:29:01.038+0700 W/SHealthCommon( 1734): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: timeline_summary_updated, pendingClientInfoList.size(): 0[0;m
05-03 14:29:01.038+0700 W/SHealthServiceCommon( 1734): EnergyExpenditureFeatureController.cpp: OnTotalEnergyExpenditureChanged(119) > [1;40;33mstart 1525280400000.000000, end 1525332541049.000000, calories 976.680527[0;m
05-03 14:29:01.038+0700 W/SHealthCommon( 1734): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: energy_expenditure_updated, pendingClientInfoList.size(): 1[0;m
05-03 14:29:01.048+0700 W/SHealthServiceCommon( 1734): ContextRestingHeartrateProxy.cpp: OnRestingHrUpdatedCB(347) > [1;40;33mhrValue: 1008[0;m
05-03 14:29:01.058+0700 W/SHealthServiceCommon( 1734): ContextRestingHeartrateProxy.cpp: OnRestingHrUpdatedCB(347) > [1;40;33mhrValue: 1007[0;m
05-03 14:29:01.608+0700 W/KEYROUTER(  935): e_mod_main.c: DeliverDeviceKeyEvents(3164) > Deliver KeyPress. value=2669, window=0x3c00002
05-03 14:29:01.608+0700 E/EFL     ( 3314): ecore_x<3314> ecore_x_events.c:537 _ecore_x_event_handle_key_press() KeyEvent:press time=718240
05-03 14:29:01.608+0700 W/KEYROUTER(  935): e_mod_main.c: DeliverDeviceKeyEvents(3175) > Deliver KeyRelease. value=2669, window=0x3c00002
05-03 14:29:01.608+0700 E/EFL     ( 3314): ecore_x<3314> ecore_x_events.c:551 _ecore_x_event_handle_key_release() KeyEvent:release time=718370
05-03 14:29:01.608+0700 E/efl-extension( 3314): efl_extension_events.c: _eext_key_grab_rect_key_up_cb(240) > key up called
05-03 14:29:01.938+0700 W/CRASH_MANAGER( 3389): worker.c: worker_job(1205) > 1103314726561152533254
