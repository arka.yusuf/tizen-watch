S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2676
Date: 2018-04-27 11:10:33+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2676, uid 5000)

Register Information
r0   = 0x00000004, r1   = 0x000186a0
r2   = 0xb591f443, r3   = 0x00000000
r4   = 0x00000004, r5   = 0x00000000
r6   = 0xb1e13cb8, r7   = 0xbebef1d0
r8   = 0x000186a0, r9   = 0xb8919548
r10  = 0xb6cf5b10, fp   = 0x00000000
ip   = 0x00000174, sp   = 0xbebef160
lr   = 0xb591f443, pc   = 0xb6aab6f0
cpsr = 0x20000010

Memory Information
MemTotal:   405512 KB
MemFree:      5880 KB
Buffers:      6380 KB
Cached:     108528 KB
VmPeak:     102452 KB
VmSize:     101368 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       26348 KB
VmRSS:       26348 KB
VmData:      41136 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24516 KB
VmPTE:          68 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2676 TID = 2676
2676 2699 2710 2713 

Maps Information
b150c000 b1e00000 rw-p [stack:2713]
b1ffc000 b2000000 r-xp /usr/lib/libogg.so.0.7.1
b2008000 b202a000 r-xp /usr/lib/libvorbis.so.0.4.3
b2032000 b2079000 r-xp /usr/lib/libsndfile.so.1.0.26
b2085000 b20ce000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b20d7000 b20dc000 r-xp /usr/lib/libjson.so.0.0.1
b397d000 b3a83000 r-xp /usr/lib/libicuuc.so.57.1
b3a99000 b3c21000 r-xp /usr/lib/libicui18n.so.57.1
b3c31000 b3c3e000 r-xp /usr/lib/libail.so.0.1.0
b3c47000 b3c4a000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3c52000 b3c8a000 r-xp /usr/lib/libpulse.so.0.16.2
b3c8b000 b3c8e000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3c96000 b3cf7000 r-xp /usr/lib/libasound.so.2.0.0
b3d01000 b3d1a000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3d23000 b3d27000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3d2f000 b3d3a000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3d47000 b3d4b000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3d54000 b3d6c000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3d7d000 b3d84000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3d8c000 b3d97000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3d9f000 b3da1000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3da9000 b3daa000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3e39000 b3e3a000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3e42000 b3e45000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3f06000 b3f0e000 r-xp /usr/lib/libfeedback.so.0.1.4
b3f27000 b3f28000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3faf000 b4036000 rw-s anon_inode:dmabuf
b404b000 b40d2000 rw-s anon_inode:dmabuf
b40d3000 b48d2000 rw-p [stack:2710]
b4a72000 b4a73000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4b7a000 b5379000 rw-p [stack:2699]
b5379000 b537b000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b5383000 b539a000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b53a7000 b53a9000 r-xp /usr/lib/libdri2.so.0.0.0
b53b1000 b53bc000 r-xp /usr/lib/libtbm.so.1.0.0
b53c4000 b53cc000 r-xp /usr/lib/libdrm.so.2.4.0
b53d4000 b53d6000 r-xp /usr/lib/libgenlock.so
b53de000 b53e3000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b53eb000 b53f6000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b55ff000 b56c9000 r-xp /usr/lib/libCOREGL.so.4.0
b56da000 b56ea000 r-xp /usr/lib/libmdm-common.so.1.1.25
b56f2000 b56f8000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5700000 b5701000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b570a000 b570d000 r-xp /usr/lib/libEGL.so.1.4
b5715000 b5723000 r-xp /usr/lib/libGLESv2.so.2.0
b572c000 b5775000 r-xp /usr/lib/libmdm.so.1.2.70
b577e000 b5784000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b578c000 b5795000 r-xp /usr/lib/libcom-core.so.0.0.1
b579e000 b5856000 r-xp /usr/lib/libcairo.so.2.11200.14
b5861000 b587a000 r-xp /usr/lib/libnetwork.so.0.0.0
b5882000 b588e000 r-xp /usr/lib/libnotification.so.0.1.0
b5897000 b58a6000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b58af000 b58d0000 r-xp /usr/lib/libefl-extension.so.0.1.0
b58d8000 b58dd000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b58e5000 b58ea000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b58f2000 b5902000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b590a000 b5912000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b591a000 b5923000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5ac8000 b5ad2000 r-xp /lib/libnss_files-2.13.so
b5adb000 b5baa000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5bc0000 b5be4000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5bed000 b5bf3000 r-xp /usr/lib/libappsvc.so.0.1.0
b5bfb000 b5bff000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5c0c000 b5c17000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5c1f000 b5c21000 r-xp /usr/lib/libiniparser.so.0
b5c2a000 b5c2f000 r-xp /usr/lib/libappcore-common.so.1.1
b5c37000 b5c39000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5c42000 b5c46000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5c53000 b5c55000 r-xp /usr/lib/libXau.so.6.0.0
b5c5d000 b5c64000 r-xp /lib/libcrypt-2.13.so
b5c94000 b5c96000 r-xp /usr/lib/libiri.so
b5c9f000 b5e31000 r-xp /usr/lib/libcrypto.so.1.0.0
b5e52000 b5e99000 r-xp /usr/lib/libssl.so.1.0.0
b5ea5000 b5ed3000 r-xp /usr/lib/libidn.so.11.5.44
b5edb000 b5ee4000 r-xp /usr/lib/libcares.so.2.1.0
b5eee000 b5f01000 r-xp /usr/lib/libxcb.so.1.1.0
b5f0a000 b5f0d000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5f15000 b5f17000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5f20000 b5fec000 r-xp /usr/lib/libxml2.so.2.7.8
b5ff9000 b5ffb000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b6004000 b6009000 r-xp /usr/lib/libffi.so.5.0.10
b6011000 b6012000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b601a000 b601d000 r-xp /lib/libattr.so.1.1.0
b6025000 b60b9000 r-xp /usr/lib/libstdc++.so.6.0.16
b60cc000 b60e9000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b60f3000 b610b000 r-xp /usr/lib/libpng12.so.0.50.0
b6113000 b6129000 r-xp /lib/libexpat.so.1.6.0
b6133000 b6177000 r-xp /usr/lib/libcurl.so.4.3.0
b6180000 b618a000 r-xp /usr/lib/libXext.so.6.4.0
b6194000 b6198000 r-xp /usr/lib/libXtst.so.6.1.0
b61a0000 b61a6000 r-xp /usr/lib/libXrender.so.1.3.0
b61ae000 b61b4000 r-xp /usr/lib/libXrandr.so.2.2.0
b61bc000 b61bd000 r-xp /usr/lib/libXinerama.so.1.0.0
b61c6000 b61cf000 r-xp /usr/lib/libXi.so.6.1.0
b61d7000 b61da000 r-xp /usr/lib/libXfixes.so.3.1.0
b61e3000 b61e5000 r-xp /usr/lib/libXgesture.so.7.0.0
b61ed000 b61ef000 r-xp /usr/lib/libXcomposite.so.1.0.0
b61f7000 b61f9000 r-xp /usr/lib/libXdamage.so.1.1.0
b6201000 b6208000 r-xp /usr/lib/libXcursor.so.1.0.2
b6210000 b6213000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b621c000 b6220000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6229000 b622e000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6237000 b6318000 r-xp /usr/lib/libX11.so.6.3.0
b6323000 b6346000 r-xp /usr/lib/libjpeg.so.8.0.2
b635e000 b6374000 r-xp /lib/libz.so.1.2.5
b637d000 b637f000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b6387000 b63fc000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6406000 b6420000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6428000 b645c000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b6465000 b6538000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6544000 b6554000 r-xp /lib/libresolv-2.13.so
b6558000 b6570000 r-xp /usr/lib/liblzma.so.5.0.3
b6578000 b657b000 r-xp /lib/libcap.so.2.21
b6583000 b65b2000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b65ba000 b65bb000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b65c4000 b65ca000 r-xp /usr/lib/libecore_imf.so.1.7.99
b65d2000 b65e9000 r-xp /usr/lib/liblua-5.1.so
b65f2000 b65f9000 r-xp /usr/lib/libembryo.so.1.7.99
b6601000 b6607000 r-xp /lib/librt-2.13.so
b6610000 b6666000 r-xp /usr/lib/libpixman-1.so.0.28.2
b6674000 b66ca000 r-xp /usr/lib/libfreetype.so.6.11.3
b66d6000 b66fe000 r-xp /usr/lib/libfontconfig.so.1.8.0
b66ff000 b6744000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b674d000 b6760000 r-xp /usr/lib/libfribidi.so.0.3.1
b6768000 b6782000 r-xp /usr/lib/libecore_con.so.1.7.99
b678c000 b6795000 r-xp /usr/lib/libedbus.so.1.7.99
b679d000 b67ed000 r-xp /usr/lib/libecore_x.so.1.7.99
b67ef000 b67f8000 r-xp /usr/lib/libvconf.so.0.2.45
b6800000 b6811000 r-xp /usr/lib/libecore_input.so.1.7.99
b6819000 b681e000 r-xp /usr/lib/libecore_file.so.1.7.99
b6826000 b6848000 r-xp /usr/lib/libecore_evas.so.1.7.99
b6851000 b6892000 r-xp /usr/lib/libeina.so.1.7.99
b689b000 b68b4000 r-xp /usr/lib/libeet.so.1.7.99
b68c5000 b692e000 r-xp /lib/libm-2.13.so
b6937000 b693d000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6946000 b6947000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b694f000 b6972000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b697a000 b697f000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6987000 b69b1000 r-xp /usr/lib/libdbus-1.so.3.8.12
b69ba000 b69d1000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b69d9000 b69e4000 r-xp /lib/libunwind.so.8.0.1
b6a11000 b6a2f000 r-xp /usr/lib/libsystemd.so.0.4.0
b6a39000 b6b5d000 r-xp /lib/libc-2.13.so
b6b6b000 b6b73000 r-xp /lib/libgcc_s-4.6.so.1
b6b74000 b6b78000 r-xp /usr/lib/libsmack.so.1.0.0
b6b81000 b6b87000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6b8f000 b6c5f000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6c60000 b6cbe000 r-xp /usr/lib/libedje.so.1.7.99
b6cc8000 b6cdf000 r-xp /usr/lib/libecore.so.1.7.99
b6cf6000 b6dc4000 r-xp /usr/lib/libevas.so.1.7.99
b6dea000 b6f26000 r-xp /usr/lib/libelementary.so.1.7.99
b6f3d000 b6f51000 r-xp /lib/libpthread-2.13.so
b6f5c000 b6f5e000 r-xp /usr/lib/libdlog.so.0.0.0
b6f66000 b6f69000 r-xp /usr/lib/libbundle.so.0.1.22
b6f71000 b6f73000 r-xp /lib/libdl-2.13.so
b6f7c000 b6f89000 r-xp /usr/lib/libaul.so.0.1.0
b6f9b000 b6fa1000 r-xp /usr/lib/libappcore-efl.so.1.1
b6faa000 b6fae000 r-xp /usr/lib/libsys-assert.so
b6fb7000 b6fd4000 r-xp /lib/ld-2.13.so
b6fdd000 b6fe2000 r-xp /usr/bin/launchpad-loader
b88e1000 b8e80000 rw-p [heap]
bebcf000 bebf0000 rw-p [stack]
b88e1000 b8e80000 rw-p [heap]
bebcf000 bebf0000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2676)
Call Stack Count: 1
 0: realloc + 0x4c (0xb6aab6f0) [/lib/libc.so.6] + 0x726f0
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
 windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
04-27 11:10:29.379+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:1193 _on_item_push_finished() item(0xb8a20330) transition finished
04-27 11:10:29.379+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
04-27 11:10:29.379+0700 W/W_INDICATOR( 1142): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 2
04-27 11:10:29.379+0700 W/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 14 / signal : type_wifi_connected_02
04-27 11:10:29.379+0700 E/W_INDICATOR( 1142): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 14) / (hide : 0)
04-27 11:10:29.379+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
04-27 11:10:29.379+0700 E/W_INDICATOR( 1142): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
04-27 11:10:29.379+0700 W/W_INDICATOR( 1142): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
04-27 11:10:29.379+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:1218 _on_item_show_finished() item(0xb8b4f1a0) transition finished
04-27 11:10:29.389+0700 E/EFL     ( 2676): <2676> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:10:29.779+0700 E/EFL     ( 2676): ecore_x<2676> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=342538 button=1
04-27 11:10:29.789+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ab86a0 : elm_genlist] mouse move
04-27 11:10:29.789+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ab86a0 : elm_genlist] mouse move
04-27 11:10:29.789+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ab86a0 : elm_genlist] hold(0), freeze(0)
04-27 11:10:29.799+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ab86a0 : elm_genlist] mouse move
04-27 11:10:29.799+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ab86a0 : elm_genlist] hold(0), freeze(0)
04-27 11:10:29.829+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ab86a0 : elm_genlist] mouse move
04-27 11:10:29.829+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ab86a0 : elm_genlist] hold(0), freeze(0)
04-27 11:10:29.909+0700 E/EFL     ( 2676): ecore_x<2676> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=342665 button=1
04-27 11:10:29.949+0700 I/efl-extension( 2676): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:10:29.949+0700 I/efl-extension( 2676): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:10:29.949+0700 I/efl-extension( 2676): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb8b139b8, elm_image, _activated_obj : 0xb8b4ed38, activated : 1
04-27 11:10:29.959+0700 I/efl-extension( 2676): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:10:29.969+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8b85a00 : elm_scroller] mx(300), my(86), minx(0), miny(0), px(0), py(0)
04-27 11:10:29.969+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8b85a00 : elm_scroller] cw(300), ch(86), pw(0), ph(0)
04-27 11:10:29.979+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-27 11:10:29.989+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb8b14868) will be pushed
04-27 11:10:29.989+0700 E/EFL     ( 2676): <2676> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:10:29.999+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8b85a00 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:10:29.999+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8b85a00 : elm_scroller] cw(356), ch(270), pw(356), ph(270)
04-27 11:10:29.999+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8b85a00 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:10:29.999+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8b85a00 : elm_scroller] cw(356), ch(270), pw(356), ph(270)
04-27 11:10:30.039+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:2796 _push_transition_cb() item(0xb8b14868) will transition
04-27 11:10:30.459+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:1193 _on_item_push_finished() item(0xb8b4f1a0) transition finished
04-27 11:10:30.469+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:1218 _on_item_show_finished() item(0xb8b14868) transition finished
04-27 11:10:30.989+0700 I/CAPI_NETWORK_CONNECTION( 2676): connection.c: connection_create(453) > New handle created[0xb8c75650]
04-27 11:10:31.019+0700 E/NOTI    ( 2676): 67026
04-27 11:10:31.019+0700 E/NOTIF ID( 2676): 40198
04-27 11:10:31.019+0700 E/problem ( 2676): Dirty
04-27 11:10:31.019+0700 E/problem_desc( 2676): 19A-Fender Lh Fender Dirty & Stain
04-27 11:10:31.039+0700 E/NOTIF ID( 2676): 40199
04-27 11:10:31.039+0700 E/problem ( 2676): Dirty
04-27 11:10:31.039+0700 E/problem_desc( 2676): 19A-Fender Rh Fender Dirty & Stain
04-27 11:10:31.079+0700 E/BODY NO ( 2676): 67026
04-27 11:10:31.079+0700 E/JSON PARSING( 2676): �бp7��
04-27 11:10:31.159+0700 I/AUL     ( 1146): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-27 11:10:31.189+0700 I/AUL     ( 1146): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-27 11:10:31.289+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:10:31.319+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:10:31.319+0700 E/wnoti-service( 1443): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1082, op_type : 1  //insert = 1, update = 2, delete = 3
04-27 11:10:31.329+0700 E/wnoti-service( 1443): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 2
04-27 11:10:31.329+0700 E/wnoti-service( 1443): wnoti-native-client.c: _receive_notification_changed_cb(1552) > category : 100000
04-27 11:10:31.329+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 0, t_image_path: (null), err: 0
04-27 11:10:31.329+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 7, t_image_path: (null), err: 0
04-27 11:10:31.329+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 6, t_image_path: (null), err: 0
04-27 11:10:31.349+0700 E/wnoti-service( 1443): wnoti-native-client.c: _insert_notification(1208) > fail to get metadata_value, ret : -2
04-27 11:10:31.349+0700 I/CAPI_NETWORK_CONNECTION( 2676): connection.c: connection_create(453) > New handle created[0xb8c68a48]
04-27 11:10:31.359+0700 E/wnoti-service( 1443): wnoti-db-server.c: wnoti_update_panel(2712) > id : -1016, source : 1
04-27 11:10:31.359+0700 E/wnoti-service( 1443): wnoti-native-client.c: _insert_notification(1346) > wearoff : 1, always_wear : 0, block_app_disable : 0, consider_noti_onoff: 0
04-27 11:10:31.359+0700 E/wnoti-service( 1443): wnoti-native-client.c: __insert_notification(727) > category :100000, type : 0, view_type 1, feedback : 2, identifier :wnoti_mobile:1082, application_id: -1016
04-27 11:10:31.379+0700 E/wnoti-service( 1443): wnoti-db-server.c: _wnoti_update_category(861) > Reuse category, application_id : -1016
04-27 11:10:31.379+0700 E/wnoti-service( 1443): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 0, count : 1
04-27 11:10:31.399+0700 E/EFL     ( 2676): ecore_x<2676> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=344151 button=1
04-27 11:10:31.399+0700 I/CAPI_NETWORK_CONNECTION( 2676): connection.c: connection_destroy(471) > Destroy handle: 0xb8c68a48
04-27 11:10:31.409+0700 I/CAPI_NETWORK_CONNECTION( 2676): connection.c: connection_destroy(471) > Destroy handle: 0xb8c75650
04-27 11:10:31.429+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:10:31.459+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:10:31.479+0700 E/APPS    ( 1251): AppsBadge.cpp: onBadgeChange(214) >  (!pAppsItem) -> onBadgeChange() return
04-27 11:10:31.489+0700 I/wnoti-service( 1443): wnoti-sap-client.c: launch_alert_view(421) > timer_id : 0, emergency_cb_mode : 0, blocking_mode : 0  
04-27 11:10:31.489+0700 E/wnoti-service( 1443): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 1
04-27 11:10:31.489+0700 E/wnoti-service( 1443): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : -1016, display_count : 1, 
04-27 11:10:31.499+0700 E/wnoti-proxy( 1251): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1251, caller_id : 0, listener_type : 0
04-27 11:10:31.499+0700 E/wnoti-proxy( 1877): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1877, caller_id : 0, listener_type : 0
04-27 11:10:31.499+0700 E/wnoti-service( 1443): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 1
04-27 11:10:31.499+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:10:31.509+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 1877
04-27 11:10:31.509+0700 E/wnoti-service( 1443): wnoti-sap-client.c: on_timer(275) > is_popup_running: 1, ret: 0
04-27 11:10:31.519+0700 E/wnoti-service( 1443): wnoti-sap-client.c: on_timer(303) > is_exist_alert_list : 0, g_launch_popup_time : 1524802231, g_use_aul_launch : 1524802231
04-27 11:10:31.649+0700 E/EFL     ( 2676): ecore_x<2676> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=344402 button=1
04-27 11:10:31.649+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: -1016, display count: 1
04-27 11:10:31.649+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-27 11:10:31.649+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(837) > Postpone notiboard update.
04-27 11:10:31.659+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_service_changed_cb(243) > Handle this change type in idler.
04-27 11:10:31.659+0700 I/efl-extension( 2676): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:10:31.659+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1161) > !!!!! application_id : -1016, db_id : 1144, is_duplicated : 0
04-27 11:10:31.659+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1311) > view_type : 1, turn_screen_on : 1, allow_gesture : 1, is_used_popup : 0, feedback : 2
04-27 11:10:31.679+0700 I/efl-extension( 2676): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:10:31.689+0700 I/efl-extension( 2676): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb1e164c0, elm_image, _activated_obj : 0xb8b139b8, activated : 1
04-27 11:10:31.689+0700 E/wnoti-proxy( 1877): wnoti-client.c: _wnoti_parse_extra_sub(373) > JSON_IS_NULL
04-27 11:10:31.689+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(226) > application_name: Real Feed Back, application_id: -1016, category_id: 114, time: 1524802231, launch_app_id: (null), bg_image: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, extracted_icon_color: -3139560, disble_block_app_action: 0, support_large_icon 0
04-27 11:10:31.689+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(236) > noti_type: 1
04-27 11:10:31.689+0700 I/wnotibp ( 1877): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(60) > db_id: 1144, noti_type: 1
04-27 11:10:31.689+0700 I/wnotibp ( 1877): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(86) > is_source_companion: 0, content_id: 0, notification_id: 0
04-27 11:10:31.689+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_convert_alert_data(67) > alert_type: 4, app_feedback_type: 2, popup_view_style: 0, feedback_pattern_app: -1
04-27 11:10:31.689+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 0, new_list count : 1
04-27 11:10:31.689+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_notification_change_cb(600) > [1,1144]
04-27 11:10:31.689+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:10:31.689+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-27 11:10:31.689+0700 I/efl-extension( 2676): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:10:31.689+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-27 11:10:31.689+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: _common_app_id_from_win(152) > 2676
04-27 11:10:31.699+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb1e16928) will be pushed
04-27 11:10:31.709+0700 E/EFL     ( 2676): <2676> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:10:31.709+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8ad41e0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:10:31.709+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8ad41e0 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:10:31.729+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8ad41e0 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:10:31.729+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8ad41e0 : elm_genlist] cw(360), ch(245), pw(360), ph(360)
04-27 11:10:31.739+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_check_env_condition(437) > focus app is com.toyota.realtimefeedback, 0
04-27 11:10:31.739+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_add_notification(991) > Add noti_queue [1144, 0]
04-27 11:10:31.739+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 1141
04-27 11:10:31.739+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 1
04-27 11:10:31.739+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(671) > ::DATA:: remove notification data, 1143
04-27 11:10:31.739+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: wnbp_data_free_alert_data_by_db_id(690) > ::DATA:: CHECK DATA : 1
04-27 11:10:31.739+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(73) > ::APP:: CHECK STATE : 7, 0, (null)
04-27 11:10:31.739+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:10:31.739+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(79) > ::APP:: CHECK DATA : 1 1 0000
04-27 11:10:31.739+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:10:31.779+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:2796 _push_transition_cb() item(0xb1e16928) will transition
04-27 11:10:31.779+0700 W/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-27 11:10:31.789+0700 I/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_active_app_id(1010) > [2676]
04-27 11:10:31.819+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:10:31.819+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1178) > [1144, 1, 0, 6, 0000]
04-27 11:10:31.819+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1179) > [0, 1, 0]
04-27 11:10:31.819+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1180) > [1, 0, 0, 0]
04-27 11:10:31.819+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(588) > ::UI:: [[[ ===> [small popup] is LOCK, 0010 ]]]
04-27 11:10:31.819+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_detail_layout(3707) > wnotiboard_popup_vi_type: 2
04-27 11:10:31.819+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_detail_layout(3712) > (1144, 1144)
04-27 11:10:31.819+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_first_action_info(6221) > No need to add default actions for companion noti.
04-27 11:10:31.849+0700 I/efl-extension( 1877): efl_extension_circle_surface.c: eext_circle_surface_layout_add(1290) > Put the surface[0xb80c2638]'s widget[0xaf317a08] to layout widget[0xaf3256b0]
04-27 11:10:31.869+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_genlist(3639) > (1144, 1144)
04-27 11:10:31.889+0700 I/efl-extension( 1877): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:10:31.939+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_card_data(3073) > 0xb28f0bf8, 0xb28f0a80, 0xb28f0a80
04-27 11:10:31.939+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb0ebd930 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:10:31.939+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb0ebd930 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:10:32.019+0700 E/EFL     ( 1877): elementary<1877> elm_genlist.c:7236 elm_genlist_item_item_class_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-27 11:10:32.019+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:10:32.019+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:10:32.019+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:10:32.019+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:10:32.029+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: stack.separator
04-27 11:10:32.029+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 11:10:32.029+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.029+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.029+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.039+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.039+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.039+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.039+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.039+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.039+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.039+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-27 11:10:32.039+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.039+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.039+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.039+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.039+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.039+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.039+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.039+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.039+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.049+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:10:32.079+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:10:32.079+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 11:10:32.079+0700 E/EFL     ( 1877): elementary<1877> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-27 11:10:32.079+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(224) > can't get layout
04-27 11:10:32.079+0700 E/EFL     ( 1877): elementary<1877> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-27 11:10:32.079+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:10:32.079+0700 I/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1144, 114, -1016.
04-27 11:10:32.129+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:10:32.129+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:10:32.129+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:10:32.129+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:10:32.159+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:10:32.159+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:10:32.159+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:10:32.159+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 11:10:32.159+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.159+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.159+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.159+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.159+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.159+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.159+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.159+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.159+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.159+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-27 11:10:32.169+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.169+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.169+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.169+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.169+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.169+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.169+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:10:32.169+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:10:32.169+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:10:32.169+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:10:32.179+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:10:32.179+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 11:10:32.179+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-27 11:10:32.179+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:10:32.179+0700 I/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1144, 114, -1016.
04-27 11:10:32.189+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb0ebd930 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:10:32.189+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb0ebd930 : elm_genlist] cw(360), ch(360), pw(360), ph(360)
04-27 11:10:32.209+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb0ebd930 : elm_genlist] mx(0), my(52), minx(0), miny(0), px(0), py(0)
04-27 11:10:32.219+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb0ebd930 : elm_genlist] cw(360), ch(412), pw(360), ph(360)
04-27 11:10:32.219+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: wnbp_view_draw_small_view(4060) > ::UI:: window type is changed by unknown causes
04-27 11:10:32.229+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1016) > _query_step failed(NO ROW)
04-27 11:10:32.239+0700 E/wnoti-service( 1443): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 2
04-27 11:10:32.249+0700 E/wnoti-service( 1443): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 0
04-27 11:10:32.259+0700 E/wnoti-proxy( 1877): wnoti.c: _wnoti_get_categories(1276) > failed: GDBus.Error:org.freedesktop.DBus.Error.Failed: Empty List
04-27 11:10:32.259+0700 E/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(311) > ::DATA:: No categories available.
04-27 11:10:32.259+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 1, new_list count : 0
04-27 11:10:32.279+0700 W/APP_CORE( 1877): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3000009
04-27 11:10:32.299+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:1193 _on_item_push_finished() item(0xb8b14868) transition finished
04-27 11:10:32.299+0700 E/EFL     ( 2676): elementary<2676> elc_naviframe.c:1218 _on_item_show_finished() item(0xb1e16928) transition finished
04-27 11:10:32.309+0700 E/EFL     ( 2676): <2676> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:10:32.309+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2809) > ::UI:: VI TYPE : 2
04-27 11:10:32.319+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:10:32.319+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2816) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-27 11:10:32.319+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:10:32.319+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(284) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-27 11:10:32.329+0700 W/TIZEN_N_RECORDER( 1877): recorder_product.c: recorder_is_in_recording(82) > pid =/proc/0 , state =0, alive=0
04-27 11:10:32.329+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(298) > ::APP:: application_id: -1016, is_disaster: 0, disaster_info: (null), app_feedback_type: 2, feedback_pattern_app: -1, is_recording: 0
04-27 11:10:32.329+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(307) > Gear side feedback setting is_sound_on: 0, is_vibration_on: 1, is_vibrate_when_noti_on: 0
04-27 11:10:32.329+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1877] goes to (3)
04-27 11:10:32.329+0700 W/AUL_AMD (  929): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-27 11:10:32.329+0700 W/AUL_AMD (  929): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-27 11:10:32.329+0700 W/AUL     (  929): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.wnotiboard-popup) pid(1877) status(fg) type(uiapp)
04-27 11:10:32.339+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(341) > ::APP:: Determined feedback: sound 0, vibration: 1
04-27 11:10:32.349+0700 I/wnotib  ( 1877): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 13, ret: -3, request_id: 0
04-27 11:10:32.349+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_set_smart_relay(501) > Set the smart relay for 0, 114, -1016, Real Feed Back
04-27 11:10:32.349+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:10:32.349+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_turn_on_lcd(534) > [4, 0, 1, 0]
04-27 11:10:32.429+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(263) > fully_obscured: 0, 0
04-27 11:10:32.429+0700 I/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(264) > [0x3000009 0x300000d 0x3000009]
04-27 11:10:32.429+0700 I/APP_CORE( 1877): appcore-efl.c: __do_app(453) > [APP 1877] Event: RESUME State: PAUSED
04-27 11:10:32.429+0700 I/CAPI_APPFW_APPLICATION( 1877): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-27 11:10:32.429+0700 I/wnotibp ( 1877): wnotiboard-popup.c: _popup_app_resume(229) > 
04-27 11:10:32.429+0700 I/GATE    ( 1877): <GATE-M>APP_FULLY_LOADED_wnotiboard-popup</GATE-M>
04-27 11:10:32.429+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(585) > ::UI:: [[[ ===> already [small popup] is LOCK, 0010 ]]]
04-27 11:10:32.429+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_sub_popup_show_animator_pre_cb(1781) > ::UI:: start showing animation
04-27 11:10:32.729+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_sub_popup_show_animator_cb(1684) > ::UI:: end show animation
04-27 11:10:32.729+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [small popup] is UNLOCK , 0000 <=== ]]]
04-27 11:10:32.979+0700 E/EFL     ( 2676): ecore_x<2676> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=345729 button=1
04-27 11:10:32.979+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ad41e0 : elm_genlist] mouse move
04-27 11:10:32.979+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ad41e0 : elm_genlist] mouse move
04-27 11:10:32.979+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ad41e0 : elm_genlist] hold(0), freeze(0)
04-27 11:10:33.069+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb8ad41e0 : elm_genlist] mouse move
04-27 11:10:33.069+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb8ad41e0 : elm_genlist] hold(0), freeze(0)
04-27 11:10:33.109+0700 E/EFL     ( 2676): ecore_x<2676> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=345848 button=1
04-27 11:10:33.139+0700 I/efl-extension( 2676): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:10:33.139+0700 I/efl-extension( 2676): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:10:33.159+0700 I/efl-extension( 2676): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb1e28db0, elm_image, _activated_obj : 0xb1e164c0, activated : 1
04-27 11:10:33.159+0700 I/efl-extension( 2676): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:10:33.189+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb1e27050 : elm_scroller] mx(300), my(86), minx(0), miny(0), px(0), py(0)
04-27 11:10:33.189+0700 E/EFL     ( 2676): elementary<2676> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb1e27050 : elm_scroller] cw(300), ch(86), pw(0), ph(0)
04-27 11:10:33.729+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1251] goes to (3)
04-27 11:10:33.729+0700 E/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1251)'s state(3)
04-27 11:10:33.759+0700 W/AUL_AMD (  929): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-27 11:10:33.759+0700 W/AUL_AMD (  929): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-27 11:10:33.759+0700 W/AUL     (  929): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1251) status(fg) type(uiapp)
04-27 11:10:33.839+0700 W/W_HOME  ( 1251): event_manager.c: _window_visibility_cb(460) > Window [0x2400003] is now visible(0)
04-27 11:10:33.839+0700 W/W_HOME  ( 1251): event_manager.c: _window_visibility_cb(470) > state: 0 -> 1
04-27 11:10:33.839+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:1(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:10:33.839+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:6, app_state:2 win_state:1(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:10:33.839+0700 W/W_HOME  ( 1251): main.c: _window_visibility_cb(996) > Window [0x2400003] is now visible(0)
04-27 11:10:33.839+0700 I/APP_CORE( 1251): appcore-efl.c: __do_app(453) > [APP 1251] Event: RESUME State: PAUSED
04-27 11:10:33.839+0700 I/CAPI_APPFW_APPLICATION( 1251): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-27 11:10:33.839+0700 W/W_HOME  ( 1251): main.c: _appcore_resume_cb(480) > appcore resume
04-27 11:10:33.839+0700 W/W_HOME  ( 1251): event_manager.c: _app_resume_cb(373) > state: 2 -> 1
04-27 11:10:33.839+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:10:33.839+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:0, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:10:33.839+0700 W/W_HOME  ( 1251): main.c: home_resume(528) > journal_multimedia_screen_loaded_home called
04-27 11:10:33.839+0700 W/W_HOME  ( 1251): main.c: home_resume(532) > clock/widget resumed
04-27 11:10:33.849+0700 W/W_INDICATOR( 1142): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
04-27 11:10:33.849+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
04-27 11:10:33.849+0700 E/W_HOME  ( 1251): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
04-27 11:10:33.849+0700 W/W_HOME  ( 1251): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:1(1) pm_state:1 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
04-27 11:10:33.849+0700 I/GATE    ( 1251): <GATE-M>APP_FULLY_LOADED_w-home</GATE-M>
04-27 11:10:33.859+0700 I/wnotib  ( 1251): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 0
04-27 11:10:33.859+0700 E/wnotib  ( 1251): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
04-27 11:10:33.859+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: wnb_nm_do_postponed_job(958) > Do the postponed update job with is_for_VI: 0, postponed_for_VI: 0.
04-27 11:10:33.859+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(444) > idler for type: 0
04-27 11:10:33.889+0700 W/AUL_PAD ( 1876): sigchild.h: __launchpad_process_sigchld(188) > dead_pid = 2676 pgid = 2676
04-27 11:10:33.889+0700 W/AUL_PAD ( 1876): sigchild.h: __launchpad_process_sigchld(189) > ssi_code = 2 ssi_status = 11
04-27 11:10:33.989+0700 E/wnoti-proxy( 1251): wnoti-client.c: _wnoti_parse_extra_sub(373) > JSON_IS_NULL
04-27 11:10:33.989+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(492) > unread_count: 1
04-27 11:10:33.989+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_add_category_data(192) > category_id: 114, application_id: -1016, application_name: Real Feed Back, time_stamp: 1524802231, content_id: 0, spannable_string_version: 0, disble_block_app_action: 0, support_large_icon: 0, android_app_color: 0
04-27 11:10:33.989+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(530) > noti_type [1]
04-27 11:10:33.989+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: wnb_nm_add_detail_data(85) > noti_type: 1, application_id: -1016, application_name: Real Feed Back, category_id: 114, spannable_string_version: 0
04-27 11:10:33.989+0700 I/wnotib  ( 1251): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(60) > db_id: 1144, noti_type: 1
04-27 11:10:33.989+0700 I/wnotib  ( 1251): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(86) > is_source_companion: 0, content_id: 0, notification_id: 0
04-27 11:10:33.989+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(544) > package_name com.toyota.realtimefeedback
04-27 11:10:33.989+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(679) > New noti is here. Add it on panel  114, Real Feed Back.
04-27 11:10:33.989+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_add_card(340) > db_id: 1144, application_id: -1016, application_name: Real Feed Back
04-27 11:10:33.989+0700 W/wnotib  ( 1251): w-notification-board-panel-manager.c: wnb_pm_create_page_instance_with_id(257) > Create a panel for panel_id: 1, instance_id: 114
04-27 11:10:33.989+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_initialize(5531) > Initialize the panel with id: 114
04-27 11:10:33.989+0700 W/wnotib  ( 1251): w-notification-board-panel-manager.c: wnb_pm_create_page_instance_with_id(272) > Page instance, instance_id [114] has been created.
04-27 11:10:33.989+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_message_receive(4800) > New event with category_id: 114, application_id: -1016, type: 1
04-27 11:10:34.099+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_message_receive(5143) > Skip adding the new noti to the second depth genlist. second_depth_type: 0.
04-27 11:10:34.099+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_message_receive(4800) > New event with category_id: 114, application_id: -1016, type: 10
04-27 11:10:34.099+0700 W/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_message_receive(5214) > notiboard panel creation count [6], notiboard card appending count [6].
04-27 11:10:34.119+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xac4ae110 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:10:34.119+0700 E/EFL     ( 1251): elementary<1251> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xac4ae110 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:10:34.129+0700 I/wnotib  ( 1251): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 11:10:34.149+0700 I/wnotib  ( 1251): w-notification-board-common.c: wnb_common_add_badge(2015) > badge is unused.
04-27 11:10:34.179+0700 W/AUL_PAD ( 1876): sigchild.h: __launchpad_process_sigchld(197) > after __sigchild_action
04-27 11:10:34.189+0700 I/AUL_AMD (  929): amd_main.c: __app_dead_handler(262) > __app_dead_handler, pid: 2676
04-27 11:10:34.189+0700 W/AUL     (  929): app_signal.c: aul_send_app_terminated_signal(799) > aul_send_app_terminated_signal pid(2676)
04-27 11:10:34.239+0700 W/ELM_RPANEL( 1251): elm-rpanel.c: _panel_reorder_compare_cb(1413) > Ordering can not be working fine.
04-27 11:10:34.249+0700 W/ELM_RPANEL( 1251): elm-rpanel.c: _panel_reorder_compare_cb(1414) > rit1->unique_number [0]
04-27 11:10:34.259+0700 W/ELM_RPANEL( 1251): elm-rpanel.c: _panel_reorder_compare_cb(1415) > rit2->unique_number [114]
04-27 11:10:34.259+0700 W/ELM_RPANEL( 1251): elm-rpanel.c: _panel_reorder_compare_cb(1416) > rit1->last_update_timestamp [0]
04-27 11:10:34.259+0700 W/ELM_RPANEL( 1251): elm-rpanel.c: _panel_reorder_compare_cb(1417) > rit2->last_update_timestamp [1524802231]
04-27 11:10:34.259+0700 I/wnotib  ( 1251): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 11:10:34.279+0700 I/wnotib  ( 1251): w-notification-board-common.c: wnb_common_add_badge(2015) > badge is unused.
04-27 11:10:34.309+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(714) > This noti will be removed from panel: 114, Real Feed Back.
04-27 11:10:34.309+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_remove_card(419) > db_id: 1141, application_id: -1016, application_name: Real Feed Back
04-27 11:10:34.309+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_message_receive(4800) > New event with category_id: 114, application_id: -1016, type: 2
04-27 11:10:34.309+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_delete_card(3278) > db_id: 1141, is_request_from_noti_service: 1
04-27 11:10:34.309+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_delete_card(3432) > We don't need to delete the item for second_depth_type: 0.
04-27 11:10:34.309+0700 E/EFL     ( 1251): elementary<1251> elm_genlist.c:6332 elm_genlist_items_count() obj ((nil)) is NULL or type is not correct
04-27 11:10:34.309+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(714) > This noti will be removed from panel: 115, LINE.
04-27 11:10:34.309+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_remove_card(419) > db_id: 1143, application_id: 219, application_name: LINE
04-27 11:10:34.309+0700 I/wnotib  ( 1251): w-notification-board-basic-panel.c: _wnb_bp_message_receive(4800) > New event with category_id: 114, application_id: -1016, type: 9
04-27 11:10:34.309+0700 I/wnotib  ( 1251): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 11:10:34.319+0700 I/wnotib  ( 1251): w-notification-board-common.c: wnb_common_add_badge(2015) > badge is unused.
04-27 11:10:34.319+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(724) > Num categories: 1, num total noti: 1
04-27 11:10:34.319+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_free_data(248) > Free noti manager data.
04-27 11:10:34.319+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_free_data(253) > Free previous notifications.
04-27 11:10:34.319+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_free_data(274) > Free previous categories.
04-27 11:10:34.319+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(747) > before_rpanel_count: 0, after_rpanel_count: 2.
04-27 11:10:34.329+0700 W/W_HOME  ( 1251): noti_broker.c: _handler_indicator_update(560) > 0x2
04-27 11:10:34.379+0700 W/CRASH_MANAGER( 2718): worker.c: worker_job(1205) > 1102676726561152480223
