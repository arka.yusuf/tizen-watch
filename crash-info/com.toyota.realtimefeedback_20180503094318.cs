S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2690
Date: 2018-05-03 09:43:18+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2690, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb6f5d954
r2   = 0xb6f5df68, r3   = 0x00000000
r4   = 0xb7135c50, r5   = 0xb741d338
r6   = 0x00000013, r7   = 0xbef91188
r8   = 0x00002dd1, r9   = 0xb7135c50
r10  = 0x00000000, fp   = 0x00000000
ip   = 0xb6f5dba8, sp   = 0xbef91168
lr   = 0xb6f4b50f, pc   = 0xb6f4b544
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:      7080 KB
Buffers:      5472 KB
Cached:     104748 KB
VmPeak:     174164 KB
VmSize:     168264 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       30688 KB
VmRSS:       30488 KB
VmData:      95976 KB
VmStk:         136 KB
VmExe:          40 KB
VmLib:       35904 KB
VmPTE:         128 KB
VmSwap:          0 KB

Threads Information
Threads: 4
PID = 2690 TID = 2690
2690 2695 2702 2708 

Maps Information
b04f3000 b04f7000 r-xp /usr/lib/libogg.so.0.7.1
b04ff000 b0521000 r-xp /usr/lib/libvorbis.so.0.4.3
b0529000 b0570000 r-xp /usr/lib/libsndfile.so.1.0.26
b057c000 b05c5000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b05ce000 b05d3000 r-xp /usr/lib/libjson.so.0.0.1
b1e74000 b1f7a000 r-xp /usr/lib/libicuuc.so.57.1
b1f90000 b2118000 r-xp /usr/lib/libicui18n.so.57.1
b2128000 b2135000 r-xp /usr/lib/libail.so.0.1.0
b213e000 b2141000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b2149000 b2181000 r-xp /usr/lib/libpulse.so.0.16.2
b2182000 b2185000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b218d000 b21ee000 r-xp /usr/lib/libasound.so.2.0.0
b21f8000 b2211000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b221a000 b221e000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b2226000 b2231000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b223e000 b2242000 r-xp /usr/lib/libmmfsession.so.0.0.0
b224b000 b2263000 r-xp /usr/lib/libmmfsound.so.0.1.0
b2274000 b227b000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b2283000 b228e000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b2296000 b2298000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b22a0000 b22a1000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b22a9000 b22b1000 r-xp /usr/lib/libfeedback.so.0.1.4
b22ca000 b22cb000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b2347000 b23ce000 rw-s anon_inode:dmabuf
b23ce000 b2455000 rw-s anon_inode:dmabuf
b24e5000 b256c000 rw-s anon_inode:dmabuf
b27ee000 b2875000 rw-s anon_inode:dmabuf
b2876000 b3075000 rw-p [stack:2708]
b335d000 b335e000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b33d1000 b33d2000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b33da000 b33dd000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3466000 b3c65000 rw-p [stack:2702]
b3c65000 b3c67000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b3c6f000 b3c86000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b3e97000 b4696000 rw-p [stack:2695]
b46ad000 b4fd9000 r-xp /usr/lib/libsc-a3xx.so
b523d000 b523f000 r-xp /usr/lib/libdri2.so.0.0.0
b5247000 b524f000 r-xp /usr/lib/libdrm.so.2.4.0
b5257000 b525b000 r-xp /usr/lib/libxcb-xfixes.so.0.0.0
b5263000 b5266000 r-xp /usr/lib/libxcb-dri2.so.0.0.0
b526e000 b526f000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b5277000 b5282000 r-xp /usr/lib/libtbm.so.1.0.0
b528a000 b528d000 r-xp /usr/lib/libnative-buffer.so.0.1.0
b5295000 b5297000 r-xp /usr/lib/libgenlock.so
b529f000 b52a4000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b52ac000 b53e7000 r-xp /usr/lib/egl/libGLESv2.so
b5423000 b5425000 r-xp /usr/lib/libadreno_utils.so
b542f000 b5456000 r-xp /usr/lib/libgsl.so
b5465000 b546c000 r-xp /usr/lib/egl/eglsubX11.so
b5476000 b5498000 r-xp /usr/lib/egl/libEGL.so
b54a1000 b5516000 r-xp /usr/lib/evas/modules/engines/gl_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5726000 b5730000 r-xp /lib/libnss_files-2.13.so
b5739000 b573c000 r-xp /lib/libattr.so.1.1.0
b5744000 b574b000 r-xp /lib/libcrypt-2.13.so
b577b000 b577e000 r-xp /lib/libcap.so.2.21
b5786000 b5788000 r-xp /usr/lib/libiri.so
b5790000 b57ad000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b57b6000 b57ba000 r-xp /usr/lib/libsmack.so.1.0.0
b57c3000 b57f2000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b57fa000 b588e000 r-xp /usr/lib/libstdc++.so.6.0.16
b58a1000 b5970000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5986000 b59aa000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b59b3000 b5a7d000 r-xp /usr/lib/libCOREGL.so.4.0
b5a94000 b5a96000 r-xp /usr/lib/libXau.so.6.0.0
b5a9f000 b5aaf000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5ab7000 b5aba000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5ac2000 b5ada000 r-xp /usr/lib/liblzma.so.5.0.3
b5ae3000 b5ae5000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b5aed000 b5af0000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b5af8000 b5afc000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b5b05000 b5b0a000 r-xp /usr/lib/libecore_fb.so.1.7.99
b5b14000 b5b37000 r-xp /usr/lib/libjpeg.so.8.0.2
b5b4f000 b5b65000 r-xp /lib/libexpat.so.1.6.0
b5b6f000 b5b82000 r-xp /usr/lib/libxcb.so.1.1.0
b5b8b000 b5b91000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5b99000 b5b9a000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5ba4000 b5bbc000 r-xp /usr/lib/libpng12.so.0.50.0
b5bc4000 b5bc7000 r-xp /usr/lib/libEGL.so.1.4
b5bcf000 b5bdd000 r-xp /usr/lib/libGLESv2.so.2.0
b5be6000 b5be7000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b5bef000 b5c06000 r-xp /usr/lib/liblua-5.1.so
b5c10000 b5c17000 r-xp /usr/lib/libembryo.so.1.7.99
b5c1f000 b5c29000 r-xp /usr/lib/libXext.so.6.4.0
b5c32000 b5c36000 r-xp /usr/lib/libXtst.so.6.1.0
b5c3e000 b5c44000 r-xp /usr/lib/libXrender.so.1.3.0
b5c4c000 b5c52000 r-xp /usr/lib/libXrandr.so.2.2.0
b5c5a000 b5c5b000 r-xp /usr/lib/libXinerama.so.1.0.0
b5c65000 b5c68000 r-xp /usr/lib/libXfixes.so.3.1.0
b5c70000 b5c72000 r-xp /usr/lib/libXgesture.so.7.0.0
b5c7a000 b5c7c000 r-xp /usr/lib/libXcomposite.so.1.0.0
b5c84000 b5c86000 r-xp /usr/lib/libXdamage.so.1.1.0
b5c8e000 b5c95000 r-xp /usr/lib/libXcursor.so.1.0.2
b5c9e000 b5cae000 r-xp /lib/libresolv-2.13.so
b5cb2000 b5cb4000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5cbc000 b5cc1000 r-xp /usr/lib/libffi.so.5.0.10
b5cc9000 b5cca000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5cd2000 b5d1b000 r-xp /usr/lib/libmdm.so.1.2.70
b5d25000 b5d2b000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b5d33000 b5d39000 r-xp /usr/lib/libecore_imf.so.1.7.99
b5d41000 b5d5b000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5d63000 b5d81000 r-xp /usr/lib/libsystemd.so.0.4.0
b5d8c000 b5d8d000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b5d95000 b5d9a000 r-xp /usr/lib/libxdgmime.so.1.1.0
b5da2000 b5db9000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b5dc1000 b5dc7000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b5dd0000 b5dd9000 r-xp /usr/lib/libcom-core.so.0.0.1
b5de3000 b5de5000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5dee000 b5e44000 r-xp /usr/lib/libpixman-1.so.0.28.2
b5e51000 b5ea7000 r-xp /usr/lib/libfreetype.so.6.11.3
b5eb3000 b5ef8000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b5f01000 b5f14000 r-xp /usr/lib/libfribidi.so.0.3.1
b5f1d000 b5f37000 r-xp /usr/lib/libecore_con.so.1.7.99
b5f40000 b5f6a000 r-xp /usr/lib/libdbus-1.so.3.8.12
b5f73000 b5f7c000 r-xp /usr/lib/libedbus.so.1.7.99
b5f84000 b5f95000 r-xp /usr/lib/libecore_input.so.1.7.99
b5f9d000 b5fa2000 r-xp /usr/lib/libecore_file.so.1.7.99
b5fab000 b5fcd000 r-xp /usr/lib/libecore_evas.so.1.7.99
b5fd6000 b5fef000 r-xp /usr/lib/libeet.so.1.7.99
b6000000 b6028000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6029000 b6032000 r-xp /usr/lib/libXi.so.6.1.0
b603a000 b611b000 r-xp /usr/lib/libX11.so.6.3.0
b6127000 b61df000 r-xp /usr/lib/libcairo.so.2.11200.14
b61ea000 b6248000 r-xp /usr/lib/libedje.so.1.7.99
b6252000 b62a2000 r-xp /usr/lib/libecore_x.so.1.7.99
b62a4000 b630d000 r-xp /lib/libm-2.13.so
b6316000 b631c000 r-xp /lib/librt-2.13.so
b6325000 b633b000 r-xp /lib/libz.so.1.2.5
b6344000 b64d6000 r-xp /usr/lib/libcrypto.so.1.0.0
b64f7000 b653e000 r-xp /usr/lib/libssl.so.1.0.0
b654a000 b6578000 r-xp /usr/lib/libidn.so.11.5.44
b6580000 b6589000 r-xp /usr/lib/libcares.so.2.1.0
b6592000 b665e000 r-xp /usr/lib/libxml2.so.2.7.8
b666c000 b666e000 r-xp /usr/lib/libiniparser.so.0
b6677000 b66ab000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b66b4000 b6787000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6792000 b67ab000 r-xp /usr/lib/libnetwork.so.0.0.0
b67b3000 b67bc000 r-xp /usr/lib/libvconf.so.0.2.45
b67c5000 b6895000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6896000 b68d7000 r-xp /usr/lib/libeina.so.1.7.99
b68e0000 b68e5000 r-xp /usr/lib/libappcore-common.so.1.1
b68ed000 b68f3000 r-xp /usr/lib/libappcore-efl.so.1.1
b68fb000 b68fe000 r-xp /usr/lib/libbundle.so.0.1.22
b6906000 b690c000 r-xp /usr/lib/libappsvc.so.0.1.0
b6914000 b6928000 r-xp /lib/libpthread-2.13.so
b6933000 b6956000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b695e000 b696b000 r-xp /usr/lib/libaul.so.0.1.0
b6975000 b6977000 r-xp /lib/libdl-2.13.so
b6980000 b698b000 r-xp /lib/libunwind.so.8.0.1
b69b8000 b69c0000 r-xp /lib/libgcc_s-4.6.so.1
b69c1000 b6ae5000 r-xp /lib/libc-2.13.so
b6af3000 b6b68000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6b72000 b6b7e000 r-xp /usr/lib/libnotification.so.0.1.0
b6b87000 b6b96000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b6b9f000 b6c6d000 r-xp /usr/lib/libevas.so.1.7.99
b6c93000 b6dcf000 r-xp /usr/lib/libelementary.so.1.7.99
b6de6000 b6e07000 r-xp /usr/lib/libefl-extension.so.0.1.0
b6e0f000 b6e26000 r-xp /usr/lib/libecore.so.1.7.99
b6e3d000 b6e3f000 r-xp /usr/lib/libdlog.so.0.0.0
b6e47000 b6e8b000 r-xp /usr/lib/libcurl.so.4.3.0
b6e94000 b6e99000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b6ea1000 b6ea6000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b6eae000 b6ebe000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b6ec6000 b6ece000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b6ed6000 b6eda000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b6ee2000 b6ee6000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b6eef000 b6ef1000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b6efc000 b6f07000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b6f11000 b6f15000 r-xp /usr/lib/libsys-assert.so
b6f1e000 b6f3b000 r-xp /lib/ld-2.13.so
b6f44000 b6f4e000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b7087000 b7b48000 rw-p [heap]
bef71000 bef92000 rw-p [stack]
b6f44000 b6f4e000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b7087000 b7b48000 rw-p [heap]
bef71000 bef92000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2690)
Call Stack Count: 20
 0: set_targeted_view + 0x87 (0xb6f4b544) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7544
 1: pop_from_stack_uib_vc + 0x38 (0xb6f4ba49) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a49
 2: uib_views_destroy_callback + 0x38 (0xb6f4ba05) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a05
 3: (0xb6bb6af9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_object_del + 0x94 (0xb6bcdc39) [/usr/lib/libevas.so.1] + 0x2ec39
 5: (0xb6bc037b) [/usr/lib/libevas.so.1] + 0x2137b
 6: evas_free + 0x200 (0xb6bc0ab5) [/usr/lib/libevas.so.1] + 0x21ab5
 7: (0xb5fbacf1) [/usr/lib/libecore_evas.so.1] + 0xfcf1
 8: (0xb6d91953) [/usr/lib/libelementary.so.1] + 0xfe953
 9: (0xb6e1a3f5) [/usr/lib/libecore.so.1] + 0xb3f5
10: (0xb6e17e53) [/usr/lib/libecore.so.1] + 0x8e53
11: (0xb6e1b46b) [/usr/lib/libecore.so.1] + 0xc46b
12: ecore_main_loop_iterate + 0x22 (0xb6e1b7db) [/usr/lib/libecore.so.1] + 0xc7db
13: elm_shutdown + 0x2c (0xb6d4e44d) [/usr/lib/libelementary.so.1] + 0xbb44d
14: appcore_efl_main + 0x44c (0xb68f0c61) [/usr/lib/libappcore-efl.so.1] + 0x3c61
15: ui_app_main + 0xb0 (0xb6ed7ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16:  + 0x0 (0xb6f4940f) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x540f
17: main + 0x34 (0xb6f49b0d) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5b0d
18: __libc_start_main + 0x114 (0xb69d885c) [/lib/libc.so.6] + 0x1785c
19: (0xb6f47124) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x3124
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
rt(115) > _MessagePortStub::OnCheckRemotePort.
05-03 09:43:11.529+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 09:43:11.529+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 09:43:11.529+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
05-03 09:43:11.529+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 09:43:11.529+0700 E/EFL     ( 1150): <1150> elm_main.c:1622 elm_object_signal_emit() safety check failed: obj == NULL
05-03 09:43:11.529+0700 I/TIZEN_N_SOUND_MANAGER( 1150): sound_manager.c: sound_manager_get_volume(84) > returns : type=3, volume=11, ret=0x0
05-03 09:43:11.529+0700 W/TIZEN_N_SOUND_MANAGER( 1150): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_get_volume] ERROR_NONE (0x00000000)
05-03 09:43:11.529+0700 W/W_INDICATOR( 1150): windicator_quick_setting_brightness.c: windicator_quick_setting_brightness_update(94) > [windicator_quick_setting_brightness_update:94] hyun 20
05-03 09:43:11.539+0700 W/W_INDICATOR( 1150): windicator_connection.c: windicator_connection_resume(2158) > [windicator_connection_resume:2158] 
05-03 09:43:11.539+0700 W/W_INDICATOR( 1150): windicator_connection.c: windicator_connection_resume(2223) > [windicator_connection_resume:2223] primary rssi level : (0), s_info.rssi_level : (8) / primary svc type : (1)
05-03 09:43:11.539+0700 W/W_INDICATOR( 1150): windicator_connection.c: _rssi_changed_cb(1924) > [_rssi_changed_cb:1924] roaming status : 0
05-03 09:43:11.539+0700 W/W_INDICATOR( 1150): windicator_connection.c: _rssi_changed_cb(1933) > [_rssi_changed_cb:1933] svc type : 1
05-03 09:43:11.539+0700 W/W_INDICATOR( 1150): windicator_connection.c: _rssi_changed_cb(1968) > [_rssi_changed_cb:1968] Rssi level has already been 0!, Don't need to show rssi down animation. Just Show No Signal icon
05-03 09:43:11.539+0700 W/W_INDICATOR( 1150): windicator_connection.c: _rssi_icon_set(1124) > [_rssi_icon_set:1124] RSSI level : 8/5, (0xb8ccac30)
05-03 09:43:11.539+0700 E/W_INDICATOR( 1150): windicator_connection.c: _rssi_icon_set(1147) > [_rssi_icon_set:1147] Set RSSI SHOW sw.icon_0 (rssi_level : 8) (rssi_hide : 0)(b8ccac30)
05-03 09:43:11.539+0700 W/W_INDICATOR( 1150): windicator_connection.c: _rssi_icon_set(1215) > [_rssi_icon_set:1215] NETWORK_ATT or NETWORK_TMB : there is no roaming icon
05-03 09:43:11.539+0700 W/W_INDICATOR( 1150): windicator_connection.c: _rssi_icon_set(1217) > [_rssi_icon_set:1217] rssi name : set_rssi_verizon_No_signal (0xb8ccac30)
05-03 09:43:11.549+0700 W/W_INDICATOR( 1150): windicator_connection.c: _tapi_changed_cb(2115) > [_tapi_changed_cb:2115] modem_power : 0
05-03 09:43:11.549+0700 W/W_INDICATOR( 1150): windicator_connection.c: _connection_type_changed_cb(1324) > [_connection_type_changed_cb:1324] wifi state : 2
05-03 09:43:11.549+0700 W/W_INDICATOR( 1150): windicator_connection.c: _connection_type_changed_cb(1327) > [_connection_type_changed_cb:1327] Show wifi icon!
05-03 09:43:11.549+0700 W/W_INDICATOR( 1150): windicator_connection.c: _wifi_state_changed_cb(930) > [_wifi_state_changed_cb:930] wifi state : 2
05-03 09:43:11.549+0700 W/W_INDICATOR( 1150): windicator_connection.c: _wifi_state_changed_cb(974) > [_wifi_state_changed_cb:974] ap_name : (ATD-Wifi-Cisco)
05-03 09:43:11.549+0700 W/W_INDICATOR( 1150): windicator_connection.c: _wifi_state_changed_cb(994) > [_wifi_state_changed_cb:994] wifi strength : 2
05-03 09:43:11.549+0700 W/W_INDICATOR( 1150): windicator_connection.c: _connection_icon_set(713) > [_connection_icon_set:713] type : 14 / signal : type_wifi_connected_02
05-03 09:43:11.549+0700 E/W_INDICATOR( 1150): windicator_connection.c: _connection_icon_set(735) > [_connection_icon_set:735] Set Connection / show sw.icon_1 (type : 14) / (hide : 0)
05-03 09:43:11.549+0700 W/W_INDICATOR( 1150): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
05-03 09:43:11.549+0700 E/W_INDICATOR( 1150): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
05-03 09:43:11.549+0700 W/W_INDICATOR( 1150): windicator_connection.c: _packet_icon_set(840) > [_packet_icon_set:840] packet : 3 / signal : packet_inout_connected
05-03 09:43:11.549+0700 W/W_INDICATOR( 1150): windicator_connection.c: _packet_type_changed_cb(1251) > [_packet_type_changed_cb:1251] _packet_type_changed_cb
05-03 09:43:11.549+0700 E/W_INDICATOR( 1150): windicator_connection.c: _packet_type_changed_cb(1261) > [_packet_type_changed_cb:1261] WIFI MODE
05-03 09:43:11.589+0700 W/MUSIC_CONTROL_SERVICE( 1760): music-control-consumer-control.c: _display_changed_cb(449) > [36m[TID:1760]   [MUSIC_PLAYER_EVENT]send mediachanged-req[0m
05-03 09:43:11.589+0700 W/MUSIC_CONTROL_SERVICE( 1760): music-control-consumer-control.c: music_control_consumer_media_changed_ind_request(199) > [33m[TID:1760]   remote mode[1],resume[1][0m
05-03 09:43:11.629+0700 W/SHealthCommon( 1810): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:1[0;m
05-03 09:43:11.629+0700 W/SHealthServiceCommon( 1810): SHealthServiceController.cpp: OnSystemUtilLcdStateChanged(645) > [1;35m ###[0;m
05-03 09:43:11.659+0700 W/W_INDICATOR( 1150): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
05-03 09:43:11.669+0700 W/MC_CONSUMER( 1760): wmc-service-consumer.c: wmc_service_consumer_send_data(589) > [33m Unable to send data [0][0m
05-03 09:43:11.669+0700 W/SHealthServiceCommon( 1810): EnergyExpenditureFeatureController.cpp: OnTotalEnergyExpenditureChanged(119) > [1;40;33mstart 1525280400000.000000, end 1525315391674.000000, calories 650.484746[0;m
05-03 09:43:11.669+0700 W/SHealthCommon( 1810): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: energy_expenditure_updated, pendingClientInfoList.size(): 1[0;m
05-03 09:43:11.669+0700 W/SHealthCommon( 1810): TimelineSessionStorage.cpp: OnTriggered(1290) > [1;40;33mlocalStartTime: 1525305600000.000000[0;m
05-03 09:43:11.689+0700 W/MUSIC_PLAYER( 1760): mp-weconn.c: mp_weconn_is_bt_and_wms_connected(44) > [33m[TID:1760]   W_SERVICE_TYPE_BT[1][0m
05-03 09:43:11.689+0700 W/MUSIC_PLAYER( 1760): mp-weconn.c: mp_weconn_is_bt_and_wms_connected(56) > [33m[TID:1760]   bt[1], wms[0][0m
05-03 09:43:11.689+0700 E/MC_CONSUMER( 1760): wmc-service-cotroller.c: wmc_service_controller_set_media_changed_request(321) > [31m wmc_service_consumer_send_data failed : [-4]!!![0m
05-03 09:43:11.689+0700 W/MUSIC_CONTROL_SERVICE( 1760): music-control-consumer-control.c: music_control_consumer_media_changed_ind_request(224) > [31m[TID:1760]   wmc_service_controller_set_media_changed_request() .. [0][0m
05-03 09:43:11.709+0700 W/WATCH_CORE( 1319): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 2
05-03 09:43:11.709+0700 I/WATCH_CORE( 1319): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
05-03 09:43:11.709+0700 I/CAPI_WATCH_APPLICATION( 1319): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 09:43:11.709+0700 E/watchface-app( 1319): watchface.cpp: OnAppTimeTick(1157) > 
05-03 09:43:11.709+0700 E/wnoti-service( 1404): wnoti-db-utility.c: context_wearonoff_status_cb(1781) > status changed from 2 to 1 
05-03 09:43:11.709+0700 E/wnoti-service( 1404): wnoti-native-client.c: handle_cache_notification(790) > >>
05-03 09:43:11.709+0700 E/WMS     ( 1057): wms_event_handler.c: _wms_event_handler_cb_wearonoff_monitor(23513) > wear_monitor_status update[0] = 2 -> 1
05-03 09:43:11.719+0700 W/SHealthCommon( 1810): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: timeline_summary_updated, pendingClientInfoList.size(): 0[0;m
05-03 09:43:11.719+0700 W/SHealthServiceCommon( 1810): EnergyExpenditureFeatureController.cpp: OnTotalEnergyExpenditureChanged(119) > [1;40;33mstart 1525280400000.000000, end 1525315391722.000000, calories 654.490676[0;m
05-03 09:43:11.719+0700 W/SHealthCommon( 1810): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: energy_expenditure_updated, pendingClientInfoList.size(): 1[0;m
05-03 09:43:11.729+0700 I/TIZEN_N_SOUND_MANAGER( 1694): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Enable end. (ret: 0x0)
05-03 09:43:11.729+0700 W/TIZEN_N_SOUND_MANAGER( 1694): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
05-03 09:43:11.729+0700 W/WAKEUP-SERVICE( 1694): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 1[0;m
05-03 09:43:11.729+0700 I/HIGEAR  ( 1694): WakeupService.cpp: WakeupServiceStart(393) > [svoice:Application:WakeupServiceStart:IN]
05-03 09:43:11.729+0700 W/SHealthServiceCommon( 1810): ContextRestingHeartrateProxy.cpp: OnRestingHrUpdatedCB(347) > [1;40;33mhrValue: 1007[0;m
05-03 09:43:11.739+0700 W/WAKEUP-SERVICE( 1694): WakeupService.cpp: OnReceiveDisplayChanged(970) > [0;32mINFO: LCDOn receive data : -1226646772[0;m
05-03 09:43:11.739+0700 W/WAKEUP-SERVICE( 1694): WakeupService.cpp: OnReceiveDisplayChanged(971) > [0;32mINFO: WakeupServiceStart[0;m
05-03 09:43:11.739+0700 W/WAKEUP-SERVICE( 1694): WakeupService.cpp: WakeupServiceStart(367) > [0;32mINFO: SEAMLESS WAKEUP START REQUEST[0;m
05-03 09:43:11.739+0700 W/WAKEUP-SERVICE( 1694): WakeupService.cpp: WakeupServiceStart(387) > [0;32mINFO: 500[0;m
05-03 09:43:11.739+0700 I/TIZEN_N_SOUND_MANAGER( 1694): sound_manager_product.c: sound_manager_svoice_set_param(1287) > [SVOICE] set param [keyword length] : 500
05-03 09:43:11.749+0700 W/TIZEN_N_SOUND_MANAGER( 1694): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_set_param] ERROR_NONE (0x00000000)
05-03 09:43:11.759+0700 E/WAKEUP-SERVICE( 1694): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
05-03 09:43:11.759+0700 E/WAKEUP-SERVICE( 1694): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
05-03 09:43:11.759+0700 I/TIZEN_N_SOUND_MANAGER( 1694): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Enable start
05-03 09:43:11.759+0700 I/TIZEN_N_SOUND_MANAGER( 1694): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Enable end. (ret: 0x0)
05-03 09:43:11.759+0700 W/TIZEN_N_SOUND_MANAGER( 1694): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
05-03 09:43:11.759+0700 W/WAKEUP-SERVICE( 1694): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 1[0;m
05-03 09:43:11.759+0700 I/HIGEAR  ( 1694): WakeupService.cpp: WakeupServiceStart(393) > [svoice:Application:WakeupServiceStart:IN]
05-03 09:43:11.919+0700 I/GATE    ( 1225): <GATE-M>SCREEN_LOADED_HOME</GATE-M>
05-03 09:43:11.989+0700 I/CAPI_WATCH_APPLICATION( 1319): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 09:43:11.989+0700 E/watchface-app( 1319): watchface.cpp: OnAppTimeTick(1157) > 
05-03 09:43:12.439+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 09:43:12.439+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 09:43:12.439+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 09:43:12.439+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
05-03 09:43:12.439+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 09:43:12.439+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 09:43:12.439+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
05-03 09:43:12.439+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 09:43:12.449+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 09:43:12.449+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 09:43:12.449+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 09:43:12.449+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
05-03 09:43:12.449+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
05-03 09:43:12.449+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 09:43:12.449+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
05-03 09:43:12.449+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
05-03 09:43:12.449+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
05-03 09:43:12.449+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 09:43:12.449+0700 W/MUSIC_CONTROL_SERVICE( 1760): music-control-service.c: _music_control_service_pasre_request(565) > [33m[TID:1760]   [com.samsung.w-home]register msg port [true][0m
05-03 09:43:12.449+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:43:12.459+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 1225
05-03 09:43:12.469+0700 W/MUSIC_CONTROL_SERVICE( 1760): music-control-message.c: music_control_message_send_media_changed_ind(231) > [36m[TID:1760]   [MUSIC_PLAYER_EVENT][0m
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-home:music-control-service-message-port]
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-home:music-control-service-message-port]
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 09:43:12.469+0700 W/W_HOME  ( 1225): clock_shortcut.c: _music_service_messageport_cb(361) > mode:remote state:unknown 
05-03 09:43:12.469+0700 E/W_HOME  ( 1225): clock_shortcut.c: _mp_state_get(104) > (s_info.music_service.state != 1) -> _mp_state_get() return
05-03 09:43:12.469+0700 W/MUSIC_CONTROL_SERVICE( 1760): music-control-message.c: music_control_message_send_player_state_changed_ind(255) > [36m[TID:1760]   [MUSIC_PLAYER_EVENT][0m
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-home:music-control-service-message-port]
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-home:music-control-service-message-port]
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
05-03 09:43:12.469+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 09:43:12.469+0700 W/W_HOME  ( 1225): clock_shortcut.c: _music_service_messageport_cb(361) > mode:remote state:unknown 
05-03 09:43:12.469+0700 E/W_HOME  ( 1225): clock_shortcut.c: _mp_state_get(104) > (s_info.music_service.state != 1) -> _mp_state_get() return
05-03 09:43:12.729+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2690): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:43:12.729+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2690): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:43:12.729+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2690): preference.c: preference_get_string(1258) > preference_get_string(2690) : URL error
05-03 09:43:12.769+0700 I/CAPI_NETWORK_CONNECTION( 2690): connection.c: connection_create(453) > New handle created[0xb75a9808]
05-03 09:43:12.869+0700 E/JSON PARSING( 2690): No data could be display
05-03 09:43:12.869+0700 E/JSON PARSING( 2690): p������;
05-03 09:43:12.869+0700 I/CAPI_NETWORK_CONNECTION( 2690): connection.c: connection_destroy(471) > Destroy handle: 0xb75a9808
05-03 09:43:12.989+0700 I/CAPI_WATCH_APPLICATION( 1319): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 09:43:12.989+0700 E/watchface-app( 1319): watchface.cpp: OnAppTimeTick(1157) > 
05-03 09:43:13.989+0700 I/CAPI_WATCH_APPLICATION( 1319): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 09:43:13.989+0700 E/watchface-app( 1319): watchface.cpp: OnAppTimeTick(1157) > 
05-03 09:43:14.989+0700 I/CAPI_WATCH_APPLICATION( 1319): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 09:43:14.989+0700 E/watchface-app( 1319): watchface.cpp: OnAppTimeTick(1157) > 
05-03 09:43:15.989+0700 I/CAPI_WATCH_APPLICATION( 1319): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 09:43:15.989+0700 E/watchface-app( 1319): watchface.cpp: OnAppTimeTick(1157) > 
05-03 09:43:16.669+0700 W/WATCH_CORE( 1319): appcore-watch.c: __signal_context_handler(1298) > _signal_context_handler: type: 0, state: 2
05-03 09:43:16.669+0700 I/WATCH_CORE( 1319): appcore-watch.c: __signal_context_handler(1315) > Call the time_tick_cb
05-03 09:43:16.669+0700 I/CAPI_WATCH_APPLICATION( 1319): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 09:43:16.669+0700 E/watchface-app( 1319): watchface.cpp: OnAppTimeTick(1157) > 
05-03 09:43:16.679+0700 E/wnoti-service( 1404): wnoti-db-utility.c: context_wearonoff_status_cb(1781) > status changed from 1 to 2 
05-03 09:43:16.679+0700 E/WMS     ( 1057): wms_event_handler.c: _wms_event_handler_cb_wearonoff_monitor(23513) > wear_monitor_status update[0] = 1 -> 2
05-03 09:43:16.729+0700 W/SHealthServiceCommon( 1810): ContextRestingHeartrateProxy.cpp: OnRestingHrUpdatedCB(347) > [1;40;33mhrValue: 1008[0;m
05-03 09:43:16.999+0700 I/CAPI_WATCH_APPLICATION( 1319): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 09:43:16.999+0700 E/watchface-app( 1319): watchface.cpp: OnAppTimeTick(1157) > 
05-03 09:43:17.379+0700 E/PKGMGR  ( 2794): pkgmgr.c: __pkgmgr_client_uninstall(2061) > uninstall pkg start.
05-03 09:43:17.629+0700 E/PKGMGR_SERVER( 2796): pkgmgr-server.c: main(2242) > package manager server start
05-03 09:43:17.769+0700 E/PKGMGR_SERVER( 2796): pm-mdm.c: _pm_check_mdm_policy(1078) > [0;31m[_pm_check_mdm_policy(): 1078](ret != MDM_RESULT_SUCCESS) can not connect mdm server[0;m
05-03 09:43:17.779+0700 E/PKGMGR  ( 2794): pkgmgr.c: __pkgmgr_client_uninstall(2186) > uninstall pkg finish, ret=[27940002]
05-03 09:43:17.779+0700 E/PKGMGR_SERVER( 2797): pkgmgr-server.c: queue_job(1962) > UNINSTALL start, pkgid=[com.toyota.realtimefeedback]
05-03 09:43:17.859+0700 W/WATCH_CORE( 1319): appcore-watch.c: __signal_lcd_status_handler(1231) > signal_lcd_status_signal: LCDOff
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): dbus.c: _dbus_message_recv_cb(204) > LCD off
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): gesture.c: _manual_render_cancel_schedule(226) > cancel schedule, manual render
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): gesture.c: _manual_render_disable_timer_del(157) > timer del
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): gesture.c: _manual_render_enable(138) > 1
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): event_manager.c: _lcd_off_cb(723) > lcd state: 0
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): event_manager.c: _state_control(176) > control:4, app_state:1 win_state:0(1) pm_state:0 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): event_manager.c: _state_control(320) > appcore paused manually
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): main.c: home_appcore_pause(516) > Home Appcore Paused
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): event_manager.c: _app_pause_cb(390) > state: 1 -> 2
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:0(1) pm_state:0 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): event_manager.c: _state_control(176) > control:0, app_state:2 win_state:0(1) pm_state:0 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): main.c: home_pause(550) > clock/widget paused
05-03 09:43:17.859+0700 W/W_HOME  ( 1225): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:0(1) pm_state:0 home_visible:1 clock_visible:1 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 09:43:17.859+0700 W/W_INDICATOR( 1150): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
05-03 09:43:17.859+0700 W/W_INDICATOR( 1150): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnCheckRemotePort(115) > _MessagePortStub::OnCheckRemotePort.
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: CheckRemotePort(200) > _MessagePortService::CheckRemotePort
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: CheckRemotePort(213) > Check a remote message port: [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 09:43:17.869+0700 W/STARTER ( 1149): clock-mgr.c: _on_lcd_signal_receive_cb(1284) > [_on_lcd_signal_receive_cb:1284] _on_lcd_signal_receive_cb, 1284, Pre LCD off by [timeout]
05-03 09:43:17.869+0700 W/STARTER ( 1149): clock-mgr.c: _pre_lcd_off(1089) > [_pre_lcd_off:1089] Will LCD OFF as wake_up_setting[1]
05-03 09:43:17.869+0700 E/STARTER ( 1149): scontext_util.c: sconstext_util_check_lock_type(47) > [sconstext_util_check_lock_type:47] current lock state :[0],testmode[0]
05-03 09:43:17.869+0700 E/STARTER ( 1149): scontext_util.c: scontext_util_handle_lock_state(72) > [scontext_util_handle_lock_state:72] wear state[0],bPossible [0],usage [0]
05-03 09:43:17.869+0700 W/STARTER ( 1149): clock-mgr.c: _check_reserved_popup_status(211) > [_check_reserved_popup_status:211] Current reserved apps status : 0
05-03 09:43:17.869+0700 W/STARTER ( 1149): clock-mgr.c: _check_reserved_apps_status(247) > [_check_reserved_apps_status:247] Current reserved apps status : 0
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: OnReadMessage(739) > _MessagePortIpcServer::OnReadMessage
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: HandleReceivedMessage(578) > _MessagePortIpcServer::HandleReceivedMessage
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnIpcRequestReceived(147) > MessagePort message received
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: OnSendMessage(126) > MessagePort OnSendMessage
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: SendMessage(284) > _MessagePortService::SendMessage
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: GetKey(358) > _MessagePortService::GetKey
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortService.cpp: SendMessage(292) > Sends a message to a remote message port [com.samsung.w-music-player.music-control-service:music-control-service-request-message-port]
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortStub.cpp: SendMessage(138) > MessagePort SendMessage
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: SendResponse(884) > _MessagePortIpcServer::SendResponse
05-03 09:43:17.869+0700 I/MESSAGE_PORT(  942): MessagePortIpcServer.cpp: Send(847) > _MessagePortIpcServer::Stop
05-03 09:43:17.879+0700 W/WATCH_CORE( 1319): appcore-watch.c: __widget_pause(1113) > widget_pause
05-03 09:43:17.939+0700 W/AUL     ( 1319): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.techgraphy.DigitalTick) pid(1319) status(bg) type(watchapp)
05-03 09:43:17.899+0700 W/WAKEUP-SERVICE( 1694): WakeupService.cpp: OnReceiveDisplayChanged(979) > [0;32mINFO: LCDOff receive data : -1226646772[0;m
05-03 09:43:17.939+0700 E/watchface-app( 1319): watchface.cpp: OnAppPause(1122) > 
05-03 09:43:17.939+0700 W/WAKEUP-SERVICE( 1694): WakeupService.cpp: OnReceiveDisplayChanged(980) > [0;32mINFO: WakeupServiceStop[0;m
05-03 09:43:17.899+0700 W/MUSIC_CONTROL_SERVICE( 1760): music-control-service.c: _music_control_service_pasre_request(565) > [33m[TID:1760]   [com.samsung.w-home]register msg port [false][0m
05-03 09:43:17.939+0700 W/WAKEUP-SERVICE( 1694): WakeupService.cpp: WakeupServiceStop(399) > [0;32mINFO: SEAMLESS WAKEUP STOP REQUEST[0;m
05-03 09:43:17.939+0700 E/rpm-installer( 2797): rpm-appcore-intf.c: main(120) > ------------------------------------------------
05-03 09:43:17.939+0700 E/rpm-installer( 2797): rpm-appcore-intf.c: main(121) >  [START] rpm-installer: version=[201610629.1]
05-03 09:43:17.939+0700 E/rpm-installer( 2797): rpm-appcore-intf.c: main(122) > ------------------------------------------------
05-03 09:43:17.959+0700 E/WAKEUP-SERVICE( 1694): WakeupService.cpp: _WakeupIsAvailable(288) > [0;31mERROR: db/private/com.samsung.wfmw/is_locked FAILED[0;m
05-03 09:43:17.959+0700 E/WAKEUP-SERVICE( 1694): WakeupService.cpp: _WakeupIsAvailable(314) > [0;31mERROR: file/calendar/alarm_state FAILED[0;m
05-03 09:43:17.959+0700 I/TIZEN_N_SOUND_MANAGER( 1694): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1255) > [SVOICE] Wake up Disable start
05-03 09:43:18.009+0700 I/TIZEN_N_SOUND_MANAGER( 1694): sound_manager_product.c: sound_manager_svoice_wakeup_enable(1258) > [SVOICE] Wake up Disable end. (ret: 0x0)
05-03 09:43:18.009+0700 W/TIZEN_N_SOUND_MANAGER( 1694): sound_manager_private.c: __convert_sound_manager_error_code(156) > [sound_manager_svoice_wakeup_enable] ERROR_NONE (0x00000000)
05-03 09:43:18.009+0700 W/WAKEUP-SERVICE( 1694): WakeupService.cpp: WakeupSetSeamlessValue(360) > [0;32mINFO: WAKEUP SET : 0[0;m
05-03 09:43:17.939+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2690): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 09:43:18.009+0700 I/HIGEAR  ( 1694): WakeupService.cpp: WakeupServiceStop(403) > [svoice:Application:WakeupServiceStop:IN]
05-03 09:43:18.009+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2690): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 09:43:18.009+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2690): preference.c: preference_get_string(1258) > preference_get_string(2690) : URL error
05-03 09:43:18.099+0700 I/CAPI_NETWORK_CONNECTION( 2690): connection.c: connection_create(453) > New handle created[0xb75a0388]
05-03 09:43:18.099+0700 W/W_INDICATOR( 1150): windicator_util.c: _pm_state_changed_cb(917) > [_pm_state_changed_cb:917] LCD off
05-03 09:43:18.099+0700 W/W_INDICATOR( 1150): windicator_connection.c: windicator_connection_pause(2268) > [windicator_connection_pause:2268] 
05-03 09:43:18.109+0700 W/SHealthCommon( 1467): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:3[0;m
05-03 09:43:18.119+0700 W/W_INDICATOR( 1150): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
05-03 09:43:18.139+0700 W/STARTER ( 1149): clock-mgr.c: _on_lcd_signal_receive_cb(1297) > [_on_lcd_signal_receive_cb:1297] _on_lcd_signal_receive_cb, 1297, Post LCD off by [timeout]
05-03 09:43:18.139+0700 W/STARTER ( 1149): clock-mgr.c: _post_lcd_off(1190) > [_post_lcd_off:1190] LCD OFF as reserved app[(null)] alpm mode[0]
05-03 09:43:18.139+0700 W/STARTER ( 1149): clock-mgr.c: _post_lcd_off(1197) > [_post_lcd_off:1197] Current reserved apps status : 0
05-03 09:43:18.139+0700 W/STARTER ( 1149): clock-mgr.c: _post_lcd_off(1215) > [_post_lcd_off:1215] raise homescreen after 20 sec. home_visible[0]
05-03 09:43:18.139+0700 E/ALARM_MANAGER( 1149): alarm-lib.c: alarmmgr_add_alarm_withcb(1178) > trigger_at_time(20), start(3-5-2018, 09:43:38), repeat(1), interval(20), type(-1073741822)
05-03 09:43:18.139+0700 W/W_INDICATOR( 1150): windicator_dbus.c: _windicator_dbus_lcd_off_completed_cb(620) > [_windicator_dbus_lcd_off_completed_cb:620] LCD Off completed signal is received
05-03 09:43:18.139+0700 W/W_INDICATOR( 1150): windicator_dbus.c: _windicator_dbus_lcd_off_completed_cb(625) > [_windicator_dbus_lcd_off_completed_cb:625] Moment bar status -> idle. (Hide Moment bar)
05-03 09:43:18.139+0700 W/W_INDICATOR( 1150): windicator_moment_bar.c: windicator_moment_bar_hide_directly(548) > [windicator_moment_bar_hide_directly:548] windicator_moment_bar_hide_directly
05-03 09:43:18.139+0700 I/APP_CORE( 1225): appcore-efl.c: __do_app(453) > [APP 1225] Event: PAUSE State: RUNNING
05-03 09:43:18.139+0700 I/CAPI_APPFW_APPLICATION( 1225): app_main.c: app_appcore_pause(202) > app_appcore_pause
05-03 09:43:18.139+0700 W/W_HOME  ( 1225): main.c: _appcore_pause_cb(489) > appcore pause
05-03 09:43:18.139+0700 E/W_HOME  ( 1225): main.c: _pause_cb(467) > paused already
05-03 09:43:18.139+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __is_cached_cookie(230) > Find cached cookie for [1149].
05-03 09:43:18.169+0700 E/rpm-installer( 2797): rpm-cmdline.c: __ri_is_core_tpk_app(358) > This is a core tpk app.
05-03 09:43:18.219+0700 E/ALARM_MANAGER(  949): alarm-manager-schedule.c: _alarm_next_duetime(509) > alarm_id: 759248201, next duetime: 1525315418
05-03 09:43:18.219+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __alarm_add_to_list(496) > [alarm-server]: After add alarm_id(759248201)
05-03 09:43:18.219+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __alarm_create(1061) > [alarm-server]:alarm_context.c_due_time(1525316134), due_time(1525315418)
05-03 09:43:18.249+0700 W/SHealthCommon( 1810): SystemUtil.cpp: OnDeviceStatusChanged(1007) > [1;35mlcdState:3[0;m
05-03 09:43:18.249+0700 W/SHealthServiceCommon( 1810): SHealthServiceController.cpp: OnSystemUtilLcdStateChanged(645) > [1;35m ###[0;m
05-03 09:43:18.299+0700 I/watchface-app( 1319): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
05-03 09:43:18.299+0700 W/W_HOME  ( 1225): clock_event.c: _pkgmgr_event_cb(194) > Pkg:com.toyota.realtimefeedback is being uninstalled
05-03 09:43:18.299+0700 W/W_HOME  ( 1225): dbox.c: uninstall_cb(1434) > uninstalled:com.toyota.realtimefeedback
05-03 09:43:18.309+0700 W/AUL_AMD (  946): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(984) > pkgid(com.toyota.realtimefeedback), key(start), value(uninstall)
05-03 09:43:18.309+0700 W/AUL_AMD (  946): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(997) > __amd_pkgmgrinfo_start_handler
05-03 09:43:18.319+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
05-03 09:43:18.319+0700 E/WMS     ( 1057): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [UNINSTALL, STARTED]
05-03 09:43:18.329+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:43:18.339+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2690
05-03 09:43:18.339+0700 E/rpm-installer( 2797): installer-util.c: __check_running_app(1774) > app[com.toyota.realtimefeedback] is running, need to terminate it.
05-03 09:43:18.339+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 12
05-03 09:43:18.339+0700 W/AUL     ( 2797): launch.c: app_request_to_launchpad(284) > request cmd(5) to(2690)
05-03 09:43:18.339+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 5
05-03 09:43:18.349+0700 W/AUL     (  946): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(com.toyota.realtimefeedback) pid(2690) type(uiapp)
05-03 09:43:18.349+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
05-03 09:43:18.349+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 3-5-2018, 02:43:38 (UTC).
05-03 09:43:18.349+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
05-03 09:43:18.349+0700 W/AUL_AMD (  946): amd_launch.c: __reply_handler(999) > listen fd(23) , send fd(22), pid(2690), cmd(4)
05-03 09:43:18.349+0700 W/AUL     ( 2797): launch.c: app_request_to_launchpad(298) > request cmd(5) result(0)
05-03 09:43:18.349+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:43:18.359+0700 I/APP_CORE( 2690): appcore-efl.c: __do_app(453) > [APP 2690] Event: TERMINATE State: PAUSED
05-03 09:43:18.359+0700 W/APP_CORE( 2690): appcore-efl.c: appcore_efl_main(1788) > power off : 0
05-03 09:43:18.359+0700 W/APP_CORE( 2690): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3800003] -> redirected win[600915] for com.toyota.realtimefeedback[2690]
05-03 09:43:18.359+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2690
05-03 09:43:18.359+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 22
05-03 09:43:18.359+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(999) > app status : 4
05-03 09:43:18.369+0700 E/JSON PARSING( 2690): No data could be display
05-03 09:43:18.369+0700 E/JSON PARSING( 2690):  �����;
05-03 09:43:18.369+0700 I/CAPI_NETWORK_CONNECTION( 2690): connection.c: connection_destroy(471) > Destroy handle: 0xb75a0388
05-03 09:43:18.369+0700 I/APP_CORE( 1225): appcore-efl.c: __do_app(453) > [APP 1225] Event: MEM_FLUSH State: PAUSED
05-03 09:43:18.389+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
05-03 09:43:18.419+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
05-03 09:43:18.419+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
05-03 09:43:18.419+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 3-5-2018, 02:43:38 (UTC).
05-03 09:43:18.419+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
05-03 09:43:18.429+0700 E/ALARM_MANAGER(  949): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
05-03 09:43:18.429+0700 I/APP_CORE( 2690): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
05-03 09:43:18.429+0700 I/CAPI_APPFW_APPLICATION( 2690): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
05-03 09:43:18.449+0700 I/efl-extension( 2690): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 09:43:18.449+0700 I/efl-extension( 2690): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb7356be8 is freed
05-03 09:43:18.449+0700 E/EFL     ( 2690): elementary<2690> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb7350438 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
05-03 09:43:18.449+0700 I/efl-extension( 2690): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb7350e80, obj: 0xb7350e80
05-03 09:43:18.449+0700 I/efl-extension( 2690): efl_extension_rotary.c: _object_deleted_cb(618) > done
05-03 09:43:18.459+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:43:18.459+0700 E/EFL     ( 2690): elementary<2690> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 09:43:18.459+0700 E/EFL     ( 2690): elementary<2690> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 09:43:18.469+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2690
05-03 09:43:18.469+0700 E/EFL     ( 2690): elementary<2690> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7350e80 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 09:43:18.469+0700 E/EFL     ( 2690): elementary<2690> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7350e80 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 09:43:18.479+0700 E/EFL     ( 2690): elementary<2690> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7350e80 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 09:43:18.479+0700 E/EFL     ( 2690): elementary<2690> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7350e80 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb3210dc8 is freed
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7350e80, elm_genlist, func : 0xb6df3ea1
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 09:43:18.479+0700 I/APP_CORE( 2690): appcore-efl.c: __after_loop(1243) > [APP 2690] After terminate() callback
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb744f2c0 is freed
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7404b08, elm_genlist, func : 0xb6df3ea1
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb7440ae8
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb7448a80 is freed
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb740b9c8, elm_scroller, func : 0xb6df7379
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7440ae8, elm_image, func : 0xb6df7379
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 09:43:18.479+0700 I/efl-extension( 2690): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb740b9c8 : elm_scroller] rotary callabck is deleted
05-03 09:43:18.569+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:43:18.589+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2690
05-03 09:43:18.689+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:43:18.699+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2690
05-03 09:43:18.799+0700 W/AUL_AMD (  946): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 09:43:18.809+0700 W/AUL_AMD (  946): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
05-03 09:43:18.839+0700 E/PKGMGR_PARSER( 2797): pkgmgr_parser_signature.c: __ps_check_mdm_policy_by_pkgid(1056) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-CLIENT( 2797): SocketClient.cpp: SocketClient(37) > Client created
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: mainloop(227) > Got incoming connection
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: connectionThread(253) > Starting connection thread
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketStream.h: SocketStream(31) > Created
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketConnection.h: SocketConnection(44) > Created
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-CLIENT( 2797): SocketStream.h: SocketStream(31) > Created
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-CLIENT( 2797): SocketConnection.h: SocketConnection(44) > Created
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-CLIENT( 2797): SocketClient.cpp: connect(62) > Client connected
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: connectionService(304) > Calling service
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: connectionService(307) > Removing client
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: connectionService(311) > Call served
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-SERVER(  944): SocketService.cpp: connectionThread(262) > Client serviced
05-03 09:43:18.859+0700 I/PRIVACY-MANAGER-CLIENT( 2797): SocketClient.cpp: disconnect(72) > Client disconnected
05-03 09:43:19.039+0700 W/CRASH_MANAGER( 2803): worker.c: worker_job(1205) > 1102690726561152531539
