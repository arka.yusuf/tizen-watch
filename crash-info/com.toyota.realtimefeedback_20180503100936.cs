S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2231
Date: 2018-05-03 10:09:36+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2231, uid 5000)

Register Information
r0   = 0x00000000, r1   = 0xb588461c
r2   = 0xb5884c30, r3   = 0x00000000
r4   = 0xb84e93e0, r5   = 0xb85b7dd8
r6   = 0x00000013, r7   = 0xbebfb208
r8   = 0x00004833, r9   = 0xb84e93e0
r10  = 0x00000000, fp   = 0x00000000
ip   = 0xb5884870, sp   = 0xbebfb1e8
lr   = 0xb587250f, pc   = 0xb5872544
cpsr = 0x60000030

Memory Information
MemTotal:   405512 KB
MemFree:      2656 KB
Buffers:      5720 KB
Cached:     106200 KB
VmPeak:     195200 KB
VmSize:     194656 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       32720 KB
VmRSS:       32720 KB
VmData:     106692 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24520 KB
VmPTE:         148 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2231 TID = 2231
2231 2283 2293 

Maps Information
b2074000 b2078000 r-xp /usr/lib/libogg.so.0.7.1
b2080000 b20a2000 r-xp /usr/lib/libvorbis.so.0.4.3
b20aa000 b20f1000 r-xp /usr/lib/libsndfile.so.1.0.26
b20fd000 b2146000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b214f000 b2154000 r-xp /usr/lib/libjson.so.0.0.1
b39f5000 b3afb000 r-xp /usr/lib/libicuuc.so.57.1
b3b11000 b3c99000 r-xp /usr/lib/libicui18n.so.57.1
b3ca9000 b3cb6000 r-xp /usr/lib/libail.so.0.1.0
b3cbf000 b3cc2000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3cca000 b3d02000 r-xp /usr/lib/libpulse.so.0.16.2
b3d03000 b3d04000 r-xp /usr/lib/evas/modules/savers/jpeg/linux-gnueabi-armv7l-1.7.99/module.so
b3d0c000 b3d0f000 r-xp /usr/lib/evas/modules/engines/buffer/linux-gnueabi-armv7l-1.7.99/module.so
b3e27000 b3e2a000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3e32000 b3e93000 r-xp /usr/lib/libasound.so.2.0.0
b3e9d000 b3eb6000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3ebf000 b3eca000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3ed7000 b3eef000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3f9c000 b4023000 rw-s anon_inode:dmabuf
b4024000 b4823000 rw-p [stack:2293]
b49c3000 b49c4000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b49d3000 b49d7000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b49df000 b49e3000 r-xp /usr/lib/libmmfsession.so.0.0.0
b49ec000 b49f3000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b49fb000 b4a06000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b4a0e000 b4a10000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b4a18000 b4a19000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b4a21000 b4a29000 r-xp /usr/lib/libfeedback.so.0.1.4
b4a42000 b4a43000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b4acb000 b52ca000 rw-p [stack:2283]
b52ca000 b52cc000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b52d4000 b52eb000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b52f8000 b52fa000 r-xp /usr/lib/libdri2.so.0.0.0
b5302000 b530d000 r-xp /usr/lib/libtbm.so.1.0.0
b5315000 b531d000 r-xp /usr/lib/libdrm.so.2.4.0
b5325000 b5327000 r-xp /usr/lib/libgenlock.so
b532f000 b5334000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b533c000 b5347000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b5550000 b561a000 r-xp /usr/lib/libCOREGL.so.4.0
b562b000 b563b000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5643000 b5649000 r-xp /usr/lib/libxcb-render.so.0.0.0
b5651000 b5652000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b565b000 b565e000 r-xp /usr/lib/libEGL.so.1.4
b5666000 b5674000 r-xp /usr/lib/libGLESv2.so.2.0
b567d000 b56c6000 r-xp /usr/lib/libmdm.so.1.2.70
b56cf000 b56d5000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b56dd000 b56e6000 r-xp /usr/lib/libcom-core.so.0.0.1
b56ef000 b57a7000 r-xp /usr/lib/libcairo.so.2.11200.14
b57b2000 b57cb000 r-xp /usr/lib/libnetwork.so.0.0.0
b57d3000 b57df000 r-xp /usr/lib/libnotification.so.0.1.0
b57e8000 b57f7000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b5800000 b5821000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5829000 b582e000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b5836000 b583b000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b5843000 b5853000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b585b000 b5863000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b586b000 b5875000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a1a000 b5a24000 r-xp /lib/libnss_files-2.13.so
b5a2d000 b5afc000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5b12000 b5b36000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b3f000 b5b45000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b4d000 b5b51000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b5e000 b5b69000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5b71000 b5b73000 r-xp /usr/lib/libiniparser.so.0
b5b7c000 b5b81000 r-xp /usr/lib/libappcore-common.so.1.1
b5b89000 b5b8b000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5b94000 b5b98000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5ba5000 b5ba7000 r-xp /usr/lib/libXau.so.6.0.0
b5baf000 b5bb6000 r-xp /lib/libcrypt-2.13.so
b5be6000 b5be8000 r-xp /usr/lib/libiri.so
b5bf1000 b5d83000 r-xp /usr/lib/libcrypto.so.1.0.0
b5da4000 b5deb000 r-xp /usr/lib/libssl.so.1.0.0
b5df7000 b5e25000 r-xp /usr/lib/libidn.so.11.5.44
b5e2d000 b5e36000 r-xp /usr/lib/libcares.so.2.1.0
b5e40000 b5e53000 r-xp /usr/lib/libxcb.so.1.1.0
b5e5c000 b5e5f000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e67000 b5e69000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e72000 b5f3e000 r-xp /usr/lib/libxml2.so.2.7.8
b5f4b000 b5f4d000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f56000 b5f5b000 r-xp /usr/lib/libffi.so.5.0.10
b5f63000 b5f64000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5f6c000 b5f6f000 r-xp /lib/libattr.so.1.1.0
b5f77000 b600b000 r-xp /usr/lib/libstdc++.so.6.0.16
b601e000 b603b000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b6045000 b605d000 r-xp /usr/lib/libpng12.so.0.50.0
b6065000 b607b000 r-xp /lib/libexpat.so.1.6.0
b6085000 b60c9000 r-xp /usr/lib/libcurl.so.4.3.0
b60d2000 b60dc000 r-xp /usr/lib/libXext.so.6.4.0
b60e6000 b60ea000 r-xp /usr/lib/libXtst.so.6.1.0
b60f2000 b60f8000 r-xp /usr/lib/libXrender.so.1.3.0
b6100000 b6106000 r-xp /usr/lib/libXrandr.so.2.2.0
b610e000 b610f000 r-xp /usr/lib/libXinerama.so.1.0.0
b6118000 b6121000 r-xp /usr/lib/libXi.so.6.1.0
b6129000 b612c000 r-xp /usr/lib/libXfixes.so.3.1.0
b6135000 b6137000 r-xp /usr/lib/libXgesture.so.7.0.0
b613f000 b6141000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6149000 b614b000 r-xp /usr/lib/libXdamage.so.1.1.0
b6153000 b615a000 r-xp /usr/lib/libXcursor.so.1.0.2
b6162000 b6165000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b616e000 b6172000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b617b000 b6180000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6189000 b626a000 r-xp /usr/lib/libX11.so.6.3.0
b6275000 b6298000 r-xp /usr/lib/libjpeg.so.8.0.2
b62b0000 b62c6000 r-xp /lib/libz.so.1.2.5
b62cf000 b62d1000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b62d9000 b634e000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6358000 b6372000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b637a000 b63ae000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b63b7000 b648a000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6496000 b64a6000 r-xp /lib/libresolv-2.13.so
b64aa000 b64c2000 r-xp /usr/lib/liblzma.so.5.0.3
b64ca000 b64cd000 r-xp /lib/libcap.so.2.21
b64d5000 b6504000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b650c000 b650d000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b6516000 b651c000 r-xp /usr/lib/libecore_imf.so.1.7.99
b6524000 b653b000 r-xp /usr/lib/liblua-5.1.so
b6544000 b654b000 r-xp /usr/lib/libembryo.so.1.7.99
b6553000 b6559000 r-xp /lib/librt-2.13.so
b6562000 b65b8000 r-xp /usr/lib/libpixman-1.so.0.28.2
b65c6000 b661c000 r-xp /usr/lib/libfreetype.so.6.11.3
b6628000 b6650000 r-xp /usr/lib/libfontconfig.so.1.8.0
b6651000 b6696000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b669f000 b66b2000 r-xp /usr/lib/libfribidi.so.0.3.1
b66ba000 b66d4000 r-xp /usr/lib/libecore_con.so.1.7.99
b66de000 b66e7000 r-xp /usr/lib/libedbus.so.1.7.99
b66ef000 b673f000 r-xp /usr/lib/libecore_x.so.1.7.99
b6741000 b674a000 r-xp /usr/lib/libvconf.so.0.2.45
b6752000 b6763000 r-xp /usr/lib/libecore_input.so.1.7.99
b676b000 b6770000 r-xp /usr/lib/libecore_file.so.1.7.99
b6778000 b679a000 r-xp /usr/lib/libecore_evas.so.1.7.99
b67a3000 b67e4000 r-xp /usr/lib/libeina.so.1.7.99
b67ed000 b6806000 r-xp /usr/lib/libeet.so.1.7.99
b6817000 b6880000 r-xp /lib/libm-2.13.so
b6889000 b688f000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6898000 b6899000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b68a1000 b68c4000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b68cc000 b68d1000 r-xp /usr/lib/libxdgmime.so.1.1.0
b68d9000 b6903000 r-xp /usr/lib/libdbus-1.so.3.8.12
b690c000 b6923000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b692b000 b6936000 r-xp /lib/libunwind.so.8.0.1
b6963000 b6981000 r-xp /usr/lib/libsystemd.so.0.4.0
b698b000 b6aaf000 r-xp /lib/libc-2.13.so
b6abd000 b6ac5000 r-xp /lib/libgcc_s-4.6.so.1
b6ac6000 b6aca000 r-xp /usr/lib/libsmack.so.1.0.0
b6ad3000 b6ad9000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6ae1000 b6bb1000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6bb2000 b6c10000 r-xp /usr/lib/libedje.so.1.7.99
b6c1a000 b6c31000 r-xp /usr/lib/libecore.so.1.7.99
b6c48000 b6d16000 r-xp /usr/lib/libevas.so.1.7.99
b6d3c000 b6e78000 r-xp /usr/lib/libelementary.so.1.7.99
b6e8f000 b6ea3000 r-xp /lib/libpthread-2.13.so
b6eae000 b6eb0000 r-xp /usr/lib/libdlog.so.0.0.0
b6eb8000 b6ebb000 r-xp /usr/lib/libbundle.so.0.1.22
b6ec3000 b6ec5000 r-xp /lib/libdl-2.13.so
b6ece000 b6edb000 r-xp /usr/lib/libaul.so.0.1.0
b6eed000 b6ef3000 r-xp /usr/lib/libappcore-efl.so.1.1
b6efc000 b6f00000 r-xp /usr/lib/libsys-assert.so
b6f09000 b6f26000 r-xp /lib/ld-2.13.so
b6f2f000 b6f34000 r-xp /usr/bin/launchpad-loader
b83ce000 b9a91000 rw-p [heap]
bebdb000 bebfc000 rw-p [stack]
bebdb000 bebfc000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2231)
Call Stack Count: 21
 0: set_targeted_view + 0x87 (0xb5872544) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7544
 1: pop_from_stack_uib_vc + 0x38 (0xb5872a49) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a49
 2: uib_views_destroy_callback + 0x38 (0xb5872a05) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x7a05
 3: (0xb6c5faf9) [/usr/lib/libevas.so.1] + 0x17af9
 4: evas_object_del + 0x94 (0xb6c76c39) [/usr/lib/libevas.so.1] + 0x2ec39
 5: (0xb6c6937b) [/usr/lib/libevas.so.1] + 0x2137b
 6: evas_free + 0x200 (0xb6c69ab5) [/usr/lib/libevas.so.1] + 0x21ab5
 7: (0xb6787cf1) [/usr/lib/libecore_evas.so.1] + 0xfcf1
 8: (0xb6e3a953) [/usr/lib/libelementary.so.1] + 0xfe953
 9: (0xb6c253f5) [/usr/lib/libecore.so.1] + 0xb3f5
10: (0xb6c22e53) [/usr/lib/libecore.so.1] + 0x8e53
11: (0xb6c2646b) [/usr/lib/libecore.so.1] + 0xc46b
12: ecore_main_loop_iterate + 0x22 (0xb6c267db) [/usr/lib/libecore.so.1] + 0xc7db
13: elm_shutdown + 0x2c (0xb6df744d) [/usr/lib/libelementary.so.1] + 0xbb44d
14: appcore_efl_main + 0x454 (0xb6ef0c69) [/usr/lib/libappcore-efl.so.1] + 0x3c69
15: ui_app_main + 0xb0 (0xb5b95ed5) [/usr/lib/libcapi-appfw-application.so.0] + 0x1ed5
16:  + 0x0 (0xb587040f) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x540f
17: main + 0x34 (0xb5870b0d) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x5b0d
18: (0xb6f30a53) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1a53
19: __libc_start_main + 0x114 (0xb69a285c) [/lib/libc.so.6] + 0x1785c
20: (0xb6f30e0c) [/opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1] + 0x1e0c
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 10:09:28.299+0700 I/wnotib  ( 1192): w-notification-board-basic-panel.c: _wnb_bpanel_coverview_gl_item_del(4095) > card[0xae66b360]
05-03 10:09:28.299+0700 I/wnotib  ( 1192): w-notification-board-basic-panel.c: _wnb_bp_delete_card(3529) > Hide the drawer for the current panel.
05-03 10:09:28.299+0700 I/wnotib  ( 1192): w-notification-board-basic-panel.c: _wnb_bp_destroy(5758) > Destory panel, panel application_id [-1016], panel category_id [180]
05-03 10:09:28.299+0700 I/wnotib  ( 1192): w-notification-board-action.c: wnb_action_deinitialize(5671) > Deinit drawer.
05-03 10:09:28.299+0700 I/wnotib  ( 1192): w-notification-board-action.c: _wnb_action_terminate_input_selector(5550) > No need to close w-input-selector.
05-03 10:09:28.299+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:09:28.309+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] mouse move
05-03 10:09:28.309+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] hold(0), freeze(0)
05-03 10:09:28.309+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] direction_x(0), direction_y(1)
05-03 10:09:28.309+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] drag_child_locked_y(0)
05-03 10:09:28.309+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] move content x(0), y(14)
05-03 10:09:28.309+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
05-03 10:09:28.309+0700 I/wnotib  ( 1192): w-notification-board-action.c: _wnb_action_terminate_noti_composer(5585) > ret : 0, is_running : 0
05-03 10:09:28.309+0700 I/wnotib  ( 1192): w-notification-board-action.c: _wnb_action_sending_popup_del_cb(705) > 0xb8539508, g_sending_popup_state: 0
05-03 10:09:28.319+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] mouse move
05-03 10:09:28.319+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] hold(0), freeze(0)
05-03 10:09:28.319+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb85991d8 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:28.319+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb85991d8 : elm_scroller] cw(0), ch(0), pw(360), ph(360)
05-03 10:09:28.329+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] direction_x(0), direction_y(1)
05-03 10:09:28.329+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] drag_child_locked_y(0)
05-03 10:09:28.329+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] move content x(0), y(13)
05-03 10:09:28.329+0700 I/efl-extension( 1192): efl_extension_more_option.c: eext_more_option_items_clear(572) > called!!
05-03 10:09:28.329+0700 I/efl-extension( 1192): efl_extension_rotary_selector.c: eext_rotary_selector_items_clear(2473) > called!!
05-03 10:09:28.329+0700 I/efl-extension( 1192): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
05-03 10:09:28.329+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] mouse move
05-03 10:09:28.329+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] hold(0), freeze(0)
05-03 10:09:28.329+0700 I/efl-extension( 1192): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
05-03 10:09:28.329+0700 I/efl-extension( 1192): efl_extension_rotary_selector.c: _item_del_cb(1128) > called!!
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_more_option.c: _more_option_del_cb(268) > called!!
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_more_option.c: _panel_del_cb(173) > called!!
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary_selector.c: _rotary_selector_del_cb(826) > called!!
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb83e4970, elm_layout, func : 0xb68038c9
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb83e4970, obj: 0xb83e4970
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: _object_deleted_cb(618) > done
05-03 10:09:28.339+0700 E/EFL     ( 1192): ecore<1192> ecore.c:573 _ecore_magic_fail() 
05-03 10:09:28.339+0700 E/EFL     ( 1192): *** ECORE ERROR: Ecore Magic Check Failed!!!
05-03 10:09:28.339+0700 E/EFL     ( 1192): *** IN FUNCTION: ecore_timer_del()
05-03 10:09:28.339+0700 E/EFL     ( 1192): ecore<1192> ecore.c:577 _ecore_magic_fail()   Input handle has already been freed!
05-03 10:09:28.339+0700 E/EFL     ( 1192): ecore<1192> ecore.c:586 _ecore_magic_fail() *** NAUGHTY PROGRAMMER!!!
05-03 10:09:28.339+0700 E/EFL     ( 1192): *** SPANK SPANK SPANK!!!
05-03 10:09:28.339+0700 E/EFL     ( 1192): *** Now go fix your code. Tut tut tut!
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary_selector.c: _event_area_del_cb(494) > called!!
05-03 10:09:28.339+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] mouse move
05-03 10:09:28.339+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] hold(0), freeze(0)
05-03 10:09:28.339+0700 I/wnotib  ( 1192): w-notification-board-action.c: wnb_action_deinitialize(5721) > g_wnb_action_data is freed.
05-03 10:09:28.339+0700 I/wnotib  ( 1192): w-notification-board-basic-panel.c: _wnb_bp_reset_scroll_config(528) > Reset thumbscroll_sensitivity_friction to 1.000000
05-03 10:09:28.339+0700 I/wnotib  ( 1192): w-notification-board-basic-panel.c: _wnb_bp_reset_scroll_config(530) > After thumbscroll_sensitivity_friction: 1.000000
05-03 10:09:28.339+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] direction_x(1), direction_y(1)
05-03 10:09:28.339+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] drag_child_locked_x(0)
05-03 10:09:28.339+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] drag_child_locked_y(0)
05-03 10:09:28.339+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] move content x(6), y(10)
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xae63c918
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb7fb97a0, elm_box, _activated_obj : 0x0, activated : 1
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xae63c918, elm_genlist, _activated_obj : 0xb7fb97a0, activated : 0
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xae63c918, elm_genlist, func : 0xaf215fa1
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:09:28.339+0700 I/wnotib  ( 1192): w-notification-board-basic-panel.c: _wnb_bp_set_current_detail_noti(1441) > Set current_detail_noti_db_id to 0
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xae63c918, obj: 0xae63c918
05-03 10:09:28.339+0700 I/efl-extension( 1192): efl_extension_rotary.c: _object_deleted_cb(618) > done
05-03 10:09:28.349+0700 E/EFL     ( 1192): elementary<1192> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 10:09:28.349+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xae63c918 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:28.349+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xae63c918 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:09:28.349+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xae63c918 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:28.349+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xae63c918 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:09:28.349+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] mouse move
05-03 10:09:28.349+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] hold(0), freeze(0)
05-03 10:09:28.349+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:09:28.349+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xae63c918, elm_genlist, func : 0xb67f9ea1
05-03 10:09:28.349+0700 I/efl-extension( 1192): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:09:28.349+0700 W/wnotib  ( 1192): w-notification-board-basic-panel.c: _wnb_bp_create_noti_detail_default_image(543) > Panel will be removed , skip make default image. stack list count [0].
05-03 10:09:28.359+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8602838 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:28.359+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8602838 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:09:28.359+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb8602838 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:28.359+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb8602838 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:09:28.359+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] direction_x(1), direction_y(1)
05-03 10:09:28.359+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] drag_child_locked_x(0)
05-03 10:09:28.359+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] drag_child_locked_y(0)
05-03 10:09:28.359+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] move content x(7), y(13)
05-03 10:09:28.359+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] mouse move
05-03 10:09:28.359+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] hold(0), freeze(0)
05-03 10:09:28.359+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] mx(3600), my(0), minx(0), miny(0), px(360), py(0)
05-03 10:09:28.359+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] cw(3960), ch(360), pw(360), ph(360)
05-03 10:09:28.369+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] x(0), y(0), nx(0), px(360), ny(0) py(0)
05-03 10:09:28.369+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1991 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] x(0), y(0)
05-03 10:09:28.369+0700 W/W_HOME  ( 1192): noti_broker.c: _handler_indicator_select(586) > 0
05-03 10:09:28.369+0700 W/W_HOME  ( 1192): noti_broker.c: _handler_indicator_select(596) > select index:1
05-03 10:09:28.369+0700 E/ELM_RPANEL( 1192): elm-rpanel.c: _panel_will_be_activated(954) > 
05-03 10:09:28.369+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] mouse move
05-03 10:09:28.369+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] hold(0), freeze(0)
05-03 10:09:28.369+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] mx(3600), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:28.369+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] cw(3960), ch(360), pw(360), ph(360)
05-03 10:09:28.369+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
05-03 10:09:28.369+0700 W/W_HOME  ( 1192): noti_broker.c: _handler_indicator_update(560) > 0x1
05-03 10:09:28.379+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] direction_x(1), direction_y(1)
05-03 10:09:28.379+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] drag_child_locked_x(0)
05-03 10:09:28.379+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] drag_child_locked_y(0)
05-03 10:09:28.379+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] move content x(5), y(8)
05-03 10:09:28.379+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] mouse move
05-03 10:09:28.379+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] hold(0), freeze(0)
05-03 10:09:28.389+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3868 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] direction_x(1), direction_y(1)
05-03 10:09:28.389+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3871 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] drag_child_locked_x(0)
05-03 10:09:28.389+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3878 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] drag_child_locked_y(0)
05-03 10:09:28.389+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:3889 _elm_scroll_hold_animator() [0xb85828c8 : elm_genlist] move content x(8), y(10)
05-03 10:09:28.389+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] mouse move
05-03 10:09:28.389+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb85828c8 : elm_genlist] hold(0), freeze(0)
05-03 10:09:28.399+0700 E/W_HOME  ( 1192): page_indicator.c: page_indicator_item_at(292) > (index < 0 || index >= PAGE_INDICATOR_MAX) -> page_indicator_item_at() return
05-03 10:09:28.399+0700 E/W_HOME  ( 1192): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
05-03 10:09:28.399+0700 W/W_HOME  ( 1192): noti_broker.c: _handler_indicator_select(586) > 0
05-03 10:09:28.399+0700 W/W_HOME  ( 1192): noti_broker.c: _handler_indicator_select(596) > select index:1
05-03 10:09:28.409+0700 I/wnotib  ( 1192): w-notification-board-basic-panel.c: _wnb_bp_destroy(5920) > Destory panel done.
05-03 10:09:28.409+0700 W/wnotib  ( 1192): w-notification-board-panel-manager.c: wnb_pm_destroy_panel(330) > page_instance 180, 0xae635720 is destoryed!
05-03 10:09:28.409+0700 I/wnotib  ( 1192): w-notification-board-noti-manager.c: _wnb_nm_free_data(248) > Free noti manager data.
05-03 10:09:28.409+0700 I/wnotib  ( 1192): w-notification-board-noti-manager.c: _wnb_nm_free_data(253) > Free previous notifications.
05-03 10:09:28.409+0700 I/wnotib  ( 1192): w-notification-board-noti-manager.c: _wnb_nm_free_data(274) > Free previous categories.
05-03 10:09:28.409+0700 I/wnotib  ( 1192): w-notification-board-noti-manager.c: _wnb_nm_idler_cb(747) > before_rpanel_count: 2, after_rpanel_count: 0.
05-03 10:09:28.409+0700 I/wnotib  ( 1192): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(60) > type: 0
05-03 10:09:28.409+0700 W/wnotib  ( 1192): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(79) > uncleared_category_count: 0, setting_power_saving_mode: 0
05-03 10:09:28.409+0700 W/wnotib  ( 1192): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(82) > Put empty view into panel body
05-03 10:09:28.409+0700 W/W_HOME  ( 1192): noti_broker.c: _handler_indicator_select(586) > 0
05-03 10:09:28.409+0700 W/W_HOME  ( 1192): noti_broker.c: _handler_indicator_select(596) > select index:1
05-03 10:09:28.409+0700 E/EFL     ( 2231): ecore_x<2231> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=425479 button=1
05-03 10:09:28.409+0700 I/wnotib  ( 1192): w-notification-board-empty-panel.c: _wnb_empty_panel_message_receive(99) > is_notification_enabled: 1, blocking mode: 0, is_connected_vendor_LO: 0
05-03 10:09:28.409+0700 I/wnotib  ( 1192): w-notification-board-empty-panel.c: _wnb_ep_set_texts(348) > is_connected_vendor_LO: 0, is_standalone_mode: 0
05-03 10:09:28.419+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1192): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
05-03 10:09:28.419+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1192): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
05-03 10:09:28.419+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1192): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
05-03 10:09:28.419+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1192): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
05-03 10:09:28.449+0700 W/W_HOME  ( 1192): noti_broker.c: _handler_indicator_update(560) > 0x1
05-03 10:09:28.479+0700 E/W_HOME  ( 1192): page_indicator.c: page_indicator_item_at(292) > (index < 0 || index >= PAGE_INDICATOR_MAX) -> page_indicator_item_at() return
05-03 10:09:28.479+0700 E/W_HOME  ( 1192): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
05-03 10:09:28.479+0700 W/W_HOME  ( 1192): noti_broker.c: _handler_indicator_update(560) > 0x1
05-03 10:09:28.509+0700 E/W_HOME  ( 1192): page_indicator.c: page_indicator_item_at(292) > (index < 0 || index >= PAGE_INDICATOR_MAX) -> page_indicator_item_at() return
05-03 10:09:28.509+0700 E/W_HOME  ( 1192): page_indicator_effect.c: page_indicator_focus_activate(494) > Failed to focus image object
05-03 10:09:29.109+0700 W/W_HOME  ( 1192): noti_broker.c: _handler_indicator_hide(550) > 
05-03 10:09:29.109+0700 W/W_HOME  ( 1192): index.c: index_hide(338) > hide VI:1 visibility:0 vi:(nil)
05-03 10:09:29.409+0700 E/EFL     (  935): ecore_x<935> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3400002 time=425479
05-03 10:09:29.409+0700 E/EFL     ( 2231): ecore_x<2231> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=425479
05-03 10:09:29.409+0700 E/EFL     (  935): ecore_x<935> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=425479
05-03 10:09:29.439+0700 W/KEYROUTER(  935): e_mod_main.c: DeliverDeviceKeyEvents(3164) > Deliver KeyPress. value=2669, window=0x3400002
05-03 10:09:29.439+0700 E/EFL     ( 2231): ecore_x<2231> ecore_x_events.c:537 _ecore_x_event_handle_key_press() KeyEvent:press time=426370
05-03 10:09:29.439+0700 W/KEYROUTER(  935): e_mod_main.c: DeliverDeviceKeyEvents(3175) > Deliver KeyRelease. value=2669, window=0x3400002
05-03 10:09:29.439+0700 E/EFL     ( 2231): ecore_x<2231> ecore_x_events.c:551 _ecore_x_event_handle_key_release() KeyEvent:release time=426510
05-03 10:09:29.439+0700 E/efl-extension( 2231): efl_extension_events.c: _eext_key_grab_rect_key_up_cb(240) > key up called
05-03 10:09:29.439+0700 E/EFL     ( 2231): elementary<2231> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 10:09:29.439+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb85828c8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:29.439+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb85828c8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:09:29.439+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb85828c8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:29.439+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb85828c8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:09:29.449+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb85828c8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:29.449+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb85828c8 : elm_genlist] cw(360), ch(115), pw(360), ph(360)
05-03 10:09:30.329+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2231): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 10:09:30.329+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2231): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 10:09:30.329+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2231): preference.c: preference_get_string(1258) > preference_get_string(2231) : URL error
05-03 10:09:30.359+0700 I/CAPI_NETWORK_CONNECTION( 2231): connection.c: connection_create(453) > New handle created[0xb85669a8]
05-03 10:09:31.279+0700 E/JSON PARSING( 2231): No data could be display
05-03 10:09:31.279+0700 E/JSON PARSING( 2231):  h��L��;
05-03 10:09:31.279+0700 I/CAPI_NETWORK_CONNECTION( 2231): connection.c: connection_destroy(471) > Destroy handle: 0xb85669a8
05-03 10:09:31.299+0700 I/APP_CORE( 1192): appcore-efl.c: __do_app(453) > [APP 1192] Event: MEM_FLUSH State: PAUSED
05-03 10:09:32.599+0700 E/WMS     ( 1016): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
05-03 10:09:34.839+0700 E/WMS     ( 1016): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
05-03 10:09:34.989+0700 E/CAPI_MEDIA_CONTROLLER( 1342): media_controller_main.c: __mc_main_check_connection(34) > [0m[No-error] Timer is Called but there is working Process, connection_cnt = 3
05-03 10:09:35.749+0700 E/PKGMGR  ( 2395): pkgmgr.c: __pkgmgr_client_uninstall(2061) > uninstall pkg start.
05-03 10:09:35.959+0700 E/PKGMGR_SERVER( 2397): pkgmgr-server.c: main(2242) > package manager server start
05-03 10:09:36.059+0700 E/PKGMGR_SERVER( 2397): pm-mdm.c: _pm_check_mdm_policy(1078) > [0;31m[_pm_check_mdm_policy(): 1078](ret != MDM_RESULT_SUCCESS) can not connect mdm server[0;m
05-03 10:09:36.059+0700 E/PKGMGR  ( 2395): pkgmgr.c: __pkgmgr_client_uninstall(2186) > uninstall pkg finish, ret=[23950002]
05-03 10:09:36.069+0700 E/PKGMGR_SERVER( 2398): pkgmgr-server.c: queue_job(1962) > UNINSTALL start, pkgid=[com.toyota.realtimefeedback]
05-03 10:09:36.069+0700 W/SHealthCommon( 1749): CpuLock.cpp: CheckAndReset(168) > [1;40;33mREQUEST POWER LOCK CPU [5000][0;m
05-03 10:09:36.139+0700 W/SHealthServiceCommon( 1749): StepLevelSensorProxy.cpp: StepLevelMonitorUpdated(374) > [1;40;33mOnNotWearingStop[0;m
05-03 10:09:36.139+0700 W/SHealthCommon( 1749): CpuLock.cpp: CheckAndReset(168) > [1;40;33mREQUEST POWER LOCK CPU [4930][0;m
05-03 10:09:36.189+0700 E/rpm-installer( 2398): rpm-appcore-intf.c: main(120) > ------------------------------------------------
05-03 10:09:36.189+0700 E/rpm-installer( 2398): rpm-appcore-intf.c: main(121) >  [START] rpm-installer: version=[201610629.1]
05-03 10:09:36.189+0700 E/rpm-installer( 2398): rpm-appcore-intf.c: main(122) > ------------------------------------------------
05-03 10:09:36.289+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2231): preference.c: _preference_check_retry_err(507) > key(URL), check retry err: -21/(2/No such file or directory).
05-03 10:09:36.289+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2231): preference.c: _preference_get_key(1101) > _preference_get_key(URL) step(-17825744) failed(2 / No such file or directory)
05-03 10:09:36.289+0700 E/CAPI_APPFW_APPLICATION_PREFERENCE( 2231): preference.c: preference_get_string(1258) > preference_get_string(2231) : URL error
05-03 10:09:36.319+0700 I/CAPI_NETWORK_CONNECTION( 2231): connection.c: connection_create(453) > New handle created[0xb85669a8]
05-03 10:09:36.339+0700 E/rpm-installer( 2398): rpm-cmdline.c: __ri_is_core_tpk_app(358) > This is a core tpk app.
05-03 10:09:36.369+0700 E/JSON PARSING( 2231): No data could be display
05-03 10:09:36.369+0700 E/JSON PARSING( 2231): }��L��;
05-03 10:09:36.369+0700 I/CAPI_NETWORK_CONNECTION( 2231): connection.c: connection_destroy(471) > Destroy handle: 0xb85669a8
05-03 10:09:36.469+0700 I/watchface-app( 1302): watchface-package-control.cpp: operator()(196) >  events of not interested package!!
05-03 10:09:36.469+0700 W/W_HOME  ( 1192): clock_event.c: _pkgmgr_event_cb(194) > Pkg:com.toyota.realtimefeedback is being uninstalled
05-03 10:09:36.469+0700 W/W_HOME  ( 1192): dbox.c: uninstall_cb(1434) > uninstalled:com.toyota.realtimefeedback
05-03 10:09:36.479+0700 W/AUL_AMD (  971): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(984) > pkgid(com.toyota.realtimefeedback), key(start), value(uninstall)
05-03 10:09:36.479+0700 W/AUL_AMD (  971): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(997) > __amd_pkgmgrinfo_start_handler
05-03 10:09:36.489+0700 E/WMS     ( 1016): wms_event_handler.c: _wms_event_handler_cb_log_package(4750) > package [_________] callback : [UNINSTALL, STARTED]
05-03 10:09:36.489+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:09:36.499+0700 E/ALARM_MANAGER(  975): alarm-manager.c: __display_lock_state(1884) > Lock LCD OFF is successfully done
05-03 10:09:36.499+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2231
05-03 10:09:36.509+0700 E/rpm-installer( 2398): installer-util.c: __check_running_app(1774) > app[com.toyota.realtimefeedback] is running, need to terminate it.
05-03 10:09:36.509+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 12
05-03 10:09:36.509+0700 W/AUL     ( 2398): launch.c: app_request_to_launchpad(284) > request cmd(5) to(2231)
05-03 10:09:36.509+0700 E/ALARM_MANAGER(  975): alarm-manager.c: __rtc_set(325) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
05-03 10:09:36.509+0700 E/ALARM_MANAGER(  975): alarm-manager.c: __rtc_set(332) > Setted RTC Alarm date/time is 3-5-2018, 03:10:45 (UTC).
05-03 10:09:36.509+0700 E/ALARM_MANAGER(  975): alarm-manager.c: __rtc_set(347) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
05-03 10:09:36.519+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 5
05-03 10:09:36.519+0700 W/AUL     (  971): app_signal.c: aul_send_app_terminate_request_signal(627) > aul_send_app_terminate_request_signal app(com.toyota.realtimefeedback) pid(2231) type(uiapp)
05-03 10:09:36.519+0700 I/APP_CORE( 2231): appcore-efl.c: __do_app(453) > [APP 2231] Event: TERMINATE State: RUNNING
05-03 10:09:36.519+0700 W/AUL_AMD (  971): amd_launch.c: __reply_handler(999) > listen fd(23) , send fd(22), pid(2231), cmd(4)
05-03 10:09:36.519+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 22
05-03 10:09:36.519+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(999) > app status : 4
05-03 10:09:36.519+0700 W/AUL     ( 2398): launch.c: app_request_to_launchpad(298) > request cmd(5) result(0)
05-03 10:09:36.519+0700 E/ALARM_MANAGER(  975): alarm-manager.c: __display_unlock_state(1927) > Unlock LCD OFF is successfully done
05-03 10:09:36.519+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:09:36.519+0700 W/APP_CORE( 2231): appcore-efl.c: appcore_efl_main(1788) > power off : 0
05-03 10:09:36.529+0700 W/APP_CORE( 2231): appcore-efl.c: _capture_and_make_file(1721) > Capture : win[3400002] -> redirected win[600771] for com.toyota.realtimefeedback[2231]
05-03 10:09:36.539+0700 W/SHealthCommon( 1749): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
05-03 10:09:36.539+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2231
05-03 10:09:36.589+0700 I/APP_CORE( 2231): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
05-03 10:09:36.589+0700 I/CAPI_APPFW_APPLICATION( 2231): app_main.c: _ui_app_appcore_terminate(585) > app_appcore_terminate
05-03 10:09:36.599+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] mx(3240), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:36.599+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] cw(3600), ch(360), pw(360), ph(360)
05-03 10:09:36.599+0700 E/EFL     ( 1192): elementary<1192> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7f9b7d0 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
05-03 10:09:36.609+0700 W/STARTER ( 1119): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1192] goes to (3)
05-03 10:09:36.609+0700 E/STARTER ( 1119): pkg-monitor.c: _proc_mgr_status_cb(457) > [_proc_mgr_status_cb:457] >>>>H(pid 1192)'s state(3)
05-03 10:09:36.609+0700 W/AUL_AMD (  971): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
05-03 10:09:36.609+0700 W/AUL_AMD (  971): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
05-03 10:09:36.609+0700 W/AUL     (  971): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-home) pid(1192) status(fg) type(uiapp)
05-03 10:09:36.609+0700 I/efl-extension( 2231): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 10:09:36.609+0700 I/efl-extension( 2231): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb84d8270 is freed
05-03 10:09:36.609+0700 E/EFL     ( 2231): elementary<2231> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb84fbdb0 in function: elm_layout_edje_get, of type: 'elm_grid' when expecting type: 'elm_layout'
05-03 10:09:36.619+0700 W/SHealthCommon( 1749): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
05-03 10:09:36.629+0700 I/efl-extension( 2231): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb84fc7f8, obj: 0xb84fc7f8
05-03 10:09:36.629+0700 I/efl-extension( 2231): efl_extension_rotary.c: _object_deleted_cb(618) > done
05-03 10:09:36.629+0700 E/EFL     ( 2231): elementary<2231> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 10:09:36.629+0700 E/EFL     ( 2231): elementary<2231> elm_genlist.c:6965 elm_genlist_item_prev_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
05-03 10:09:36.629+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb84fc7f8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:36.629+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb84fc7f8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:09:36.629+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb84fc7f8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
05-03 10:09:36.629+0700 E/EFL     ( 2231): elementary<2231> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb84fc7f8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
05-03 10:09:36.639+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:09:36.649+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2231
05-03 10:09:36.659+0700 W/SHealthCommon( 1749): TimelineSessionStorage.cpp: OnTriggered(1290) > [1;40;33mlocalStartTime: 1525305600000.000000[0;m
05-03 10:09:36.679+0700 I/efl-extension( 2231): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 10:09:36.679+0700 I/efl-extension( 2231): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb854df38 is freed
05-03 10:09:36.679+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:09:36.679+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb84fc7f8, elm_genlist, func : 0xb580dea1
05-03 10:09:36.679+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:09:36.689+0700 I/APP_CORE( 2231): appcore-efl.c: __after_loop(1243) > [APP 2231] After terminate() callback
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb87134b0 is freed
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb85828c8, elm_genlist, func : 0xb580dea1
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb871cd70
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb87237f0 is freed
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb87107e8, elm_scroller, func : 0xb5811379
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(248) > Removed cb from callbacks
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(266) > Freed cb
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb871cd70, elm_image, func : 0xb5811379
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
05-03 10:09:36.699+0700 I/efl-extension( 2231): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb87107e8 : elm_scroller] rotary callabck is deleted
05-03 10:09:36.749+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:09:36.759+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2231
05-03 10:09:36.789+0700 W/PROCESSMGR(  935): e_mod_processmgr.c: _e_mod_processmgr_send_update_watch_action(663) > [PROCESSMGR] =====================> send UpdateClock
05-03 10:09:36.789+0700 W/W_HOME  ( 1192): event_manager.c: _ecore_x_message_cb(421) > state: 1 -> 0
05-03 10:09:36.789+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:09:36.789+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:2, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:09:36.789+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:1, app_state:2 win_state:0(0) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:09:36.789+0700 W/W_HOME  ( 1192): main.c: _ecore_x_message_cb(1029) > main_info.is_win_on_top: 1
05-03 10:09:36.789+0700 W/WATCH_CORE( 1302): appcore-watch.c: __signal_process_manager_handler(1269) > process_manager_signal
05-03 10:09:36.789+0700 I/WATCH_CORE( 1302): appcore-watch.c: __signal_process_manager_handler(1285) > Call the time_tick_cb
05-03 10:09:36.799+0700 I/CAPI_WATCH_APPLICATION( 1302): watch_app_main.c: _watch_core_time_tick(313) > _watch_core_time_tick
05-03 10:09:36.799+0700 E/watchface-app( 1302): watchface.cpp: OnAppTimeTick(1157) > 
05-03 10:09:36.799+0700 I/watchface-app( 1302): watchface.cpp: OnAppTimeTick(1168) > set force update!!
05-03 10:09:36.799+0700 W/W_INDICATOR( 1122): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
05-03 10:09:36.799+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
05-03 10:09:36.829+0700 W/W_HOME  ( 1192): event_manager.c: _window_visibility_cb(460) > Window [0x2000003] is now visible(0)
05-03 10:09:36.829+0700 W/W_HOME  ( 1192): event_manager.c: _window_visibility_cb(470) > state: 0 -> 1
05-03 10:09:36.829+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:4, app_state:2 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:09:36.829+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:6, app_state:2 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:09:36.829+0700 W/W_HOME  ( 1192): main.c: _window_visibility_cb(996) > Window [0x2000003] is now visible(0)
05-03 10:09:36.829+0700 I/APP_CORE( 1192): appcore-efl.c: __do_app(453) > [APP 1192] Event: RESUME State: PAUSED
05-03 10:09:36.829+0700 I/CAPI_APPFW_APPLICATION( 1192): app_main.c: app_appcore_resume(223) > app_appcore_resume
05-03 10:09:36.829+0700 W/W_HOME  ( 1192): main.c: _appcore_resume_cb(480) > appcore resume
05-03 10:09:36.829+0700 W/W_HOME  ( 1192): event_manager.c: _app_resume_cb(373) > state: 2 -> 1
05-03 10:09:36.829+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:2, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:09:36.829+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:0, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:09:36.829+0700 W/W_HOME  ( 1192): main.c: home_resume(528) > journal_multimedia_screen_loaded_home called
05-03 10:09:36.829+0700 W/W_HOME  ( 1192): main.c: home_resume(532) > clock/widget resumed
05-03 10:09:36.829+0700 W/W_INDICATOR( 1122): windicator.c: _home_screen_clock_visibility_changed_cb(1023) > [_home_screen_clock_visibility_changed_cb:1023] Clock visibility : 0
05-03 10:09:36.829+0700 W/W_INDICATOR( 1122): windicator_battery.c: windicator_battery_vconfkey_unregister(600) > [windicator_battery_vconfkey_unregister:600] Unset battery cb
05-03 10:09:36.839+0700 E/W_HOME  ( 1192): retailmode.c: retailmode_enabled_get(245) > failed to get value VCONFKEY_RETAILMODE_ENABLED
05-03 10:09:36.839+0700 W/W_HOME  ( 1192): event_manager.c: _state_control(176) > control:1, app_state:1 win_state:0(1) pm_state:1 home_visible:1 clock_visible:0 tutorial_state:0 editing : 0, home_clocklist:0, addviewer:0 scrolling : 0, powersaving : 0, apptray state : 1, apptray visibility : 0, apptray edit visibility : 0
05-03 10:09:36.839+0700 I/GATE    ( 1192): <GATE-M>APP_FULLY_LOADED_w-home</GATE-M>
05-03 10:09:36.839+0700 I/wnotib  ( 1192): w-notification-board-broker-main.c: _wnb_ecore_x_event_visibility_changed_cb(746) > fully_obscured: 0
05-03 10:09:36.839+0700 E/wnotib  ( 1192): w-notification-board-action.c: wnb_action_is_drawer_hidden(5192) > [NULL==g_wnb_action_data] msg Drawer not initialized.
05-03 10:09:36.839+0700 W/wnotib  ( 1192): w-notification-board-noti-manager.c: wnb_nm_do_postponed_job(981) > No postponed update with is_for_VI: 0.
05-03 10:09:36.859+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:09:36.879+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2231
05-03 10:09:36.899+0700 W/SHealthCommon( 1749): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: timeline_session_updated, pendingClientInfoList.size(): 0[0;m
05-03 10:09:36.899+0700 W/SHealthCommon( 1749): CpuLock.cpp: CheckAndReset(178) > [1;40;33mRELEASE POWER LOCK CPU[0;m
05-03 10:09:36.979+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:09:36.989+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2231
05-03 10:09:37.089+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:09:37.099+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2231
05-03 10:09:37.199+0700 W/AUL_AMD (  971): amd_request.c: __request_handler(669) > __request_handler: 14
05-03 10:09:37.209+0700 W/AUL_AMD (  971): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
05-03 10:09:37.249+0700 E/PKGMGR_PARSER( 2398): pkgmgr_parser_signature.c: __ps_check_mdm_policy_by_pkgid(1056) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
05-03 10:09:37.289+0700 I/PRIVACY-MANAGER-CLIENT( 2398): SocketClient.cpp: SocketClient(37) > Client created
05-03 10:09:37.289+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: mainloop(227) > Got incoming connection
05-03 10:09:37.299+0700 I/PRIVACY-MANAGER-CLIENT( 2398): SocketStream.h: SocketStream(31) > Created
05-03 10:09:37.299+0700 I/PRIVACY-MANAGER-CLIENT( 2398): SocketConnection.h: SocketConnection(44) > Created
05-03 10:09:37.299+0700 I/PRIVACY-MANAGER-CLIENT( 2398): SocketClient.cpp: connect(62) > Client connected
05-03 10:09:37.299+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: connectionThread(253) > Starting connection thread
05-03 10:09:37.299+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketStream.h: SocketStream(31) > Created
05-03 10:09:37.299+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketConnection.h: SocketConnection(44) > Created
05-03 10:09:37.299+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: connectionService(304) > Calling service
05-03 10:09:37.299+0700 W/SHealthCommon( 1749): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: timeline_summary_updated, pendingClientInfoList.size(): 0[0;m
05-03 10:09:37.299+0700 W/SHealthServiceCommon( 1749): EnergyExpenditureFeatureController.cpp: OnTotalEnergyExpenditureChanged(119) > [1;40;33mstart 1525280400000.000000, end 1525316977306.000000, calories 684.136536[0;m
05-03 10:09:37.299+0700 W/SHealthCommon( 1749): SHealthMessagePortConnection.cpp: SendServiceMessage(558) > [1;40;33mmessageName: energy_expenditure_updated, pendingClientInfoList.size(): 1[0;m
05-03 10:09:37.299+0700 W/SHealthCommon( 1749): TimelineSessionStorage.cpp: OnTriggered(1290) > [1;40;33mlocalStartTime: 1525305600000.000000[0;m
05-03 10:09:37.309+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: connectionService(307) > Removing client
05-03 10:09:37.309+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: connectionService(311) > Call served
05-03 10:09:37.309+0700 I/PRIVACY-MANAGER-SERVER(  969): SocketService.cpp: connectionThread(262) > Client serviced
05-03 10:09:37.309+0700 I/PRIVACY-MANAGER-CLIENT( 2398): SocketClient.cpp: disconnect(72) > Client disconnected
05-03 10:09:37.329+0700 I/GATE    ( 1192): <GATE-M>SCREEN_LOADED_HOME</GATE-M>
05-03 10:09:37.329+0700 W/CRASH_MANAGER( 2402): worker.c: worker_job(1205) > 1102231726561152531697
