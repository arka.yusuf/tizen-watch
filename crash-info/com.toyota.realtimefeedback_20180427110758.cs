S/W Version Information
Model: SM-R730A
Tizen-Version: 2.3.2.7
Build-Number: R730AUCU3DRC1
Build-Date: 2018.03.02 17:39:24

Crash Information
Process Name: realtimefeedback1
PID: 2418
Date: 2018-04-27 11:07:58+0700
Executable File Path: /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 2418, uid 5000)

Register Information
r0   = 0x00000004, r1   = 0x000186a0
r2   = 0xb585e443, r3   = 0x00000000
r4   = 0x00000004, r5   = 0x00000000
r6   = 0xb90df070, r7   = 0xbe8f51d0
r8   = 0x000186a0, r9   = 0xb8f2fe40
r10  = 0xb6c34b10, fp   = 0x00000000
ip   = 0x00000174, sp   = 0xbe8f5150
lr   = 0xb585e443, pc   = 0xb69ea6f0
cpsr = 0x20000010

Memory Information
MemTotal:   405512 KB
MemFree:      9120 KB
Buffers:      5696 KB
Cached:     109892 KB
VmPeak:      90012 KB
VmSize:      89468 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       23344 KB
VmRSS:       23344 KB
VmData:      29316 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       24500 KB
VmPTE:          62 KB
VmSwap:          0 KB

Threads Information
Threads: 3
PID = 2418 TID = 2418
2418 2513 2514 

Maps Information
b1f24000 b1f28000 r-xp /usr/lib/libogg.so.0.7.1
b1f30000 b1f52000 r-xp /usr/lib/libvorbis.so.0.4.3
b1f5a000 b1fa1000 r-xp /usr/lib/libsndfile.so.1.0.26
b1fad000 b1ff6000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b1fff000 b2004000 r-xp /usr/lib/libjson.so.0.0.1
b38a5000 b39ab000 r-xp /usr/lib/libicuuc.so.57.1
b39c1000 b3b49000 r-xp /usr/lib/libicui18n.so.57.1
b3b59000 b3b66000 r-xp /usr/lib/libail.so.0.1.0
b3b6f000 b3b72000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3b7a000 b3bb2000 r-xp /usr/lib/libpulse.so.0.16.2
b3bb3000 b3bb6000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3bbe000 b3c1f000 r-xp /usr/lib/libasound.so.2.0.0
b3c29000 b3c42000 r-xp /usr/lib/libavsysaudio.so.0.0.1
b3c4b000 b3c4f000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3c57000 b3c62000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.0
b3c6f000 b3c73000 r-xp /usr/lib/libmmfsession.so.0.0.0
b3c7c000 b3c94000 r-xp /usr/lib/libmmfsound.so.0.1.0
b3ca5000 b3cac000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3cb4000 b3cbf000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.1.54
b3cc7000 b3cc9000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.11
b3cd1000 b3cd2000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3cda000 b3ce2000 r-xp /usr/lib/libfeedback.so.0.1.4
b3cfb000 b3cfc000 r-xp /usr/lib/edje/modules/feedback/linux-gnueabi-armv7l-1.0.0/module.so
b3f89000 b4010000 rw-s anon_inode:dmabuf
b4011000 b4810000 rw-p [stack:2514]
b4a30000 b4a31000 r-xp /usr/lib/evas/modules/loaders/eet/linux-gnueabi-armv7l-1.7.99/module.so
b4ab9000 b52b8000 rw-p [stack:2513]
b52b8000 b52ba000 r-xp /usr/lib/evas/modules/loaders/png/linux-gnueabi-armv7l-1.7.99/module.so
b52c2000 b52d9000 r-xp /usr/lib/edje/modules/elm/linux-gnueabi-armv7l-1.0.0/module.so
b52e6000 b52e8000 r-xp /usr/lib/libdri2.so.0.0.0
b52f0000 b52fb000 r-xp /usr/lib/libtbm.so.1.0.0
b5303000 b530b000 r-xp /usr/lib/libdrm.so.2.4.0
b5313000 b5315000 r-xp /usr/lib/libgenlock.so
b531d000 b5322000 r-xp /usr/lib/bufmgr/libtbm_msm.so.0.0.0
b532a000 b5335000 r-xp /usr/lib/evas/modules/engines/software_generic/linux-gnueabi-armv7l-1.7.99/module.so
b553e000 b5608000 r-xp /usr/lib/libCOREGL.so.4.0
b5619000 b5629000 r-xp /usr/lib/libmdm-common.so.1.1.25
b5631000 b5637000 r-xp /usr/lib/libxcb-render.so.0.0.0
b563f000 b5640000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b5649000 b564c000 r-xp /usr/lib/libEGL.so.1.4
b5654000 b5662000 r-xp /usr/lib/libGLESv2.so.2.0
b566b000 b56b4000 r-xp /usr/lib/libmdm.so.1.2.70
b56bd000 b56c3000 r-xp /usr/lib/libcsc-feature.so.0.0.0
b56cb000 b56d4000 r-xp /usr/lib/libcom-core.so.0.0.1
b56dd000 b5795000 r-xp /usr/lib/libcairo.so.2.11200.14
b57a0000 b57b9000 r-xp /usr/lib/libnetwork.so.0.0.0
b57c1000 b57cd000 r-xp /usr/lib/libnotification.so.0.1.0
b57d6000 b57e5000 r-xp /usr/lib/libjson-glib-1.0.so.0.1001.3
b57ee000 b580f000 r-xp /usr/lib/libefl-extension.so.0.1.0
b5817000 b581c000 r-xp /usr/lib/libcapi-system-info.so.0.2.0
b5824000 b5829000 r-xp /usr/lib/libcapi-system-device.so.0.1.0
b5831000 b5841000 r-xp /usr/lib/libcapi-network-connection.so.1.0.63
b5849000 b5851000 r-xp /usr/lib/libcapi-appfw-preference.so.0.3.2.5
b5859000 b5862000 r-xp /opt/usr/apps/com.toyota.realtimefeedback/bin/realtimefeedback1
b5a07000 b5a11000 r-xp /lib/libnss_files-2.13.so
b5a1a000 b5ae9000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b5aff000 b5b23000 r-xp /usr/lib/ecore/immodules/libisf-imf-module.so
b5b2c000 b5b32000 r-xp /usr/lib/libappsvc.so.0.1.0
b5b3a000 b5b3e000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.2.5
b5b4b000 b5b56000 r-xp /usr/lib/evas/modules/engines/software_x11/linux-gnueabi-armv7l-1.7.99/module.so
b5b5e000 b5b60000 r-xp /usr/lib/libiniparser.so.0
b5b69000 b5b6e000 r-xp /usr/lib/libappcore-common.so.1.1
b5b76000 b5b78000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.2.5
b5b81000 b5b85000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.2.5
b5b92000 b5b94000 r-xp /usr/lib/libXau.so.6.0.0
b5b9c000 b5ba3000 r-xp /lib/libcrypt-2.13.so
b5bd3000 b5bd5000 r-xp /usr/lib/libiri.so
b5bde000 b5d70000 r-xp /usr/lib/libcrypto.so.1.0.0
b5d91000 b5dd8000 r-xp /usr/lib/libssl.so.1.0.0
b5de4000 b5e12000 r-xp /usr/lib/libidn.so.11.5.44
b5e1a000 b5e23000 r-xp /usr/lib/libcares.so.2.1.0
b5e2d000 b5e40000 r-xp /usr/lib/libxcb.so.1.1.0
b5e49000 b5e4c000 r-xp /usr/lib/journal/libjournal.so.0.1.0
b5e54000 b5e56000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5e5f000 b5f2b000 r-xp /usr/lib/libxml2.so.2.7.8
b5f38000 b5f3a000 r-xp /usr/lib/libgmodule-2.0.so.0.3200.3
b5f43000 b5f48000 r-xp /usr/lib/libffi.so.5.0.10
b5f50000 b5f51000 r-xp /usr/lib/libgthread-2.0.so.0.3200.3
b5f59000 b5f5c000 r-xp /lib/libattr.so.1.1.0
b5f64000 b5ff8000 r-xp /usr/lib/libstdc++.so.6.0.16
b600b000 b6028000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b6032000 b604a000 r-xp /usr/lib/libpng12.so.0.50.0
b6052000 b6068000 r-xp /lib/libexpat.so.1.6.0
b6072000 b60b6000 r-xp /usr/lib/libcurl.so.4.3.0
b60bf000 b60c9000 r-xp /usr/lib/libXext.so.6.4.0
b60d3000 b60d7000 r-xp /usr/lib/libXtst.so.6.1.0
b60df000 b60e5000 r-xp /usr/lib/libXrender.so.1.3.0
b60ed000 b60f3000 r-xp /usr/lib/libXrandr.so.2.2.0
b60fb000 b60fc000 r-xp /usr/lib/libXinerama.so.1.0.0
b6105000 b610e000 r-xp /usr/lib/libXi.so.6.1.0
b6116000 b6119000 r-xp /usr/lib/libXfixes.so.3.1.0
b6122000 b6124000 r-xp /usr/lib/libXgesture.so.7.0.0
b612c000 b612e000 r-xp /usr/lib/libXcomposite.so.1.0.0
b6136000 b6138000 r-xp /usr/lib/libXdamage.so.1.1.0
b6140000 b6147000 r-xp /usr/lib/libXcursor.so.1.0.2
b614f000 b6152000 r-xp /usr/lib/libecore_input_evas.so.1.7.99
b615b000 b615f000 r-xp /usr/lib/libecore_ipc.so.1.7.99
b6168000 b616d000 r-xp /usr/lib/libecore_fb.so.1.7.99
b6176000 b6257000 r-xp /usr/lib/libX11.so.6.3.0
b6262000 b6285000 r-xp /usr/lib/libjpeg.so.8.0.2
b629d000 b62b3000 r-xp /lib/libz.so.1.2.5
b62bc000 b62be000 r-xp /usr/lib/libsecurity-extension-common.so.1.0.1
b62c6000 b633b000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6345000 b635f000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b6367000 b639b000 r-xp /usr/lib/libgobject-2.0.so.0.3200.3
b63a4000 b6477000 r-xp /usr/lib/libgio-2.0.so.0.3200.3
b6483000 b6493000 r-xp /lib/libresolv-2.13.so
b6497000 b64af000 r-xp /usr/lib/liblzma.so.5.0.3
b64b7000 b64ba000 r-xp /lib/libcap.so.2.21
b64c2000 b64f1000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b64f9000 b64fa000 r-xp /usr/lib/libecore_imf_evas.so.1.7.99
b6503000 b6509000 r-xp /usr/lib/libecore_imf.so.1.7.99
b6511000 b6528000 r-xp /usr/lib/liblua-5.1.so
b6531000 b6538000 r-xp /usr/lib/libembryo.so.1.7.99
b6540000 b6546000 r-xp /lib/librt-2.13.so
b654f000 b65a5000 r-xp /usr/lib/libpixman-1.so.0.28.2
b65b3000 b6609000 r-xp /usr/lib/libfreetype.so.6.11.3
b6615000 b663d000 r-xp /usr/lib/libfontconfig.so.1.8.0
b663e000 b6683000 r-xp /usr/lib/libharfbuzz.so.0.10200.4
b668c000 b669f000 r-xp /usr/lib/libfribidi.so.0.3.1
b66a7000 b66c1000 r-xp /usr/lib/libecore_con.so.1.7.99
b66cb000 b66d4000 r-xp /usr/lib/libedbus.so.1.7.99
b66dc000 b672c000 r-xp /usr/lib/libecore_x.so.1.7.99
b672e000 b6737000 r-xp /usr/lib/libvconf.so.0.2.45
b673f000 b6750000 r-xp /usr/lib/libecore_input.so.1.7.99
b6758000 b675d000 r-xp /usr/lib/libecore_file.so.1.7.99
b6765000 b6787000 r-xp /usr/lib/libecore_evas.so.1.7.99
b6790000 b67d1000 r-xp /usr/lib/libeina.so.1.7.99
b67da000 b67f3000 r-xp /usr/lib/libeet.so.1.7.99
b6804000 b686d000 r-xp /lib/libm-2.13.so
b6876000 b687c000 r-xp /usr/lib/libcapi-base-common.so.0.1.8
b6885000 b6886000 r-xp /usr/lib/libsecurity-extension-interface.so.1.0.1
b688e000 b68b1000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b68b9000 b68be000 r-xp /usr/lib/libxdgmime.so.1.1.0
b68c6000 b68f0000 r-xp /usr/lib/libdbus-1.so.3.8.12
b68f9000 b6910000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6918000 b6923000 r-xp /lib/libunwind.so.8.0.1
b6950000 b696e000 r-xp /usr/lib/libsystemd.so.0.4.0
b6978000 b6a9c000 r-xp /lib/libc-2.13.so
b6aaa000 b6ab2000 r-xp /lib/libgcc_s-4.6.so.1
b6ab3000 b6ab7000 r-xp /usr/lib/libsmack.so.1.0.0
b6ac0000 b6ac6000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6ace000 b6b9e000 r-xp /usr/lib/libglib-2.0.so.0.3200.3
b6b9f000 b6bfd000 r-xp /usr/lib/libedje.so.1.7.99
b6c07000 b6c1e000 r-xp /usr/lib/libecore.so.1.7.99
b6c35000 b6d03000 r-xp /usr/lib/libevas.so.1.7.99
b6d29000 b6e65000 r-xp /usr/lib/libelementary.so.1.7.99
b6e7c000 b6e90000 r-xp /lib/libpthread-2.13.so
b6e9b000 b6e9d000 r-xp /usr/lib/libdlog.so.0.0.0
b6ea5000 b6ea8000 r-xp /usr/lib/libbundle.so.0.1.22
b6eb0000 b6eb2000 r-xp /lib/libdl-2.13.so
b6ebb000 b6ec8000 r-xp /usr/lib/libaul.so.0.1.0
b6eda000 b6ee0000 r-xp /usr/lib/libappcore-efl.so.1.1
b6ee9000 b6eed000 r-xp /usr/lib/libsys-assert.so
b6ef6000 b6f13000 r-xp /lib/ld-2.13.so
b6f1c000 b6f21000 r-xp /usr/bin/launchpad-loader
b8ef7000 b952f000 rw-p [heap]
be8d5000 be8f6000 rw-p [stack]
End of Maps Information

Callstack Information (PID:2418)
Call Stack Count: 1
 0: realloc + 0x4c (0xb69ea6f0) [/lib/libc.so.6] + 0x726f0
End of Call Stack

Package Information
Package Name: com.toyota.realtimefeedback
Package ID : com.toyota.realtimefeedback
Version: 1.0.0
Package Type: rpm
App Name: Real Feed Back
App ID: com.toyota.realtimefeedback
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
xpecting type: 'elm_layout'
04-27 11:07:49.729+0700 I/efl-extension( 2418): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb902c160, elm_image, _activated_obj : 0x0, activated : 1
04-27 11:07:49.789+0700 I/GATE    (  930): <GATE-M>BATTERY_LEVEL_76</GATE-M>
04-27 11:07:49.789+0700 W/W_INDICATOR( 1142): windicator_battery.c: windicator_battery_update(98) > [windicator_battery_update:98] 
04-27 11:07:49.789+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(312) > [_battery_icon_update:312] battery level(76), length(2)
04-27 11:07:49.789+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(336) > [_battery_icon_update:336] 76%
04-27 11:07:49.789+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(351) > [_battery_icon_update:351] battery_level: 76, isCharging: 0
04-27 11:07:49.789+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(385) > [_battery_icon_update:385] battery file : change_level_80
04-27 11:07:49.789+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(464) > [_battery_icon_update:464] [ATT] Battery level : 80
04-27 11:07:49.789+0700 W/W_INDICATOR( 1142): windicator_battery.c: _battery_icon_update(531) > [_battery_icon_update:531] Normal charging status !!
04-27 11:07:49.789+0700 I/watchface-viewer( 1362): viewer-data-provider.cpp: AddPendingChanges(2527) > added [60] to pending list
04-27 11:07:49.809+0700 I/efl-extension( 2418): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:07:49.809+0700 I/efl-extension( 2418): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:07:49.819+0700 I/efl-extension( 2418): efl_extension_rotary.c: eext_rotary_event_handler_add(77) > init_count: 0
04-27 11:07:49.819+0700 I/efl-extension( 2418): efl_extension_rotary.c: _init_Xi2_system(314) > In
04-27 11:07:49.819+0700 I/efl-extension( 2418): efl_extension_rotary.c: _init_Xi2_system(375) > Done
04-27 11:07:49.819+0700 I/efl-extension( 2418): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb9034988, elm_image, _activated_obj : 0xb902c160, activated : 1
04-27 11:07:49.819+0700 I/efl-extension( 2418): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:07:49.849+0700 E/EFL     ( 2418): elementary<2418> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb902c160 in function: elm_progressbar_pulse_set, of type: 'elm_image' when expecting type: 'elm_progressbar'
04-27 11:07:49.849+0700 E/EFL     ( 2418): elementary<2418> elm_widget.c:4382 elm_widget_type_check() Passing Object: 0xb902c160 in function: elm_progressbar_pulse, of type: 'elm_image' when expecting type: 'elm_progressbar'
04-27 11:07:49.869+0700 E/EFL     ( 2418): elementary<2418> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-27 11:07:49.879+0700 E/EFL     ( 2418): elementary<2418> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb907c9a0) will be pushed
04-27 11:07:49.889+0700 I/APP_CORE( 2418): appcore-efl.c: __do_app(453) > [APP 2418] Event: RESET State: CREATED
04-27 11:07:49.889+0700 I/CAPI_APPFW_APPLICATION( 2418): app_main.c: _ui_app_appcore_reset(645) > app_appcore_reset
04-27 11:07:49.899+0700 I/APP_CORE( 2418): appcore-efl.c: __do_app(522) > Legacy lifecycle: 0
04-27 11:07:49.899+0700 I/APP_CORE( 2418): appcore-efl.c: __do_app(524) > [APP 2418] Initial Launching, call the resume_cb
04-27 11:07:49.899+0700 I/CAPI_APPFW_APPLICATION( 2418): app_main.c: _ui_app_appcore_resume(628) > app_appcore_resume
04-27 11:07:49.909+0700 W/APP_CORE( 2418): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3200002
04-27 11:07:49.919+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb902c740 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:49.919+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb902c740 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:49.919+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb902c740 : elm_genlist] mx(99999639), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:49.919+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb902c740 : elm_genlist] cw(99999999), ch(0), pw(360), ph(360)
04-27 11:07:49.959+0700 I/APP_CORE( 2418): appcore-efl.c: __do_app(453) > [APP 2418] Event: RESUME State: RUNNING
04-27 11:07:49.959+0700 I/GATE    ( 2418): <GATE-M>APP_FULLY_LOADED_realtimefeedback</GATE-M>
04-27 11:07:49.959+0700 I/APP_CORE( 2364): appcore-efl.c: __do_app(453) > [APP 2364] Event: PAUSE State: RUNNING
04-27 11:07:49.959+0700 I/CAPI_APPFW_APPLICATION( 2364): app_main.c: app_appcore_pause(202) > app_appcore_pause
04-27 11:07:49.959+0700 W/AUL     (  929): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.w-taskmanager) pid(2364) status(bg) type(uiapp)
04-27 11:07:49.959+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2364] goes to (4)
04-27 11:07:49.969+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[2418] goes to (3)
04-27 11:07:49.969+0700 W/AUL     (  929): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.toyota.realtimefeedback) pid(2418) status(fg) type(uiapp)
04-27 11:07:50.119+0700 I/efl-extension( 2499): efl_extension.c: eext_mod_init(40) > Init
04-27 11:07:50.169+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 22
04-27 11:07:50.169+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(999) > app status : 4
04-27 11:07:50.169+0700 E/APP_CORE( 2364): appcore.c: __del_vconf(453) > [FAILED]vconfkey_ignore_key_changed
04-27 11:07:50.169+0700 I/APP_CORE( 2364): appcore-efl.c: __after_loop(1232) > Legacy lifecycle: 0
04-27 11:07:50.169+0700 I/CAPI_APPFW_APPLICATION( 2364): app_main.c: app_appcore_terminate(177) > app_appcore_terminate
04-27 11:07:50.169+0700 I/efl-extension( 2364): efl_extension_rotary.c: _object_deleted_cb(589) > In: data: 0xb7847f50, obj: 0xb7847f50
04-27 11:07:50.169+0700 I/efl-extension( 2364): efl_extension_rotary.c: _remove_ecore_handlers(571) > In
04-27 11:07:50.169+0700 I/efl-extension( 2364): efl_extension_rotary.c: _remove_ecore_handlers(576) > removed _motion_handler
04-27 11:07:50.169+0700 I/efl-extension( 2364): efl_extension_rotary.c: _remove_ecore_handlers(582) > removed _rotate_handler
04-27 11:07:50.179+0700 I/efl-extension( 2364): efl_extension_rotary.c: _object_deleted_cb(618) > done
04-27 11:07:50.179+0700 E/EFL     ( 2364): elementary<2364> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb7847f50 : elm_scroller] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:50.179+0700 E/EFL     ( 2364): elementary<2364> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb7847f50 : elm_scroller] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:50.179+0700 E/EFL     ( 2364): elementary<2364> elm_interface_scrollable.c:1965 _elm_scroll_content_region_show_internal() [0xb7847f50 : elm_scroller] x(0), y(0), nx(0), px(0), ny(0) py(0)
04-27 11:07:50.179+0700 I/efl-extension( 2364): efl_extension_rotary.c: _activated_obj_del_cb(624) > _activated_obj_del_cb : 0xb7866188
04-27 11:07:50.179+0700 I/efl-extension( 2364): efl_extension_circle_surface.c: _eext_circle_surface_del_cb(687) > Surface is going to free in delete callback for image widget.
04-27 11:07:50.179+0700 I/efl-extension( 2364): efl_extension_circle_surface.c: _eext_circle_surface_del_internal(988) > surface 0xb7885a58 is freed
04-27 11:07:50.179+0700 I/efl-extension( 2364): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 11:07:50.179+0700 I/efl-extension( 2364): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7847f50, elm_scroller, func : 0xb36eb379
04-27 11:07:50.179+0700 I/efl-extension( 2364): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 11:07:50.179+0700 I/efl-extension( 2364): efl_extension_rotary.c: eext_rotary_object_event_callback_del(235) > In
04-27 11:07:50.179+0700 I/efl-extension( 2364): efl_extension_rotary.c: eext_rotary_object_event_callback_del(240) > callback del 0xb7866188, elm_image, func : 0xb36eb379
04-27 11:07:50.179+0700 I/efl-extension( 2364): efl_extension_rotary.c: eext_rotary_object_event_callback_del(273) > done
04-27 11:07:50.179+0700 I/efl-extension( 2364): efl_extension_circle_object_scroller.c: _eext_circle_object_scroller_del_cb(852) > [0xb7847f50 : elm_scroller] rotary callabck is deleted
04-27 11:07:50.219+0700 I/APP_CORE( 2364): appcore-efl.c: __after_loop(1243) > [APP 2364] After terminate() callback
04-27 11:07:50.279+0700 I/UXT     ( 2499): Uxt_ObjectManager.cpp: OnInitialized(753) > Initialized.
04-27 11:07:50.359+0700 E/AUL     (  929): app_signal.c: __app_dbus_signal_filter(97) > reject by security issue - no interface
04-27 11:07:50.369+0700 I/UXT     ( 2364): uxt_theme_private.c: uxt_theme_delete_file_monitor(1655) > deleted color config file monitor
04-27 11:07:50.369+0700 I/UXT     ( 2364): uxt_theme_private.c: uxt_theme_delete_file_monitor(1662) > deleted font config file monitor
04-27 11:07:50.369+0700 I/UXT     ( 2364): Uxt_ObjectManager.cpp: OnTerminating(774) > Terminating.
04-27 11:07:50.459+0700 I/AUL_PAD ( 2499): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
04-27 11:07:50.479+0700 I/Adreno-EGL( 2499): <qeglDrvAPI_eglInitialize:410>: EGL 1.4 QUALCOMM build:  ()
04-27 11:07:50.479+0700 I/Adreno-EGL( 2499): OpenGL ES Shader Compiler Version: E031.24.00.16
04-27 11:07:50.479+0700 I/Adreno-EGL( 2499): Build Date: 09/02/15 Wed
04-27 11:07:50.479+0700 I/Adreno-EGL( 2499): Local Branch: 
04-27 11:07:50.479+0700 I/Adreno-EGL( 2499): Remote Branch: 
04-27 11:07:50.479+0700 I/Adreno-EGL( 2499): Local Patches: 
04-27 11:07:50.479+0700 I/Adreno-EGL( 2499): Reconstruct Branch: 
04-27 11:07:50.719+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:07:50.729+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2418
04-27 11:07:50.729+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 12
04-27 11:07:50.739+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:07:50.749+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 2418
04-27 11:07:50.749+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 12
04-27 11:07:50.749+0700 I/GATE    ( 2364): <GATE-M>APP_CLOSED_w-taskmanager</GATE-M>
04-27 11:07:50.759+0700 W/AUL_PAD ( 2364): launchpad_loader.c: __at_exit_to_release_bundle(301) > __at_exit_to_release_bundle
04-27 11:07:50.839+0700 I/efl-extension( 2364): efl_extension.c: eext_mod_shutdown(46) > Shutdown
04-27 11:07:51.049+0700 I/AUL_PAD ( 2508): launchpad_loader.c: main(591) > [candidate] elm init, returned: 1
04-27 11:07:51.189+0700 W/AUL_PAD ( 1876): sigchild.h: __launchpad_process_sigchld(188) > dead_pid = 2364 pgid = 2364
04-27 11:07:51.189+0700 W/AUL_PAD ( 1876): sigchild.h: __launchpad_process_sigchld(189) > ssi_code = 1 ssi_status = 0
04-27 11:07:51.229+0700 W/AUL_PAD ( 1876): sigchild.h: __launchpad_process_sigchld(197) > after __sigchild_action
04-27 11:07:51.229+0700 I/AUL_AMD (  929): amd_main.c: __app_dead_handler(262) > __app_dead_handler, pid: 2364
04-27 11:07:51.229+0700 W/AUL     (  929): app_signal.c: aul_send_app_terminated_signal(799) > aul_send_app_terminated_signal pid(2364)
04-27 11:07:51.949+0700 I/CAPI_NETWORK_CONNECTION( 2418): connection.c: connection_create(453) > New handle created[0xb90c5960]
04-27 11:07:52.009+0700 I/CAPI_NETWORK_CONNECTION( 2418): connection.c: connection_destroy(471) > Destroy handle: 0xb90c5960
04-27 11:07:52.009+0700 E/EFL     ( 2418): <2418> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:52.019+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb902c740 : elm_genlist] mx(0), my(272), minx(0), miny(0), px(0), py(0)
04-27 11:07:52.019+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb902c740 : elm_genlist] cw(360), ch(632), pw(360), ph(360)
04-27 11:07:52.169+0700 W/AUL_AMD (  929): amd_status.c: __app_terminate_timer_cb(168) > send SIGKILL: No such process
04-27 11:07:52.719+0700 E/EFL     ( 2418): ecore_x<2418> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=185477 button=1
04-27 11:07:52.729+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] mouse move
04-27 11:07:52.729+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] mouse move
04-27 11:07:52.729+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] hold(0), freeze(0)
04-27 11:07:52.749+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] mouse move
04-27 11:07:52.749+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] hold(0), freeze(0)
04-27 11:07:52.769+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] mouse move
04-27 11:07:52.769+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] hold(0), freeze(0)
04-27 11:07:52.779+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] mouse move
04-27 11:07:52.779+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] hold(0), freeze(0)
04-27 11:07:52.789+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] mouse move
04-27 11:07:52.789+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] hold(0), freeze(0)
04-27 11:07:52.799+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] mouse move
04-27 11:07:52.799+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] hold(0), freeze(0)
04-27 11:07:52.829+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] mouse move
04-27 11:07:52.829+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] hold(0), freeze(0)
04-27 11:07:52.839+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] mouse move
04-27 11:07:52.839+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb902c740 : elm_genlist] hold(0), freeze(0)
04-27 11:07:52.849+0700 E/EFL     ( 2418): ecore_x<2418> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=185607 button=1
04-27 11:07:53.049+0700 E/GL SELECTED( 2418): ID GL : TRM1
04-27 11:07:53.399+0700 E/EFL     ( 2418): ecore_x<2418> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=186154 button=1
04-27 11:07:53.599+0700 E/EFL     ( 2418): ecore_x<2418> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=186350 button=1
04-27 11:07:53.599+0700 I/efl-extension( 2418): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:07:53.599+0700 I/efl-extension( 2418): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:07:53.599+0700 I/efl-extension( 2418): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb90e00f8, elm_image, _activated_obj : 0xb9034988, activated : 1
04-27 11:07:53.599+0700 I/efl-extension( 2418): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:07:53.609+0700 E/EFL     ( 2418): elementary<2418> elc_naviframe.c:2939 elm_naviframe_item_push() naviframe item push
04-27 11:07:53.609+0700 E/EFL     ( 2418): elementary<2418> elc_naviframe.c:2950 elm_naviframe_item_push() item(0xb90e0560) will be pushed
04-27 11:07:53.609+0700 E/EFL     ( 2418): <2418> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:53.609+0700 E/EFL     ( 2418): <2418> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:53.619+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb90ff340 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:53.619+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb90ff340 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:53.629+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb90ff340 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:53.629+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb90ff340 : elm_genlist] cw(360), ch(245), pw(360), ph(360)
04-27 11:07:53.649+0700 E/EFL     ( 2418): elementary<2418> elc_naviframe.c:2796 _push_transition_cb() item(0xb90e0560) will transition
04-27 11:07:53.769+0700 I/APP_CORE( 1251): appcore-efl.c: __do_app(453) > [APP 1251] Event: MEM_FLUSH State: PAUSED
04-27 11:07:54.069+0700 E/EFL     ( 2418): elementary<2418> elc_naviframe.c:1193 _on_item_push_finished() item(0xb907c9a0) transition finished
04-27 11:07:54.069+0700 E/EFL     ( 2418): elementary<2418> elc_naviframe.c:1218 _on_item_show_finished() item(0xb90e0560) transition finished
04-27 11:07:54.099+0700 E/EFL     ( 2418): <2418> elm_image.c:1269 elm_image_add() safety check failed: parent == NULL
04-27 11:07:54.599+0700 E/EFL     (  893): ecore_x<893> ecore_x_netwm.c:1520 ecore_x_netwm_ping_send() Send ECORE_X_ATOM_NET_WM_PING to client win:0x3200002 time=186387
04-27 11:07:54.599+0700 E/EFL     ( 2418): ecore_x<2418> ecore_x_events.c:1958 _ecore_x_event_handle_client_message() Received ECORE_X_ATOM_NET_WM_PING, so send pong to root time=186387
04-27 11:07:54.599+0700 E/EFL     (  893): ecore_x<893> ecore_x_events.c:1964 _ecore_x_event_handle_client_message() Received pong ECORE_X_ATOM_NET_WM_PING from client time=186387
04-27 11:07:55.659+0700 I/CAPI_NETWORK_CONNECTION( 2418): connection.c: connection_create(453) > New handle created[0xb90bde58]
04-27 11:07:55.759+0700 E/NOTI    ( 2418): 67026
04-27 11:07:55.759+0700 E/NOTIF ID( 2418): 40198
04-27 11:07:55.759+0700 E/problem ( 2418): Dirty
04-27 11:07:55.759+0700 E/problem_desc( 2418): 19A-Fender Lh Fender Dirty & Stain
04-27 11:07:55.769+0700 E/NOTIF ID( 2418): 40199
04-27 11:07:55.769+0700 E/problem ( 2418): Dirty
04-27 11:07:55.769+0700 E/problem_desc( 2418): 19A-Fender Rh Fender Dirty & Stain
04-27 11:07:55.779+0700 E/BODY NO ( 2418): 67026
04-27 11:07:55.779+0700 E/JSON PARSING( 2418): �$��'��
04-27 11:07:55.809+0700 I/AUL     ( 1146): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-27 11:07:55.809+0700 I/AUL     ( 1146): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/data-provider-master, ret : 0
04-27 11:07:55.849+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:07:55.859+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:07:55.859+0700 E/wnoti-service( 1443): wnoti-native-client.c: _receive_notification_changed_cb(1423) > [#1] priv_id : 1081, op_type : 1  //insert = 1, update = 2, delete = 3
04-27 11:07:55.869+0700 E/wnoti-service( 1443): wnoti-db-utility.c: lock_pm(601) > >> lock_pm status : 2
04-27 11:07:55.869+0700 E/wnoti-service( 1443): wnoti-native-client.c: _receive_notification_changed_cb(1552) > category : 100000
04-27 11:07:55.869+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 0, t_image_path: (null), err: 0
04-27 11:07:55.869+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 7, t_image_path: (null), err: 0
04-27 11:07:55.869+0700 E/wnoti-service( 1443): wnoti-native-client.c: _copy_mobile_noti_image_to_wearable(467) > type: 6, t_image_path: (null), err: 0
04-27 11:07:55.879+0700 E/wnoti-service( 1443): wnoti-native-client.c: _insert_notification(1208) > fail to get metadata_value, ret : -2
04-27 11:07:55.889+0700 E/wnoti-service( 1443): wnoti-db-server.c: wnoti_update_panel(2712) > id : -1016, source : 1
04-27 11:07:55.889+0700 E/wnoti-service( 1443): wnoti-native-client.c: _insert_notification(1346) > wearoff : 1, always_wear : 0, block_app_disable : 0, consider_noti_onoff: 0
04-27 11:07:55.889+0700 E/wnoti-service( 1443): wnoti-native-client.c: __insert_notification(727) > category :100000, type : 0, view_type 1, feedback : 2, identifier :wnoti_mobile:1081, application_id: -1016
04-27 11:07:55.899+0700 I/CAPI_NETWORK_CONNECTION( 2418): connection.c: connection_create(453) > New handle created[0xb90bdac0]
04-27 11:07:55.899+0700 E/wnoti-service( 1443): wnoti-db-server.c: _wnoti_update_category(861) > Reuse category, application_id : -1016
04-27 11:07:55.909+0700 E/wnoti-service( 1443): wnoti-db-server.c: set_global_noti_count(1608) > pre_count : 0, count : 1
04-27 11:07:55.919+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:07:55.929+0700 I/AUL     ( 1443): menu_db_util.h: _get_app_info_from_db_by_apppath(239) > path : /usr/bin/wnoti-service2, ret : 0
04-27 11:07:55.939+0700 E/APPS    ( 1251): AppsBadge.cpp: onBadgeChange(214) >  (!pAppsItem) -> onBadgeChange() return
04-27 11:07:55.949+0700 I/wnoti-service( 1443): wnoti-sap-client.c: launch_alert_view(421) > timer_id : 0, emergency_cb_mode : 0, blocking_mode : 0  
04-27 11:07:55.949+0700 E/wnoti-service( 1443): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 1
04-27 11:07:55.949+0700 E/wnoti-service( 1443): wnoti-msg-builder.c: _publish_notification(1576) > operation_type : 0, source : 1, application_id : -1016, display_count : 1, 
04-27 11:07:55.949+0700 E/wnoti-service( 1443): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 1
04-27 11:07:55.949+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:07:55.949+0700 E/wnoti-proxy( 1877): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1877, caller_id : 0, listener_type : 0
04-27 11:07:55.949+0700 E/wnoti-proxy( 1251): wnoti-client.c: on_wnoti_event(1029) > !!!!!!!!!! on_noti_handle_cb : noti is comming ............. !!, pid : 1251, caller_id : 0, listener_type : 0
04-27 11:07:55.959+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: 1877
04-27 11:07:55.959+0700 E/wnoti-service( 1443): wnoti-sap-client.c: on_timer(275) > is_popup_running: 1, ret: 0
04-27 11:07:55.959+0700 E/wnoti-service( 1443): wnoti-sap-client.c: on_timer(303) > is_exist_alert_list : 0, g_launch_popup_time : 1524802075, g_use_aul_launch : 1524802075
04-27 11:07:55.979+0700 E/WMS     ( 1008): wms_event_handler.c: _wms_event_handler_cb_nomove_detector(23573) > _wms_event_handler_cb_nomove_detector is called
04-27 11:07:56.009+0700 I/CAPI_NETWORK_CONNECTION( 2418): connection.c: connection_destroy(471) > Destroy handle: 0xb90bdac0
04-27 11:07:56.009+0700 I/CAPI_NETWORK_CONNECTION( 2418): connection.c: connection_destroy(471) > Destroy handle: 0xb90bde58
04-27 11:07:56.099+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_service_changed_cb(243) > Handle this change type in idler.
04-27 11:07:56.099+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(799) > Change type : 0, op_type: 1, category_id: -1016, display count: 1
04-27 11:07:56.099+0700 I/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(833) > Handle this change type in idler.
04-27 11:07:56.099+0700 W/wnotib  ( 1251): w-notification-board-noti-manager.c: _wnb_nm_data_changed_cb(837) > Postpone notiboard update.
04-27 11:07:56.109+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1161) > !!!!! application_id : -1016, db_id : 1141, is_duplicated : 0
04-27 11:07:56.109+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1311) > view_type : 1, turn_screen_on : 1, allow_gesture : 1, is_used_popup : 0, feedback : 2
04-27 11:07:56.119+0700 E/wnoti-proxy( 1877): wnoti-client.c: _wnoti_parse_extra_sub(373) > JSON_IS_NULL
04-27 11:07:56.119+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(226) > application_name: Real Feed Back, application_id: -1016, category_id: 114, time: 1524802075, launch_app_id: (null), bg_image: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, extracted_icon_color: -3139560, disble_block_app_action: 0, support_large_icon 0
04-27 11:07:56.119+0700 I/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(236) > noti_type: 1
04-27 11:07:56.119+0700 I/wnotibp ( 1877): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(60) > db_id: 1141, noti_type: 1
04-27 11:07:56.119+0700 I/wnotibp ( 1877): w-notification-board-noti-manager-common.c: wnb_nm_fill_common_detail_fields(86) > is_source_companion: 0, content_id: 0, notification_id: 0
04-27 11:07:56.119+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_convert_alert_data(67) > alert_type: 4, app_feedback_type: 2, popup_view_style: 0, feedback_pattern_app: -1
04-27 11:07:56.119+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 0, new_list count : 1
04-27 11:07:56.119+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_notification_change_cb(600) > [1,1141]
04-27 11:07:56.119+0700 W/AUL_AMD (  929): amd_request.c: __request_handler(669) > __request_handler: 14
04-27 11:07:56.119+0700 W/AUL_AMD (  929): amd_request.c: __send_result_to_client(91) > __send_result_to_client, pid: -1
04-27 11:07:56.119+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: _common_app_id_from_win(152) > 2418
04-27 11:07:56.139+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_check_env_condition(437) > focus app is com.toyota.realtimefeedback, 0
04-27 11:07:56.139+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_add_notification(991) > Add noti_queue [1141, 0]
04-27 11:07:56.139+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(73) > ::APP:: CHECK STATE : 8, 0, (null)
04-27 11:07:56.139+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:07:56.139+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_popup_draw_idler_cb(79) > ::APP:: CHECK DATA : 1 1 0000
04-27 11:07:56.139+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:07:56.149+0700 W/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_focus_app(136) > fg_app : 1
04-27 11:07:56.149+0700 I/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_active_app_id(1010) > [2418]
04-27 11:07:56.169+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_get_view_lock_state(666) > ::UI:: lock state=0000
04-27 11:07:56.169+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1178) > [1141, 1, 0, 4, 0000]
04-27 11:07:56.169+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1179) > [0, 1, 0]
04-27 11:07:56.169+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_draw_noti_view(1180) > [1, 0, 0, 0]
04-27 11:07:56.169+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(588) > ::UI:: [[[ ===> [small popup] is LOCK, 0010 ]]]
04-27 11:07:56.169+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_detail_layout(3707) > wnotiboard_popup_vi_type: 2
04-27 11:07:56.169+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_detail_layout(3712) > (1141, 1141)
04-27 11:07:56.169+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_first_action_info(6221) > No need to add default actions for companion noti.
04-27 11:07:56.189+0700 I/efl-extension( 1877): efl_extension_circle_surface.c: eext_circle_surface_layout_add(1290) > Put the surface[0xb0b1a0f0]'s widget[0xaf810a28] to layout widget[0xaf8102d0]
04-27 11:07:56.199+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_genlist(3639) > (1141, 1141)
04-27 11:07:56.199+0700 I/efl-extension( 1877): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:07:56.219+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: _view_create_card_data(3073) > 0xb28f0bf8, 0xb28f0a80, 0xb28f0a80
04-27 11:07:56.219+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xaf86f5e8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:56.219+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xaf86f5e8 : elm_genlist] cw(0), ch(0), pw(360), ph(360)
04-27 11:07:56.239+0700 E/EFL     ( 1877): elementary<1877> elm_genlist.c:7236 elm_genlist_item_item_class_get() Elm_Widget_Item ((Elm_Widget_Item *)item) is NULL
04-27 11:07:56.239+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:07:56.239+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:07:56.239+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:07:56.239+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:07:56.239+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2606) > Unhandled part: stack.separator
04-27 11:07:56.239+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 11:07:56.239+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.239+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.239+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.239+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.239+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.239+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.239+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.239+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.239+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.249+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-27 11:07:56.249+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.249+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.249+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.249+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.249+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.249+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.249+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.249+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.249+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.249+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:56.259+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:56.259+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 11:07:56.259+0700 E/EFL     ( 1877): elementary<1877> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-27 11:07:56.259+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(224) > can't get layout
04-27 11:07:56.259+0700 E/EFL     ( 1877): elementary<1877> elm_widget.c:5761 _elm_widget_item_edje_get() Elm_Widget_Item item is NULL
04-27 11:07:56.259+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:56.259+0700 I/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1141, 114, -1016.
04-27 11:07:56.279+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value_int(463) > Enter [system_settings_get_value_int]
04-27 11:07:56.279+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_value(386) > Enter [system_settings_get_value]
04-27 11:07:56.279+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(361) > Enter [system_settings_get_item], key=3
04-27 11:07:56.279+0700 E/TIZEN_N_SYSTEM_SETTINGS( 1877): system_settings.c: system_settings_get_item(374) > Enter [system_settings_get_item], index = 3, key = 3, type = 1
04-27 11:07:56.289+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1440 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:07:56.289+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1445 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:07:56.289+0700 E/EFL     ( 1877): evas_main<1877> evas_object_main.c:1450 evas_object_color_set() Evas only handles pre multiplied colors!
04-27 11:07:56.289+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 58, height: 58
04-27 11:07:56.289+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.289+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.289+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.289+0700 I/wnotibp ( 1877): w-notification-board-common.c: wnb_common_get_application_icon(1744) > application_id: -1016, thumbnail path: /opt/usr/apps/com.toyota.realtimefeedback/shared/res/realtimefeedback1.png, summary_large_image path: (null), width: 130, height: 130
04-27 11:07:56.289+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.289+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.289+0700 F/EFL     ( 1877): evas_main<1877> main.c:122 evas_debug_magic_wrong() Input object is wrong type
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Expected: 747ad76c - Evas_Object (Image)
04-27 11:07:56.289+0700 F/EFL     ( 1877):     Supplied: 78c7c73f - Evas_Object (Smart)
04-27 11:07:56.299+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:56.299+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:56.299+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6428) > Number of pages: 1 for -1016
04-27 11:07:56.299+0700 W/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_inline_action_object_get(219) > reduce inline action button
04-27 11:07:56.299+0700 I/wnotibp ( 1877): w-notification-board-action.c: wnb_action_get_item_info(6368) > No need to add default actions for companion noti.
04-27 11:07:56.299+0700 I/wnotibp ( 1877): w-notification-board-basic-panel-common.c: _wnb_bp_expand_basic_gl_group_content_get(2592) > Action is not available for this noti 1141, 114, -1016.
04-27 11:07:56.299+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xaf86f5e8 : elm_genlist] mx(0), my(0), minx(0), miny(0), px(0), py(0)
04-27 11:07:56.309+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xaf86f5e8 : elm_genlist] cw(360), ch(360), pw(360), ph(360)
04-27 11:07:56.319+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xaf86f5e8 : elm_genlist] mx(0), my(52), minx(0), miny(0), px(0), py(0)
04-27 11:07:56.319+0700 E/EFL     ( 1877): elementary<1877> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xaf86f5e8 : elm_genlist] cw(360), ch(412), pw(360), ph(360)
04-27 11:07:56.329+0700 I/wnotibp ( 1877): wnotiboard-popup-view.c: wnbp_view_draw_small_view(4060) > ::UI:: window type is changed by unknown causes
04-27 11:07:56.359+0700 W/APP_CORE( 1877): appcore-efl.c: __show_cb(860) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:3000009
04-27 11:07:56.399+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2809) > ::UI:: VI TYPE : 2
04-27 11:07:56.399+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:07:56.399+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_vi_setup_idler_cb(2816) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-27 11:07:56.399+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:07:56.399+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(284) > alert_type : 4, is_source_companion: 0, is_do_not_disturb: 0
04-27 11:07:56.399+0700 W/TIZEN_N_RECORDER( 1877): recorder_product.c: recorder_is_in_recording(82) > pid =/proc/0 , state =0, alive=0
04-27 11:07:56.399+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(298) > ::APP:: application_id: -1016, is_disaster: 0, disaster_info: (null), app_feedback_type: 2, feedback_pattern_app: -1, is_recording: 0
04-27 11:07:56.399+0700 I/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(307) > Gear side feedback setting is_sound_on: 0, is_vibration_on: 1, is_vibrate_when_noti_on: 0
04-27 11:07:56.399+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_play_feedback(341) > ::APP:: Determined feedback: sound 0, vibration: 1
04-27 11:07:56.399+0700 I/wnotib  ( 1877): w-notification-board-common-logging.c: wnotib_common_send_logging_data(163) > Log for type: 13, ret: -3, request_id: 0
04-27 11:07:56.399+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_set_smart_relay(501) > Set the smart relay for 0, 114, -1016, Real Feed Back
04-27 11:07:56.399+0700 I/wnotibp ( 1877): wnotiboard-popup-common.c: wnbp_common_get_block_mode(531) > Returning false.
04-27 11:07:56.399+0700 W/wnotibp ( 1877): wnotiboard-popup-control.c: _ctrl_turn_on_lcd(534) > [4, 0, 1, 0]
04-27 11:07:56.419+0700 W/STARTER ( 1141): pkg-monitor.c: _proc_mgr_status_cb(455) > [_proc_mgr_status_cb:455] >> P[1877] goes to (3)
04-27 11:07:56.419+0700 W/AUL_AMD (  929): amd_key.c: _key_ungrab(254) > fail(-1) to ungrab key(XF86Stop)
04-27 11:07:56.419+0700 W/AUL_AMD (  929): amd_launch.c: __e17_status_handler(2391) > back key ungrab error
04-27 11:07:56.419+0700 W/AUL     (  929): app_signal.c: aul_send_app_status_change_signal(686) > aul_send_app_status_change_signal app(com.samsung.wnotiboard-popup) pid(1877) status(fg) type(uiapp)
04-27 11:07:56.439+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(263) > fully_obscured: 0, 0
04-27 11:07:56.439+0700 I/wnotibp ( 1877): wnotiboard-popup-manager.c: _mgr_x_event_visibility_changed_cb(264) > [0x3000009 0x300000d 0x3000009]
04-27 11:07:56.449+0700 I/APP_CORE( 1877): appcore-efl.c: __do_app(453) > [APP 1877] Event: RESUME State: PAUSED
04-27 11:07:56.449+0700 I/CAPI_APPFW_APPLICATION( 1877): app_main.c: app_appcore_resume(223) > app_appcore_resume
04-27 11:07:56.449+0700 I/wnotibp ( 1877): wnotiboard-popup.c: _popup_app_resume(229) > 
04-27 11:07:56.449+0700 I/GATE    ( 1877): <GATE-M>APP_FULLY_LOADED_wnotiboard-popup</GATE-M>
04-27 11:07:56.449+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_lock(585) > ::UI:: [[[ ===> already [small popup] is LOCK, 0010 ]]]
04-27 11:07:56.449+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_sub_popup_show_animator_pre_cb(1781) > ::UI:: start showing animation
04-27 11:07:56.539+0700 E/wnoti-service( 1443): wnoti-db-client.c: wnoti_get_alert_categories(1016) > _query_step failed(NO ROW)
04-27 11:07:56.539+0700 E/wnoti-service( 1443): wnoti-db-utility.c: set_pm_lock(585) > >> set_pm_lock status : 2
04-27 11:07:56.549+0700 E/wnoti-service( 1443): wnoti-db-utility.c: unlock_pm(616) > >> unlock_pm status : 0
04-27 11:07:56.549+0700 E/wnoti-proxy( 1877): wnoti.c: _wnoti_get_categories(1276) > failed: GDBus.Error:org.freedesktop.DBus.Error.Failed: Empty List
04-27 11:07:56.549+0700 E/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(311) > ::DATA:: No categories available.
04-27 11:07:56.549+0700 W/wnotibp ( 1877): wnotiboard-popup-data.c: _data_get_alert_list(314) > ::DATA:: read alert list : 1, new_list count : 0
04-27 11:07:56.749+0700 W/wnotibp ( 1877): wnotiboard-popup-view.c: _view_sub_popup_show_animator_cb(1684) > ::UI:: end show animation
04-27 11:07:56.749+0700 W/wnotibp ( 1877): wnotiboard-popup-manager.c: wnbp_mgr_view_unlock(638) > ::UI:: [[[ [small popup] is UNLOCK , 0000 <=== ]]]
04-27 11:07:57.989+0700 E/EFL     ( 2418): ecore_x<2418> ecore_x_events.c:563 _ecore_x_event_handle_button_press() ButtonEvent:press time=190742 button=1
04-27 11:07:57.989+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] mouse move
04-27 11:07:58.009+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] mouse move
04-27 11:07:58.009+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] hold(0), freeze(0)
04-27 11:07:58.019+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] mouse move
04-27 11:07:58.019+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] hold(0), freeze(0)
04-27 11:07:58.029+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] mouse move
04-27 11:07:58.029+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] hold(0), freeze(0)
04-27 11:07:58.059+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] mouse move
04-27 11:07:58.059+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] hold(0), freeze(0)
04-27 11:07:58.069+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] mouse move
04-27 11:07:58.069+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] hold(0), freeze(0)
04-27 11:07:58.109+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] mouse move
04-27 11:07:58.109+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] hold(0), freeze(0)
04-27 11:07:58.179+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4002 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] mouse move
04-27 11:07:58.179+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:4166 _elm_scroll_mouse_move_event_cb() [0xb90ff340 : elm_genlist] hold(0), freeze(0)
04-27 11:07:58.209+0700 E/EFL     ( 2418): ecore_x<2418> ecore_x_events.c:722 _ecore_x_event_handle_button_release() ButtonEvent:release time=190959 button=1
04-27 11:07:58.229+0700 I/efl-extension( 2418): efl_extension_circle_surface.c: _eext_circle_surface_show_cb(740) > Surface will be initialized!
04-27 11:07:58.229+0700 I/efl-extension( 2418): efl_extension_rotary.c: eext_rotary_object_event_callback_add(147) > In
04-27 11:07:58.229+0700 I/efl-extension( 2418): efl_extension_rotary.c: eext_rotary_object_event_activated_set(283) > eext_rotary_object_event_activated_set : 0xb92d9570, elm_image, _activated_obj : 0xb90e00f8, activated : 1
04-27 11:07:58.229+0700 I/efl-extension( 2418): efl_extension_rotary.c: eext_rotary_object_event_activated_set(291) > Activation delete!!!!
04-27 11:07:58.239+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1893 _elm_scroll_content_region_show_internal() [0xb92d7810 : elm_scroller] mx(300), my(86), minx(0), miny(0), px(0), py(0)
04-27 11:07:58.239+0700 E/EFL     ( 2418): elementary<2418> elm_interface_scrollable.c:1894 _elm_scroll_content_region_show_internal() [0xb92d7810 : elm_scroller] cw(300), ch(86), pw(0), ph(0)
04-27 11:07:58.339+0700 W/CRASH_MANAGER( 2399): worker.c: worker_job(1205) > 1102418726561152480207
