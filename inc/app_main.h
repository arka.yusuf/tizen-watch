#ifndef __APP_MAIN_H__
#define __APP_MAIN_H__

#define ENABLE_UIB_DELOG

#include "uib_GLChoose_view.h"
#include "uib_NotifView_view.h"
#include "uib_NotifDetail_view.h"
#include "Func.h"
#include "HttpReq.h"
#include <ecore/Ecore.h>
#include "sql.h"
#include "GLHttp.h"

/**
 * @brief Forward declaration of model
 */
typedef struct {

} app_data;

extern int notifCount;
extern int ondetail;
extern uib_GLChoose_view_context *glvc;
extern uib_NotifView_view_context *NVvc;
extern uib_NotifDetail_view_context *NDvc;
uib_GLChoose_view_context getGLvc();

void loadAllNotification();
void loadAllBodyDefect();
static void selectBodyNo(char *data, Evas_Object *obj, void *event_info);
void loadServerNotification();
void set_GLvc(uib_GLChoose_view_context *vc);
int setget_NotifCount(int count);
void set_NVvc(uib_NotifView_view_context *vc);
void set_NDvc(uib_NotifDetail_view_context *vc);

/**< H/W Back Key Event */
/**
 * @brief Add (register) a callback function for H/W Back Key Event to a given evas object.
 * @param[in]   pv_param     The void pointer to be passed to this func.
 * @param[in]   p_evas_obj    evas object (naviframe)
 */
void
nf_hw_back_cb(void* param, Evas_Object * evas_obj, void* event_info);

void
win_del_request_cb(void *data, Evas_Object *obj, void *event_info);

Eina_Bool
nf_root_it_pop_cb(void* elm_win, Elm_Object_Item *it);

/**
 * @brief Create application instance
 * @return Application instance on success, otherwise NULL
 */
app_data *uib_app_create();

/**
 * @brief Destroy application instance
 * @param[in]   app     Application instance
 */
void uib_app_destroy(app_data *app);

/**
 * @brief Run Tizen application
 * @param[in]   app     Application instance
 * @param[in]   argc    argc paremeter received in main
 * @param[in]   argv    argv parameter received in main
 */
int uib_app_run(app_data *app, int argc, char **argv);

void app_get_resource(const char *edj_file_in, char *edj_path_out, int edj_path_max);

#endif /* __APP_MAIN_H__ */

