/*
 * HttpReq.h
 *
 *  Created on: Feb 8, 2018
 *      Author: Hartaji
 */

#ifndef HTTPREQ_H_
#define HTTPREQ_H_


#include <curl/curl.h>
#include <curl/easy.h>
#include <ecore/Ecore.h>
#include <net_connection.h>
#include <glib-object.h>
#include <json-glib/json-glib.h>
#include "Func.h"
#include "sql.h"

void setURL(char *url);
void HTTPReq();
void setURLResponseRead(char *url);

Ecore_Thread_Cb threadMain(Ecore_Thread *thread);
void jsonParsing(char *data, char *result[100000]);

void CurlRequestHTTP();
void endCurlConnection(connection_h connection,CURL *curl);

void endReq(void *data, Ecore_Thread *thread);
void cancelReq(void *data, Ecore_Thread *thread);

#endif /* HTTPREQ_H_ */
