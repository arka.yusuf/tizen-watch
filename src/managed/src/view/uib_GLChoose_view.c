
/*******************************************************************************
* This file was generated by UI Builder.
* This file will be auto-generated each and everytime you save your project.
* Do not hand edit this file.
********************************************************************************/
		#include "g_inc_uib.h"
#include "uib_views.h"
#include "uib_views_inc.h"
#include "uib_app_manager.h"


/* event handler declarations */
void GLChoose_onuib_view_create(uib_GLChoose_view_context*, Evas_Object*, void*);
void connection_GLChoose_GLList_onselected(uib_GLChoose_view_context*, Evas_Object*, void*);

uib_view_context* uib_view_GLChoose_create(Evas_Object* parent, void* create_callback_param) {
	uib_GLChoose_view_context* vc = calloc(1, sizeof(uib_GLChoose_view_context));
	vc->parent = parent;
	vc->view_name = "GLChoose";
	vc->indicator_state = ELM_WIN_INDICATOR_SHOW;
	vc->is_user_view = false;
	uib_app_manager_get_instance()->add_view_context((uib_view_context*)vc);
	if (!vc->grid2) {
		vc->grid2= elm_grid_add(parent);
		vc->root_container = vc->grid2;
	}
	uib_views_get_instance()->set_targeted_view((uib_view_context*)vc);

	//bind event handler
	evas_object_smart_callback_add(vc->root_container, "uib,view,create", (Evas_Smart_Cb)GLChoose_onuib_view_create, vc);
	evas_object_smart_callback_add(vc->GLList, "selected", (Evas_Smart_Cb)connection_GLChoose_GLList_onselected, vc);



	evas_object_data_set(vc->root_container, KEY_VIEW_CONTEXT, vc);
	uib_views_create_callback(vc, evas_object_evas_get(vc->root_container), vc->root_container, create_callback_param);
	evas_object_event_callback_add(vc->root_container, EVAS_CALLBACK_DEL, (Evas_Object_Event_Cb)uib_views_destroy_callback, vc);

	return (uib_view_context*)vc;
}
void uib_GLChoose_config_CIRCLE_360x360_portrait() {
	uib_app_manager_st* uib_app_manager = uib_app_manager_get_instance();
	uib_GLChoose_view_context* vc = (uib_GLChoose_view_context*)uib_app_manager->find_view_context("GLChoose");
	if(!vc) {
		return;
	}
	if(vc->grid2) {
		elm_grid_clear(vc->grid2, EINA_FALSE);
		evas_object_size_hint_align_set(vc->grid2, -1.0, -1.0);		evas_object_size_hint_weight_set(vc->grid2, .0, .0);		elm_grid_size_set(vc->grid2, 1000, 1000);
		if (!vc->progressbar1) {
			vc->progressbar1 = eext_circle_object_progressbar_add(vc->grid2, uib_views_get_instance()->get_window_obj()->circle_surface);
		}
		if (vc->progressbar1) {
			eext_rotary_object_event_activated_set(vc->progressbar1, EINA_TRUE);
			evas_object_size_hint_align_set(vc->progressbar1, 0.5, 0.5);			evas_object_size_hint_weight_set(vc->progressbar1, .5, .5);			elm_object_style_set(vc->progressbar1,"default");
			evas_object_show(vc->progressbar1);
		}
		if (!vc->GLList) {
			vc->GLList= elm_genlist_add(vc->grid2);
		}
		if (!vc->circle_object_GLList) {
			vc->circle_object_GLList= eext_circle_object_genlist_add(vc->GLList, uib_views_get_instance()->get_window_obj()->circle_surface);
			eext_rotary_object_event_activated_set(vc->circle_object_GLList, EINA_TRUE);
		}
		if(vc->GLList) {
			evas_object_size_hint_align_set(vc->GLList, -1.0, -1.0);			evas_object_size_hint_weight_set(vc->GLList, .0, .0);			evas_object_hide(vc->GLList);
		}
		if (!vc->loadingLabel) {
			vc->loadingLabel = elm_label_add(vc->grid2);
		}
		if(vc->loadingLabel) {
			evas_object_size_hint_align_set(vc->loadingLabel, 0.5, 0.5);			evas_object_size_hint_weight_set(vc->loadingLabel, 1.0, 1.0);			elm_object_text_set(vc->loadingLabel,_UIB_LOCALE("<br><br><br><align=center><font_size=30><color=#90BDF0><b>loading</b><br>Group Leaders ..</color></font_size></align>"));
			elm_label_line_wrap_set(vc->loadingLabel, (Elm_Wrap_Type)ELM_WRAP_WORD);
			elm_label_wrap_width_set(vc->loadingLabel,300);
			elm_label_ellipsis_set(vc->loadingLabel, EINA_TRUE);
			evas_object_show(vc->loadingLabel);
		}
		elm_grid_pack(vc->grid2, vc->progressbar1, 0, 0, 1000, 1000);		elm_grid_pack(vc->grid2, vc->GLList, 0, 0, 1000, 1000);		elm_grid_pack(vc->grid2, vc->loadingLabel, 0, 0, 1000, 1000);		evas_object_show(vc->grid2);
	}
}

