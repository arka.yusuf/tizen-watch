/*
 * Sql.c
 *
 *  Created on: Feb 11, 2018
 *      Author: Hartaji
 */

#include <Sql.h>
#include <dlog.h>
#define DB_NAME "sample.db"
#define TABLE_NAME "DEFECTS"
#define COL_ID "ID"
#define COL_BODYNO "BODY_NO"
#define COL_NOTIFID "NOTIF_ID"
#define COL_PROBLEM "PROBLEM"
#define COL_PROBLEM_DESCRIPTION "PROBLEM_DESCRIPTION"

#define BUFLEN 1000

sqlite3 *notif; /*name of database*/
int select_row_count = 0;
int select_row_count2 = 0;
int select_row_count3 = 0;
NotifData *qrydata;
BodyData *qrydata2;

char *returnData;

int opendb()
{
     char * dataPath = app_get_data_path(); /*fetched package path available physically in the device*/
	 int size = strlen(dataPath)+10;

	 char * path = malloc(sizeof(char)*size);

	 strcpy(path,dataPath);
	 strncat(path, DB_NAME, size);

	 //DBG("DB Path = [%s]", path); /*prepared full path, database will be stored there*/

	 int ret = sqlite3_open_v2( path , &notif, SQLITE_OPEN_CREATE|SQLITE_OPEN_READWRITE, NULL);
	 if(ret != SQLITE_OK){
		//ERR("DB Create Error! [%s]", sqlite3_errmsg(sampleDb));
	 }
	 free(dataPath);
	 free(path);

	 return ret;
}

int initdb()
{
	if (opendb() != SQLITE_OK) /*create database instance*/
		return SQLITE_ERROR;

	returnData=malloc(0);

   int ret;
   char *ErrMsg;
   char *sql = "CREATE TABLE IF NOT EXISTS "\
     		    TABLE_NAME" ("  \
     			COL_BODYNO" TEXT NOT NULL, "
  			COL_NOTIFID" TEXT NOT NULL, "
  			COL_PROBLEM" TEXT NOT NULL, "
  			COL_PROBLEM_DESCRIPTION" TEXT NOT NULL, "
     			COL_ID" INTEGER PRIMARY KEY AUTOINCREMENT);"; /*id autoincrement*/



   ret = sqlite3_exec(notif, sql, NULL, 0, &ErrMsg); /*execute query*/
   if(ret != SQLITE_OK)
   {
	   //ERR("Table Create Error! [%s]", ErrMsg);
	   sqlite3_free(ErrMsg);
	   sqlite3_close(notif); /*close db instance as instance is still open*/

	   return SQLITE_ERROR;
   }

   sqlite3_close(notif); /*close the db instance as operation is done here*/

   return SQLITE_OK;
}

int Insert(char * bodyno,char * notifid,char * problem,char * problem_description)
{
	if(opendb() != SQLITE_OK) /*create database instance*/
		return SQLITE_ERROR;

	char sqlbuff[BUFLEN];
	char *ErrMsg;
	int ret;


        /*prepare query for INSERT operation*/
	snprintf(sqlbuff, BUFLEN, "INSERT INTO "\
			TABLE_NAME" VALUES(\'%s'\,\'%s'\,\'%s'\,\'%s'\, NULL);",
	            		bodyno, notifid, problem, problem_description);

	ret = sqlite3_exec(notif, sqlbuff, NULL, 0, &ErrMsg); /*execute query*/
	if (ret != SQLITE_OK)
	{
	   //ERR("Insertion Error! [%s]", sqlite3_errmsg(sampleDb));
	   sqlite3_free(ErrMsg);
	   sqlite3_close(notif); /*close db instance for failed case*/

	   return SQLITE_ERROR;
	}

	sqlite3_close(notif); /*close db instance for success case*/

	return SQLITE_OK;
}

static int selectAllItem(void *data, int argc, char **argv, char **azColName){
	NotifData *temp = (NotifData*)realloc(qrydata, ((select_row_count + 1) * sizeof(NotifData)));
	if(temp == NULL){
		return SQLITE_ERROR;
	} else {
		/*//for (int i = 0; i < argc; i++){
			size_t  mallocated=0;
			if(select_row_count==0){
				mallocated=(strlen(argv[0])+1)*sizeof(* argv[0]);
				returnData=realloc(returnData,mallocated);
				snprintf(returnData,mallocated , "%s", argv[0]);
			}else{
				mallocated=(strlen(argv[0])+1+strlen(returnData))*sizeof(char);
				returnData=(char *)realloc(returnData,mallocated);
				if(returnData){
					snprintf(returnData,mallocated , "%s%s", argv[0],returnData);
				}
			}
		    //temp[select_row_count].id = atoi(argv[1]);
		    //temp[select_row_count].StringHtml = atoi(argv[0]);
		    //dlog_print(DLOG_DEBUG, "SQLC", "%s = %s | ", azColName[i], argv[i] ? argv[i] : "NULL");
		//}
		*/
		char *BODY_NO = malloc(100000);
		char *NOTIF_ID = malloc(100000);
		char *PROBLEM = malloc(100000);
		char *PROBLEM_DESCRIPTION = malloc(100000);

		snprintf(BODY_NO,100000, "<align=0.5 0.5><font color=#5494f9><b>%s</b></font></align>", argv[1]);
		snprintf(PROBLEM,100000, "<align=0.5 0.5>%s</align>", argv[3]);
		snprintf(PROBLEM_DESCRIPTION,100000, "<align=0.5 0.5>%s</align>", argv[4]);
		temp[select_row_count].BODY_NO = BODY_NO;
		temp[select_row_count].PROBLEM = PROBLEM;
		temp[select_row_count].PROBLEM_DESCRIPTION = PROBLEM_DESCRIPTION;
		qrydata = temp;
	}

	select_row_count ++; /*keep row count*/

   return SQLITE_OK;
}

static int selectAllBody(void *data, int argc, char **argv, char **azColName){
	BodyData *temp = (BodyData*)realloc(qrydata2, ((select_row_count2 + 1) * sizeof(BodyData)));
	if(temp == NULL){
		return SQLITE_ERROR;
	} else {
		size_t  mallocated=0;
		char *BODY_NO = malloc(100000);
		snprintf(BODY_NO,100000, "%s", argv[1]);
		temp[select_row_count2].count = atoi(argv[0]);
		temp[select_row_count2].BODY_NO = BODY_NO;
		qrydata2 = temp;
	}
	select_row_count2 ++;
	return SQLITE_OK;
}


static int selectAllItemCount(void *data, int argc, char **argv, char **azColName){
	int *count = 0;

		count = atoi(argv[0]);

	select_row_count3 = count;
   return SQLITE_OK;
}


int getAllNotifCount(int* num_of_rows)
{
	if(opendb() != SQLITE_OK) /*create database instance*/
		return SQLITE_ERROR;
	char *sql =malloc(100000);
	sprintf(sql, "SELECT COUNT(ID) as count FROM DEFECTS");
	int ret;
	char *ErrMsg;
	select_row_count = 0;

    ret = sqlite3_exec(notif, sql, selectAllItemCount, NULL, &ErrMsg);
	if (ret != SQLITE_OK)
	{
	   //DBG("select query execution error [%s]", ErrMsg);
	   sqlite3_free(ErrMsg);
	   sqlite3_close(notif);

	   return SQLITE_ERROR;
	}

    *num_of_rows = select_row_count3;

	sqlite3_close(notif);

   return SQLITE_OK;
}


int getAllNotif(NotifData **notifsD, int* num_of_rows, char *body_no)
{
	if(opendb() != SQLITE_OK) /*create database instance*/
		return SQLITE_ERROR;

	qrydata = (NotifData *) calloc (1, sizeof(NotifData));

	char *sql =malloc(100000);
	sprintf(sql, "SELECT ID, BODY_NO, NOTIF_ID, PROBLEM, PROBLEM_DESCRIPTION FROM DEFECTS WHERE BODY_NO = '%s'",body_no);
	int ret;
	char *ErrMsg;
	select_row_count = 0;

    ret = sqlite3_exec(notif, sql, selectAllItem, (void*)notifsD, &ErrMsg);
	if (ret != SQLITE_OK)
	{
	   //DBG("select query execution error [%s]", ErrMsg);
	   sqlite3_free(ErrMsg);
	   sqlite3_close(notif);

	   return SQLITE_ERROR;
	}

	*notifsD = qrydata;
    *num_of_rows = select_row_count;

	sqlite3_close(notif);

   return SQLITE_OK;
}


int getAllBodyDefect(BodyData **notifsD, int* num_of_rows)
{
	if(opendb() != SQLITE_OK) /*create database instance*/
		return SQLITE_ERROR;

	qrydata2 = (BodyData *) calloc (1, sizeof(BodyData));

	char *sql = "SELECT COUNT(BODY_NO) as count, BODY_NO FROM DEFECTS GROUP BY BODY_NO";
	int ret;
	char *ErrMsg;
	select_row_count2 = 0;

    ret = sqlite3_exec(notif, sql, selectAllBody, (void*)notifsD, &ErrMsg);
	if (ret != SQLITE_OK)
	{
	   //DBG("select query execution error [%s]", ErrMsg);
	   sqlite3_free(ErrMsg);
	   sqlite3_close(notif);

	   return SQLITE_ERROR;
	}

	*notifsD = qrydata2;
    *num_of_rows = select_row_count2;

	sqlite3_close(notif);

   return SQLITE_OK;
}





static int deletecb(void *data, int argc, char **argv, char **azColName){
   int i;
   for(i=0; i<argc; i++){
	/*no need to do anything*/
   }

   return 0;
}

int deleteALL(char *body_no)
{
	if(opendb() != SQLITE_OK) /*create database instance*/
		return SQLITE_ERROR;

   char *sql=malloc(100000);
   sprintf(sql, "DELETE FROM DEFECTS WHERE BODY_NO = '%s'",body_no);
   int counter = 0, ret = 0;
   char *ErrMsg;

   ret = sqlite3_exec(notif, sql, deletecb, &counter, &ErrMsg);
	if (ret != SQLITE_OK)
	{
		//ERR("Delete Error! [%s]", sqlite3_errmsg(sampleDb));
	   sqlite3_free(ErrMsg);
	   sqlite3_close(notif);

	   return SQLITE_ERROR;
	}

	sqlite3_close(notif);

   return SQLITE_OK;
}

char *getReturnDatas(){
	return returnData;
}
