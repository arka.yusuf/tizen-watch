#include "app_main.h"

#include "uib_app_manager.h"
#include <app_preference.h>


#include "g_inc_uib.h"
#include "uib_views.h"
#include "uib_views_inc.h"

uib_GLChoose_view_context *glvc;
uib_NotifView_view_context *NVvc;
uib_NotifDetail_view_context *NDvc;
int notifCount=0;
int ondetail=0;
char *selected_body_no;
/* app event callbacks */
static bool _on_create_cb(void *user_data);
static void _on_terminate_cb(void *user_data);
static void _on_app_control_cb(app_control_h app_control, void *user_data);
static void _on_resume_cb(void *user_data);
static void _on_pause_cb(void *user_data);
static void _on_low_memory_cb(app_event_info_h event_info, void *user_data);
static void _on_low_battery_cb(app_event_info_h event_info, void *user_data);
static void _on_device_orientation_cb(app_event_info_h event_info, void *user_data);
static void _on_language_changed_cb(app_event_info_h event_info, void *user_data);
static void _on_region_format_changed_cb(app_event_info_h event_info, void *user_data);


void set_GLvc(uib_GLChoose_view_context *vc){
	glvc=vc;
	NVvc=NULL;
	NDvc=NULL;
	//initGLList(vc,"http://192.168.4.100/rtf/gl.php");
	initGLList(vc,"http://arkamaya.net:1222/MasterData/getDataPic");
	ecore_thread_run(getAndSetGLList,NULL,NULL,NULL);

}


void set_NVvc(uib_NotifView_view_context *vc){
	NVvc=vc;
	glvc=NULL;
	NDvc=NULL;
	//start procedure to get notification from server
	loadServerNotification();
}

void set_NDvc(uib_NotifDetail_view_context *vc){
	NDvc = vc;
	glvc = NULL;
	NVvc = NULL;


}

int setget_NotifCount(int count){
	if(count>-1){
		notifCount=count;//set count defect when count has more then -1
	}else if(count==-2){
		notifCount--;//set count down -- (for silence notif mode only)
	}else if(count==-1){
		notifCount++;//set count up
	}
	return notifCount;
}

void
nf_hw_back_cb(void* param, Evas_Object * evas_obj, void* event_info) {
	//stopVibrate();
	//if(glvc == NULL && NVvc == NULL){

	loadAllBodyDefect();
	/*
	if(ondetail==1)
	{
		ondetail=0;
		evas_object_hide(NDvc->ScrollContainer);
		//uib_NotifView_view_context* vcx = calloc(1, sizeof(uib_NotifView_view_context));
		//evas_object_hide(NDvc->ScrollContainer);
		//loadAllBodyDefect();
		Evas_Object* app_nf = uib_views_get_instance()->get_window_obj()->app_naviframe;
		uib_view_NotifView_create(app_nf, NULL);



		//elm_naviframe_item_simple_push(app_nf, app_nf);
		//uib_view_NotifDetail_destroy(app_nf, NULL);
		elm_naviframe_item_pop(app_nf);
		elm_win_lower(app_nf);

		//Evas_Object *win = param;


			//elm_naviframe_item_pop(app_nf);
		    //elm_win_lower(app_nf);

	}*/
}

void
win_del_request_cb(void *data, Evas_Object *obj, void *event_info)
{
	ui_app_exit();
}

Eina_Bool
nf_root_it_pop_cb(void* elm_win, Elm_Object_Item *it) {
	elm_win_lower(elm_win);
	return EINA_FALSE;
}

app_data *uib_app_create()
{
	return calloc(1, sizeof(app_data));
}

void uib_app_destroy(app_data *user_data)
{
	uib_app_manager_get_instance()->free_all_view_context();
	free(user_data);
}

int uib_app_run(app_data *user_data, int argc, char **argv)
{
	ui_app_lifecycle_callback_s cbs =
	{
		.create = _on_create_cb,
		.terminate = _on_terminate_cb,
		.pause = _on_pause_cb,
		.resume = _on_resume_cb,
		.app_control = _on_app_control_cb,
	};

	app_event_handler_h handlers[5] =
	{NULL, };

	ui_app_add_event_handler(&handlers[APP_EVENT_LOW_BATTERY], APP_EVENT_LOW_BATTERY, _on_low_battery_cb, user_data);
	ui_app_add_event_handler(&handlers[APP_EVENT_LOW_MEMORY], APP_EVENT_LOW_MEMORY, _on_low_memory_cb, user_data);
	ui_app_add_event_handler(&handlers[APP_EVENT_DEVICE_ORIENTATION_CHANGED], APP_EVENT_DEVICE_ORIENTATION_CHANGED, _on_device_orientation_cb, user_data);
	ui_app_add_event_handler(&handlers[APP_EVENT_LANGUAGE_CHANGED], APP_EVENT_LANGUAGE_CHANGED, _on_language_changed_cb, user_data);
	ui_app_add_event_handler(&handlers[APP_EVENT_REGION_FORMAT_CHANGED], APP_EVENT_REGION_FORMAT_CHANGED, _on_region_format_changed_cb, user_data);

	return ui_app_main(argc, argv, &cbs, user_data);
}

void
app_get_resource(const char *res_file_in, char *res_path_out, int res_path_max)
{
	char *res_path = app_get_resource_path();
	if (res_path) {
		snprintf(res_path_out, res_path_max, "%s%s", res_path, res_file_in);
		free(res_path);
	}
}


static bool _on_create_cb(void *user_data)
{
	/*
	 * This area will be auto-generated when you add or delete user_view
	 * Please do not hand edit this area. The code may be lost.
	 */
	uib_app_manager_st* app_manager = uib_app_manager_get_instance();

	app_manager->initialize();
	/*
	 * End of area
	 */
	return true;
}


static void _on_terminate_cb(void *user_data)
{
	uib_views_get_instance()->destroy_window_obj();
}

static void _on_resume_cb(void *user_data)
{

	if(NVvc==NULL)return;
	//loadAllNotification();
	/*if(ondetail==1)
	{
		NDvc = NULL;
		ondetail=0;
		Evas_Object *evas_obj;
		evas_obj = uib_views_get_instance()->get_window_obj()->app_naviframe;
		elm_naviframe_item_pop(evas_obj);

	}
	loadAllBodyDefect();
	*/
	loadAllBodyDefect();
}

void loadAllNotification(char *body_no){

	//elm_object_text_set(NVvc->label1,"<br><br><align=0.5 0.5><color=#F27916><b>LOADING..</b></color><br><b>Retrieving Notifications</b></align>");

	NotifData *notD= (NotifData*) calloc (1, sizeof(NotifData));;
	int count=0;

	initdb();
	//if(inited==false){
		//if()
			//inited=true;
	//}
	char *CONTENT=malloc(100000);
	CONTENT=realloc(CONTENT,100000);
	sprintf(CONTENT, "");
	int get =getAllNotif(&notD,&count,body_no);
	if(get==SQLITE_OK && count>0){
		for(int i=0;i<count;i++)
		{
			NotifData x = notD[i];
			char *BODY_NO=malloc(100000);
			BODY_NO = realloc(BODY_NO,100000);
			char *PROBLEM=malloc(100000);
			PROBLEM = realloc(PROBLEM,100000);
			char *PROBLEM_DESCRIPTION=malloc(100000);
			PROBLEM_DESCRIPTION=realloc(PROBLEM_DESCRIPTION,100000);

			sprintf(BODY_NO, "%s",x.BODY_NO);
			sprintf(PROBLEM, "%s",x.PROBLEM);
			sprintf(PROBLEM_DESCRIPTION, "%s",x.PROBLEM_DESCRIPTION);
			sprintf(CONTENT, "%s<br><br>%s<br/>%s<br/>%s",CONTENT, BODY_NO, PROBLEM,PROBLEM_DESCRIPTION);
			//elm_object_text_set(NVvc->label1,CONTENT);
		}
		sprintf(CONTENT, "%s<br/><br/><br/>",CONTENT);
		deleteALL(body_no);
	}else{
		//elm_object_text_set(NVvc->label1,"<br><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align>");
		sprintf(CONTENT, "<br><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align>");
	}
	evas_object_show(NVvc->scroller1);
	elm_object_text_set(NVvc->label1,CONTENT);
	//"<br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align>"
	//evas_object_show(NVvc->scroller1);
	evas_object_hide(NVvc->bodyNoList);
}

void loadAllBodyDefect(){
	evas_object_hide(NVvc->scroller1);
	elm_genlist_clear(NVvc->bodyNoList);
	//elm_object_text_set(NVvc->notifList,"<br><br><align=0.5 0.5><color=#F27916><b>LOADING..</b></color><br><b>Retrieving Notifications</b></align>");

	BodyData *notD= (BodyData*) calloc (1, sizeof(BodyData));;
	int count=0;

	initdb();
	int get =getAllBodyDefect(&notD,&count);
	if(get==SQLITE_OK && count>0){
		/*
		char *dataSql=getReturnDatas();
		char *c=malloc(strlen(dataSql)+9);
		sprintf(c,"%s<br><br>",dataSql);
		*/
		create_genlist_item(NVvc->bodyNoList, "title" , "Defects", "", " "," ", " "," ", NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

		for(int i=0;i<count;i++)
		{
			BodyData x = notD[i];
			char *BODY_NO=malloc(100000);
			char *COUNT=malloc(100000);
			BODY_NO = realloc(BODY_NO,100000);
			COUNT = realloc(COUNT,100000);
			sprintf(COUNT, "<font color=#5494f9>Defects : %d</font>",x.count);
			sprintf(BODY_NO, "%s",x.BODY_NO);
			create_genlist_item(NVvc->bodyNoList, "2text" , BODY_NO, COUNT, "","", "","", NULL, ELM_GENLIST_ITEM_NONE, selectBodyNo, (void *)BODY_NO);
		}
		create_genlist_item(NVvc->bodyNoList, "2text" , " ", " ", "","", "","", NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);

		//elm_object_text_set(NVvc->notifList,c);
		//deleteALL();
		//ResetNotif();

	}else{
		create_genlist_item(NVvc->bodyNoList, "title" , "No Defects", "", " "," ", " "," ", NULL, ELM_GENLIST_ITEM_NONE, NULL, NULL);
		//elm_object_text_set(NVvc->notifList,"<br><br><br><align=0.5 0.5><color=#F27916><b>Empty Notifications</b></color></align>");
	}
	evas_object_show(NVvc->bodyNoList);
}

static void selectBodyNo(char *data, Evas_Object *obj, void *event_info)
{
    //printf("item(%d) is selected", (int)data);
	ondetail = 1;
    dlog_print(DLOG_ERROR,"GL SELECTED","ID GL : %s",data);
    selected_body_no = data;
    sprintf(selected_body_no, "%s",data);

    evas_object_hide(NVvc->bodyNoList);
    //evas_object_show(NVvc->scroller1);
    loadAllNotification(selected_body_no);
}


void loadServerNotification(){
	//loadAllNotification();
	loadAllBodyDefect();
	char *linecd = malloc(100000);
	char *url = malloc(100000);
	preference_get_string("LINE_CD", &linecd);
	sprintf(url, "http://arkamaya.net:1222/ProblemSheet/getNotif?linecd=%s",linecd);
	//sprintf(url, "http://192.168.4.100:1234/ProblemSheet/getNotif?linecd=%s",linecd);
	setURL(url);
	setURLResponseRead(url);
	ecore_thread_run(threadMain, endReq, cancelReq, NULL);
}


static void _on_pause_cb(void *user_data)
{
	/* Take necessary actions when application becomes invisible. */
}

static void _on_app_control_cb(app_control_h app_control, void *user_data)
{
	/* Handle the launch request. */
}

static void _on_low_memory_cb(app_event_info_h event_info, void *user_data)
{
	/* Take necessary actions when the system runs low on memory. */
}

static void _on_low_battery_cb(app_event_info_h event_info, void *user_data)
{
	/* Take necessary actions when the battery is low. */
}

static void _on_device_orientation_cb(app_event_info_h event_info, void *user_data)
{
	/* deprecated APIs */
}

static void _on_language_changed_cb(app_event_info_h event_info, void *user_data)
{
	/* Take necessary actions is called when language setting changes. */
	uib_views_get_instance()->uib_views_current_view_redraw();
}

static void _on_region_format_changed_cb(app_event_info_h event_info, void *user_data)
{
	/* Take necessary actions when region format setting changes. */
}

