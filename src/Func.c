/*
 * Func.cpp
 *
 *  Created on: Jan 22, 2018
 *      Author: Hartaji
 */

#include "Func.h"

notification_h notification = NULL;
haptic_device_h haptic_handle;
haptic_effect_h effect_handle;

void
get_system_info_int(char* key, int *value)
{
    int ret, val;
    ret = system_info_get_platform_int(key, &val);
    *value = val;
    if (ret != SYSTEM_INFO_ERROR_NONE)
	{
	    // Error handling
	}
	return;
}

haptic_device_h
device_vibrate(int duration, int feedback) {

	stopVibrate();

	int open=device_haptic_open(0, &haptic_handle);

	if( open == DEVICE_ERROR_NONE) {

		int vibrate =device_haptic_vibrate(haptic_handle, duration, feedback, &effect_handle);
		if( vibrate == DEVICE_ERROR_NONE) {
			//LOGI("Device vibrates!");
		}

//		When you decided not to use haptic anymore disconnect it
//		if(device_haptic_close(haptic_handle) == DEVICE_ERROR_NONE) {
//			LOGI("Vibrator disconnected");
//		}
		//device_haptic_close(haptic_handle);
	}
	return haptic_handle;
}

int stopVibrate(){
	int stop=0;
	if(haptic_handle!=NULL && effect_handle!=NULL){
		stop=device_haptic_stop(haptic_handle, effect_handle);
		if( stop == DEVICE_ERROR_NONE) {
				//LOGI("Device vibration stopped");
		}

		haptic_handle =NULL;
		effect_handle =NULL;
	}

	return stop;
}

int ShowNotif(char idVehicle[30],char description[255],bool silence){

	int vibrate=500;
	char title[30];
	bool isCriticalMessage=false;
	char *tag[255];
	const char *prevTag;


	int cnt=-1;
	if(silence){
		cnt=-2;
	}

	int NotifCount=setget_NotifCount(cnt);//count down notifCount

	//more longer vibrate defend on notifcount

	if(vibrate<7001){
		vibrate=vibrate*NotifCount;//increment vibrate
		//vibrate=7000;limit the vibrate no longer then 7 second
	}

	//if(NotifCount==1)
		sprintf(title,"%s",idVehicle);

	/*if(NotifCount>1){
		sprintf(title,"Total Defect %d",NotifCount);
	}*/

	//if(NotifCount>2)
		isCriticalMessage=true;

	char *image_path[255];
	char *sound_path[255];

	char *shared_path ;

	shared_path= app_get_shared_resource_path();

	snprintf(image_path, 255, "%sbg_red.png", shared_path);
	snprintf(sound_path, 255, "%sbruaam.wav", shared_path);

	if(notification!=NULL){
		char *split;
		notification_get_tag(notification,&prevTag);
		//split=splitChar(prevTag,"--",10);

		//int length=strlen(split);

		sprintf(tag,"%s%s\n-%s\n--\n",prevTag,idVehicle,description);

		deleteNotification();
		//free(prevTag);

	}else{
		sprintf(tag,"%s\n-%s\n--\n",idVehicle,description);
	}

	free(shared_path);
	//free(tag);

	int ret =0;
	if(silence){
		notification = notification_create(NOTIFICATION_TYPE_NONE);
	}else{
		notification = notification_create(NOTIFICATION_TYPE_NOTI);
	}

	if (notification != NULL){

		notification_set_time_to_text(notification, NOTIFICATION_TEXT_TYPE_TITLE, time(NULL));
		notification_set_display_applist(notification,NOTIFICATION_DISPLAY_APP_ALL);
		notification_set_layout(notification,NOTIFICATION_LY_NOTI_EVENT_SINGLE);
		//notification_set_property(notification,NOTIFICATION_PROP_DISABLE_AUTO_DELETE);


		//sound and image
		if(!silence){
			notification_set_tag(notification,tag);
			notification_set_sound(notification,NOTIFICATION_SOUND_TYPE_USER_DATA,sound_path);
		}

		if(isCriticalMessage==true)
			notification_set_image(notification, NOTIFICATION_IMAGE_TYPE_BACKGROUND , image_path);


		//title and description
		notification_set_text(notification, NOTIFICATION_TEXT_TYPE_TITLE, title,
									NULL, NOTIFICATION_VARIABLE_TYPE_NONE);
		if(notifCount==1){
			notification_set_text(notification, NOTIFICATION_TEXT_TYPE_CONTENT,
						  description, NULL, NOTIFICATION_VARIABLE_TYPE_NONE);
		}//else{
			//notification_set_text(notification, NOTIFICATION_TEXT_TYPE_CONTENT,
				//					  tag, NULL, NOTIFICATION_VARIABLE_TYPE_NONE);
		//}

		//notification_set_vibration(notification,NOTIFICATION_VIBRATION_TYPE_DEFAULT,)
		app_control_h launchapp = NULL;
		app_control_create(&launchapp);
		app_control_set_app_id(launchapp, "com.toyota.realtimefeedback");
		app_control_set_mime(launchapp,"test");



		//notification_set_event_handler(notification,NOTIFICATION_EVENT_TYPE_CLICK_ON_ICON,(void *)launchapp);
		notification_set_launch_option(notification,NOTIFICATION_LAUNCH_OPTION_APP_CONTROL,(void *)launchapp);

		app_control_destroy(launchapp);
		ret = notification_post(notification);

		if(silence)return ret;

		if(vibrate>0)
			device_vibrate(vibrate,100);
	}

	return ret;
}

int
UpdateNotif(notification_h notification){
	int ret = notification_update(notification);

	return ret;
}

int ResetNotif(){
	//deleteNotification();
	return setget_NotifCount(0);
}

char splitChar(char *splitChar,const char *delimeter,int limit){

	char *spl;
	char *splitted;
	spl = strtok (splitChar,delimeter);

	int i = 0;
	while (spl != NULL)
	{
		splitted[i] = malloc( (strlen(spl)+1) * sizeof(char));
		strcpy(splitted[i++], spl);
		//printf ("%s\n",spl);
		spl = strtok (NULL, delimeter);
		if(i==limit)
			break;
	}

	free(spl);

	return splitted;

}

void deleteNotification(){
	if(notification!=NULL){
		notification_delete(notification);
		notification_free(notification);
	}
}

char* get_write_filepath(char *filename){

    char write_filepath[1000] = {0,};
    char *resource_path = app_get_data_path(); // get the application data directory path

    if(resource_path){
        snprintf(write_filepath,1000,"%s%s",resource_path,filename);
        free(resource_path);
    }

    return write_filepath;
}

void read_file(
		char *filename,
		char *result
		){

	char* buf = NULL;

	const char* filepath=get_write_filepath(filename);

    FILE *fp;
    fp = fopen(filepath,"r");
    if(fp==NULL){
    	fclose(fp);
    	return;
    }

    fseek(fp, 0, SEEK_SET);
    int fsize = ftell(fp);
    fread(buf, fsize, 1, fp);
    fclose(fp);

    result=buf;

}

void write_file(
		char *filename,
		const char* buf
		){

	const char* filepath=get_write_filepath(filename);

    FILE *fp;
    fp = fopen(filepath,"w");
    fputs(buf,fp);
    fclose(fp);

}

void setNotifListItem(
		char jam[200],
		char idVehicle[200],
		char *jobCodes[200],
		char *shopCodes[200],
		char *listDefects[200],
		char *result[504],
		Ecore_Thread *thread
		){

	//eina_str_toupper(idVehicle);
	char *Content[504];
	char *headerIDVehicle[200];
	char *hour[255];
	char *jobCode[200];
	char *listDefect[200];

	sprintf(hour, "<color=#999999><font_size=27><b>%s</b></font_size></color><br>", jam);
	sprintf(headerIDVehicle, "<color=#F27916><b>%s</b></color>", idVehicle);

	int i=0;
	while(jobCodes[i]!=NULL){
		char *jc=jobCodes[i];
		char *sc=shopCodes[i];

		int u=0;
		while(listDefects[u]!=NULL){
			char *ld=listDefects[u];
			if(u==0){
				sprintf(listDefect, "<br><font_size=32>- %s</font_size>", ld);
			}else{
				sprintf(listDefect, "%s<br><font_size=32>- %s</font_size>", listDefect,ld);
			}
			u++;
		}

		if(i==0){
			sprintf(jobCode, "<br><font_size=28><color=#3AE6F2>%s | %s</color></font_size>%s<br>",jc ,sc,listDefect);
		}else{
			sprintf(jobCode, "%s<br><font_size=28><color=#3AE6F2>%s | %s</color></font_size>%s<br>",jobCode, jc ,sc,listDefect);
		}

		i++;
	}

	sprintf(Content, "<br><align=center>%s%s%s</align>",hour,headerIDVehicle,jobCode);

	*result=Content;
}

void Toast(char *stringToast){

	//Evas_Object *popup;
	//Evas_Object *win = data;

	//popup = elm_popup_add(win);
	//elm_object_style_set(popup, "toast");
	//evas_object_size_hint_weight_set(popup, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	//eext_object_event_callback_add(popup, EEXT_CALLBACK_BACK, eext_popup_back_cb, NULL);
	//elm_object_text_set(popup, "Toast popup text Toast popup text");
	//evas_object_smart_callback_add(popup, "block,clicked", popup_block_clicked_cb, win);
	//elm_popup_timeout_set(popup, 2.0);
	//evas_object_smart_callback_add(popup, stringToast, popup_timeout_cb, NULL);

	//evas_object_show(popup);
}

char mallocCharArray(char *string, int length){
	char *arr = malloc(length * sizeof(char));
	strcpy(arr, string);
	// ...
	free(arr);
	return arr;
}
